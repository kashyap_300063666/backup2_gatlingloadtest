package simulations.grpc.exmaple

import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation
import io.gatling.core.Predef.{DurationJLong, Simulation, atOnceUsers, constantUsersPerSec, rampUsers, rampUsersPerSec}
import scala.math._
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils}
import scala.util.Random
import com.myntra.commons.util.{GatlingPropertiesLoader, LoggerTrait}


class ScaleitTestSimulation extends BaseSimulation{
    
  val ramp_dur_per = System.getProperty("ramp_dur_per", "50").toInt
  val hold_for = 100 - ramp_dur_per
    
  val repeatCount = System.getProperty("repeatCount", "1").toInt
  
  var agentHeader = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36 Edg/81.0.416.77".toString()
  
  val agent = System.getProperty("agent", "firefox").toString

  val url = System.getProperty("baseUrl", "https://www.scaleit.myntra.com").toString;
  
  if (agent.equals("mobile")) {
   agentHeader = "Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36".toString()
  } 
  
  
  val httpProtocol = http
    .baseUrl(url)
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader(agentHeader).shareConnections.disableWarmUp
  
  
   private val home = scenario("home")
       .exec(http("home")
       .get("/"))
  
  
  
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
  
    
//    
//    setUp(scenarios map (e => e.inject(constantUsersPerSec(noOfUsers) during (duration))) toList).throttle(
//     reachRps(noOfUsers/2) in ((duration * 10)/100),
//     holdFor((duration * 10)/100),
//     reachRps(noOfUsers) in ((duration * 40)/100),
//     holdFor((duration * 40)/100)
//      ).protocols(httpProtocol)
    
//    setUp(scenarios map (e => e.inject(constantUsersPerSec(noOfUsers) during (duration))) toList).throttle(
//     reachRps(noOfUsers/2) in ((duration * 20)/100),
//     holdFor((duration * 40)/100),
//     jumpToRps(noOfUsers),
//     holdFor((duration * 40)/100)
//      ).protocols(segmentServiceBaseUrl)
//  
//    setUp(scenarios.inject(constantUsersPerSec(500) during (10 minutes)))

    
}
