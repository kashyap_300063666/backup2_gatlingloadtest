package simulations.grpc.exmaple

import com.github.phisgr.gatling.grpc.Predef.grpc
import io.gatling.core.Predef._
import simulations.BaseSimulation
import io.grpc.ManagedChannelBuilder
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import com.myntra.charge.dto.charge._
import com.myntra.charge.enums.charge_enum.ChargeType
import io.grpc.Metadata


class GrpcDynamicPayloadCreation extends BaseSimulation{


  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "systems"
  /* Name of File to be downloaded */
  val fileName = "charges_giftwrap.csv"
  val grpcConf = grpc(ManagedChannelBuilder.forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.CHARGES.toString, ""), grpcPortNum).usePlaintext())
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
  //val styleIdFeeder = csv("test-data/charges_giftwrap.csv").circular
  val styleIdFeeder = csv(filePath).circular
  
  
  /*
   * runtime parameter to specify the number of object that need to be formed in the grpc request.
   * 
   * In below example it will form 2 ItemInputContext object will be formed in the request by default , but we can override this value in runtime
   * using -DnoOfInputContext=5  
   * 
   * */
  protected val noOfInputContext = System.getProperty("noOfInputContext", "2").toInt 
  
  
  /*
   * Below is the method that form sequences of object in the request.
   * 
   * In below example it takes styleId,sellerPartnerId,sellerId and skuId from CSV file and forms the next object in the grpc
   * request based the runtime parameter noOfInputContext 
   *     
   */
  
  def seqOfItemInputContext(session:Session) : Seq[ItemInputContext]= {
    var seq:Seq[ItemInputContext] = Seq[ItemInputContext]()
     
    for(i <- 1 to noOfInputContext){
    val eachitem=ItemInputContext( 
       Option(ItemContext.of(
             quantity=Some(1.toInt),
             styleId=Option(session("styleId"+i).as[Long]),
             mrp=Option(2500),
             sellerPartnerId=Option(session("sellerPartnerId"+i).as[Long]),
             discountedPrice=Option(400),
             sellerId=Option(session("sellerId"+i).as[Long]),
             skuId=Option(session("skuId"+i).as[Long]),
             articleTypeId=Option(90),
             tax=Option(90),
             tryNBuyOpted=Option(false) 
         )))
         
         seq = seq :+ eachitem 
      
    }
    seq
 }
  
  val giftwrapcharge = scenario("GiftWrapCharge - Apply charge")
    .feed(styleIdFeeder,noOfInputContext)   // Feeder we need to pass the no of rows it should take from csv i.e no of object that need to based in grpc request
    .exec(grpc("GiftWrapCharge - applyCharge")
    .rpc(ChargeServiceGrpc.METHOD_APPLY_CHARGE)
      .payload(session => 
      ChargeInputContext(
            giftWrapOpted =Option(true),
            coverFeeOpted=Option(false),
            cartLevelCharges=Seq(ChargeType.giftwrap),
            totalValueWithBagDiscount=Option(500),
            totalValueWithoutBagDiscount=Option(700),  
            itemDetails=seqOfItemInputContext(session)))
                
    .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))("storeid=2297")
 )
     
      
  setUp(giftwrapcharge.inject(
    nothingFor(5),
    constantUsersPerSec(noOfUsers) during (DurationJLong(getLongProperty("duration.seconds")) seconds)
  ).protocols(grpcConf)
  ).maxDuration(maxDurationInSec)
}
  
