package simulations.comm.pushampl

import com.myntra.commons.util.BaseUrlConstructor
import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class pushAmplProducer extends BaseSimulation {

  val httpConf = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.COMMPUSHAMPLPRODUCER.toString)).disableWarmUp


  val producerapi = scenario("producer api")
    .exec(http("producer api")
      .get("/comm/v1/send")
      .headers(headers)
      .check(status.is(200))
    )


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpConf)
}
