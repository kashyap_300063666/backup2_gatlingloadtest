package simulations.comm.pushampl

import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class pushAmplConsumer extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name  */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "CommPullNotifConsumer.csv"

  private val httpConf = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.COMMPUSHAMPLCONSUMER.toString)).disableWarmUp

  val csvFile: String = System.getProperty("deviceId", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)
  private val deviceIdList = csv(filePath).circular

  //private val lastSeenEpochTimeList = csv(System.getProperty("lastSeenEpochTime","test-data/CommPullNotifConsumer.csv")).circular



  val Consumerapi = scenario("Consumer api").feed(deviceIdList)
    .exec(http("Consumer api")
      .get("/v1/notifications/{deviceId}/{lastSeenEpochTime}")
      .queryParam("deviceId","${deviceId}")
      .queryParam("lastSeenEpochTime","1612176765")
      .headers(headers)
      .check(status.is(200))
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpConf)
}
