package simulations.ims
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

class ImsSimulation2 extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "wms"
  /* Name of File to be downloaded */
  val fileName = "imsBulkUpdateData.csv"

  //private val imsBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.IMSSYNC.toString)).disableWarmUp
  private val imsBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.IMSASYNC.toString)).disableWarmUp
  //private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.ADDRESS_SERVICE.toString)




  //val csvFile = System.getProperty("imsdataprovider", "test-data/imsBulkUpdateData.csv")
  val csvFile = System.getProperty("imsdataprovider", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  //val csvfeeder = csv(csvFile).circular
  val csvfeeder = csv(filePath).circular
  val imspageCalls = new ImsUtil().ImsApis


  private val preRequisiteScenario =
    scenario("Pre Requisite Script")
      .feed(csvfeeder)
      .exec(
        imspageCalls.bulkSellerInventoryUpload
      )


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(imsBaseUrl)




}