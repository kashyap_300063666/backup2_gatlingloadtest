package simulations.rewarddistribution


import simulations.BaseSimulation
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}

class UserSimulation extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "rewarddistribution_uidx.csv"

  private val url = System.getProperty("url", BaseUrlConstructor.getBaseUrl(ServiceType.REWARD_DISTRIBUTION.toString))
  private val distributionBaseUrl = http.baseUrl(url).disableWarmUp
  //val csvFile = System.getProperty("addressUidxFile", "test-data/rewarddistribution_uidx.csv")
  val csvFile: String = System.getProperty("addressUidxFile", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)
  val csvfeeder = csv(filePath).circular

  println("Url is " + url)

  private val userStatus = scenario("User Status")
    .feed(csvfeeder)
    .exec(http("User Status")
      .post("/userStatus")
      .body(StringBody(
        """{"campaignId":"e5b9abc7.ecca.4b77.9bea.45aeeef24402","uidx":"${uidx}"}""".stripMargin))
      .asJson
      .check(status.is(200)))


  private val rewardUser = scenario("Reward User")
    .feed(csvfeeder)
    .exec(http("Reward User")
      .post("/rewardUser")
      .body(StringBody(
        """{"campaignId":"e5b9abc7.ecca.4b77.9bea.45aeeef24402","uidx":"${uidx}"}""".stripMargin))
      .asJson
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(distributionBaseUrl)
}
