package simulations.rewarddistribution

import simulations.BaseSimulation
import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor

import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}

class CampaignQuerySimulation extends BaseSimulation{
  private val url = System.getProperty("url", BaseUrlConstructor.getBaseUrl(ServiceType.REWARD_DISTRIBUTION.toString))
  private val distributionBaseUrl = http.baseUrl(url).disableWarmUp
  println("Url is " + url)
  private val searchCampaign = scenario("Search Campaign")
    .exec(http("Search Campaign")
      .get("/campaigns?target=Test&includeFuture=false")
      .check(status.is(200)))

  private val campaignDetails = scenario("Campaign Details")
    .exec(http("Campaign Details")
      .get("/campaign?campaignId=d0a37741.e486.4fe9.948d.a42affef6d12")
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(distributionBaseUrl)
}
