package simulations.biro

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class FetchLayout extends BaseSimulation{

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "android"
  /* Name of File to be downloaded */
  val fileName = "biroLayoutName.csv"

  private val biroBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.BIRO.toString)).disableWarmUp
  //val layoutNameFeeder = csv("test-data/biroLayoutName.csv").circular
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
  val layoutNameFeeder = csv(filePath).circular

  private val fetchLayout =
    scenario("fetch-biro-layout")
      .feed(layoutNameFeeder)
      .exec(http("fetchLayout")
        .get("/api/android/${layoutName}")
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(biroBaseUrl)

}
