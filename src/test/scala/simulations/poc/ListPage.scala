package simulations.poc

import com.myntra.commons.util.{HLTUtility, RedisUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class ListPage {
  /* Utility class for HLT */
  val util = new HLTUtility()
  val lpHeaders = Map(
    "content-type" -> "application/json",
    "at" -> "${at}"
  )


  object Search {
    val pagination = repeat(session => session("pageNos").validate[Int], "scrollToPage") {
      doIf(session => util.redisUtil.hget("ApiEnableDisable", "pagination").toBoolean) {
        exec(http("Search Scrolling to Page")
          .get("/v2/search/${queryParam}?rows=48&o=0&p=${scrollToPage}")
          .headers(lpHeaders)
          .check(status.is(200))
          .check(bodyString.saveAs("productsListResponse")))

      }
    }

    val sort =
      doIf(session => util.redisUtil.hget("ApiEnableDisable", "sort").toBoolean) {
        exec(http("Search With Sort")
          .get("/v2/search/${queryParam}?userQuery=true&rows=48&request_id=null&${filterquery}")
          .headers(lpHeaders)
          .check(status.is(200)))
      }
  }

}
