package simulations.poc

import com.myntra.commons.util.{HLTUtility, RedisUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class HomePage {
  /* Utility class for HLT */
  val util = new HLTUtility()

  val csheaders = Map(
    "cache-control" -> "no-cache",
    "accept" -> "application/json",
    "Content-Type" -> "application/json",
    "x-myntra-app" -> "deviceID=devID;appFamily=MyntraRetailAndroid;deviceIP=locationIP;appVersion=4.2001.3;",
    "x-myntra-abtest" -> "lgp.smart.store=inactiveVarB;",
    "at" -> "${at}",
    "xid" -> "${xid}",
    "sxid" -> "${sxid}"
  )


  object Login {

    val home = doIf(session => util.redisUtil.hget("ApiEnableDisable", "home").toBoolean) {
      exec(http("get home page")
        .get("/lgp/v2.9/stream")
        .headers(csheaders)
        .check(status.is(200)))
    }

    val topnav = doIf(session => util.redisUtil.hget("ApiEnableDisable", "topnav").toBoolean) {
      exec(http("get topnav")
        .get("/lgp/v2.9/stream/topnav")
        .headers(csheaders)
        .check(status.is(200)))
    }

    val recommended = doIf(session => util.redisUtil.hget("ApiEnableDisable", "recommended").toBoolean) {
      exec(http("get recommended")
        .get("/lgp/v2.9/stream/recommended")
        .headers(csheaders)
        .check(status.is(200)))
    }
  }

}



