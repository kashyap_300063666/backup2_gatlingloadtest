package simulations.cartservice

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, HLTUtility}
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation
import scala.util.Random

class  CartSimulation extends BaseSimulation {

  val baseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.CARTSERVICEV2.toString)).disableWarmUp
  val cartHeaders = Map("accept" -> "application/json", "Content-Type" -> "application/json")

  //to fetch active styles from solr and create custom feeder
  val util = new HLTUtility()
  util.downloadFile("styles.csv")
  private val csvData: List[String] = CSVUtils.readCSV2("styles.csv", isIncludeHeader = false)
    .map(e => e.apply(0)).toList
  private val numberOfStyles: Int = csvData.length
  val styleDetailsFeeder: Iterator[Map[String, String]] =
    Iterator.continually(
      Map(
        "styleId" -> csvData.apply(Random.nextInt(numberOfStyles))
      )
    )
  val possiblePincodes = Seq("560068","560070","560103","560051")
  val availableCoupons = Seq("MYNFAB19","MVISA500","MYNTRA10","SELFCARE10","ICONICEXTRA10","LT1","LT2","LT3","LT4","LT5")
  val commonApiFeeder = Iterator.continually(Map(
    "lpApplied" -> Random.nextBoolean(),
    "mcApplied" -> Random.nextBoolean(),
    "pincode" -> possiblePincodes(Random.nextInt(possiblePincodes.length)),
    "couponCode" -> availableCoupons(Random.nextInt(availableCoupons.length))
  ))

  //Redis implementation
  private val counterKeyName = "cartData_"
  private val counterStart = System.getProperty("startingRange", "1").toInt
  private val counterEnd = System.getProperty("endingRange", util.redisUtil.get(counterKeyName)).toInt

  /* Initialize Page Objects */
  val pdpPageCalls =  new StylePdpPage().StyleDetails

  def addToCart(): ChainBuilder = {
    exec(http("addToCart")
      .put("/myntra-absolut-service/cart/v2/default/add")
      .headers(cartHeaders)
      .header("x-mynt-ctx", "${xMyntCtx}")
      .body(StringBody("""{"styleId":${styleId},"skuId": ${skuIdsList.random()},"quantity":1}""")).asJson
      .check(status.is(200)))
  }

  def cartSummary(): ChainBuilder = {
    exec(http("cartSummary")
      .get("/myntra-absolut-service/cart/v2/default/summary")
      .headers(cartHeaders)
      .header("x-mynt-ctx", "${xMyntCtx}")
      .check(status.is(200)))
  }

  def getCart(): ChainBuilder = {
    exec(http("getCart")
      .get("/myntra-absolut-service/cart/v2/default")
      .headers(cartHeaders)
      .header("x-mynt-ctx", "${xMyntCtx}")
      .check(status.is(200)))
  }

  def getCartById(): ChainBuilder = {
    exec(http("getCartById")
      .get("/myntra-absolut-service/securedcart/v2/id/${cartId}?rt=true&forAddress=false&inflateVirtual=false&paymentMethod=")
      .headers(cartHeaders)
      .header("x-mynt-ctx", "${xMyntCtx}")
      .check(status.is(200)))
  }

  def commonApi(): ChainBuilder = {
    exec(http("commonApi")
      .put("/myntra-absolut-service/cart/v2/default/apply")
      .headers(cartHeaders)
      .header("x-mynt-ctx", "${xMyntCtx}")
      .header("pagesource","cart")
      .body(StringBody("""{"myntraCredit":{"apply":${mcApplied}},"loyaltyPoint": {"apply": ${lpApplied}},"serviceability":{"apply":true,"pincode":${pincode}}}""")).asJson
      .check(status.is(200)))
  }

  def applyCoupon(): ChainBuilder = {
    exec(http("applyCoupon")
      .post("myntra-absolut-service/cart/v2/default/coupon/apply?sendApplicableCoupons=true")
      .headers(cartHeaders)
      .header("x-mynt-ctx", "${xMyntCtx}")
      .body(StringBody("""[{"type":"coupon", "code":"${couponCode}"}]""")).asJson
      .check(status.is(200)))
  }

  private val cartScenario =
    scenario(" Cart Api")
      .feed(styleDetailsFeeder)
      .feed(commonApiFeeder)
      .exec(
        Session => {
          val redisData = util.redisUtil.get(counterKeyName + (counterStart + Random.nextInt((counterEnd - counterStart) + 1)))
          val cartData = redisData.split(",")
          Session.set("xMyntCtx", cartData(0)).set("cartId", cartData(1))
        })
      .exec(
        pdpPageCalls.styleData
      )
      .exec(addToCart)
      .exec(cartSummary)
      .exec(commonApi)
      .exec(cartSummary)
      .exec(getCart)
      .exec(cartSummary)
      .exec(getCartById)

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(baseUrl)
}
