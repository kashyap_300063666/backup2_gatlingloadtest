package simulations.gauntlet

import com.github.phisgr.gatling.grpc.Predef.grpc
import io.gatling.core.Predef._
import simulations.BaseSimulation
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.gauntlet.domain.maximus._
import io.gatling.core.session.Expression
import io.grpc.{ManagedChannelBuilder, Metadata}


class GauntletSPEGrpcSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "revenue"
  /* Name of File to be downloaded */
  val fileName = "gauntletStyles1.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val baseUrl = System.getProperty("singleServerURL") //Pass the complete url of single server if req
  private val headerValue = System.getProperty("headerGauntlet","") //
  private val storeId = System.getProperty("storeId","storeid=2297")

  var grpcConf = grpc(ManagedChannelBuilder.forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.GAUNTLET.toString, ""), grpcPortNum).usePlaintext())

  if (baseUrl != null) {
    grpcConf = grpc(ManagedChannelBuilder.forAddress(baseUrl, grpcPortNum).usePlaintext())
    println("Updated BaseUrl:" + baseUrl)
  }
  val styleIdFeeder = csv(filePath).random.convert {
    case ("styleId", string) => string.toLong
  }

  def seqOfStyleRequest(session: Session): StyleRequest = {
    val sr: StyleRequest = StyleRequest(styleId = Option(session("styleId").as[Long]))
    sr
  }

  val dynamicPayload: Expression[StyleRequest] = session => seqOfStyleRequest(session)

  val pdp = scenario("gauntletSPEGrpc")
    .feed(styleIdFeeder)
    .exec(grpc("gauntletSPEGrpc - discountDetails")
      .rpc(GauntletInfoGrpc.METHOD_DISCOUNT_DETAILS_MINI)
      .payload(dynamicPayload)
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))(storeId)
      .header(Metadata.Key.of("x-mynt-client-id", Metadata.ASCII_STRING_MARSHALLER))(headerValue)
    )

  setUp(scenarios map (e => e.inject(rampUsersPerSec(getLongProperty("ramp_up_rate.start"))
    to getLongProperty("ramp_up_rate.end")
    during getIntProperty("ramp_duration"),
    constantUsersPerSec(getIntProperty("hold_rate"))
      during getIntProperty("hold_duration"))))
    .maxDuration(maxDurationInSec)
    .protocols(grpcConf)

}
