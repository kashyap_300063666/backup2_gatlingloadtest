package simulations.gauntlet

import com.github.phisgr.gatling.grpc.Predef.grpc
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.gauntlet.domain.maximus.{GauntletInfoGrpc, StyleRequest, StyleRequestBulk}
import io.gatling.core.Predef.{csv, scenario}
import io.gatling.core.session.Expression
import io.gatling.core.Predef._
import io.grpc.{ManagedChannelBuilder, Metadata}
import simulations.BaseSimulation

import scala.util.Random


class GauntletSearchIndexingGRPCSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "revenue"
  /* Name of File to be downloaded */
  val fileName = "gauntletStyles1.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val baseUrl = System.getProperty("singleServerURL") //Pass the compl
  // ete url of single server if req
  private val headerValue = System.getProperty("headerGauntlet", "")
  private val storeId = System.getProperty("storeId","storeid=2297")

  var grpcConf = grpc(ManagedChannelBuilder.forAddress(BaseUrlConstructor
    .getBaseUrl(ServiceType.GAUNTLET.toString, ""), grpcPortNum).usePlaintext())

  if (baseUrl != null) {
    grpcConf = grpc(ManagedChannelBuilder.forAddress(baseUrl, grpcPortNum).usePlaintext())
    println("Updated BaseUrl:" + baseUrl)
  }

  //val styleIdFeeder = csv("test-data/gauntletStyles1.csv").random
  val styleIdFeeder = csv(filePath).random

  val dist = Map("1" -> 0.3418, "2" -> 0.1549, "3" -> 0.0741, "4" -> 0.0675, "5" -> 0.0227, "6" -> 0.0345
    , "8" -> 0.0353, "10" -> 0.1336, "20" -> 0.1044)

  final def sample[A](dist: Map[A, Double]): String = {
    val p = scala.util.Random.nextDouble
    val it = dist.iterator
    var accum = 0.0
    while (it.hasNext) {
      val (item, itemProb) = it.next
      accum += itemProb
      if (accum >= p) {
        return item.toString
      }
    }
    val list: List[Int] = List(5, 10, 15, 20, 25, 30, 35, 40, 45, 49)
    val random = new Random()
    list(random.nextInt(list.length)).toString
  }

  def seqOfStyleRequest(session: Session): Seq[StyleRequest] = {
    var seq: Seq[StyleRequest] = Seq[StyleRequest]()
    val noOfStyleRequest: Int = sample(dist).toInt
    for (i <- 1 to noOfStyleRequest) {
      val sr: StyleRequest = StyleRequest(styleId = Option(session("styleId" + i).as[Long]))
      seq = seq :+ sr
    }
    seq
  }

  val dynamicPayload: Expression[StyleRequestBulk] = session => StyleRequestBulk(styleRequestList = seqOfStyleRequest(session))
  val pdp = scenario("gauntletSearchIndexingGrpc")
    .feed(styleIdFeeder, 50)
    .exec(grpc("gauntletSearchIndexingGrpc - discountDetailsBulk ")
      .rpc(GauntletInfoGrpc.METHOD_DISCOUNT_DETAILS_BULK)
      .payload(dynamicPayload)
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))(storeId)
      .header(Metadata.Key.of("x-mynt-client-id", Metadata.ASCII_STRING_MARSHALLER))(headerValue)
    )

  setUp(scenarios map (e => e.inject(rampUsersPerSec(getLongProperty("ramp_up_rate.start"))
    to getLongProperty("ramp_up_rate.end")
    during getIntProperty("ramp_duration"),
    constantUsersPerSec(getIntProperty("hold_rate"))
      during getIntProperty("hold_duration"))))
    .maxDuration(maxDurationInSec)
    .protocols(grpcConf)

}
