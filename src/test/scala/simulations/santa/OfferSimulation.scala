package simulations.santa

import com.github.phisgr.gatling.grpc.Predef.grpc
import io.gatling.core.Predef._
import simulations.BaseSimulation
import com.github.phisgr.gatling.grpc.Predef._
import io.grpc.ManagedChannelBuilder
import com.github.phisgr.gatling.pb._
import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import com.myntra.santa.gateway.grpc.santa.{Client, OfferByUserRequest, OfferServiceGrpc, Pagination, RedeemLogRequest}
import com.myntra.santa.gateway.grpc._
import io.gatling.core.session.Expression

class OfferSimulation extends BaseSimulation {
  private val baseUrl = System.getProperty("singleServerURL")
  var grpcConf = grpc(ManagedChannelBuilder.forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.SANTA.toString,""), grpcPortNum).usePlaintext())

  if(baseUrl != null) {
    grpcConf = grpc(ManagedChannelBuilder.forAddress(baseUrl, grpcPortNum).usePlaintext())
  }

  val offerByUserRequest: Expression[OfferByUserRequest] = OfferByUserRequest(userId =
    "4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0", tenantId = 1, page = Some(Pagination(pageNumber = 10)))
  val offerByIdRequest: Expression[OfferByUserRequest] = OfferByUserRequest(userId = "4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0",
    tenantId = 1, offerId = 2116)
  val redeemLogRequest: Expression[RedeemLogRequest] = RedeemLogRequest(uidx = "4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0",
    page = Some(Pagination(pageNumber = 10)), client = Some(Client(tenantId = 1)))

  // FetchAvailableOffersByUser
  val fetchAvailableOffersByUser = scenario("fetch_available_offers_by_user")
    .exec(grpc("FetchAvailableOffersByUser")
    .rpc(OfferServiceGrpc.METHOD_FETCH_AVAILABLE_OFFERS_BY_USER).payload(offerByUserRequest))

  // FetchAllOffersByUser
  val fetchAllOffersByUser = scenario("fetch_all_offers_by_user")
    .exec(grpc("FetchAllOffersByUser").rpc(OfferServiceGrpc.METHOD_FETCH_ALL_OFFERS_BY_USER).payload(offerByUserRequest))

  // FetchClaimedOffersByUser
  val fetchClaimedOffersByUser = scenario("fetch_claimed_offers_by_user")
    .exec(grpc("FetchClaimedOffersByUser").rpc(OfferServiceGrpc.METHOD_FETCH_CLAIMED_OFFERS_BY_USER).payload(offerByUserRequest))

  // GetOneOfferById
  val getOneOfferById = scenario("get_offer_by_id")
    .exec(grpc("GetOneOfferById").rpc(OfferServiceGrpc.METHOD_GET_ONE_OFFER_BY_ID).payload(offerByIdRequest))

  // FetchAllOffersByIds
  val fetAllOffersByIds = scenario("get_all_offers_by_id")
    .exec(grpc("FetchAllOffersByIds").rpc(OfferServiceGrpc.METHOD_FETCH_ALL_OFFERS_BY_IDS).payload(offerByIdRequest))

  // FetchRedeemedOffers
  val fetchRedeemedOffers = scenario("fetch_redeemed_offers")
    .exec(grpc("FetchRedeemedOffers").rpc(OfferServiceGrpc.METHOD_FETCH_REDEEMED_OFFERS).payload(redeemLogRequest))

  // FetchActiveOffers
  val fetchActiveOffers = scenario("fetch_active_offers")
    .exec(grpc("FetchActiveOffers").rpc(OfferServiceGrpc.METHOD_FETCH_ACTIVE_OFFERS).payload(offerByUserRequest))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(grpcConf)
}