package simulations.mynsta

import com.github.phisgr.gatling.grpc.Predef.grpc
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.mynsta.mynsta.{AuthorServiceGrpc, GenericStringUidxRequest}
import io.gatling.core.Predef._
import io.gatling.core.session.Expression
import io.grpc.{ManagedChannelBuilder, Metadata}
import simulations.BaseSimulation

//018473c0.751a.48cf.a088.be21742efa2fAqjnutxgfU
class MynstaGetAuthorFollowStatusGrpcSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "mynstaUsers.csv"


  private val url = System.getProperty("url", BaseUrlConstructor.getBaseUrl(ServiceType.MYNSTA.toString, ""))
  val grpcConf = grpc(ManagedChannelBuilder.forAddress(url, grpcPortNum).usePlaintext())

  //val uidxFile = System.getProperty("mynstaUidxFile", "test-data/mynstaUsers.csv")
  val uidxFile = System.getProperty("mynstaUidxFile", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, uidxFile)
  //val uidxFeeder = csv(uidxFile).circular
  val uidxFeeder = csv(filePath).circular

  val authorUidx = System.getProperty("author.uidx", "c3a05e83.d8be.4d3d.bf7c.36a690a33baa6XzpXliK6U")

  println("Url is " + url)
  println("uidxFile is " + uidxFile)

  val authorUidxPayload: Expression[GenericStringUidxRequest] = GenericStringUidxRequest(uidx = authorUidx)

  val s1 = scenario("Get author follow status")
    .feed(uidxFeeder)
    .exec(grpc("AuthorService_GetAuthorFollowStatus")
      .rpc(AuthorServiceGrpc.METHOD_GET_AUTHOR_FOLLOW_STATUS)
      .payload(authorUidxPayload)
      .header(Metadata.Key.of("m-uidx", Metadata.ASCII_STRING_MARSHALLER))("${m-uidx}")
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))("storeid=2297")
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(grpcConf)

}
