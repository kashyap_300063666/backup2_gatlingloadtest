package simulations.buying.purchaseorder

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

/**
  * Created by 300067190 on 30/11/19.
  */
class FbmPoCreation extends BaseSimulation {

  private val purchaseOrderBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PURCHASE_ORDER.toString)).disableWarmUp

  private val poheaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "application/json")

  private val createFBMPO = scenario("createFbmPo")
    .exec(http("create fbm po")
      .post("/purchase-order-service/po/fbm")
      .headers(poheaders)
      .body(StringBody("""{"enabled":"true","vendorId": "6320", "warehouseId":"363",  "buyerId":"6320","buyerWarehouseId":"363","commercialType":"OUTRIGHT","description":"Load Testing Dummy POs","sourceReferenceId":121, "totalQuantity":1,"purchaseOrderSKUEntries":[{"skuId":"32445641","quantity": 1, "listPrice":101.0,"mrp":100,"landedPrice":100,"createdBy":"WOMRS","createdOn":1575463796000,"lastModifiedOn":1575463796000}]}""")).asJson
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(purchaseOrderBaseURL)

}
