package simulations.buying.purchaseorder

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.http.Predef._
import simulations.BaseSimulation
import io.gatling.core.Predef._

import scala.util.Random

/**
  * Created by 300067932 on 09/12/2020.
  */
class RateLimitingByClientConfigInSwitchTest extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileName = "buying_pos.csv"

  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val purchaseOrderBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PURCHASE_ORDER.toString)).disableWarmUp

  private val poheaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "application/json", "x-myntra-client-id" -> "testClient")

  var myntraPoCodesFeeder = Iterator.continually(
    Map("poIds" ->  Random.shuffle(CSVUtils.readCSV2(filePath, false)
      .map(e => e.apply(0)).toList).take(2).mkString(",")))

  private val bulkFetchByIds = scenario("bulkFetchByIds")
    .feed(myntraPoCodesFeeder)
    .exec(http("BulkFetchByIds")
      .post("/purchase-order-service/po/bulk-fetch-by-ids/")
      .headers(poheaders)
      .body(StringBody("""{"ids":[${poIds}]}""")).asJson
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(purchaseOrderBaseURL)
}
