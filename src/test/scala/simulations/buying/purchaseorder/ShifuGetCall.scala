package simulations.buying.purchaseorder

import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import com.myntra.commons.util.BaseUrlConstructor

/**
 * Created by 300035300 on 28/11/18.
 */
class ShifuGetCall extends BaseSimulation {

  private val shifuBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.SHIFU.toString)).disableWarmUp

  private val shifuheaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "application/json")
  
  private val brandFeeder = Array(499,513,499,499,499).map(e => Map("brand" -> e)).array.circular
  
  private val articleTypeFeeder = Array("Waistcoat","Jeans","Kurtis","Shrug","Tops").map(e => Map("articleType" -> e)).array.circular
  
  private val genderFeeder = Array("Women","Women","Women","Women","Women").map(e => Map("gender" -> e)).array.circular

  private val shifuGet = scenario("shifu search")
      .feed(brandFeeder)
      .feed(articleTypeFeeder)
      .feed(genderFeeder)
      .exec(http("shifu search")
        .get("/myntra-shifu-service/shifu/planningtaxonomy/v2/search?q=brand.eq:${brand}___articleType.eq:${articleType}___gender.eq:${gender}")
        .headers(headers)
        .check(status.is(200)))


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(shifuBaseURL)
}
