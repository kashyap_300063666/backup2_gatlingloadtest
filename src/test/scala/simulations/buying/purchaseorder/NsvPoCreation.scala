package simulations.buying.purchaseorder

import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import com.myntra.commons.util.BaseUrlConstructor

/**
 * Created by 300035300 on 12/09/18.
 */
class NsvPoCreation extends BaseSimulation {

  private val purchaseOrderBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PURCHASE_ORDER.toString)).disableWarmUp

  private val poheaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "application/json")

  private val createNSVPO = scenario("Create NSV PO")
    .exec(http("create nsv po")
      .post("/purchase-order-service/po/nsv/")
      .headers(poheaders)
      .body(StringBody("""{"vendorId": "1010", "vendorAddressId":3781,  "buyerId":"3974","buyerWarehouseId":"36","commercialType":"JIT_MARKETPLACE","description":"Load Testing Dummy POs","sourceReferenceId":"Dummy_Test","marketPlaceOrderSKUEntries":[{"skuId":"12070040","quantity": 10, "mrp":101.0}]}""")).asJson
      .check(status.is(200)))


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(purchaseOrderBaseURL)
}
