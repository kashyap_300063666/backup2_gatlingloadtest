package simulations.platform

import java.time.{Instant, LocalDateTime, ZoneId}
import java.util.Date

import io.gatling.core.Predef._
import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.http.Predef._
import simulations.BaseSimulation
/**
  * Created by 14977 on 02/01/19.
  */
class MWSSimulation extends BaseSimulation {
  private val mwsBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.MWS.toString)).disableWarmUp
  private val poheaders = Map("Authorization" -> "Basic YTpi", "Accept" -> "application/json", "Content-Type" -> "application/json")
  val date1 = new Date()

  // convert to LocalDateTime
  val ldt = LocalDateTime.ofInstant(Instant.ofEpochMilli(date1.getTime), ZoneId.systemDefault)

  // add five minutes
  val incrementedBy2 = ldt.plusMinutes(5)
  val sTime = java.lang.Long.getLong("sTime", 0L)

  // convert to Long
  val scheduleTime: Long = incrementedBy2.atZone(ZoneId.systemDefault()).toEpochSecond
  println(scheduleTime)
  println(sTime)

  object randomStringGenerator {
    def randomString(length: Int) = scala.util.Random.alphanumeric.filter(_.isLetter).take(length).mkString
  }

  val randomString1 = randomStringGenerator.randomString(12)
  val randomString2 = randomStringGenerator.randomString(12)

  val workflowFeeder = Array(randomString1).map(e => Map("workflowFeedData" -> e)).array.circular
  val appFeeder = Array(randomString2).map(e => Map("appFeedData" -> e)).array.circular

  private val createMwsWorkFlow1= scenario("Workflow for STN approval process")
    .feed(workflowFeeder)
    .feed(appFeeder)

    .exec(http("Workflow for STN approval process")
      .post("/workflow-service/workflow")
      .headers(poheaders)
      .body(StringBody(
        """
          {
            "id": "${workflowFeedData}",
            "name": "${workflowFeedData}",
            "description": "${workflowFeedData}",
            "workflow": {
              "nodes": [
                {
                  "id": "STN_APPROVED",
                  "name": "Approve STN",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "augustus-stn-mark-approved___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "pathParams": {
                      "fields": {
                        "stnId": "START.stnId"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "SKU_AGGREGATOR",
                  "name": "SKU Quantity Aggregator",
                  "job": {
                    "maxRetry": 0,
                    "config": {
                      "aggregateFields": {
                        "skuBlockFailures[0].skuId": "BLOCK_INVENTORY_IMS[0].skuId",
                        "skuBlockFailures[0].ownerId": "BLOCK_INVENTORY_IMS[0].ownerInventoryRequestEntries[0].ownerId",
                        "skuBlockFailures[0].requestedCount": "BLOCK_INVENTORY_IMS[0].ownerInventoryRequestEntries[0].requestCount",
                        "skuBlockFailures[0].failedCount": "BLOCK_INVENTORY_IMS[0].ownerInventoryRequestEntries[0].failedCount"
                      }
                    }
                  },
                  "type": "AGGREGATOR"
                },
                {
                  "id": "ITEM_SPLITTER",
                  "name": "Item Splitter",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "stnId": "START.stnId",
                        "stnBarcode": "START.stnBarcode",
                        "warehouseId": "START.warehouseId",
                        "stnType": "START.stnType"
                      }
                    },
                    "config": {
                      "batchSize": 20,
                      "dataField": "FETCH_ITEMS"
                    }
                  },
                  "type": "SPLITTER"
                },
                {
                  "id": "BLOCK_INVENTORY_IMS",
                  "name": "Block Inventory in IMS",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "warehouseId": "SKU_SPLITTER.data[0].warehouseId",
                        "skuId": "SKU_SPLITTER.data[0].skuId",
                        "ownerInventoryEntries[0].ownerId": "SKU_SPLITTER.data[0].invGroupId",
                        "ownerInventoryEntries[0].quality": "SKU_SPLITTER.data[0].quality",
                        "ownerInventoryEntries[0].requestCount": "SKU_SPLITTER.data[0].quantity",
                        "ignoreBlockedManual": "SKU_SPLITTER.ignoreBlockedManual"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId",
                        "taskInstanceId": "taskInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "retryInterval": 10,
                      "id": "ims-block-inventory-for-processing___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": false
                    },
                    "headers": {
                      "payload": {
                        "doCachedRetry": "true"
                      },
                      "workflowInstanceFields": {
                        "retryId": "taskInstanceId"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "UPLOAD_ITEM_ERROR_FILE",
                  "name": "Upload Item Error File",
                  "job": {
                    "maxRetry": 0,
                    "config": {
                      "mode": "S3",
                      "format": "CSV",
                      "dataNodeId": "ITEM_AGGREGATOR",
                      "dataField": "aggregatedFields.itemBlockFailure",
                      "fileName": "error-file.csv"
                    }
                  },
                  "type": "FILE_UPLOAD"
                },
                {
                  "id": "END",
                  "name": "End  - STN Approval",
                  "type": "END",
                  "job": {}
                },
                {
                  "id": "START",
                  "name": "Start - STN Approval",
                  "type": "START",
                  "job": {}
                },
                {
                  "id": "ITEM_AGGREGATOR",
                  "name": "Item Aggregator",
                  "job": {
                    "maxRetry": 0,
                    "config": {
                      "aggregateFields": {
                        "itemBlockFailure": "BLOCK_ITEMS_FOR_WH_OPS.itemUpdateFailures"
                      }
                    }
                  },
                  "type": "AGGREGATOR"
                },
                {
                  "id": "FETCH_ITEMS",
                  "name": "Fetch Items",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "augustus-stn-fetch-items___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "queryParams": {
                      "payload": {
                        "start": "0",
                        "fetchSize": "20000"
                      }
                    },
                    "pathParams": {
                      "fields": {
                        "stnId": "START.stnId"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "SKU_SPLITTER",
                  "name": "SKU Quantity Splitter",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "stnId": "START.stnId",
                        "stnBarcode": "START.stnBarcode",
                        "ignoreBlockManual": "START.ignoreBlockManual"
                      }
                    },
                    "config": {
                      "batchSize": 1,
                      "dataField": "FETCH_SKUS"
                    }
                  },
                  "type": "SPLITTER"
                },
                {
                  "id": "SKU_BLOCK_FAILURE_HANDLER",
                  "name": "Handle SKU Block Failure",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "data[0].warehouseId": "BLOCK_INVENTORY_IMS[0].warehouseId",
                        "data[0].skuId": "BLOCK_INVENTORY_IMS[0].skuId",
                        "data[0].invGroupId": "BLOCK_INVENTORY_IMS[0].ownerInventoryRequestEntries[0].ownerId",
                        "data[0].quality": "BLOCK_INVENTORY_IMS[0].ownerInventoryRequestEntries[0].quality",
                        "data[0].quantity": "BLOCK_INVENTORY_IMS[0].ownerInventoryRequestEntries[0].failedCount"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "augustus-stn-remove-skus___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": false
                    },
                    "pathParams": {
                      "fields": {
                        "stnId": "SKU_SPLITTER.stnId"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "ITEM_BLOCK_FAILURE_HANDLER",
                  "name": "item-block-failure-handler",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "data": "BLOCK_ITEMS_FOR_WH_OPS.itemIds"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "augustus-stn-remove-items___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": false
                    },
                    "pathParams": {
                      "fields": {
                        "stnId": "ITEM_SPLITTER.stnId"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "UPLOAD_SKU_ERROR_FILE",
                  "name": "Upload SKU Error File",
                  "job": {
                    "maxRetry": 0,
                    "config": {
                      "mode": "S3",
                      "format": "CSV",
                      "dataNodeId": "SKU_AGGREGATOR",
                      "dataField": "aggregatedFields.skuBlockFailures",
                      "fileName": "error-file.csv"
                    }
                  },
                  "type": "FILE_UPLOAD"
                },
                {
                  "id": "FETCH_SKUS",
                  "name": "Fetch SKUs",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "augustus-stn-fetch-skus___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "queryParams": {
                      "payload": {
                        "start": "0",
                        "fetchSize": "20000"
                      }
                    },
                    "pathParams": {
                      "fields": {
                        "stnId": "START.stnId"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "BLOCK_ITEMS_FOR_WH_OPS",
                  "name": "Block Items for WH Operations",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "data": "ITEM_SPLITTER.data"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId",
                        "taskInstanceId": "taskInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "retryInterval": 10,
                      "id": "wms-issue-items-for-ops___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "queryParams": {
                      "fields": {
                        "warehouseId": "ITEM_SPLITTER.warehouseId",
                        "orderId": "ITEM_SPLITTER.stnBarcode",
                        "stnType": "ITEM_SPLITTER.stnType"
                      }
                    },
                    "headers": {
                      "payload": {
                        "doCachedRetry": "true"
                      },
                      "workflowInstanceFields": {
                        "retryId": "taskInstanceId"
                      }
                    }
                  },
                  "type": "WORKER"
                }
              ],
              "edges": [
                {
                  "sourceNodeId": "SKU_BLOCK_FAILURE_HANDLER",
                  "destinationNodeId": "SKU_AGGREGATOR",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "FETCH_SKUS",
                  "destinationNodeId": "SKU_SPLITTER",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "SKU_SPLITTER",
                  "destinationNodeId": "BLOCK_INVENTORY_IMS",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "START",
                  "destinationNodeId": "FETCH_ITEMS",
                  "condition": {
                    "key": "data.pickMode",
                    "values": [
                      "ITEM_PICKING"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.pickMode\",\"values\":[\"ITEM_PICKING\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "SKU_AGGREGATOR",
                  "destinationNodeId": "UPLOAD_SKU_ERROR_FILE",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "ITEM_BLOCK_FAILURE_HANDLER",
                  "destinationNodeId": "ITEM_AGGREGATOR",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "FETCH_ITEMS",
                  "destinationNodeId": "ITEM_SPLITTER",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "ITEM_AGGREGATOR",
                  "destinationNodeId": "UPLOAD_ITEM_ERROR_FILE",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "START",
                  "destinationNodeId": "FETCH_SKUS",
                  "condition": {
                    "key": "data.pickMode",
                    "values": [
                      "SKU_PICKING"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.pickMode\",\"values\":[\"SKU_PICKING\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "STN_APPROVED",
                  "destinationNodeId": "END",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "BLOCK_INVENTORY_IMS",
                  "destinationNodeId": "SKU_BLOCK_FAILURE_HANDLER",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "UPLOAD_SKU_ERROR_FILE",
                  "destinationNodeId": "STN_APPROVED",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "ITEM_SPLITTER",
                  "destinationNodeId": "BLOCK_ITEMS_FOR_WH_OPS",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "UPLOAD_ITEM_ERROR_FILE",
                  "destinationNodeId": "STN_APPROVED",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "BLOCK_ITEMS_FOR_WH_OPS",
                  "destinationNodeId": "ITEM_BLOCK_FAILURE_HANDLER",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                }
              ]
            }
          }
        """
        )).asJson
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("2001"))
      .check(jsonPath("$.data[0].id").saveAs("workFlowId")))

    .exec(http("Register APP with workflow")
      .post("/workflow-service/config/app")
      .headers(poheaders)
      .body(StringBody(
        """
          {
            "name": "${appFeedData}",
            "appId": "${appFeedData}",
            "owners": [
              "anuj.modi@myntra.com",
              "ajay.mehra@myntra.com"
            ],
            "workflowConfigs": [
              {
                "workflowId": "${workFlowId}",
                "entityType": "INLINE",
                "callbackType": "NONE",
                "sla": 500
              }
            ]
          }
        """
      )).asJson
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("2001")))

    .exec(http("JobSubmit of workflow")
      .post("/workflow-service/job/submit")
      .headers(poheaders)
      .body(StringBody(
        """
          {
            "application": "${appFeedData}",
            "workflowId": "${workflowFeedData}",
            "requestPayload": {
                 "stnId": "51082",
                 "stnType": "NORMAL",
                 "stnBarcode": "STNAHBP231218-12",
                 "warehouseId": "213",
                 "ignoreBlockManual": "false",
                 "pickMode": "ITEM_PICKING"
             }
          }
        """)).asJson
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("2002")))

  private val createMwsWorkFlow2= scenario("Warehouse user role creation")
    .feed(workflowFeeder)
    .feed(appFeeder)

    .exec(http("Warehouse user role creation")
      .post("/workflow-service/workflow")
      .headers(poheaders)
      .body(StringBody(
        """
            {
            "id": "${workflowFeedData}",
            "name": "${workflowFeedData}",
            "description": "${workflowFeedData}",
            "workflow": {
              "nodes": [
                {
                  "id": "ASSIGN_ROLES_0_0",
                  "name": "Assign roles",
                  "job": {
                    "maxRetry": 3,
                    "data": {
                      "fields": {
                        "roles": "GET_ROLES_ID_0_0",
                        "team.id": "TPC_SPLITTER.data[0].buildingId",
                        "teamValue[0].attributeValue": "TPC_SPLITTER.data[0].buildingId"
                      },
                      "payload": {
                        "enabled": "true",
                        "team.teamType.id": "4",
                        "teamType.id": "4",
                        "teamValue[0].attributeTypeCode": "WAREHOUSE",
                        "teamValue[0].level": "0"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "assign-roles___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "pathParams": {
                      "fields": {
                        "uidx": "CHECK_IDEA.uidx"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "TPC_SPLITTER",
                  "name": "TPC Splitter",
                  "job": {
                    "maxRetry": 1,
                    "data": {
                      "fields": {
                        "createdBy": "START.createdBy"
                      }
                    },
                    "config": {
                      "batchSize": 1,
                      "dataField": "TPC_FILE_DOWNLOAD"
                    }
                  },
                  "type": "SPLITTER"
                },
                {
                  "id": "CHECK_IDEA",
                  "name": "See if user already exists in IDEA",
                  "job": {
                    "maxRetry": 3,
                    "data": {
                      "payload": {}
                    },
                    "config": {
                      "maxRetry": 1,
                      "id": "existing-idea-user___1.0.0",
                      "onFailure": "CONTINUE",
                      "isResponseMandatory": false
                    },
                    "pathParams": {
                      "fields": {
                        "email": "TPC_SPLITTER.data[0].emailAddress"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "END",
                  "name": "End",
                  "type": "END",
                  "job": {}
                },
                {
                  "id": "START",
                  "name": "Start - TPC",
                  "type": "START",
                  "job": {}
                },
                {
                  "id": "TPC_AGGREGATOR",
                  "name": "Aggregator",
                  "job": {
                    "maxRetry": 1,
                    "config": {}
                  },
                  "type": "AGGREGATOR"
                },
                {
                  "id": "CREATE_IN_SECURITY_0",
                  "name": "Create user in ERP security",
                  "job": {
                    "maxRetry": 3,
                    "data": {
                      "fields": {
                        "email": "TPC_SPLITTER.data[0].emailAddress",
                        "loginId": "TPC_SPLITTER.data[0].employeeId",
                        "name": "TPC_SPLITTER.data[0].firstName",
                        "password": "TPC_SPLITTER.data[0].password",
                        "uidx": "CHECK_IDEA.uidx"
                      },
                      "payload": {
                        "enabled": "true",
                        "userType": "USER"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "security-user___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "GET_ROLES_ID_0_1",
                  "name": "get role IDs for role names",
                  "job": {
                    "maxRetry": 3,
                    "data": {
                      "payload": {
                        "appClient": "START.client",
                        "status.statusCode": "803",
                        "status.statusMessage": "Role(s) retrieved successfully",
                        "status.statusType": "SUCCESS",
                        "status.totalCount": "0"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "get-roles-id___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "pathParams": {
                      "fields": {
                        "employeeType": "TPC_SPLITTER.data[0].employeeType"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "CHECK_SECURITY_1",
                  "name": "See if user already exists in Security",
                  "job": {
                    "maxRetry": 1,
                    "data": {
                      "payload": {}
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "existing-security-user___1.0.0",
                      "onFailure": "CONTINUE",
                      "isResponseMandatory": false
                    },
                    "pathParams": {
                      "fields": {
                        "email": "TPC_SPLITTER.data[0].emailAddress"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "ASSIGN_ROLES_1_1",
                  "name": "Assign roles",
                  "job": {
                    "maxRetry": 3,
                    "data": {
                      "fields": {
                        "roles": "GET_ROLES_ID_1_1",
                        "team.id": "TPC_SPLITTER.data[0].buildingId",
                        "teamValue[0].attributeValue": "TPC_SPLITTER.data[0].buildingId"
                      },
                      "payload": {
                        "enabled": "true",
                        "team.teamType.id": "4",
                        "teamType.id": "4",
                        "teamValue[0].attributeTypeCode": "WAREHOUSE",
                        "teamValue[0].level": "0"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "assign-roles___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "pathParams": {
                      "fields": {
                        "uidx": "CREATE_IN_IDEA.uidx"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "ASSIGN_ROLES_0_1",
                  "name": "Assign roles",
                  "job": {
                    "maxRetry": 3,
                    "data": {
                      "fields": {
                        "roles": "GET_ROLES_ID_0_1",
                        "team.id": "TPC_SPLITTER.data[0].buildingId",
                        "teamValue[0].attributeValue": "TPC_SPLITTER.data[0].buildingId"
                      },
                      "payload": {
                        "enabled": "true",
                        "team.teamType.id": "4",
                        "teamType.id": "4",
                        "teamValue[0].attributeTypeCode": "WAREHOUSE",
                        "teamValue[0].level": "0"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "assign-roles___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "pathParams": {
                      "fields": {
                        "uidx": "CHECK_IDEA.uidx"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "CHECK_SECURITY_0",
                  "name": "See if user already exists in Security",
                  "job": {
                    "maxRetry": 1,
                    "data": {
                      "payload": {
                        "createdBy": "START.createdBy"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "existing-security-user___1.0.0",
                      "onFailure": "CONTINUE",
                      "isResponseMandatory": false
                    },
                    "pathParams": {
                      "fields": {
                        "email": "TPC_SPLITTER.data[0].emailAddress"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "TPC_FILE_DOWNLOAD",
                  "name": "file-download",
                  "job": {
                    "maxRetry": 1,
                    "config": {
                      "onFailure": "HALT",
                      "mode": "S3",
                      "format": "CSV",
                      "filePath": {
                        "nodeId": "START",
                        "field": "fileAttachment"
                      },
                      "headersKeyMap": {
                        "Building ID": "buildingId",
                        "Email Address": "emailAddress",
                        "Employee ID": "employeeId",
                        "Employee Title": "employeeTitle",
                        "Employee Type": "employeeType",
                        "First Name": "firstName",
                        "Last Name": "lastName",
                        "password": "password"
                      }
                    }
                  },
                  "type": "FILE_DOWNLOAD"
                },
                {
                  "id": "CREATE_IN_IDEA",
                  "name": "Create user in Idea",
                  "job": {
                    "maxRetry": 3,
                    "data": {
                      "fields": {
                        "accessKey": "TPC_SPLITTER.data[0].password",
                        "email": "TPC_SPLITTER.data[0].emailAddress",
                        "username": "TPC_SPLITTER.data[0].employeeId"
                      },
                      "payload": {
                        "appName": "myntra"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "idea-signup___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "pathParams": {
                      "fields": {
                        "client": "TPC_SPLITTER.data[0].client"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "ASSIGN_ROLES_1_0",
                  "name": "Assign roles",
                  "job": {
                    "maxRetry": 3,
                    "data": {
                      "fields": {
                        "roles": "GET_ROLES_ID_1_0",
                        "team.id": "TPC_SPLITTER.data[0].buildingId",
                        "teamValue[0].attributeValue": "TPC_SPLITTER.data[0].buildingId"
                      },
                      "payload": {
                        "enabled": "true",
                        "team.teamType.id": "4",
                        "teamType.id": "4",
                        "teamValue[0].attributeTypeCode": "WAREHOUSE",
                        "teamValue[0].level": "0"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "assign-roles___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "pathParams": {
                      "fields": {
                        "uidx": "CREATE_IN_IDEA.uidx"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "GET_ROLES_ID_0_0",
                  "name": "get role IDs for role names",
                  "job": {
                    "maxRetry": 3,
                    "data": {
                      "payload": {
                        "appClient": "START.client",
                        "status.statusCode": "803",
                        "status.statusMessage": "Role(s) retrieved successfully",
                        "status.statusType": "SUCCESS",
                        "status.totalCount": "0"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "get-roles-id___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "pathParams": {
                      "fields": {
                        "employeeType": "TPC_SPLITTER.data[0].employeeType"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "GET_ROLES_ID_1_0",
                  "name": "get role IDs for role names",
                  "job": {
                    "maxRetry": 1,
                    "data": {
                      "payload": {
                        "appClient": "START.client",
                        "status.statusCode": "803",
                        "status.statusMessage": "Role(s) retrieved successfully",
                        "status.statusType": "SUCCESS",
                        "status.totalCount": "0"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "get-roles-id___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "pathParams": {
                      "fields": {
                        "employeeType": "TPC_SPLITTER.data[0].employeeType"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "GET_ROLES_ID_1_1",
                  "name": "get role IDs for role names",
                  "job": {
                    "maxRetry": 1,
                    "data": {
                      "payload": {
                        "appClient": "START.client",
                        "status.statusCode": "803",
                        "status.statusMessage": "Role(s) retrieved successfully",
                        "status.statusType": "SUCCESS",
                        "status.totalCount": "0"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "get-roles-id___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    },
                    "pathParams": {
                      "fields": {
                        "employeeType": "TPC_SPLITTER.data[0].employeeType"
                      }
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "CREATE_IN_SECURITY_1",
                  "name": "Create user in ERP security",
                  "job": {
                    "maxRetry": 3,
                    "data": {
                      "fields": {
                        "email": "TPC_SPLITTER.data[0].emailAddress",
                        "loginId": "TPC_SPLITTER.data[0].employeeId",
                        "name": "TPC_SPLITTER.data[0].firstName",
                        "password": "TPC_SPLITTER.data[0].password",
                        "uidx": "CREATE_IN_IDEA.uidx"
                      },
                      "payload": {
                        "enabled": "true",
                        "userType": "USER"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "security-user___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                }
              ],
              "edges": [
                {
                  "sourceNodeId": "TPC_SPLITTER",
                  "destinationNodeId": "CHECK_IDEA",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "ASSIGN_ROLES_1_0",
                  "destinationNodeId": "TPC_AGGREGATOR",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "TPC_FILE_DOWNLOAD",
                  "destinationNodeId": "TPC_SPLITTER",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "CREATE_IN_SECURITY_0",
                  "destinationNodeId": "GET_ROLES_ID_0_1",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "ASSIGN_ROLES_1_1",
                  "destinationNodeId": "TPC_AGGREGATOR",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "TPC_AGGREGATOR",
                  "destinationNodeId": "END",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "CREATE_IN_IDEA",
                  "destinationNodeId": "CHECK_SECURITY_1",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "ASSIGN_ROLES_0_0",
                  "destinationNodeId": "TPC_AGGREGATOR",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "CHECK_SECURITY_0",
                  "destinationNodeId": "GET_ROLES_ID_0_0",
                  "condition": {
                    "key": "status.statusCode",
                    "values": [
                      "811"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"status.statusCode\",\"values\":[\"811\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "GET_ROLES_ID_1_0",
                  "destinationNodeId": "ASSIGN_ROLES_1_0",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "START",
                  "destinationNodeId": "TPC_FILE_DOWNLOAD",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "CHECK_IDEA",
                  "destinationNodeId": "CHECK_SECURITY_0",
                  "condition": {
                    "key": "status.statusCode",
                    "values": [
                      "3"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"status.statusCode\",\"values\":[\"3\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "CHECK_IDEA",
                  "destinationNodeId": "CREATE_IN_IDEA",
                  "condition": {
                    "key": "status.statusCode",
                    "values": [
                      "3"
                    ],
                    "operator": "NOT_IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"status.statusCode\",\"values\":[\"3\"],\"operator\":\"NOT_IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "CHECK_SECURITY_1",
                  "destinationNodeId": "CREATE_IN_SECURITY_1",
                  "condition": {
                    "key": "status.statusCode",
                    "values": [
                      "811"
                    ],
                    "operator": "NOT_IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"status.statusCode\",\"values\":[\"811\"],\"operator\":\"NOT_IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "GET_ROLES_ID_1_1",
                  "destinationNodeId": "ASSIGN_ROLES_1_1",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "CREATE_IN_SECURITY_1",
                  "destinationNodeId": "GET_ROLES_ID_1_1",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "GET_ROLES_ID_0_0",
                  "destinationNodeId": "ASSIGN_ROLES_0_0",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "CHECK_SECURITY_0",
                  "destinationNodeId": "CREATE_IN_SECURITY_0",
                  "condition": {
                    "key": "status.statusCode",
                    "values": [
                      "811"
                    ],
                    "operator": "NOT_IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"status.statusCode\",\"values\":[\"811\"],\"operator\":\"NOT_IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "ASSIGN_ROLES_0_1",
                  "destinationNodeId": "TPC_AGGREGATOR",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "GET_ROLES_ID_0_1",
                  "destinationNodeId": "ASSIGN_ROLES_0_1",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "CHECK_SECURITY_1",
                  "destinationNodeId": "GET_ROLES_ID_1_0",
                  "condition": {
                    "key": "status.statusCode",
                    "values": [
                      "811"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"status.statusCode\",\"values\":[\"811\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                }
              ]
            }
          }
        """
      )).asJson
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("2001"))
      .check(jsonPath("$.data[0].id").saveAs("workFlowId")))

    .exec(http("Register APP with workflow")
      .post("/workflow-service/config/app")
      .headers(poheaders)
      .body(StringBody(
        """
          {
             "name": "${appFeedData}",
             "appId": "${appFeedData}",
             "owners": [
               "anuj.modi@myntra.com",
               "ajay.mehra@myntra.com"
             ],
             "workflowConfigs": [
               {
                 "workflowId": "${workFlowId}",
                 "entityType": "INLINE",
                 "callbackType": "NONE",
                 "sla": 500
               }
             ]
           }
        """
        )).asJson
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("2001")))

    .exec(http("JobSubmit of workflow")
        .post("/workflow-service/job/submit")
        .headers(poheaders)
        .body(StringBody(
          """
          {
            "application": "${appFeedData}",
            "workflowId": "${workflowFeedData}",
            "requestPayload": {
                 "fileAttachment": "tpc/EMPACTIVATED_27122018_0801.csv"
             }
          }
        """)).asJson
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("2002")))

  private val createMwsWorkFlow3 = scenario("CMS MP Cataloging")
    .feed(workflowFeeder)
    .feed(appFeeder)

    .exec(http("CMS MP Cataloging")
      .post("/workflow-service/workflow")
      .headers(poheaders)
      .body(StringBody(
        """
          {
            "id": "${workflowFeedData}",
            "name": "${workflowFeedData}",
            "description": "${workflowFeedData}",
            "workflow": {
              "nodes": [
                {
                  "id": "MANUAL_EDITING",
                  "name": "update-status-manual-editing",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "status": "MANUAL_EDITING"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 0,
                      "id": "cms-update-style-async___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "REJECT",
                  "name": "update-status-pending",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "status": "REJECT"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "cms-update-style___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "BIN_SPLITTER",
                  "name": "Bin Splitter",
                  "job": {
                    "maxRetry": 0,
                    "config": {
                      "batchSize": 1,
                      "dataField": "START.items"
                    }
                  },
                  "type": "SPLITTER"
                },
                {
                  "id": "MP_READY_FOR_EDITING",
                  "name": "update-status-ready_for_editing",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "status": "MP_READY_FOR_EDITING"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "cms-update-style___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "PICTOR_IN_QC",
                  "name": "update-status-pictor_in_qc",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "status": "PICTOR_IN_QC"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 0,
                      "id": "cms-update-style-async___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "MP_CREATED",
                  "name": "update-status-created",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "status": "MP_CREATED"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId",
                        "instanceId": "jobId"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "cms-update-style-instance___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "PRE_ACTIVE",
                  "name": "update-status-pre-active",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "status": "PRE_ACTIVE"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "cms-update-style___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "PENDING_WITH_PICTOR",
                  "name": "update-status-pending-with-pictor",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "id": "BIN_SPLITTER.data[0].style_id",
                        "payload.styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "type": "PICTOR"
                      }
                    },
                    "config": {
                      "maxRetry": 0,
                      "id": "send-to-pictor-async___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "READY_FOR_FINAL_QC",
                  "name": "update-status-pending-qa",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "status": "READY_FOR_FINAL_QC"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 0,
                      "id": "cms-update-style-async___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "MP_READY_FOR_CONTENT_WRITING",
                  "name": "update-status-ready-for-content-writing",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "status": "MP_READY_FOR_CONTENT_WRITING"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 0,
                      "id": "cms-update-style-async___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "START",
                  "name": "Start - MP Cataloging",
                  "type": "START",
                  "job": {}
                },
                {
                  "id": "END",
                  "name": "End",
                  "type": "END",
                  "job": {}
                },
                {
                  "id": "FINAL_QC_COMPLETE",
                  "name": "update-status-qc-done",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "status": "FINAL_QC_COMPLETE"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 0,
                      "id": "cms-update-style-async___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "DIZEN_IN_QC",
                  "name": "update-status-dizen-in-qc",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "status": "DIZEN_IN_QC"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 0,
                      "id": "cms-update-style-async___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "ACTIVE",
                  "name": "update-status-active",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "status": "ACTIVE"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 3,
                      "id": "cms-update-style___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "PENDING_WITH_DIZEN",
                  "name": "update-status-pending-with-dizen",
                  "job": {
                    "maxRetry": 0,
                    "data": {
                      "fields": {
                        "styleId": "BIN_SPLITTER.data[0].style_id"
                      },
                      "payload": {
                        "status": "PENDING_WITH_DIZEN"
                      },
                      "workflowInstanceFields": {
                        "workflowInstanceId": "workflowInstanceId"
                      }
                    },
                    "config": {
                      "maxRetry": 0,
                      "id": "cms-update-style-async___1.0.0",
                      "onFailure": "HALT",
                      "isResponseMandatory": true
                    }
                  },
                  "type": "WORKER"
                },
                {
                  "id": "BIN_AGGREGATOR",
                  "name": "Bin Aggregator",
                  "job": {
                    "maxRetry": 0
                  },
                  "type": "AGGREGATOR"
                }
              ],
              "edges": [
                {
                  "sourceNodeId": "FINAL_QC_COMPLETE",
                  "destinationNodeId": "PRE_ACTIVE",
                  "condition": {
                    "key": "data.nextStatus",
                    "values": [
                      "PRE_ACTIVE"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.nextStatus\",\"values\":[\"PRE_ACTIVE\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "READY_FOR_FINAL_QC",
                  "destinationNodeId": "FINAL_QC_COMPLETE",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "PENDING_WITH_DIZEN",
                  "destinationNodeId": "MANUAL_EDITING",
                  "condition": {
                    "key": "data.nextStatus",
                    "values": [
                      "MANUAL_EDITING"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.nextStatus\",\"values\":[\"MANUAL_EDITING\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "MP_READY_FOR_EDITING",
                  "destinationNodeId": "PENDING_WITH_PICTOR",
                  "condition": {
                    "key": "data[0].outsourcedStudio",
                    "values": [
                      "test_studios"
                    ],
                    "operator": "NOT_IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data[0].outsourcedStudio\",\"values\":[\"test_studios\"],\"operator\":\"NOT_IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "MANUAL_EDITING",
                  "destinationNodeId": "READY_FOR_FINAL_QC",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "PICTOR_IN_QC",
                  "destinationNodeId": "READY_FOR_FINAL_QC",
                  "condition": {
                    "key": "data.nextStatus",
                    "values": [
                      "READY_FOR_FINAL_QC"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.nextStatus\",\"values\":[\"READY_FOR_FINAL_QC\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "FINAL_QC_COMPLETE",
                  "destinationNodeId": "REJECT",
                  "condition": {
                    "key": "data.nextStatus",
                    "values": [
                      "REJECT"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.nextStatus\",\"values\":[\"REJECT\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "PENDING_WITH_PICTOR",
                  "destinationNodeId": "MANUAL_EDITING",
                  "condition": {
                    "key": "data.nextStatus",
                    "values": [
                      "MANUAL_EDITING"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.nextStatus\",\"values\":[\"MANUAL_EDITING\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "MP_CREATED",
                  "destinationNodeId": "MP_READY_FOR_CONTENT_WRITING",
                  "condition": {
                    "key": "data[0].bin_purpose",
                    "values": [
                      "CATALOG"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data[0].bin_purpose\",\"values\":[\"CATALOG\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "FINAL_QC_COMPLETE",
                  "destinationNodeId": "ACTIVE",
                  "condition": {
                    "key": "data.nextStatus",
                    "values": [
                      "ACTIVE"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.nextStatus\",\"values\":[\"ACTIVE\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "REJECT",
                  "destinationNodeId": "BIN_AGGREGATOR",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "BIN_AGGREGATOR",
                  "destinationNodeId": "END",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "DIZEN_IN_QC",
                  "destinationNodeId": "MANUAL_EDITING",
                  "condition": {
                    "key": "data.nextStatus",
                    "values": [
                      "MANUAL_EDITING"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.nextStatus\",\"values\":[\"MANUAL_EDITING\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "MP_READY_FOR_EDITING",
                  "destinationNodeId": "PENDING_WITH_DIZEN",
                  "condition": {
                    "key": "data[0].outsourcedStudio",
                    "values": [
                      "test_studios"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data[0].outsourcedStudio\",\"values\":[\"test_studios\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "ACTIVE",
                  "destinationNodeId": "BIN_AGGREGATOR",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "BIN_SPLITTER",
                  "destinationNodeId": "MP_CREATED",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "PENDING_WITH_PICTOR",
                  "destinationNodeId": "PICTOR_IN_QC",
                  "condition": {
                    "key": "data.nextStatus",
                    "values": [
                      "PICTOR_IN_QC"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.nextStatus\",\"values\":[\"PICTOR_IN_QC\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "DIZEN_IN_QC",
                  "destinationNodeId": "READY_FOR_FINAL_QC",
                  "condition": {
                    "key": "data.nextStatus",
                    "values": [
                      "READY_FOR_FINAL_QC"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.nextStatus\",\"values\":[\"READY_FOR_FINAL_QC\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "MP_CREATED",
                  "destinationNodeId": "MP_READY_FOR_EDITING",
                  "condition": {
                    "key": "data[0].bin_purpose",
                    "values": [
                      "PHOTOSHOOT"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data[0].bin_purpose\",\"values\":[\"PHOTOSHOOT\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "PICTOR_IN_QC",
                  "destinationNodeId": "MANUAL_EDITING",
                  "condition": {
                    "key": "data.nextStatus",
                    "values": [
                      "MANUAL_EDITING"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.nextStatus\",\"values\":[\"MANUAL_EDITING\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "MP_READY_FOR_CONTENT_WRITING",
                  "destinationNodeId": "READY_FOR_FINAL_QC",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "PENDING_WITH_DIZEN",
                  "destinationNodeId": "DIZEN_IN_QC",
                  "condition": {
                    "key": "data.nextStatus",
                    "values": [
                      "DIZEN_IN_QC"
                    ],
                    "operator": "IN"
                  },
                  "direction": "OUT",
                  "edgeType": "CONDITIONAL",
                  "properties": {
                    "condition": "{\"key\":\"data.nextStatus\",\"values\":[\"DIZEN_IN_QC\"],\"operator\":\"IN\",\"condition1\":null,\"condition2\":null}"
                  }
                },
                {
                  "sourceNodeId": "START",
                  "destinationNodeId": "BIN_SPLITTER",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                },
                {
                  "sourceNodeId": "PRE_ACTIVE",
                  "destinationNodeId": "BIN_AGGREGATOR",
                  "direction": "OUT",
                  "edgeType": "NO_CONDITION",
                  "properties": {}
                }
              ]
            }
          }
        """
      )).asJson
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("2001"))
      .check(jsonPath("$.data[0].id").saveAs("workFlowId")))

    .exec(http("Register APP with workflow")
      .post("/workflow-service/config/app")
      .headers(poheaders)
      .body(StringBody(
        """
          {
                      "name": "${appFeedData}",
                      "appId": "${appFeedData}",
                      "owners": [
                        "anuj.modi@myntra.com",
                        "ajay.mehra@myntra.com"
                      ],
                      "workflowConfigs": [
                        {
                          "workflowId": "${workFlowId}",
                          "entityType": "INLINE",
                          "callbackType": "NONE",
                          "sla": 500
                        }
                      ]
                    }
        """
      )).asJson
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("2001")))

    .exec(http("JobSubmit of workflow")
      .post("/workflow-service/job/submit")
      .headers(poheaders)
      .body(StringBody(
        """
          {
            "application": "${appFeedData}",
            "workflowId": "${workflowFeedData}",
            "requestPayload": {
                  "id": 412375,
                  "createdBy": null,
                  "createdOn": null,
                  "lastModifiedOn": null,
                  "version": null,
                  "bin_code": "MBADMOC-170951",
                  "bin_type": "MARKETPLACE",
                  "location": "BENGALURU",
                  "team": "MARKETPLACE",
                  "set_type": "Double Set",
                  "bin_purpose": "CATALOG",
                  "brand_type": "Others",
                  "bin_item_category": "Apparels",
                  "binStatus": "MP_CREATED",
                  "items": [
                    {
                      "id": null,
                      "createdBy": null,
                      "createdOn": null,
                      "lastModifiedOn": null,
                      "version": null,
                      "style_id": 8390509,
                      "item_code": null,
                      "sku_code": null,
                      "vendor_article_no": null,
                      "vendor_article_name": null,
                      "size": null,
                      "gender": "Women",
                      "brand_name": "The Dry State",
                      "article_type_name": "Tshirts",
                      "bin_code": null,
                      "rejectReason": null,
                      "partnerProcessDetail": null,
                      "workflowInstanceId": null,
                      "isCSDone": null,
                      "isEnrichmentDone": null,
                      "qcrejected": null
                    }
                  ],
                  "binSuffix": "170951",
                  "enrichmentType": null,
                  "itemCount": 1,
                  "workflowInstanceId": null,
                  "outsourcedStudio": "urban_studios_delhi"
                }
          }
        """
      )).asJson
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("2002")))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(mwsBaseUrl)

}

