package simulations.platform

import com.myntra.commons.util.FeederUtil
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

/*
ingestion/payment_event
/entity/{entityId}/dimension{dimension}
 */
class CentaurusSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "centaurus-uidx.csv"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }

  //val uidxFileLocation = System.getProperty("uidxFileLocation", "test-data/centaurus-uidx.csv")
  val uidxFileLocation = System.getProperty("uidxFileLocation", fileName)
  val filePathUidxFileLocation: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  val centaurusBaseUrl = System.getProperty("baseUrl", "http://centaurus.myntra.com")

  val httpProtocol = http
    .baseUrl(centaurusBaseUrl)

  val r = scala.util.Random

  val linesUidx = readFile(filePathUidxFileLocation)

  var paymentOptions = Array("debitcard", "creditcard", "netbanking", "cod", "wallet")
  var paymentBanks = Array("HDFC","ICICI","CITIBANK","AXIS","SBI","YESBANK","IDFC","Kotak","UCO","Union","Vijaya")
  var digits = "0123456789"



    private val ingestEvent =scenario("ingestEvent")
    .exec(session => session.set("uidx", linesUidx(r.nextInt(200000))))
    .exec(session => session.set("paymentOption", paymentOptions(r.nextInt(paymentOptions.length))))
    .exec(session => session.set("paymentBank", paymentBanks(r.nextInt(paymentBanks.length))))
    .exec(session => session.set("paymentStatus", "SUCCESS"))
    .exec(session => session.set("binNumber", (1 to 6).map(_ => digits(r.nextInt(digits.length))).mkString))
    .exec(session => session.set("transactionTime", System.currentTimeMillis()))
    .exec(http("ingestEvent")
      .post("/ingestion/payment_event")
      .body(StringBody("""{"uidx": "${uidx}", "paymentOption":"${paymentOption}", "paymentBank":"${paymentBank}", "paymentStatus":"${paymentStatus}", "binNumber":"${binNumber}", "transactionTime":"${transactionTime}"}""")).asJson
      .headers(headers)
      .check(status.is(200))
    )

  private val getDimension =scenario("getDimension")
    .exec(session => session.set("entityId", linesUidx(r.nextInt(200000))))
    .exec(http("getDimension")
      .get("/entity/${entityId}/dimension/user.payments")
      .headers(headers)
      .check(status.is(200))
    )


  setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)

}

/*
  Command for the simulation.
  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=3 -jar inbound-performance-test-all.jar -s simulations.platform.LinkedAccountsSimulation  -df . -rf .
 */