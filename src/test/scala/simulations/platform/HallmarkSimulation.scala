package simulations.platform

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.math._

class HallmarkSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "uidxlist1.csv"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }


  def randomStringFromCharList(length: Int, chars: Seq[Char]): String = {
    val sb = new StringBuilder
    for (i <- 1 to length) {
      val randomNum = util.Random.nextInt(chars.length)
      sb.append(chars(randomNum))
    }
    sb.toString
  }

  def randomAlphaNumericString(length: Int): String = {
    val chars = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9')
    randomStringFromCharList(length, chars)
  }

  val userCount = System.getProperty("users.count", "15").toInt
  //val uidxFileLocation = System.getProperty("uidxFileLocation", "test-data/uidxlist1.csv").toString
  val uidxFileLocation = System.getProperty("uidxFileLocation", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.HALLMARK_SERVICE.toString)
  private val hallmarkServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp
  val r = scala.util.Random

  val lines = readFile(filePath)

   val readapi =scenario("hallmark read test")
    .exec(session => session.set("readuidx", lines(r.nextInt(100000))))
    .exec(http("hallmark read test")
      .post("/myntra-hallmark-service/attributes/${readuidx}")
      .body(StringBody("""["loadtest"]""")).asJson
      .headers(headers)
      .header("clientId", "2297")
      .check(status.is(200))
    )

  //  private val addapi = scenario("hallmark add test")
  //    .exec(session => session.set("adduidx", lines(r.nextInt(300000))))
  //    .exec(session =>session.set("radd", randomAlphaNumericString(20)))
  //    .exec(http("hallmark add test")
  //      .post("/myntra-hallmark-service/attributes/updateall")
  //      .body(StringBody("""[{"uidx":"${adduidx}","attributeEntryList":[{"namespace":"loadtest", "key":"test_key", "action":"ADD", "value":"${radd}"}]}]""")).asJson
  //      .headers(headers)
  //      .header("clientId", "2297")
  //      .check(status.is(200))
  //    )
  //
  //  private val deleteapi =scenario("hallmark delete test")
  //    .exec(session => session.set("deleteuidx", lines(r.nextInt(300000))))
  //    .exec(session =>session.set("rdelete", randomAlphaNumericString(20)))
  //    .exec(http("hallmark delete test")
  //      .post("/myntra-hallmark-service/attributes/updateall")
  //      .body(StringBody("""[{"uidx":"${deleteuidx}","attributeEntryList":[{"namespace":"loadtest", "key":"test_key", "action":"DELETE", "value":"${rdelete}"}]}]""")).asJson
  //      .headers(headers)
  //      .header("clientId", "2297")
  //      .check(status.is(200))
  //    )

  private val updateapi =scenario("hallmark update test")
    .exec(session => session.set("updateuidx", lines(r.nextInt(100000))))
    .exec(session =>session.set("rupdate", randomAlphaNumericString(20)))
    .exec(http("hallmark update test")
      .post("/myntra-hallmark-service/attributes/updateall")
      .body(StringBody("""[{"uidx":"${updateuidx}","attributeEntryList":[{"namespace":"loadtest", "key":"test_key", "action":"UPDATE", "value":"${rupdate}"}]}]""")).asJson
      .headers(headers)
      .header("clientId", "2297")
      .check(status.is(200))
    )

  val readCount =floor(userCount)
  //    val addCount = floor(userCount*0.5)
  //    val deleteCount = floor(userCount*0.5)
  val updateCountTemp = (userCount*2)/5
  val updateCount = floor(updateCountTemp)
  //


  //private val ReadAndWrite = Seq(readCount, updateapi)
  //private val ReadAndWriteScenarios = scenario("Hallmark read and write").(ReadAndWrite);
  //private val ReadAndWriteScenarios = scenario("Hallmark read and write scenario").exec(ReadAndWrite)


  LOG.info("readCount "+readCount)
  //    LOG.info("addCount "+addCount)
  //    LOG.info("deleteCount "+deleteCount)
  LOG.info("updateCount "+updateCount)

   // var list = List(readapi, updateapi);

 //  list = list filter (filterScenariosByName)
  //   var newList = list.map(x => x.inject(constantUsersPerSec(userCount) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized))

  //  protected var toFilterScenariosList = getProperty("filter.scenarios").split(",") map (e => e.trim.toLowerCase) toList;
  //list = scenarios map (e => e.inject(step).protocols(hallmarkServiceBaseUrl))

  // var list1 = readapi.inject(constantUsersPerSec(readCount) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized) :: list
  //    val list2 = addapi.inject(constantUsersPerSec(addCount) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized) :: list1
  //var list3 = updateapi.inject(constantUsersPerSec(updateCount) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized) :: list
  //    val list4 = deleteapi.inject(constantUsersPerSec(addCount) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized) :: list3

  // setUp(list map (e => e.inject(step)) toList)
  // .protocols(hallmarkServiceBaseUrl)

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(hallmarkServiceBaseUrl)

  /*setUp(readapi.inject(constantUsersPerSec(readCount) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized)

    .protocols(hallmarkServiceBaseUrl),

    updateapi.inject(constantUsersPerSec(updateCount) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized)
      .protocols(hallmarkServiceBaseUrl)

  ).maxDuration(maxDurationInSec)*/

}

/*
  Command for the simulation.
  Refresh token is called 19 times with the token afrer login

  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=3 -Dfilter.scenarios="location" -Duidxfilelocation="/Users/11151/Downloads/uidxlist.txt" -jar inbound-performance-test-all.jar -s simulations.platform.HallmarkSimulation  -df . -rf .
  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=3 -Dfilter.scenarios="location" -Duidxfilelocation="/home/guest/perf/uidx.txt" -jar inbound-performance-test-all.jar -s simulations.platform.HallmarkSimulation  -df . -rf .
 */


/////