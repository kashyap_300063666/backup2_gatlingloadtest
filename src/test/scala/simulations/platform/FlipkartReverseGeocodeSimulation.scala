package simulations.platform

import com.myntra.commons.util.FeederUtil
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

/*
Get location details from lat, long
 */
class FlipkartReverseGeocodeSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "lat_long.csv"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }

  //val latLongFile = System.getProperty("lat_long", "test-data/lat_long.csv")
  val latLongFile = System.getProperty("lat_long", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, latLongFile)
  //val csvfeeder = csv(latLongFile).circular
  val csvfeeder = csv(filePath).circular

  val flipkartBaseUrl = "https://maps.flipkart.com/reverse-geocode"

  val httpProtocol = http.baseUrl(flipkartBaseUrl)

  val getLocationDetails =
    scenario("getLocationDetails")
      .feed(csvfeeder)
      .exec(http("getLocationDetails")
        .get("?point=${geo_lat},${geo_lat}&key=74915a5c-e4ad-af3b-e2b9-1cfda0afb323")
        .headers(headers)
        .header("Referer", "https://www.myntra.com")
        .check(status.is(200))
      )

  setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)

}

/*
  Run on stage
  ./gradlew fatJar

  java -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=5  -jar build/libs/myntra-gatling-load-tests-all.jar -s simulations.platform.FlipkartReverseGeocodeSimulation  -df . -rf .
 */