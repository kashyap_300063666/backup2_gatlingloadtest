package simulations.platform

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

import scala.math._



class AddressSimulation extends BaseSimulation {


  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "addressLT.csv"
  val csvFile = System.getProperty("addressUidxFile", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)
  val csvfeeder = csv(filePath).circular
  val fileNameUidxFileLocation = "addressLT_users.csv"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }
  val r = scala.util.Random

  //val csvFile = System.getProperty("addressUidxFile", "test-data/addressLT.csv").toString

  //val uidxFileLocation = System.getProperty("uidxFileLocation", "test-data/addressLT_users.csv")
  val uidxFileLocation = System.getProperty("uidxFileLocation", fileNameUidxFileLocation)
  val filePathUidxFileLocation: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  val linesUidx = readFile(filePathUidxFileLocation)

  val addressServiceBaseUrl = System.getProperty("baseUrl", "http://address.myntra.com")

  val httpProtocol = http
    .baseUrl(addressServiceBaseUrl)


  //val csvfeeder = csv(csvFile).circular

  var name = "Test User"
  var address = "H.No.45, Myntra VLT Street"
  var locality = "Myntra"
  var city = "Jalandhar"
  var stateCode = "PB"
  var countryCode = "IN"
  var pincode = "144022"
  var email = "testmyntra@gmail.com"
  var mobile= "2342342423"
  var addressType= "HOME"


  val getAllAddressesForUser =
    scenario("getAllAddressesForUser")
      .exec(session => session.set("uidx", linesUidx(r.nextInt(2000))))
      .exec(http("getAllAddressesForUser")
        .get("/myntra-address-service/v3/address")
        .headers(headers)
        .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("15002"))
      )

  val getAddressById =
    scenario("getAddressById")
    .feed(csvfeeder)
      .exec(http("getAddressById")
        .get("/myntra-address-service/v3/address/${id}")
        .headers(headers)
        .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("15002"))
      )

  /*
  The following function first adds an address for the user / uidx
  It then makes 2 API calls
  1. Fetch all addresses for the same user
  2. Fecth address by id and uidx . Here id is the one that is returned during add address call - Called by OMS, Cart
  3. Fetch address by id - Called by LMS
   */
  val addAndGetAllAddresses =
    scenario("addAndGetAllAddresses")
      .exec(session => session.set("uidx", linesUidx(r.nextInt(200000))))
      .exec(session => session.set("name", name))
      .exec(session => session.set("address", address))
      .exec(session => session.set("locality", locality))
      .exec(session => session.set("city", city))
      .exec(session => session.set("stateCode", stateCode))
      .exec(session => session.set("countryCode", countryCode))
      .exec(session => session.set("pincode", pincode))
      .exec(session => session.set("email", email))
      .exec(session => session.set("mobile", mobile))
      .exec(session => session.set("addressType", addressType))
      .exec(http("addAddressForUidx")
        .post("/myntra-address-service/v3/address")
        .body(StringBody("""{"name": "${name}", "address":"${address}", "email":"${email}", "locality":"${locality}", "city":"${city}", "stateCode":"${stateCode}", "countryCode":"${countryCode}",  "pincode":"${pincode}",  "mobile":"${mobile}", "addressType":"${addressType}"}""")).asJson
        .headers(headers)
        .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
        .check(status.is(200))
        .check(jsonPath("$.data[*].id").optional.saveAs("id"))
      ).exec(http("getAllAddresses")
                .get("/myntra-address-service/v3/address")
                .headers(headers)
                .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
                .check(status.is(200))
              )
      .exec(http("getSecuredAddressByIdAndUidx")
                .get("/myntra-address-service/v3/secured/users/${uidx}/addresses/${id}")
                .headers(headers)
                .header("role", "CC_USER")
                .header("token", "123")
                .header("x-mynt-ctx", "storeid=2297")
                .check(status.is(200))
                ).exec(http("getSecuredAddressById")
                    .get("/myntra-address-service/v3/secured/addresses/${id}")
                    .headers(headers)
                    .header("role", "CC_USER")
                    .header("token", "123")
                    .header("x-mynt-ctx", "storeid=2297")
                    .check(status.is(200))
  )


  val addAndDeleteAddresses =
    scenario("addAndDeleteAddresses")
      .exec(session => session.set("uidx", linesUidx(r.nextInt(200000))))
      .exec(session => session.set("name", name))
      .exec(session => session.set("address", address))
      .exec(session => session.set("locality", locality))
      .exec(session => session.set("city", city))
      .exec(session => session.set("stateCode", stateCode))
      .exec(session => session.set("countryCode", countryCode))
      .exec(session => session.set("pincode", pincode))
      .exec(session => session.set("email", email))
      .exec(session => session.set("mobile", mobile))
      .exec(session => session.set("addressType", addressType))
      .exec(http("addAddressForUidxFollowedByDelete")
        .post("/myntra-address-service/v3/address")
        .body(StringBody("""{"name": "${name}", "address":"${address}", "email":"${email}", "locality":"${locality}", "city":"${city}", "stateCode":"${stateCode}", "countryCode":"${countryCode}",  "pincode":"${pincode}",  "mobile":"${mobile}", "addressType":"${addressType}"}""")).asJson
        .headers(headers)
        .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
        .check(status.is(200))
        .check(jsonPath("$.data[*].id").optional.saveAs("id"))
      ).exec(http("deleteAddressbyIdAfterAdd")
      .delete("/myntra-address-service/v3/address/${id}")
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
      .check(status.is(200))
    )


  val addAddress =
    scenario("addAddress")
      .exec(session => session.set("uidx", linesUidx(r.nextInt(200000))))
      .exec(session => session.set("name", name))
      .exec(session => session.set("address", address))
      .exec(session => session.set("locality", locality))
      .exec(session => session.set("city", city))
      .exec(session => session.set("stateCode", stateCode))
      .exec(session => session.set("countryCode", countryCode))
      .exec(session => session.set("pincode", pincode))
      .exec(session => session.set("email", email))
      .exec(session => session.set("mobile", mobile))
      .exec(session => session.set("addressType", addressType))
      .exec(http("addAddressForUidx")
        .post("/myntra-address-service/v3/address")
        .body(StringBody("""{"name": "${name}", "address":"${address}", "email":"${email}", "locality":"${locality}", "city":"${city}", "stateCode":"${stateCode}", "countryCode":"${countryCode}",  "pincode":"${pincode}",  "mobile":"${mobile}", "addressType":"${addressType}"}""")).asJson
        .headers(headers)
        .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
        .check(status.is(200))
        .check(jsonPath("$.data[0].id").saveAs("id"))

      )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)

}

/*
  Run on stage
  ./gradlew fatJar

  java -DbaseUrl=http://address.stage.myntra.com -Dfilter.scenarios="addAndGetAllAddresses" -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=5  -jar build/libs/myntra-gatling-load-tests-all.jar -s simulations.platform.AddressSimulation  -df . -rf .
 */
