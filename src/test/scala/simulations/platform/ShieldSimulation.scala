package simulations.platform

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

/*
 */
class ShieldSimulation extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileNameAddressIdFileLocation = "addressIds.csv"
  val fileNameUidxFileLocation = "linkedAccountsUidx.csv"
  val fileNameDeviceFileLocation = "linkedAccountsDevice.csv"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }

  //val uidxFileLocation = System.getProperty("uidxFileLocation", "test-data/linkedAccountsUidx.csv")
  val uidxFileLocation = System.getProperty("uidxFileLocation", fileNameUidxFileLocation)
  val filePathUidxFileLocation: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  val deviceFileLocation = System.getProperty("deviceFileLocation", fileNameDeviceFileLocation)
  val filePathDeviceFileLocation: String = feederUtil.getFileDownloadedPath(containerName, deviceFileLocation)

  //val addressIdFileLocation = System.getProperty("addressIdFileLocation", "test-data/addressIds.csv")
  val addressIdFileLocation = System.getProperty("addressIdFileLocation", fileNameAddressIdFileLocation)
  val filePathAddressIdFileLocation: String = feederUtil.getFileDownloadedPath(containerName, addressIdFileLocation)
  val r = scala.util.Random

  val shieldServiceBaseUrl = System.getProperty("baseUrl", "http://ats.myntra.com")
  val httpProtocol = http
    .baseUrl(shieldServiceBaseUrl)

  val linesUidx = readFile(filePathUidxFileLocation)
  val linesDevice = readFile(filePathDeviceFileLocation)
  val linesAddressId = readFile(filePathAddressIdFileLocation)


  private val getUserPaymentActions =scenario("getUserPaymentActions")
    .exec(session => session.set("uidx", linesUidx(r.nextInt(20000))))
    .exec(session => session.set("addressId", linesAddressId(r.nextInt(20000))))
    .exec(session => session.set("deviceId", linesDevice(r.nextInt(20000))))
    .exec(http("getUserPaymentActions")
      .post("/myntra-ats-service/v2/shield/user/verification")
      .body(StringBody("""{"uidx":"${uidx}","addressId":"${addressId}","deviceId":"${deviceId}"}""")).asJson
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297")
      .check(status.is(200))
    )

  setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)

}

/*
  Command for the simulation.
  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=2 -Dduration.seconds=3 -jar inbound-performance-test-all.jar -s simulations.platform.LinkedAccountsSimulation  -df . -rf .
 */