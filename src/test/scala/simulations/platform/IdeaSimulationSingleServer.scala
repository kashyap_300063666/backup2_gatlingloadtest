package simulations.platform

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

import scala.math._


//auth/mobile/securerefresh
//auth email
//profile/uidx/{app}/{uidx}
//profile/uidx/{uidx}


class IdeaSimulationSingleServer extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileNameUidxFileLocation = "uidxlist1.csv"


  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }


  val secureRefreshCallPerUser = System.getProperty("securerefresh.call", "1").toInt // rattio of refresh vs login is 19:1
  val ignoreCache = System.getProperty("ignore.cache", "false").toString
  //val uidxFileLocation = System.getProperty("uidxFileLocation", "test-data/uidxlist1.csv").toString
  val uidxFileLocation = System.getProperty("uidxFileLocation", fileNameUidxFileLocation).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)


  val loginUsers = System.getProperty("users.count", "1").toInt
  val getUsersByUidxCallPerUser = System.getProperty("getuserbyuidx.call", "1").toInt
  val getUsersByUidx = loginUsers * 3 // getUserByUidx call to login ration is 1.7:1
  val pauseAfterLogin = System.getProperty("pauseAfterlogin.call", "0")
  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.IDEA_SERVICE_SS.toString)
  private val ideaServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp
  val r = scala.util.Random
  private val loginIdea = exec(session => session.set("emailId", r.nextInt(1600000) + "@myntra360.com"))
    .exec(http("idea login test")
      .post("/idea/opt/auth/email")
      .body(StringBody("""{"emailId":"${emailId}","accessKey":"${emailId}","appName":"myntra"}""")).asJson
      .headers(headers)
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("3"))
      .check(jsonPath("$.tokenEntry.at").saveAs("at"))
      .check(jsonPath("$.tokenEntry.rt").saveAs("rt"))
    )

  private val mobileRefreshChain = repeat(secureRefreshCallPerUser) {
    exec(http("idea mobile refresh")
      .post("/idea/opt/auth/mobile/securefresh?ic=" + ignoreCache)
      .headers(headers)
      .body(StringBody("""{"rt":"${rt}"}""")).asJson
      .check(status.is(200)))
  }

  private val mobileRefresh = exec(mobileRefreshChain)
  //private val loginIdeaAndMoobileRefresh = Seq(loginIdea, mobileRefresh)
  //private val loginIdeaTest = scenario("idea login test").exec(loginIdeaAndMoobileRefresh)


  val lines = readFile(filePath)

  private val ideafetchUidx = repeat(getUsersByUidxCallPerUser) {
    exec(session => session.set("id", lines(r.nextInt(300000))))
      .exec(http("idea get by uidx")
        .get("/idea/opt/profile/uidx/myntra/${id}?ic=" + ignoreCache)
        .headers(headers)
        .check(status.is(200)))
  }

  private val ideafetchUidxTest = scenario("idea fetch by uidx").exec(ideafetchUidx)

  private val loginUidxAndSecureRefresh = Seq(loginIdea, ideafetchUidx, mobileRefresh)
  private val LoginUidxAndSecureRefreshTest = scenario("idea all scenarios").exec(loginUidxAndSecureRefresh)

  //  private val ideafetchUidx = repeat(getUsersByUidxCallPerUser) {
  //    exec(session => session.set("id", lines(r.nextInt(300000))))
  //      .exec(http("idea get by uidx")
  //        .get("/idea/opt/profile/uidx/myntra/${id}?ic=" + ignoreCache)
  //        .headers(headers)
  //        .check(status.is(200)))
  //    }
  //
  //  private val ideafetchUidxTest = scenario("idea fetch by uidx").exec(ideafetchUidx)


  val list = scenarios map (e => e.inject(step).protocols(ideaServiceBaseUrl))
  protected var toFilterScenariosList = getProperty("filter.scenarios").split(",") map (e => e.trim.toLowerCase) toList;
  var newList = list;
  if (toFilterScenariosList.contains(ideafetchUidxTest.name.toLowerCase)) {
    newList = ideafetchUidxTest.inject(constantUsersPerSec(floor(getUsersByUidx)) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized) :: list
  }

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(ideaServiceBaseUrl)

}

/*
  Command for the simulation.
  Refresh token is called 19 times with the token afrer login

  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=300 -Dduration.seconds=60 -Dfilter.scenarios="idea" -Duidxfilelocation="/Users/11151/Downloads/uidxlist.txt" -jar inbound-performance-test-all.jar -s simulations.platform.IdeaSimulation  -df . -rf .
 */
