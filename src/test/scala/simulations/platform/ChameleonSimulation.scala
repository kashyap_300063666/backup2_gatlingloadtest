package simulations.platform

import simulations.BaseSimulation
import com.myntra.commons.ServiceType
import io.gatling.core.Predef.array2FeederBuilder
import io.gatling.core.Predef.atOnceUsers
import io.gatling.core.Predef.configuration
import io.gatling.core.Predef.findCheckBuilder2ValidatorCheckBuilder
import io.gatling.core.Predef.scenario
import io.gatling.core.Predef.stringToExpression
import io.gatling.core.Predef.value2Expression
import io.gatling.http.Predef.status
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}


class ChameleonSimulation extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileNameUidxFileLocation = "uidxlist2.csv"
  
//  def readFile(filename: String): Seq[String] = {
//    val bufferedSource = scala.io.Source.fromFile(filename)
//    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
//    bufferedSource.close
//    lines
//  }

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }


  val r = scala.util.Random
    private val morpheusBaseUrl = System.getProperty("baseUrl", BaseUrlConstructor.getBaseUrl(ServiceType.MORPHEUS.toString))
    private val androidNamespace = "apps.android-os"
    private val runtimecontextMyntra = "internalUser:false;partnerID:2297;"
    private val deviceId = (r.nextInt(8))+"."+(r.nextInt(4))
    private val uidx = deviceId+"."+(r.nextInt(4))+"."+(r.nextInt(4))+"."+(r.nextInt(22))
    //private val uidxFileLocation = System.getProperty("uidxfilelocation", "test-data/uidxlist2.csv").toString
    private val uidxFileLocation = System.getProperty("uidxfilelocation", fileNameUidxFileLocation).toString
    val filePath: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  private val experiments = System.getProperty("experiments").toString
  private val poheaders = Map("cache-control" -> "no-cache", "accept" -> "application/json", "Content-Type" -> "application/json", "experiments" -> experiments, "namespace" -> androidNamespace, "runtime-context" -> runtimecontextMyntra, "uuid" -> ("uidx:" + uidx + ";" + "deviceid:" + deviceId + ";"))

  val lines = readFile(filePath)

  val httpProtocol = http
    .baseUrl(morpheusBaseUrl)

  private val getUserAssignmentLoad =
    exec(http("get user assignment")
      .get("/api/v1/user")
      .headers(poheaders)
      .check(status.in(200, 417))
    )

  private val getUserAssignmentfromFile = repeat(9) {
    exec(session => session.set("id", lines(r.nextInt(100000)).toString()))
      .exec(http("get user assignment from file")
        .get("/api/v1/user")
        .headers(poheaders)
        .header("uuid", "uidx:${id};deviceid:load${id};")
        .check(status.in(200, 417))
      )
  }

  private val getUidxFromFile =
    exec(session => session.set("id", lines(r.nextInt(100000)).toString()))
      .exec(http("get user assignment from file")
        .get("/api/v1/user")
        .headers(poheaders)
        .header("uuid", "uidx:${id};")
        .check(status.in(200, 417))
      )
  private val getDeviceIdFromFile =
    exec(session => session.set("id", lines(r.nextInt(100000)).toString()))
      .exec(http("get deviceId from file")
        .get("/api/v1/user")
        .headers(poheaders)
        .header("uuid", "deviceId:${id};")
        .check(status.in(200, 417))
      )

  val finalSeq = Seq(getUserAssignmentLoad, getUserAssignmentfromFile)
  val finalScenario = scenario("finalScenario").exec(finalSeq)

  val getUserAssignment = scenario("get user assignment scenario").exec(getUserAssignmentLoad)
  val getUserAssignmentFromFileLoad = scenario("get user assignment from file scenario").exec(getUserAssignmentfromFile)
  val getUidxfromFile = scenario("Get Uidx from file scenario").exec(getUidxFromFile)
  val getDeviceIdfromFile = scenario("Get deviceId from file scenario").exec(getDeviceIdFromFile)



  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)


}