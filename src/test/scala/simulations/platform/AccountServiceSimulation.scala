package simulations.platform

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

import scala.util.Random

/**
  * @author Abhishyam.c on 08/12/20
  * @author Abhishek.Jain on 23/02/21
  */
class AccountServiceSimulation extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "AccountServiceUsers.csv"

  //Data files
  //private val uidxFileLocation = System.getProperty("uidxFileLocation", "test-data/AccountServiceUsers.csv").toString
  val uidxFileLocation = System.getProperty("uidxFileLocation", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  private val getByUidxCsvfeeder = csv(filePath).circular

  private val rand = new Random()

  private val ignoreCache = System.getProperty("ignore.cache", "false").toString
  private val templateView = System.getProperty("template.view", "T10").toString
  private val emailLoginUserCount = System.getProperty("emailLogin.count", "1").toInt
  private val getUsersByUidxCount = System.getProperty("getByUidx.count", "30").toInt
  private val phoneOTPLoginUserCount = System.getProperty("phoneOTPLogin.count", "1").toInt

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.ACCOUNT_SERVICE.toString)
  private val accountServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp

  private val accountGetByUidxChain = repeat(getUsersByUidxCount) {
    feed(getByUidxCsvfeeder)
      .exec(http("Get By UIDX")
        .get("/api/v1/accounts/users/uidx/${id}?view=" + templateView + "&ic=" + ignoreCache)
        .headers(headers)
        .header("clientId", "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61")
        .header("tenantId", "1")
        .check(status.is(200))
      )
  }

  private val loginByEmailChain = repeat(emailLoginUserCount) {
    exec(session => session.set("emailId", rand.nextInt(1600000) + 1 + "@myntra360.com"))
      .exec(http("Login By Email Password")
      .post("/api/v1/accounts/auth/email")
      .body(StringBody("""{"email":"${emailId}","password":"${emailId}"}""")).asJson
      .headers(headers)
      .header("clientId", "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61")
      .header("tenantId", "1")
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("3"))
      .check(jsonPath("$.token.at").saveAs("at"))
      .check(jsonPath("$.token.rt").saveAs("rt"))
      )
  }

  private val loginByPhoneOTPChain = repeat(phoneOTPLoginUserCount) {
    exec(session => session.set("phone", "111" + (1000000 + rand.nextInt(9000000)).toString))
      .exec(session => session.set("otp", "1234"))
      .exec(http("Send OTP V2")
        .post("/api/v1/accounts/auth/mnol/phone/send/otp")
        .body(StringBody("""{"phone":"${phone}","otp":"${otp}"}""")).asJson
        .headers(headers)
        .header("clientId", "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61")
        .header("tenantId", "1")
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.urt").saveAs("urt"))
      ).exec(http("Verify OTP V2")
      .post("/api/v1/accounts/auth/mnol/phone/verify/otp")
      .body(StringBody("""{"urt":"${urt}","otp":"${otp}"}""")).asJson
      .headers(headers)
      .header("clientId", "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61")
      .header("tenantId", "1")
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").in("3", "111077"))
      .check(jsonPath("$.urt").is("${urt}"))
    )
  }

  val getByUIDX = scenario("getByUIDX").exec(accountGetByUidxChain)
  val loginByEmail = scenario("loginByEmail").exec(loginByEmailChain)
  val loginByPhoneOTP = scenario("loginByPhoneOTP").exec(loginByPhoneOTPChain)

  val allSeq = Seq(accountGetByUidxChain, loginByEmailChain, loginByPhoneOTPChain)
  val allScenerios = scenario("allScenarios").exec(allSeq)

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(accountServiceBaseUrl)
}