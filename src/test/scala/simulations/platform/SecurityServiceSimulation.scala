/*
  Command for the simulation.
  Validate users is called 3 times with the token afrer login

  java -Denvironment=prod -Duse.constant_users=true -Dusers.count=300 -Dduration.seconds=60 -Dfilter.scenarios="security" -jar inbound-performance-test-all.jar -s simulations.platform.SecurityServiceSimulation  -df . -rf .
 */

package simulations.platform

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class SecurityServiceSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "crm"
  /* Name of File to be downloaded */
  val fileNameUidxFileLocation = "uidxlist.txt"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }

  //val uidxFileLocation = System.getProperty("uidxFileLocation", "/Users/11151/Downloads/uidxlist.txt").toString
  val uidxFileLocation = System.getProperty("uidxFileLocation", fileNameUidxFileLocation).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, uidxFileLocation)

  val validateCallPerUser = System.getProperty("validate.call", "3").toInt
  val validateUsers = System.getProperty("users.count", "3").toInt
  val getByIdUsers = validateUsers/2
  val getUsersByUidx = validateCallPerUser*1
  val lines = readFile(filePath)
  LOG.info("getByIdUsers "+getByIdUsers)

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.SECURITY_SERVICE.toString)

  private val securityServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp

  val securityTestArr = Array(5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20, 21).map(e => Map("id" -> e)).random.circular
  val r = scala.util.Random
  private val login = exec(session => session.set("username", r.nextInt(5000)+"@myntra360.com"))
    .exec(http("security login test")
    .post("/myntra-security-service/security/auth/credentials")
    .body(StringBody("""{"loginId":"${username}","password":"${username}"}""")).asJson
    .headers(headers)
    .check(status.is(200))
    .check(jsonPath("$.status.statusCode").is("817"))
    .check(jsonPath("$.token").saveAs("token"))
    )

  private val validateChain = repeat(validateCallPerUser) {
      exec(http("security validate token")
        .get("/myntra-security-service/security/auth/validate")
        .headers(headers)
        .header("token", "${token}")
        .check(status.is(200)))
  }

  private val securityfetchUidxTest = scenario("security fetch by uidx")
    .exec(session => session.set("uidx", lines(r.nextInt(10000))))
    .exec(http("security get by uidx")
      .get("/myntra-security-service/security/v2/user/uidx/{uidx}")
      .headers(headers)
      .check(status.is(200)))


  private val validate = exec(validateChain)

  private val loginAndValidate = Seq(login,validate)

  private val loginTest = scenario("security login test").exec(loginAndValidate)

  private val securityTest = scenario("security find user by id")
    .exec(session => session.set("id", r.nextInt(5000)))
    .exec(http("security get by id")
      .get("/myntra-security-service/security/user/${id}")
      .headers(headers)
      .check(status.is(200)))



  val list = scenarios map (e => e.inject(step).protocols(securityServiceBaseUrl))
  protected var toFilterScenariosList = getProperty("filter.scenarios").split(",") map (e => e.trim.toLowerCase) toList;
  var newList = list;

  if(toFilterScenariosList.contains(securityTest.name.toLowerCase)) {
    securityTest.inject(constantUsersPerSec(getByIdUsers) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized) :: list
  }

  if(toFilterScenariosList.contains(securityfetchUidxTest.name.toLowerCase)) {
    securityfetchUidxTest.inject(constantUsersPerSec(getUsersByUidx) during (DurationJLong(getLongProperty("duration.seconds")) seconds) randomized) :: list
  }

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(securityServiceBaseUrl)
  

}