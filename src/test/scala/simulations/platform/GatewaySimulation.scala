package simulations.platform

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

class GatewaySimulation extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "lat_long.csv"

  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromResource(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }

  //val latLongFile = System.getProperty("lat_long", "test-data/lat_long.csv")
  val latLongFile = System.getProperty("lat_long", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, latLongFile)
  //val csvfeeder = csv(latLongFile).circular
  val csvfeeder = csv(filePath).circular


  val adressSuggestion ="https://api.myntra.com"

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.API.toString)
  val httpProtocol = http.baseUrl(baseUrl).disableWarmUp

  val getReverseGeoCode =
    scenario("getAddressSuggestion")
      .feed(csvfeeder)
      .exec(http("getAddressSuggestion")
        .post("/v1/address-suggestions")
        .body(StringBody("""{"latitude":"${geo_lat}","longitude":"${geo_long}"}""")).asJson
        .headers(headers)
        .check(status.is(200))
      )

  setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)

}
