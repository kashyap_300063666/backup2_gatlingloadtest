package simulations.selections
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class PscLoadTest extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileNameMfb = "PSC_MFB.xlsx"
  val filePathMfb: String = feederUtil.getFileDownloadedPath(containerName, fileNameMfb)
  val fileNameMmb = "PSC_MMB.xlsx"
  val filePathMmb: String = feederUtil.getFileDownloadedPath(containerName, fileNameMmb)

  private val pscBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.JEEVES.toString)).disableWarmUp
  private val pscHeaders = Map("Authorization" -> "Basic YTpi", "Accept" -> "application/json", "content-type"->"multipart/form-data")



  //var path = System.getProperty("user.dir") used only to run on stage
  val r = scala.util.Random

  private val createCommitmentMMB = scenario("createCommitmentMMB")
    .exec(session => session.set("emailId", r.nextInt(5000)+"@myntra360.com"))
    .exec(actionBuilder = http("createCommitmentMMB")
      .post("/myntra-jeeves-service/jeeves/commitment/createNewJob/")
      .headers(pscHeaders)
      .bodyPart(RawFileBodyPart("file" , filePathMmb)).asJson
      .bodyPart(StringBodyPart(
      "name" , "Test")).asJson
      .bodyPart(StringBodyPart(
        "userEmail" , "pratik.suraywanshi@myntra.com")).asJson
      .bodyPart(StringBodyPart(
        "season" , "Spring")).asJson
      .bodyPart(StringBodyPart(
        "seasonYear" , "2020")).asJson
      .bodyPart(StringBodyPart(
        "buyerId" , "3974")).asJson
      .bodyPart(StringBodyPart(
        "businessType" , "MMB")).asJson
      .bodyPart(StringBodyPart(
        "vendorId" , "4068")).asJson
      .bodyPart(StringBodyPart(
        "businessUnitCode" , "TSRT")).asJson
      .bodyPart(StringBodyPart(
        "hit" , "2")).asJson
      .check(status.is(200)))



  private val createCommitmentMFB = scenario("createCommitmentMFB")
    .exec(session => session.set("emailId", r.nextInt(5000)+"@myntra360.com"))
    .exec(actionBuilder = http("createCommitmentMFB")
      .post("/myntra-jeeves-service/jeeves/commitment/createNewJob/")
      .headers(pscHeaders)
      .bodyPart(RawFileBodyPart("file" , filePathMfb)).asJson
      .bodyPart(StringBodyPart(
        "name" , "LoadTest")).asJson
      .bodyPart(StringBodyPart(
        "userEmail" , "${emailId}")).asJson
      .bodyPart(StringBodyPart(
        "season" , "Spring")).asJson
      .bodyPart(StringBodyPart(
        "seasonYear" , "2020")).asJson
      .bodyPart(StringBodyPart(
        "buyerId" , "3974")).asJson
      .bodyPart(StringBodyPart(
        "businessType" , "MFB")).asJson
      .bodyPart(StringBodyPart(
        "hit" , "2")).asJson
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(pscBaseUrl)

}

