package simulations.selections

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class PLMLoadTest extends BaseSimulation {

  private val plmBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.JEEVES.toString)).disableWarmUp
  private val plmHeaders = Map("Authorization" -> "Basic YTpi", "Accept" -> "application/json","content-type"->"multipart/form-data")
  private val pscTranscitonHeaders=Map("Authorization" -> "Basic YTpi", "Accept" -> "application/json","content-type"->"application/json")
  private val autoSkuHeaders=Map("Authorization" -> "Basic YTpi", "Accept" -> "application/json")


  //var path = System.getProperty("user.dir"); used only to run on Stage
        val FileUrlForPLM = System.getProperty("BlobUrlforPLM", "https://scminbound.blob.core.windows.net/scm-ib-psc/dockins/currentPLM.xlsx").toString
         val FileUrlForRP=System.getProperty("BlobUrlforPLM","https://scminbound.blob.core.windows.net/scm-ib-psc/dockins/PLMtoRP.xlsx").toString
    private val createCommitmentPLMtoPSC = scenario("createCommitmentPLM")
    .exec(http("createCommitmentPLM")
      .post("/myntra-jeeves-service/jeeves/plm/generateFromPLM/")
        .headers(plmHeaders)
      .bodyPart(StringBodyPart(
        "fileurl" , FileUrlForPLM)).asJson
      .bodyPart(StringBodyPart(
        "userEmail" , "abhishek.mishra1@myntra.com")).asJson
      .bodyPart(StringBodyPart(
        "buyerId" , "3974")).asJson
      .bodyPart(StringBodyPart(
              "season" , "Spring")).asJson
      .bodyPart(StringBodyPart(
          "seasonYear" , "2019")).asJson
      .bodyPart(StringBodyPart(
          "hit" , "1")).asJson

      .check(status.is(200))
      .check(jsonPath("$.status.statusMessage").is("PLM uploaded successfully."))
      .check(jsonPath("$.status.statusMessage").saveAs("sm"))
      .check(jsonPath("$.data[0].pscJobId").saveAs("pscId"))
      .check(bodyString.saveAs("BODY")))

  .exec( session => {
          val response = session("BODY").as[String]
          println(response)
          val jobIdPsc = session("pscId").as[String]
          println(jobIdPsc)
    session
  })

  private val createCommitmentPLMtoRP = scenario("createCommitmentPLMtoRP")
    .exec(http("createCommitmentPLMtoRP")
      .post("/myntra-jeeves-service/jeeves/plm/generateFromPLM/")
      .headers(plmHeaders)
      .bodyPart(StringBodyPart(
        "fileurl" , FileUrlForRP)).asJson
      .bodyPart(StringBodyPart(
        "userEmail" , "abhishek.mishra1@myntra.com")).asJson
      .bodyPart(StringBodyPart(
        "buyerId" , "3974")).asJson
      .bodyPart(StringBodyPart(
        "season" , "Spring")).asJson
      .bodyPart(StringBodyPart(
        "seasonYear" , "2019")).asJson
      .bodyPart(StringBodyPart(
        "hit" , "1")).asJson

      .check(status.is(200))
      .check(jsonPath("$.status.statusMessage").is("PLM uploaded successfully."))
      .check(jsonPath("$.status.statusMessage").saveAs("sm"))
      .check(jsonPath("$.data[0].pscJobId").saveAs("pscId"))
      .check(bodyString.saveAs("BODY")))

    .exec( session => {
      val response = session("BODY").as[String]
      println(response)
      val jobIdPsc = session("pscId").as[String]
      println(jobIdPsc)
      session
    })

  private val stateTransction=scenario("changeTransitionstate")
    .exec(http("changeTransitionstate")
      .post("/myntra-jeeves-service/jeeves/commitment/transition")
        .headers(pscTranscitonHeaders)
      .body(StringBody(
        """{"commitmentIds":[${pscId}],
           "role":"CATEGORY_MANAGER",
           "commitmentEvent":"SEND_FOR_APPROVAL",
           "postEventStatus":"PLANNER_APPROVED"}""")).asJson
      .check(bodyString.saveAs("BODY"))
      .check(status.is(200)))
        .exec (session =>{
          val response = session("BODY").as[String]
          println("Response body for state Transition: \n$response")
      println(session)
      session
    })


  private val autoSkuCreation=scenario("generateAutoSku")
    .exec(http("genearteAutoSku")
        .post("/myntra-jeeves-service/jeeves/commitment/triggerskucreate/${pscId}")
        .headers(autoSkuHeaders)
      .check(bodyString.saveAs("BODY"))
      .check(status.is(200)))
    .exec ( session =>{
      val response = session("BODY").as[String]
      println("Response body for autoSku: \n$response")
      println(session)
      session
    })


  val scn1=scenario("createPLMtoPSCwithAutoSku").exec(createCommitmentPLMtoPSC).exec(stateTransction).exec(autoSkuCreation)
  val scn2=scenario("createPLMtoRP").exec(createCommitmentPLMtoRP)
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(plmBaseURL)
}


