package simulations.selections

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class ShifuLoadTest extends BaseSimulation{

  private val shifuBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.SHIFU.toString)).disableWarmUp
  private val shifuheaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "application/json")
  private val shifuheadersforupload=Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "multipart/form-data", "Content-Disposition"-> "form-data")
  private val businessUnitNameFeeder =Array("Women's Western Wear","Women's Ethnic","Jewellery","Men's Casual","Personal Care","Men's Formal","Men's LTA","Accessories").map(e=> Map("businessUnitNameFeeder"-> e)).array.circular
  private val brandFeeder = Array(499,513,499,499,499).map(e => Map("brand" -> e)).array.circular
  private val articleTypeFeeder = Array("Waistcoat","Jeans","Kurtis","Shrug","Tops").map(e => Map("articleType" -> e)).array.circular
  private val genderFeeder = Array("Women","Women","Women","Women","Women").map(e => Map("gender" -> e)).array.circular
  private val businessDivisionIdFeeder =Array("3974").map(e=> Map("businessDivisionID"-> e)).array.circular
  private val fetchsizeFeeder=Array(1).map(e=> Map("fetchsize"-> e)).array.circular
  private val startFeeder=Array(0).map(e=> Map("start"-> e)).array.circular
  private val distinctFeeder=Array("false").map(e=> Map("distinctFeeder"-> e)).array.circular

  private val shifuGetPt = scenario("shifuSearch")
    .feed(brandFeeder)
    .feed(articleTypeFeeder)
    .feed(genderFeeder)
    .exec(http("shifuSearch")
      .get("/myntra-shifu-service/shifu/planningtaxonomy/v2/search?q=brand.eq:${brand}___articleType.eq:${articleType}___gender.eq:${gender}")
      .headers(shifuheaders)
      .check(status.is(200)))

  private val ShifuGetBUId= scenario("ShifuBUIdsearch")
    //.feed(businessDivisionIdFeeder)
    .exec(http("ShifuBUIdsearch")
      .get("/myntra-shifu-service/shifu/businessunit/search?q=businessDivisionId.eq:3974")
      .headers(shifuheaders)
      .check(status.is(200)))

  private val ShifuGetBUName=scenario("ShifuBUName")
    .feed(businessUnitNameFeeder)
    .exec(http("ShifuBUName")
      .get("/myntra-shifu-service/shifu/businessunit/search?q=businessUnitName.eq:{businessUnitName}")
      .headers(shifuheaders)
      .check(status.is(200)))

  private val PlanningTaaxonomySearch=scenario("ShifuPTSearchforSomeIndex")
    .exec(http("ShifuPTSearchforSomeIndex")
      .get("/myntra-shifu-service/shifu/planningtaxonomy/search/?fetchSize=20&start=0&distinct=true")
      // .queryParam("fetchsize",{fetchsizeFeeder})
      //.queryParam("start",{startFeeder})
      //.queryParam("distinct",{distinctFeeder})
      .headers(shifuheaders)
      .check(status.is(200)))

  val s3FileUrl = System.getProperty("s3FileUrl", "https://myntrapartnercontent.s3.amazonaws.com/ck3ft03sd1ic51kic32fwf4zm/Shifu-Create.xlsx").toString

  private val PlanningTaxonomyUpload=scenario("UploadPT")
    .exec(http("UploadPT")
      .put("/myntra-shifu-service/shifu/planningtaxonomy/upload")
      .headers(shifuheadersforupload)
        .bodyPart(StringBodyPart(
        "fileurl",s3FileUrl)).asJson
      .check(status.is(200)))

  private val PlanningTaxonomyExportView=scenario("ExportView")
    .exec(http("ExportView")
      .get("/myntra-shifu-service/shifu/planningtaxonomy/download")
      .headers(shifuheaders)
      .check(status.is(200)))


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(shifuBaseURL)

}
