package simulations.selections

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class VendorConfigUpload extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileName = "VendorConfigSeller.xlsx"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
  val fileNameVendorConfigVendor = "VendorConfigVendor.xlsx"
  val filePathVendorConfigVendor: String = feederUtil.getFileDownloadedPath(containerName, fileNameVendorConfigVendor)

  private val ipBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.INVENTORY_PLANNER.toString)).disableWarmUp
  private val ipHeaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "multipart/form-data")
  //var path = System.getProperty("user.dir"); used only to run on Stage

  private val updateVendorConfig = scenario("VendorConfigUploadVendor")
    .exec(actionBuilder = http("updateVendorConfig")
      .post("/inventory-planner-service/inventory-planner/vendor-config/upload/")
      .headers(headers)
      .headers(ipHeaders)
      .formUpload("file", filePathVendorConfigVendor)
      .check(status.is(200)))



  private val updateVendorConfigSeller = scenario("VendorConfigUploadSeller")
    .exec(actionBuilder = http("updateVendorConfig")
      .post("/inventory-planner-service/inventory-planner/vendor-config/upload/")
      .headers(headers)
      .headers(ipHeaders)
      .formUpload("file", filePath)
      .check(status.is(200)))
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(ipBaseURL)

}
