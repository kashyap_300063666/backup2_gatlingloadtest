package simulations.selections
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class DelistingLoadTest extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileName = "Loadtest.txt"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val delistingBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.DELISTING.toString)).disableWarmUp
  private val delistingHeader = Map("Authorization" -> "Basic YTpi", "Accept" -> "application/json", "content-type"->"multipart/form-data")
  val r = scala.util.Random

  private val getPlan = scenario("getPlan")
    .exec(session => session.set("id", (r.nextInt(4000))))
    .exec(http("getPlan")
        .get("/myntra-delisting-service/delisting/plan/${id}")
      .headers(delistingHeader)
      .check(status.is(200)))

  private val getPlanDetails = scenario("getPlanDetails")
    .exec(session => session.set("id", (r.nextInt(4000))))
    .exec(http("getPlanDetails")
      .get("/myntra-delisting-service/delisting/plan-detail/search?q=planId.eq:${id}")
      .headers(delistingHeader)
        .check(status.is(200)))

  private val createPlan = scenario("createPlan")
    .exec(http("createPlan")
      .put("/delisting/plan/centralDelistingUpload/")
      .headers(delistingHeader)
      .bodyPart(RawFileBodyPart("file" , filePath)).asJson
      .bodyPart(StringBodyPart(
        "planType" , "CENTRAL_DELISTING")).asJson
      .bodyPart(StringBodyPart(
        "reason" , "Other")).asJson
      .bodyPart(StringBodyPart(
        "emailMessage" , "LoadTest")).asJson
      .bodyPart(StringBodyPart(
        "seasonYear" , "2020")).asJson
      .bodyPart(StringBodyPart(
        "comment" , "test")).asJson
      .bodyPart(StringBodyPart(
        "businessUnit" , "MyntraTest")).asJson
      .bodyPart(StringBodyPart(
        "reason" , "Other")).asJson
      .check(status.is(200)))
      //.check(jsonPath("$.status.statusMessage").is("Plan Created")))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(delistingBaseUrl)

}
