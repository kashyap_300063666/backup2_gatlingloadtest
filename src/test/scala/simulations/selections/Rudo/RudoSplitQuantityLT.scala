  package simulations.selections.Rudo

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class RudoSplitQuantityLT extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileName = "5SKU.json"
  val csvFile = System.getProperty("jSONFileToPass", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  private val rudoBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.RUDO.toString)).disableWarmUp
  private val rudoHeaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "content-type" -> "application/json")

  private var Feeder = jsonFile(filePath).circular

  private val splitQuantity = scenario("splitQuantity").feed(Feeder)
    .exec(http("splitQuantity")
      .post("/rudo/demand-plan-tasks/split-quantity")
      .headers(rudoHeaders)
      .body(StringBody("${data.jsonStringify()}"))
      .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(rudoBaseURL)


}
