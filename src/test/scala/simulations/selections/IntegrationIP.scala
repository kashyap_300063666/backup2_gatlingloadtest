package simulations.selections

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation




class IntegrationIP extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileNameFbm = "SampleReplenishmentManualSJITFBM.xlsx"
  val filePathFbm: String = feederUtil.getFileDownloadedPath(containerName, fileNameFbm)
  val fileNameNonFbm = "SampleReplenishmentManualSJITNonFBM.xlsx"
  val filePathNonFbm: String = feederUtil.getFileDownloadedPath(containerName, fileNameNonFbm)
  val fileNameVendorConfigVendor = "VendorConfigVendor.xlsx"
  val filePathVendorConfigVendor: String = feederUtil.getFileDownloadedPath(containerName, fileNameVendorConfigVendor)


  private val ipBaseURL=http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.INVENTORY_PLANNER.toString)).disableWarmUp
  private val ipHeaders = Map("authorization" -> "Basic YTpi", "accept" -> "application/json", "Content-Type" -> "multipart/form-data")
  private val ipHeadersForAutoReplenishment=Map("authorization" -> "Basic YTpi", "accept" -> "application/json")

  private val updateVendorConfig = scenario("VendorConfigUploadVendor")
    .exec(actionBuilder = http("updateVendorConfig")
      .post("/inventory-planner-service/inventory-planner/vendor-config/upload/")
      .headers(headers)
      .headers(ipHeaders)
      .formUpload("file", filePathVendorConfigVendor)
      .check(status.is(200)))

  private val uploadManualSampleBAU = scenario("ManualuploadSampleBAU")
    .exec(actionBuilder = http("uploadSampleBAU")
      .post("/inventory-planner-service/inventory-planner/forecast/manual-replenish/BAU/")
      .headers(headers)
      .headers(ipHeaders)
      .formUpload("file", filePathFbm)
      .check(status.is(200)))

  private val updateManualSampleFileHRD = scenario("ManualuploadSampleHRD")
    .exec(actionBuilder = http("updateSampleFileHRD")
      .post("/inventory-planner-service/inventory-planner/forecast/manual-replenish/HRD/")
      .headers(headers)
      .headers(ipHeaders)
      .formUpload("file", filePathNonFbm)
      .check(status.is(200)))

  private val uploadAutoSampleBAU = scenario("AutouploadSampleBAU")
    .exec(actionBuilder = http("AutouploadSampleBAU")
      .get("/inventory-planner-service/inventory-planner/forecast/auto-replenish/BAU/")
      .headers(ipHeadersForAutoReplenishment)
      .check(status.is(200)))

  private val updateAutoSampleFileHRD = scenario("AutouploadSampleHRD")
    .exec(actionBuilder = http("updateSampleHRD")
      .get("/inventory-planner-service/inventory-planner/forecast/auto-replenish/HRD")
      .headers(ipHeadersForAutoReplenishment)
      .check(status.is(200)))

  val scn1=scenario("manualReplenishmentBAU").exec(updateVendorConfig).exec(uploadManualSampleBAU)
  val scn2=scenario("manualReplenishmentHRD").exec(updateVendorConfig).exec(updateManualSampleFileHRD)
  val scn3=scenario("AutoReplenishmentSampleBAU").exec(updateVendorConfig).exec(uploadAutoSampleBAU)
  val scn4=scenario("AutoReplenishmentSampleHRD").exec(updateVendorConfig).exec(updateAutoSampleFileHRD)

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(ipBaseURL)


}
