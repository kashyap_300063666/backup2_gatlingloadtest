package simulations.payments

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class CartPage {

  val cartHeaders = Map(
    "content-type" -> "application/json",
    "User-Agent" -> "MyntraRetailAndroid/4.2004.0 (Phone, 440dpi)",
    "origin" -> "myntra.com"
  )

  object CartApis {
    val addToCart: ChainBuilder = doIf(
      session => session.contains("skuIdsList")
    ) {
      exec(http("Add to Cart")
        .post("/v1/cart/default/add")
        .headers(cartHeaders)
        .header("at", session => session("at").as[String])
        .body(StringBody("""{"id":${styleId},"skuId": ${skuIdsList.random()},"quantity":1}""")).asJson
        .check(status.is(200))
        .check(jsonPath("$.id").saveAs("cartId"))
        .check(jsonPath("$.price.total").saveAs("price"))
      )
    }

    val addToCartForMultipleSku: ChainBuilder = doIf(
      session => session.contains("skuIdsList")
    ) {
      exec(http("Add to Cart")
        .post("/v1/cart/default/add")
        .headers(cartHeaders)
        .header("at", session => session("at").as[String])
        .body(StringBody("""{"id":${styleId},"skuId": ${skuIdsList.random()},"quantity":3}""")).asJson
        .check(status.is(200))
        .check(jsonPath("$.id").saveAs("cartId"))
        .check(jsonPath("$.price.total").saveAs("price"))
      )
    }

  }

}
