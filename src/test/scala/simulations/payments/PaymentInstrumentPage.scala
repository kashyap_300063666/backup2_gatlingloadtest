package simulations.payments

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class PaymentInstrumentPage {

  val paymentsInstrumentHeader = Map(
    "cookie" -> "at=${at}",
    "Origin" -> "myntra.com",
    "authority" -> "pps.myntra.com",
    "user-agent" -> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36"
  )

  object PaymentInstrument {
    val paymentInstument: ChainBuilder = doIf(
      session => session.contains("cartId")
    ) {
      exec(http("Get Payment Instrument")
        .post(session => session("paymentinstrumentBaseUrl").as[String] + "/myntra-payment-plan-service/v3/paymentInstruments")
        .header("x-myntraweb", "Yes")
        .header("x-meta-app", "channel=web")
        .header("client", "checkout")
        .header("content-type", "application/json")
        .header("xmetaapp", "deviceID=2466ef27-755a-4993-8670-07cafe7e9275")
        .header("payment-personalization","true")
        .header("paynow-version","v3")
        .header("saved-instruments","true")
        .header("x-location-context","pincode=560068")
        .headers(paymentsInstrumentHeader)
        .body(StringBody("""{"cartId": "${cartId}", "email": "${emailId}", "mobile":${mobile}}""")).asJson
        .check(status.is(200))
        .check(bodyString.saveAs("paymentInstrumentResponse"))
        .check(jsonPath("$.csrfToken").saveAs("csrfToken"))
        .check(jsonPath("$.formSubmitUrl").saveAs("formSubmitUrl")))
    }
  }
}
