package simulations.payments

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import org.jsoup.Jsoup
import org.jsoup.nodes.{Document, Element}

import scala.collection.mutable

class PaymentsPage {

  val paymentsPageHeader = Map(
    "cookie" -> "at=${at}",
    "Origin" -> "myntra.com",
    "user-agent" -> "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36"
  )

  object Payments {

    val onlinePayNow: ChainBuilder = doIf(
      session => session.contains("csrfToken")
    ) {
      exec(flushCookieJar)
        .exec(http("Paynow")
//          .post(session => session("formSubmitUrl").as[String])
          .post(session => session("ppsBaseUrl").as[String] + "/myntra-payment-plan-service/v2/paynowform")
//          .post("http://10.177.7.26:8080" + "/myntra-payment-plan-service/v2/paynowform")
          .header("Content-Type", "application/x-www-form-urlencoded")
          .headers(paymentsPageHeader)
          .formParamMap(
            Map
            ("address" -> "${addressId}",
              "address-sel" -> "${addressId}",
              "wallet_enabled" -> "false",
              "pm" -> "creditcard",
              "clientCode" -> "responsive",
              "cartContext" -> "DEFAULT",
              "profile" -> "myntra.com",
              "userGroup" -> "normal",
              "csrf_token" -> "${csrfToken}",
              "email" -> "${emailId}",
              "mobile" -> "${mobile}",
              "auto_giftcard_used" -> "false",
              "b_firstname" -> "${name}",
              "b_address" -> "${locality}",
              "b_city" -> "${city}",
              "b_country" -> "${country}",
              "b_state" -> "${state}",
              "b_zipcode" -> "${pincode}",
              "cartId" -> "${cartId}",
              "card_number" -> "5123456789012346",
              "bill_name" -> "${name}",
              "card_month" -> "12",
              "card_year" -> "23",
              "cvv_code" -> "123",
              "user" -> "${uidx}"
            )).disableFollowRedirect
          .check(status.is(200))
          .check(bodyString.saveAs("paynowResponse")))
        .exec(session => {
          session.set("v2", "true")
        })
        .exec(session => {
          print("paynowResponse is :: ")
          println(session("paynowResponse").as[String])
          session
        })
    }

    val codPayNow: ChainBuilder =
      exec(flushCookieJar)
        .exec(http("Paynow")
//          .post(session => session("formSubmitUrl").as[String])
//          .post("http://10.177.7.28:8080" + "/myntra-payment-plan-service/v2/paynowform")
          .post(session => session("ppsBaseUrl").as[String] + "/myntra-payment-plan-service/v2/paynowform")
          .header("Content-Type", "application/x-www-form-urlencoded")
          .headers(paymentsPageHeader)
          .formParamMap(
            Map
            ("address" -> "${addressId}",
              "address-sel" -> "${addressId}",
              "wallet_enabled" -> "false",
              "pm" -> "cod",
              "clientCode" -> "responsive",
              "cartContext" -> "DEFAULT",
              "profile" -> "myntra.com",
              "userGroup" -> "normal",
              "email" -> "${emailId}",
              "mobile" -> "${mobile}",
              "auto_giftcard_used" -> "false",
              "b_firstname" -> "${name}",
              "b_address" -> "${locality}",
              "b_city" -> "${city}",
              "b_country" -> "${country}",
              "b_state" -> "${state}",
              "b_zipcode" -> "${pincode}",
              "cartId" -> "${cartId}",
              "bill_name" -> "${name}",
              "user" -> "${uidx}"
            )).disableFollowRedirect
          .check(status.is(303))
          .check(bodyString.saveAs("paynowResponse")))

    val walletPay: ChainBuilder = doIf(
      session => session.contains("csrfToken")
    ) {
      exec(flushCookieJar)
        .exec(http("Paynow")
//          .post(session => session("formSubmitUrl").as[String])
          .post(session => session("ppsBaseUrl").as[String] + "/myntra-payment-plan-service/v2/paynowform")
          .header("Content-Type", "application/x-www-form-urlencoded")
          .headers(paymentsPageHeader)
          .formParamMap(
            Map
            ("address" -> "${addressId}",
              "address-sel" -> "${addressId}",
              "wallet_enabled" -> "true",
              "pm" -> "wallet",
              "clientCode" -> "responsive",
              "cartContext" -> "DEFAULT",
              "profile" -> "myntra.com",
              "userGroup" -> "normal",
              "email" -> "${emailId}",
              "mobile" -> "${mobile}",
              "auto_giftcard_used" -> "false",
              "csrf_token" -> "${csrfToken}",
              "b_firstname" -> "${name}",
              "b_address" -> "${locality}",
              "b_city" -> "${city}",
              "b_country" -> "${country}",
              "b_state" -> "${state}",
              "b_zipcode" -> "${pincode}",
              "cartId" -> "${cartId}",
              "bill_name" -> "${name}",
              "wallet_mobile" -> "${mobile}",
              "paymentProviderId" -> "4",
              "wallet_amount" -> "${price}",
              "user" -> "${uidx}"
            )).disableFollowRedirect
          .check(status.is(200))
          .check(bodyString.saveAs("paynowResponse")))
    }

    val onlineCallBack: ChainBuilder =
      exec(session => {
        val str = session("paynowResponse").as[String]
        var map = new mutable.HashMap[String, String]()
        if (session.contains("v2")) {
          map = extractCardInfoDetails(str)
        } else {
          map = session("callBackParams").as[mutable.HashMap[String,String]]
        }
        session.set("amount", map("vpc_Amount")).set("orderId", map("vpc_orderInfo")).set("returnUrl", map("vpc_ReturnURL"))
      })
        .exec(flushCookieJar)
        .exec(http("Callback")
          .post(session => session("returnUrl").as[String])
          .header("Origin", "myntra.com")
          .header("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36")
          .header("Content-Type", "application/x-www-form-urlencoded")
          .headers(paymentsPageHeader)
          .formParamMap(
            Map
            ("vpc_Amount" -> "${amount}",
              "vpc_MerchTxnRef" -> "${orderId}",
              "vpc_OrderInfo" -> "${orderId}",
              "vpc_ReturnURL" -> "${returnUrl}"
            )).disableFollowRedirect
          .check(status.is(303))
          .check(headerRegex("location", "transaction_status=(.*)&errorCode=(.*)#payment").ofType[(String, String)])
          .check(header("location").saveAs("result")))

    val walletCallBack: ChainBuilder =
      exec(session => {
        val str = session("paynowResponse").as[String]
        println("response is : " + str)
        session.set("returnUrl", extractWalletCallbackUrl(str))
      })
        .exec(flushCookieJar)
        .exec(http("Callback")
          .post(session => session("returnUrl").as[String])
          .header("Origin", "myntra.com")
          .header("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36")
          .header("Content-Type", "application/x-www-form-urlencoded")
          .headers(paymentsPageHeader)
          .disableFollowRedirect
          .check(status.is(303))
          .check(headerRegex("location", "transaction_status=(.*)&errorCode=(.*)#payment").ofType[(String, String)])
          .check(header("location").saveAs("result")))

    val v3PayNowCod: ChainBuilder = doIf(
      session => session.contains("cartId")
    ) {
      exec(flushCookieJar)
        .exec(http("v3 Paynow")
//          .post(session => session("formSubmitUrl").as[String].replaceAllLiterally("/v2/paynowform", "/v3/paynow"))
          .post(session => session("ppsBaseUrl").as[String] + "/myntra-payment-plan-service/v3/paynow")
          .header("accept", "application/json")
          .header("Content-Type", "application/json")
          .header("X-Forwarded-For", "10.10.10.255")
          .headers(paymentsPageHeader)
          .body(StringBody(
            """{
            "addressId": "${addressId}",
            "clientContext": "responsive",
            "cartContext": "DEFAULT",
            "paymentMethods": "cod",
            "profile": "myntra.com",
            "cartId": "${cartId}",
            "email": "${emailId}",
            "mobile": "${mobile}",
            "autoGiftCardUsed": "false",
            "channel": "desktop",
            "baddress": "${locality}",
            "bcity": "${city}",
            "bcountry": "${country}",
            "bfirstname": ${name},
            "bstate": "${state}",
            "bzipcode": "${pincode}",
            "xmetaApp": "deviceID=6b0f3c60-3201-4336-9a53-ab5f851004f1"
              }""")).asJson
          .check(status.is(200))
          .check(jsonPath("$.params.orderid").exists)
          //            .check(jsonPath("$.templateCode").find.is("104"))
          .check(bodyString.saveAs("paynowResponse"))
        ).exec(session => {
        println("###### paynow response ######" + session("paynowResponse"))
        session
      })
    }

    val v3PayNowOnline: ChainBuilder = doIf(
      session => session.contains("csrfToken")
    ) {
      exec(flushCookieJar)
        .exec(http("v3 Paynow Online")
//          .post(session => session("formSubmitUrl").as[String].replaceAllLiterally("/v2/paynowform", "/v3/paynow"))
          .post(session => session("ppsBaseUrl").as[String] + "/myntra-payment-plan-service/v3/paynow")
          .header("accept", "application/json")
          .header("Content-Type", "application/json")
          .header("X-Forwarded-For", "10.10.10.255")
          .headers(paymentsPageHeader)
          .body(StringBody(
            """{
            "addressId": "${addressId}",
            "clientContext": "responsive",
            "cartContext": "DEFAULT",
            "paymentMethods": "creditcard",
            "csrf": "${csrfToken}"
            "profile": "myntra.com",
            "cartId": "${cartId}",
            "cardNumber": "5123450484854177",
            "billName": "${name}",
            "cardMonth": "05",
            "cardYear": "2024",
            "cvvCode": "123",
            "email": "${emailId}",
            "mobile": "${mobile}",
            "autoGiftCardUsed": "false",
            "channel": "desktop",
            "baddress": "${locality}",
            "bcity": "${city}",
            "bcountry": "${country}",
            "bfirstname": "${name}",
            "bstate": "${state}",
            "bzipcode": "${pincode}",
            "xmetaApp": "deviceID=6b0f3c60-3201-4336-9a53-ab5f851004f1"
		        }""")).asJson
          .check(status.is(200))
          .check(bodyString.saveAs("paynowResponse"))
          .check(jsonPath("$.params").saveAs("callBackParams"))
        ).exec(session => {
        println("###### paynow response ######" + session("paynowResponse"))
        session
      })
    }

    val v3PayNowWallet: ChainBuilder = doIf(
      session => session.contains("csrfToken")
    ) {
      exec(flushCookieJar)
        .exec(http("v3 Paynow Wallet")
          .post(session => session("formSubmitUrl").as[String].replaceAllLiterally("/v2/paynowform", "/v3/paynow"))
          .header("accept", "application/json")
          .header("X-Forwarded-For", "10.10.10.255")
          .headers(paymentsPageHeader)
          .body(StringBody(
            """{
            "addressId": "${addressId}",
            "clientContext": "responsive",
            "cartContext": "DEFAULT",
            "paymentMethods": "wallet",
            "csrf": "${csrfToken}",
            "profile": "myntra.com",
            "cartId": "${cartId}",
            "email": "${emailId}",
            "mobile": "${mobile}",
            "autoGiftCardUsed": "false",
            "walletMobile": "${mobile}",
            "walletEnabled": "true",
            "channel": "desktop",
            "baddress": "${locality}",
            "bcity": "${city}",
            "bcountry": "${country}",
            "bfirstname": "${name}",
            "bstate": "${state}",
            "bzipcode": "${pincode}",
            "xmetaApp": "deviceID=6b0f3c60-3201-4336-9a53-ab5f851004f1",
            "paymentProviderId": "4"
              }""")).asJson
          .check(status.is(200))
          .check(bodyString.saveAs("paynowResponse"))
        ).exec(session => {
        println("###### paynow response ######" + session("paynowResponse"))
        session
      })
    }


    private def extractCardInfoDetails(responseN: String): mutable.HashMap[String, String] = {
      val doc: Document = Jsoup.parseBodyFragment(responseN)
      val body: Element = doc.body
      val amount: Element = body.select("input[name=vpc_Amount]").first
      val vpc_Amount: String = amount.attr("value")
      val info: Element = body.select("input[name=vpc_OrderInfo]").first
      val vpc_orderInfo: String = info.attr("value")
      val url: Element = body.select("input[name=vpc_ReturnURL]").first
      val vpc_ReturnURL: String = url.attr("value")
      val reference: Element = body.select("input[name=vpc_MerchTxnRef]").first
      val vpc_MerchTxnRef: String = reference.attr("value")
      cardInforFormParam(vpc_Amount, vpc_orderInfo, vpc_ReturnURL, vpc_MerchTxnRef)
    }

    private def extractWalletCallbackUrl(response: String) = {
      val doc = Jsoup.parseBodyFragment(response)
      val body = doc.body
      val data = body.select("form[name=subFrm]").first
      println("callback url is " + data.attr("action"))
      val url = data.attr("action")
      if (url.contains("http:"))
        url.replaceAllLiterally("http:", "https:")
    }

    private def cardInforFormParam(vpc_Amount: String, vpc_orderInfo: String, vpc_ReturnURL: String, vpc_MerchTxnRef: String): mutable.HashMap[String, String] = {
      mutable.HashMap("vpc_Amount" -> vpc_Amount, "vpc_orderInfo" -> vpc_orderInfo, "vpc_ReturnURL" -> vpc_ReturnURL)
    }
  }

}
