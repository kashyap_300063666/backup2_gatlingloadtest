package simulations.mlp
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Bboxgpumlp extends BaseSimulation {

	private val feederUtil = new FeederUtil()
	/* Name of the team Name */
	val containerName = "datascience"
	/* Name of File to be downloaded */
	val fileName = "bboxonlinegpu_urls.csv"

val httpConf = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.BBOXGPUMLP.toString)).disableWarmUp


//val csvFile = "test-data/bboxonlinegpu_urls.csv"
val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
val csvfeeder = csv(filePath).circular

val bbox = scenario(scenarioName =  "bbox feeder")
                .feed(csvfeeder)
		.exec(http(requestName = "all requests")
		.post("")
		.body(StringBody("""{"data": {"imageurl":"${default_image}"}}""")).asJson
		.check(status.is(200))
	)


setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpConf)
}