package simulations.mlp

import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Ltrfeeder extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "platform"
  /* Name of File to be downloaded */
  val fileName = "ltr_feeder_loads.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  val httpConf = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.LTR.toString)).disableWarmUp
  //val csvFile = System.getProperty("addressUidxFile", "test-data/ltr_feeder_loads.csv")
  //val csvFile = "test-data/ltr_feeder_loads.csv"
  //val csvfeeder = csv(csvFile).circular
  val customSeparatorFeeder = separatedValues(filePath, '#') // use your own separator ex - #,$,@ etc

//  val customSeparatorFeeder = separatedValues(csvFile, '#').convert {
//    case ("posts", string) => string.toList
//  }


  val req = scenario("ltr feeder")
    .feed(customSeparatorFeeder)
    .exec(http("all requests")
      .post("/ltr/predict")
      .body(StringBody("""{"data":{"uidx":"${uidx}","posts":${posts}}}""")).asJson
      .check(status.is(200))
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpConf)
}
