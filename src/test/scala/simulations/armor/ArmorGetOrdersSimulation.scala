package simulations.armor

import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import simulations.BaseSimulation

class ArmorGetOrdersSimulation extends BaseSimulation {

	private val feederUtil = new FeederUtil()
	/* Name of the team Name */
	val containerName = "oms"
	/* Name of File to be downloaded */
	val fileName = "armorUidx.csv"

	val httpConf: HttpProtocolBuilder = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.ARMOR.toString)).disableWarmUp
	//val csvFile: String = System.getProperty("uatuStoreOrderIds", "test-data/armorUidx.csv")
	val csvFile: String = System.getProperty("uatuStoreOrderIds", fileName)
	val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

	val uidxFeeder: BatchableFeederBuilder[String]#F = csv(filePath).circular

	val armorLT: ScenarioBuilder = scenario("armorLT")
  	.feed(uidxFeeder)
		.exec(http("armor")
		.get("/myntra-armor-service/armor/owner/2297/buyer/2297/storeOrder/getFullyEnrichedOrders")
		.header("accept", "application/json")
		.header("authorization", "Basic ZGQ6ZnNlZnJn")
		.header("content-type", "application/json")
		.header("x-myntra-client-id", "scmloadtest")
		.headers(headers)
		.queryParam("q","login.eq:"+"${login}")
		.queryParam("enrichCharges","true")
		.queryParam("enrichPaymentInstruments","false")
		.check(status.is(200))
		.check(jsonPath("$.status.statusType").is("SUCCESS"))
		.check(jsonPath("$.data[0].login").is("${login}")))


	setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec).protocols(httpConf)
}