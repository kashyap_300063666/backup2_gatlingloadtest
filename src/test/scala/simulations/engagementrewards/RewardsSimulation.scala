package simulations.engagementrewards

import com.github.phisgr.gatling.grpc.Predef.grpc
import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import com.myntra.engagement.currency.grpc.common.PageRequest
import com.myntra.engagement.reward.grpc.query._
import com.myntra.engagement.reward.grpc.redeem.{RedeemRewardRequest, RedeemServiceGrpc}
import io.gatling.core.Predef.{scenario, _}
import io.grpc.ManagedChannelBuilder
import simulations.BaseSimulation

class RewardsSimulation extends BaseSimulation {
  private val url = System.getProperty("url", BaseUrlConstructor.getBaseUrl(ServiceType.ENGAGEMENTREWARD.toString, ""))

  val grpcConf = grpc(ManagedChannelBuilder.forAddress(url, grpcPortNum).usePlaintext())
  private val uidx = System.getProperty("uidx", "Hello")
  private val tenant = System.getProperty("tenant", "2").toInt
  private val source = System.getProperty("source", "4").toInt
  private val rewardId = System.getProperty("rewardid", "1").toInt
  private val pageNum = System.getProperty("pagenum", "1").toInt
  private val pageSize = System.getProperty("pagesize", "10").toInt

  val redeemedRewardsRes = scenario("get_redeemed_rewards")
    .exec(grpc("GetRedeemedRewards")
      .rpc(QueryServiceGrpc.METHOD_GET_REDEEMED_REWARDS)
      .payload(
        GetRedeemedRewardsRequest(
          tenantId = tenant,
          uidx = uidx,
          pageRequest = Some(PageRequest(pageNum = pageNum, pageSize = pageSize))
        )
      )
    )

  val rewardDetails = scenario("get_reward_details")
    .exec(grpc("GetRewardDetails")
      .rpc(QueryServiceGrpc.METHOD_GET_REWARD_DETAILS)
      .payload(
        GetRewardDetailsRequest(
          tenantId = tenant,
          uidx = uidx,
          rewardId = rewardId,
          criteria = Criteria.WITH_UIDX
        )
      )
    )

  val userRewardsRes = scenario("get_user_rewards")
    .exec(grpc("GetUserRewards")
      .rpc(QueryServiceGrpc.METHOD_GET_USER_REWARDS)
      .payload(
        GetUserRewardsRequest(
          tenantId = tenant,
          uidx = uidx,
          capped = Capped.CAPPED_INFO_REQUIRED,
          criteria = Criteria.WITH_UIDX,
          pageRequest = Some(PageRequest(pageNum = pageNum, pageSize = pageSize))
        )
      )
    )

  val redeemRewardRes = scenario("redeem_reward")
    .exec(grpc("RedeemReward")
      .rpc(RedeemServiceGrpc.METHOD_REDEEM_REWARD)
      .payload(
        RedeemRewardRequest(
          tenantId = tenant,
          uidx = uidx,
          rewardId = rewardId
        )
      )
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(grpcConf)
}
