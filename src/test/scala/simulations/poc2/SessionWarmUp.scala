package simulations.poc2

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, HLTUtility}
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation
import simulations.poc2.pages.{CartPage, Login}

class SessionWarmUp extends BaseSimulation {
  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.API.toString)
  private val apiServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp
  private val counterKeyName = System.getProperty("counter.key", "counter")
  private val waitTimeToSetCounterVal = System.getProperty("wait.time", "5").toInt
  private val startUserCount = System.getProperty("startUserCount", "0")
  val util = new HLTUtility()

  before {
    util.redisUtil.set(counterKeyName, startUserCount)
  }

  /* Set all the APIs statuses in redis (clearCart should be enabled) */
  util.setAPIStatuses()

  private val cartCalls = new CartPage().CartApis
  private val loginApis = new Login().LoginAPIs


  /* Create SessionWarmUp(with ClearCart) Scenario */
  val sessionWarmUpWithClearCart: ScenarioBuilder = scenario("with ClearCart")
    .exec(session => {
      session.set("emailId", util.redisUtil.increamentCounter(counterKeyName) + "@myntra360.com")
    })
    .exec(
      loginApis.tokenApi,
      loginApis.loginApi,
      cartCalls.clearCart
    )

  /* Create SessionWarmUp(without ClearCart) Scenario */
  val sessionWarmUpWithoutClearCart: ScenarioBuilder = scenario("without ClearCart")
    .exec(session => {
      session.set("emailId", util.redisUtil.increamentCounter(counterKeyName) + "@myntra360.com")
    })
    .exec(
      loginApis.tokenApi,
      loginApis.loginApi
    )

  setUp(
    scenarios map (
      e => e.inject(
        nothingFor(waitTimeToSetCounterVal),
        constantUsersPerSec(noOfUsers) during (DurationJLong(getLongProperty("duration.seconds")) seconds)
      )
      )
  ).maxDuration(maxDurationInSec).protocols(apiServiceBaseUrl)
}
