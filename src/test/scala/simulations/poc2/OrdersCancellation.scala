package simulations.poc2

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import com.myntra.hlt.HLTOrdersUtil
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation
import simulations.poc2.pages.Orders

class OrdersCancellation extends BaseSimulation {
  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.OMS.toString)
  private val omsServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp

  private val orderUtil: HLTOrdersUtil = new HLTOrdersUtil()

  private val orderAPIs = new Orders().orderAPIs


  val cancelOrderScenario: ScenarioBuilder = scenario("CancelOrder")
    .exec(session => {
      val order: String = orderUtil.getCODOrder()
      if (order == None.toString) {
        Thread.sleep(5000)                        // If there are no new COD Orders then wait for 5 seconds
        session
      } else {
        val orderArr = order.split(":")
        session.set("uidx", orderArr.apply(0)).set("orderId", orderArr.apply(1))
      }
    })
    .exec(orderAPIs.cancelOrder)

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(omsServiceBaseUrl)
}
