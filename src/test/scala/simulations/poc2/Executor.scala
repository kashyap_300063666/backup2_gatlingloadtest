//package simulations.poc2
//
//import com.myntra.commons.ServiceType
//import com.myntra.commons.util.{BaseUrlConstructor, HLTUtility}
//import com.myntra.hltScenarioParams
//import com.myntra.hlt._
//import io.gatling.core.Predef._
//import io.gatling.http.Predef._
//import simulations.BaseSimulation
//
//class Executor extends BaseSimulation {
//  /* Utility class for HLT */
//  protected val util = new HLTUtility()
//
//  /* Scenario Injection Steps Builder */
//  private val injectSteps = new InjectionSteps()
//
//  /* Initializing local variables */
//  private val csBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.API.toString)).disableWarmUp
//
//  /* Download all prod StyleIds to a file */
//  //util.downloadFile("styles.csv")
//
//  /* Set all the APIs as enabled by default */
//  util.setAPIStatuses()
//
//  /* Initialize all scenarios objects */
//  val allScenarios = new Scenarios()
//
//  /* Add required load settings for each scenario */
//  setUp(
//    /* Landing Page */
//    allScenarios.landingPage.inject(
//      injectSteps.getSteps(hltScenarioParams.landingPage)
//    ),
//    /* Recommended API */
//    allScenarios.landingPageRecommended.inject(
//      injectSteps.getSteps(hltScenarioParams.landingPageRecommended)
//    ),
//    
//    /* Cart APIs */
////    allScenarios.addToCartAPI.inject(
////      injectSteps.getSteps(hltScenarioParams.addToCartAPI)
////    ),
//    allScenarios.cartFillerAPI.inject(
//      injectSteps.getSteps(hltScenarioParams.cartFillerAPI)
//    ),
////    allScenarios.cartCountDefaultAPI.inject(
////      injectSteps.getSteps(hltScenarioParams.cartCountDefaultAPI)
////    ),
//    allScenarios.applyCouponAPI.inject(
//      injectSteps.getSteps(hltScenarioParams.applyCouponAPI)
//    ),
//    allScenarios.checkoutCartNodeAPI.inject(
//      injectSteps.getSteps(hltScenarioParams.checkoutCartNodeAPI)
//    ),
////    allScenarios.removefromcartAPI.inject(
////      injectSteps.getSteps(hltScenarioParams.removefromcartAPI)
////    ),
//    
//    
//     /* Address APIs */
//      allScenarios.addressAPI.inject(
//      injectSteps.getSteps(hltScenarioParams.addressAPI)
//    ),
//      allScenarios.addressPincodeAPI.inject(
//      injectSteps.getSteps(hltScenarioParams.addressPincodeAPI)
//    ),
//    
//    
//    /* PDP Style data APIs */
//    allScenarios.pdpStyleData.inject(
//      injectSteps.getSteps(hltScenarioParams.pdpStyleData)
//    ),
//    /* All PdpStyle APIs */
//    allScenarios.pdpPageAllCalls.inject(
//      injectSteps.getSteps(hltScenarioParams.pdpPageAllCalls)
//    ),
//    /* All Search APIs */
//    allScenarios.searchAllCalls.inject(
//      injectSteps.getSteps(hltScenarioParams.searchAllCalls)
//    ),
//
//    
//    
//    
//    
//    /* Browse Flow 01 */
//    allScenarios.browseFlow01.inject(
//    injectSteps.getSteps(hltScenarioParams.browseFlow01)
//    ),
//    /* Browse Flow 02 */
//    allScenarios.browseFlow02.inject(
//      injectSteps.getSteps(hltScenarioParams.browseFlow02)
//    ),
//    /* Browse Flow 03 */
//    allScenarios.browseFlow03.inject(
//      injectSteps.getSteps(hltScenarioParams.browseFlow03)
//    ),
//    /* CODPaymentsFlow */
//    allScenarios.codPaymentsFlow.inject(
//      injectSteps.getSteps(hltScenarioParams.codPaymentsFlow)
//    ),
//    /* walletPaymentsFlow */
//    allScenarios.walletPaymentsFlow.inject(
//      injectSteps.getSteps(hltScenarioParams.walletPaymentsFlow)
//    )
//  ).protocols(csBaseUrl)
//}
