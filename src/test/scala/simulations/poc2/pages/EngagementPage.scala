package simulations.poc2.pages

import com.myntra.commons.util.HLTUtility
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class EngagementPage {
  
  val util: HLTUtility = new HLTUtility()
  
  object EngagementApis
  {
    val EngagementAccountSummary: ChainBuilder = doIf(
      session => util.getAPIStatus("EngagementPage.EngagementAccountSummary")) {
        exec(http("EngagementPage : EngagementAccountSummary")
          .get("/v1/engagement/currency/accountsummary?tenantid=2")
          .header("Content-Type", "application/json")
          .header("at", "${at}")
          .check(status.is(200))
          )
      }
    
    val EngagementRewards: ChainBuilder = doIf(
      session => util.getAPIStatus("EngagementPage.EngagementRewards")) {
        exec(http("EngagementPage : EngagementRewards")
          .get("/v1/engagement/rewards/userrewards?tenantid=2&pagenum=0&pagesize=10")
          .header("Content-Type", "application/json")
          .header("at", "${at}")
          .check(status.is(200))
          )
    }
  }
}