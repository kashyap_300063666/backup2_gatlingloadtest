package simulations.poc2.pages

import com.myntra.commons.util.HLTUtility
import io.gatling.commons.validation.Failure
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

class PdpPage {
  val util: HLTUtility = new HLTUtility()

  val pdpHeaders = Map(
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "content-type"-> "application/json",
    "at"-> "${at}",
    
  )

  object StyleDetails {

    /* Style Details */
    val stylePdp: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.stylePdp")
    ){
      exec(http("Get Style Details")
        .get("/product/${styleId}?co=1")
        .headers(pdpHeaders)
        .check(status.is(200))
        .check(jsonPath("$..sizes[?(@.inventory >= 1)].skuId").find(0).optional.saveAs("skuiId"))
        .check(jsonPath("$..sizes[?(@.inventory >= 1)].skuId").find(0).optional.saveAs("sizeRecoSkus"))
        .check(jsonPath("$..sizes[0].supplyType").find(0).optional.saveAs("supplyType"))
        .check(jsonPath("$..sizes[?(@.sellableInventoryCount >= 1)].seller").find(0).optional.saveAs("sellerName"))
        .check(jsonPath("$.cards[?(@.type == 'PRODUCT')].components[?(@.type == 'INFO')].props.price.discounted").find.optional.saveAs("discountedPrice"))
        .check(jsonPath("$.cards[?(@.type == 'PRODUCT')].components[?(@.type == 'INFO')].props.price.mrp").find.optional.saveAs("mrp"))
        .check(jsonPath("$.info.gender").find.optional.saveAs("gender"))
        .check(jsonPath("$.info.articleType").find.optional.saveAs("articleType"))
        .check(jsonPath("$.info.brand").find.optional.saveAs("brand"))
        .check(jsonPath("$.cards[?(@.type == 'PRODUCT')].components[?(@.type == 'IMAGE_SWIPE')].props.shoppableLooks.data[0].type").find.optional.saveAs("touchPointType"))
        .check(jsonPath("$.cards[?(@.type == 'PRODUCT')].components[?(@.type == 'IMAGE_SWIPE')].props.shoppableLooks.data[0].xcoordinate").find.optional.saveAs("tpPrimary"))
        .check(jsonPath("$.cards[?(@.type == 'PRODUCT')].components[?(@.type == 'IMAGE_SWIPE')].props.shoppableLooks.data[0].ycoordinate").find.optional.saveAs("tpSecondary"))
      )
      .exec(session=>{
        /* Set "price" by selecting from discountedPrice and mrp */
        if (session("discountedPrice").validate[Int].isInstanceOf[Failure]) {
          session.set("price", session("mrp").as[Int])
        } else {
          session.set("price", session("discountedPrice").as[Int])
        }
      })
    }

    val stylePdpV2: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.stylePdpV2")
    ){
      exec(http("StylePDP v2")
        .get("/v2/product/${styleId}")
        .headers(pdpHeaders)
        .check(status.is(200))
        .check(jsonPath("$..sizes[?(@.sizeSellerData[?(@.sellableInventoryCount >= 1)])].skuId").find(0).optional.saveAs("skuiId"))
        .check(jsonPath("$..sizes[?(@.sizeSellerData[?(@.sellableInventoryCount >= 1)])].skuId").find(0).optional.saveAs("sizeRecoSkus"))
        .check(jsonPath("$..sizeSellerData[0].supplyType").find(0).optional.saveAs("supplyType"))
        .check(jsonPath("$.style.sellers[0].sellerName").find(0).optional.saveAs("sellerName"))
        .check(jsonPath("$.style.sizes[*].sizeSellerData[0].discountedPrice").find(0).optional.saveAs("discountedPrice"))
        .check(jsonPath("$.style.sizes[*].sizeSellerData[0].mrp").find(0).optional.saveAs("mrp"))
        .check(jsonPath("$.style.analytics.gender").find.optional.saveAs("gender"))
        .check(jsonPath("$.style.analytics.articleType").find.optional.saveAs("articleType"))
        .check(jsonPath("$.style.analytics.brand").find.optional.saveAs("brand"))
        .check(jsonPath("$.style.buyButtonSellerOrder[0].sellerPartnerId").find.optional.saveAs("sellerPartnerId"))
        .check(jsonPath("$.style.shoppableLooks.data[0].type").find.optional.saveAs("touchPointType"))
        .check(jsonPath("$.style.shoppableLooks.data[0].xcoordinate").find.optional.saveAs("tpPrimary"))
        .check(jsonPath("$.style.shoppableLooks.data[0].ycoordinate").find.optional.saveAs("tpSecondary"))
      )
      .exec(session=>{
        /* Set "price" by selecting from discountedPrice and mrp */
        if (session("discountedPrice").validate[Int].isInstanceOf[Failure]) {
          session.set("price", session("mrp").as[Int])
        } else {
          session.set("price", session("discountedPrice").as[Int])
        }
      })
    }

    val stylePdpLego: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.stylePdpLego")
    ){
      exec(http("StylePDPLego")
        .get("/layout/v2/product/${styleId}")
        .headers(pdpHeaders)
        .header("x-meta-abtest", "pdp.content-revamp=enabled;reviews.lazyload=enabled;AA.Cart.Android= VariantB,AA.Home.Android= VariantB,AA.List.Android= VariantA,AA.PDP.Android= VariantA,CategoryStoreGrid= VariantA,LTR_GS= LTR,MSBSwap= VariantB,Myorder.insider= enabled,PDP.disclaimer= both,	Store_Experiment_50_50= VariantA,address.v2= enabled,android.helpshift= enabled,android.looksoms= flatshots,autosuggest_brandurl= true,bag.newpaymentoffers= enabled,bucketise.imageDimensions.params= enabled,bucketise.quality.params= enabled,cart.credit= enabled,cart.fsexp= VariantB,cart.newusers= enabled,cart.returnpolicy= oncard,cart.sort= onlygroup,catalog.images.image-count= two_image_bucket,catalog.images.image-position= default_bucket,catalog.images.model_vs_micro_shot= modelshot,catalog.images.moodshot-products-new= no-mood-shot,catalog.images.relative-size-image= with_relative,checkout.odometer= enabled,checkoutux.revamp= enabled,config.bucket= regular,confirmation.redesign= modular,enable.youcam= enabled,enrolledInsider= enabled,hideInlineFilters= enabled,home-bif= VariantB,ios.profile.fab= default,itemDetails.crossSell= enabled,lgp.adsrank.geo.device.jfm= geo_device,lgp.rollout= enabled,lgp.timeline.cardloadevent= enabled,list.brandhighlight= enabled,list.bundlefilter= enabled,list.interactive.variant= default,list.personal.care= enabled,map.challenge= enabled,mymyntra.armor= enabled,nav.links= store,nav.store= enabled,onlyXLeft= enabled,payment.failure= enabled,pdp.CoreValues= enabled,pdp.android.react= enabled,pdp.android.react.v1= enabled,pdp.animated.images= enabled,pdp.annotated.images= enabled,pdp.compare= show,pdp.crosssell= personalised,pdp.details= table,pdp.forum= enabled,pdp.notifyMe= enabled,pdp.productMatch= enabled,pdp.react.colorselection= enabled,pdp.react.cross-sell= enabled,pdp.shoppable.looks= enabled,pdp.similar.experiment= variant1,pdp.similarCard= show,pdp.tellreco= enabled,pdp.video= enabled,pdp.viewSimilarMiniPDP= enabled,pdp.wishlistcount= enabled,pdpRemoveStyleNote= enabled,pla_ab= plaenabled,plp.errorPage= enabled,pps= enabled,priceFilterBarChart= enabled,product.raters.listview= enabled,product.ratings= enabled,product.ratings.feedcard= enabled,product.ugcfashion= enabledugc,productclusters= showbottomoptions,profile.map= enabled,rn.update= internal,rollouts.patch.enabled= enabled,search.additionalInfo= test,search.filter.personalised= true,search.sampling= enabled,search.visual= enabled,searchASNoCharLimit= enabled,searchGenderNudge= enabled,shortlist.list.click= default,sizereco.cl= psr-old,sizereco.ps0footwear= enabled,tfbpv2= enabled,visualFilters= enabled,wallet= enabled,wishlist.pricedrop= enabled,wishlist.xcelerator= enabled,wl.sizereco= enabled;")
        .check(status.is(200))
        .check(jsonPath("$..skuData[?(@.sellersData[?(@.sellableInventoryCount >= 1)])].skuId").find(0).optional.saveAs("skuiId"))
        .check(jsonPath("$..skuData[?(@.sellersData[?(@.sellableInventoryCount >= 1)])].skuId").find(0).optional.saveAs("sizeRecoSkus"))
        .check(jsonPath("$..sellersData[0].supplyType").find(0).optional.saveAs("supplyType"))
        .check(jsonPath("$..sellerName").find(0).optional.saveAs("sellerName"))
        .check(jsonPath("$.sellerContext.skuData[*].sellersData[0].discountedPrice").find(0).optional.saveAs("discountedPrice"))
        .check(jsonPath("$.sellerContext.skuData[*].sellersData[0].mrp").find(0).optional.saveAs("mrp"))
        .check(jsonPath("$.info.gender").find.optional.saveAs("gender"))
        .check(jsonPath("$.info.articleType").find.optional.saveAs("articleType"))
        .check(jsonPath("$.info.brand").find.optional.saveAs("brand"))
        .check(jsonPath("$.sellerContext.buyButtonSellerOrder[0].sellerPartnerId").find.optional.saveAs("sellerPartnerId"))
        .check(jsonPath("$.cards[?(@.type == 'PRODUCT')].components[?(@.type == 'IMAGE_SWIPE')].props.shoppableLooks.data[0].type").find.optional.saveAs("touchPointType"))
        .check(jsonPath("$.cards[?(@.type == 'PRODUCT')].components[?(@.type == 'IMAGE_SWIPE')].props.shoppableLooks.data[0].xcoordinate").find.optional.saveAs("tpPrimary"))
        .check(jsonPath("$.cards[?(@.type == 'PRODUCT')].components[?(@.type == 'IMAGE_SWIPE')].props.shoppableLooks.data[0].ycoordinate").find.optional.saveAs("tpSecondary"))
      )
      .exec(session=>{
        /* Set "price" by selecting from discountedPrice and mrp */
        if (session("discountedPrice").validate[Int].isInstanceOf[Failure]) {
          session.set("price", session("mrp").as[Int])
        } else {
          session.set("price", session("discountedPrice").as[Int])
        }
      })
    }

    /* Style Offer Details */
    val clickForOffer: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.clickForOffer")
    ){
      exec(http("Get Style Offers")
        .get("/product/${styleId}/offers?co=1")
        .headers(pdpHeaders)
        .check(status.is(200)))
    }

    val clickForOfferV2: ChainBuilder = doIf(
      session => util.getAPIStatus("PdpPage.clickForOfferV2") && session.contains("sellerPartnerId")
    ){
      exec(http("Style Offers v2")
        .get("/v2/product/${styleId}/offers/${sellerPartnerId}")
        .headers(pdpHeaders)
        .check(status.is(200)))
    }

    val clickForOfferLego: ChainBuilder = doIf(
      session => util.getAPIStatus("PdpPage.clickForOfferLego") && session.contains("sellerPartnerId")
    ){
      exec(http("Style Offers Lego")
        .get("/layout/product/${styleId}/offers/${sellerPartnerId}")
        .headers(pdpHeaders)
        .check(status.is(200)))
    }

    /* Related Styles */
    val similarStyles: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.similarStyles")
    ){
      exec(http("Get Similar Styles")
        .get("/product/${styleId}/related?co=1&colors=false")
        .headers(pdpHeaders)
        .check(status.is(200)))
    }

    val similarStylesV2: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.similarStylesV2")
    ){
      exec(http("Style Similar v2")
        .get("/v2/product/${styleId}/related")
        .headers(pdpHeaders)
        .check(status.is(200)))
    }

    val similarStylesLego: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.similarStylesLego")
    ){
      exec(http("Style Similar Lego")
        .get("/layout/product/${styleId}/recommendation/related")
        .queryParam("co","1")
        .queryParam("colors","false")
        .headers(pdpHeaders)
        .check(status.is(200)))
    }

    /* Cross Sell */
    val crossSell: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.crossSell")
    ){
      exec(http("Style Cross-Sell")
        .get("/product/${styleId}/cross-sell")
        .queryParam("co","1")
        .headers(pdpHeaders)
        .check(status.is(200))
      )
    }

    val crossSellV2: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.crossSellV2")
    ){
      exec(http("Style Cross-Sell v2")
        .get("/v2/product/${styleId}/cross-sell")
        .headers(pdpHeaders)
        .check(status.is(200))
      )
    }

    val crossSellLego: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.crossSellLego")
    ){
      exec(http("Style Cross-Sell Lego")
        .get("/layout/product/${styleId}/recommendation/cross-sell")
        .headers(pdpHeaders)
        .check(status.is(200))
      )
    }

    /* Style Looks */
    val styleLooksV2: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.styleLooksV2")
    ){
      exec(http("Style Looks")
        .get("/layout/product/${styleId}/looks")
        .headers(pdpHeaders)
        .check(status.is(200))
      )
    }

    /* Style ShoppableLooks */
    val styleShoppableLooks: ChainBuilder = doIf(
      session =>
        util.getAPIStatus("PdpPage.styleShoppableLooks") &&
        session.contains("tpPrimary") &&
        session.contains("tpSecondary") &&
        session.contains("touchPointType")
    ){
      exec(http("Style ShoppableLooks")
        .get("/v1/shoppableLooks/style/${styleId}")
        .queryParam("tp_x","${tpPrimary}")
        .queryParam("tp_y","${tpSecondary}")
        .queryParam("type","${touchPointType}")
        .headers(pdpHeaders)
        .check(status.is(200))
      )
    }

    /* checkDeliveryOptions */
    val checkDeliveryOptions: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.checkDeliveryOptions")
    ){
      exec(http("checkDeliveryOptions")
        .post("/serviceability/check")
        .headers(pdpHeaders)
        .body(ElFileBody("hlt-test-data/delivery.json"))
        .check(status.is(200))
      )
    }

    val checkDeliveryOptionsV2: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.checkDeliveryOptionsV2")
    ){
      exec(http("checkDeliveryOptionsV2")
        .post("/v2/serviceability/check")
        .headers(pdpHeaders)
        .body(ElFileBody("hlt-test-data/deliveryV2.json"))
        .check(status.is(200))
      )
    }

    val checkDeliveryOptionsV3: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.checkDeliveryOptionsV3")
    ){
      exec(http("checkDeliveryOptionsV3")
        .post("/v3/serviceability/check")
        .headers(pdpHeaders)
        .body(ElFileBody("hlt-test-data/deliveryV3.json"))
        .check(status.is(200))
      )
    }

    val checkDeliveryOptionsLego: ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.checkDeliveryOptionsLego")
    ){
      exec(http("checkDeliveryOptionsLego")
        .post("/layout/serviceability/check")
        .headers(pdpHeaders)
        .body(ElFileBody("hlt-test-data/deliveryLego.json"))
        .check(status.is(200))
      )
    }
    
    val sizeRecommend : ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.sizeRecommend")
    ){
      exec(http("sizeRecommend")
        .post("/product/${styleId}/size/recommendation?co=1")
        .headers(pdpHeaders)
        .body(ElFileBody("hlt-test-data/sizeRecoPdp.json"))
        .check(status.is(200))
      )
    }
    
    val sizeProfile : ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.sizeProfile")
    ){
      exec(http("sizeProfile")
        .get("/user/size-profiles?gender=${gender}&articleType=${articleType}")
        .headers(pdpHeaders)
        .check(status.is(200))
      )
    }
    

    val looks : ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.looks")
    ){
      exec(http("looks")
        .get("/product/${styleId}/looks?co=1")
        .headers(pdpHeaders)
        .check(status.is(200))
      )
    }
    
     val looksPlayground : ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.looksPlayground")
    ){
      exec(http("looksPlayground")
        .post("https://developer.myntra.com/looks/looks/select/")
        .headers(pdpHeaders)
        .body(ElFileBody("hlt-test-data/looksplayground.json"))
        .check(status.is(200))
      )
    }
     
     val insiderPoints : ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.insiderPoints")
    ){
       exec(session=>{
         session.set("insiderAmount", (Random.nextInt(2000) + 500).toString)
       })
      .exec(http("insiderPoint")
        .get("/v1/product/${styleId}/insiderpoints?amount=${insiderAmount}&sku=${skuiId}&co=1")
        .headers(pdpHeaders)
        .check(status.is(200))
      )
    }
     
    val looksV2Lego : ChainBuilder = doIf(
      _ => util.getAPIStatus("PdpPage.looksV2Lego")
    ){
      exec(http("looksV2Lego")
        .get("/layout/product/${styleId}/looks")
        .headers(pdpHeaders)
        .check(status.is(200))
      )
    }
  }

}
