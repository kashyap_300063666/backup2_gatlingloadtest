package simulations.poc2.pages

import com.myntra.commons.util.HLTUtility
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class CartPage {
  val util: HLTUtility = new HLTUtility()

  val cartHeaders = Map(
    "content-type" -> "application/json",
    "at" -> "${at}")

  object CartApis {

    val clearCart: ChainBuilder = doIf(
      _ => util.getAPIStatus("CartPage.clearCart")) {
        exec(http("clear cart of the user")
          .delete("/v1/cart/default/clear")
          .header("x-myntra-app", "appFamily=MyntraRetailAndroid;appVersion=4.2009.2")
          .queryParam("uidx", "${uidx}")
          .headers(cartHeaders)
          .check(status.is(200)))
      }

    lazy val addToCart: ChainBuilder = doIf(
      session => util.getAPIStatus("CartPage.addToCart") &&
          session.contains("skuiId") &&                             // Execute only if skuId is present in the session
          session.contains("supplyType") &&                         // Execute only if supplyType is present in the session
          session("supplyType").as[String].equals("ON_HAND") &&     // Execute only if supplyType is ON_HAND
          !session("sellerName").as[String].contains("H & M")       // Do not add style to cart if the seller is "H & M"
    ) {
        exec(http("Add to Cart")
          .post("/v1/cart/default/add")
          .headers(cartHeaders)
          .body(StringBody("""{"id":${styleId},"skuId": ${skuiId},"quantity":1}""")).asJson
          .check(status.is(200)))
      }

    val addToCartWithCheckoutApplyCoupon: ChainBuilder = doIf(
      session => util.getAPIStatus("CartPage.addToCart") &&
        session.contains("skuiId") &&                             // Execute only if skuId is present in the session
        session.contains("supplyType") &&                         // Execute only if supplyType is present in the session
        session("supplyType").as[String].equals("ON_HAND") &&     // Execute only if supplyType is ON_HAND
        !session("sellerName").as[String].contains("H & M")       // Do not add style to cart if the seller is "H & M"
    ) {
      exec(http("Add to Cart")
        .post("/v1/cart/default/add")
        .headers(cartHeaders)
        .body(StringBody("""{"id":${styleId},"skuId": ${skuiId},"quantity":1}""")).asJson
        .check(status.is(200)))
        .exec(applyCoupon,checkOutCart)
    }

    val addToCartBrowseFlow: ChainBuilder = doIf(
      session => util.getAPIStatus("CartPage.addToCartBrowseFlow") &&
        session.contains("skuiId")
    ) {
      exec(http("Add to Cart")
        .post("/v1/cart/default/add")
        .headers(cartHeaders)
        .body(StringBody("""{"id":${styleId},"skuId": ${skuiId},"quantity":1}""")).asJson
        .check(status.is(200)))
    }

    val cartCountDefault: ChainBuilder = doIf(
      _ => util.getAPIStatus("CartPage.cartCountDefault")) {
        exec(http("PdpPage:cartCountDefault")
          .get("/v1/cart/default/summary")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("Content-Type", "application/json")
          .header("at", "${at}")
          .queryParam("cm", "true")
          .check(status.is(200)))
      }

    val cartDefaultApply: ChainBuilder = doIf(
      _ => util.getAPIStatus("CartPage.cartDefaultApply")) {
      exec(http("CartPage:cartDefaultApply")
        .post("/v1/cart/default/apply")
        .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
        .header("Content-Type", "application/json")
        .header("at", "${at}")
        .header("pagesource", "cart")
        .body(ElFileBody("hlt-test-data/cartDefaultApply.json")).asJson
        .check(status.is(200)))
    }

    lazy val applyCoupon: ChainBuilder = doIf(
      _ => util.getAPIStatus("CartPage.applyCoupon")) {
        exec(http("CartPage:applyCoupon")
          .put("/v1/cart/default/coupon/apply")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("Content-Type", "application/json")
          .header("at", "${at}")
          .queryParam("sendApplicableCoupons", "true")
          .body(ElFileBody("hlt-test-data/applyCoupon.json")).asJson //Read the coupons from the csv
          .check(status.is(200)))
      }

    val cartFiller: ChainBuilder = doIf(
      _ => util.getAPIStatus("CartPage.cartFiller")) {
        exec(http("CartPage:cartFiller")
          .post("/v1/cart/cartFiller")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("at", "${at}")
          .header("Content-Type", "application/json")
          .body(ElFileBody("hlt-test-data/cartFiller.json")).asJson
          .check(status.is(200)))
      }

    lazy val checkOutCart: ChainBuilder = doIf(
      _ => util.getAPIStatus("CartPage.checkOutCart")) {
        exec(http("CartPage:checkoutCart")
          .get("/v1/cart/default")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("at", "${at}")
          .header("Content-Type", "application/json")
          .check(status.is(200))
          .check(jsonPath("$.id").findAll.saveAs("cartId"))
          .check(jsonPath("$.price.total").find.saveAs("bagAmount"))
          .check(jsonPath("$.products[*].itemId").find(0).optional.saveAs("itemId")))
      }

    val checkoutCartNode: ChainBuilder = doIf(
      _ => util.getAPIStatus("CartPage.checkoutCartNode")) {
        exec(http("CartPage:checkoutCartNode")
          .get("https://www.myntra.com/checkout/cart")
          .header("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.1; en-us; GT-N7100 Build/JRO03C) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30")
          .header("Cookie", "at=${at};rt=${rt};ilgim=true")
          .header("Content-Type", "application/json")
          .check(status.is(200)))
      }

    val getAllCoupons: ChainBuilder = doIf(
      _ => util.getAPIStatus("CartPage.getAllCoupons")) {
        exec(http("CartPage:getAllCoupons")
          .get("/v1/cart/coupons")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("at", "${at}")
          .header("cartid", "${cartId}")
          .header("Content-Type", "application/json")
          .check(status.is(200)))
      }

    val giftcardPayment: ChainBuilder = doIf(
      _ => util.getAPIStatus("CartPage.giftcardPayment")) {
        exec(http("CartPage:giftcardPayment")
          .put("/v1/cart/default/giftcard/apply")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("at", "${at}")
          .header("Content-Type", "application/json")
          .check(status.is(200))
          .check(jsonPath("$.id").findAll.saveAs("cartId"))
          .check(jsonPath("$.products[*].itemId").find(0).optional.saveAs("itemId")))
      }

    val removefromcart: ChainBuilder = doIf(
      session => util.getAPIStatus("CartPage.removefromcart") &&
      session.contains("itemId")
      
    ) {
        exec(http("CartPage:removefromcart")
          .put("/v1/cart/default/remove")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("at", "${at}")
          .header("Content-Type", "application/json")
          .body(ElFileBody("hlt-test-data/removeFromCart.json")).asJson
          .check(status.is(200)))
      }

  }
}
