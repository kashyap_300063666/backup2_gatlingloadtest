package simulations.poc2.pages

import com.myntra.commons.util.HLTUtility
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import org.jsoup.Jsoup
import org.jsoup.nodes.{Document, Element}

class Payments {
  val util: HLTUtility = new HLTUtility()

  val paymentsHeaders = Map(
    "Content-Type" -> "application/json",
    "at"-> "${at}",
    "Cookie" -> "at=${at}"
  )

  val makePaymentFormDataMap = Map(
    "b_address"->"10th main road",
    "b_city"->"Bangalore",
    "b_country"->"India",
    "b_firstname"->"Myprecious",
    "b_state"->"Karnataka",
    "b_zipcode"->"999002",
    "cartContext"->"default",
    "clientCode"->"responsive",
    "mobile"->"9988998899",
    "profile"->"myntra.com",
    "userGroup"->"normal",
    "address"->"${addressId}",
    "csrf_token"->"${csrfToken}",
    "email"->"${emailId}",
    "cartId"->"${cartId}",
    "at"->"${at}"
  )

  val creditCardPaymentFormDataMap = Map(
    "card_number"->"${cardNumber}",
    "card_month"->"02",
    "bill_name"->"Test+Card",
    "card_year"->"20",
    "other_cards"->"false",
    "cvv_code"->"123",
    "pm"->"creditcard"
  )

  val walletPaymentFormDataMap = Map(
    "wallet_amount"->"${bagAmount}",
    "paymentProviderId"->"4",
    "wallet_mobile"->"9988998899",
    "userProfileMobile"->"9988998899",
    "wallet_enabled"->"true",
    "pm"->"wallet"
  )

  object PaymentRelatedAPIs {

    val cashBackService: ChainBuilder = doIf(
      session => util.getAPIStatus("Payments.cashBackService")
    ) {
        exec(http("PaymentPage:cashBackService")
          .put("/v1/cart/default/loyaltypoints/apply")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("at", "${at}")
          .header("Cookie", "xid=${xid}; sxid=${sxid}")
          .check(status.is(200)))

      }

    val checkoutPayment: ChainBuilder = doIf(
      session => util.getAPIStatus("Payments.checkoutPayment") &&
        session.contains("cartId")
    ) {
        exec(http("PaymentPage:checkoutPayment")
          .get("https://pps.myntra.com/myntra-payment-plan-service/v2/paymentInstruments")
          .queryParam("cartId", "${cartId}")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("Authorization", "3189d9e1c55b765621e13d708a21c293b0c86da6682018bfa96362588ad1e456")
          .header("Client", "checkout")
          .header("Origin", "myntra.com")
          .header("at", "${at}")
          .check(status.is(200)))

      }

    val checkoutPaymentNode: ChainBuilder = doIf(
      _ => util.getAPIStatus("Payments.checkoutPaymentNode")) {
        exec(http("PaymentPage:checkoutPaymentNode")
          .get("https://www.myntra.com/checkout/payment")
          .header("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.1; en-us; GT-N7100 Build/JRO03C) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30")
          .header("Cookie", "at=${at};rt=${rt};ilgim=true")
          .check(status.is(200)))

      }

    val getAllOrders: ChainBuilder = doIf(
      _ => util.getAPIStatus("Payments.getAllOrders")) {
        exec(http("PaymentPage:getAllOrders")
          .post("/v2/user/orders")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("at", "${at}")
          .body(ElFileBody("hlt-test-data/userOrders.json")).asJson
          .check(status.is(200)))
      }

    val getCaptcha: ChainBuilder = doIf(
      _ => util.getAPIStatus("Payments.getCaptcha")) {
        exec(http("PaymentPage:getCaptcha")
          .get("https://www.myntra.com/captcha/captcha.php")
          .queryParam("id", "codVerificationPage")
          .queryParam("_t", "12345")
          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
          .header("Cookie", "at=${at};rt=${rt};ilgim=true")
          .check(status.is(200)))

      }

//    val giftcardPayment: ChainBuilder = doIf(
//      session => util.getAPIStatus("Payments.giftcardPayment")
//    ) {
//        exec(http("PaymentPage:giftcardPayment")
//          .put("/v1/cart/default/giftcard/apply")
//          .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
//          .header("xid", "${xid}")
//          .header("at", "${at}")
//          .header("Cookie", "xid=${xid};sxid=${sxid}")
//          .check(status.is(200)))
//
//      }

    val plutusEligibility: ChainBuilder = doIf(
      session => util.getAPIStatus("Payments.plutusEligibility") &&
        session.contains("bagAmount") &&
        session.contains("cardNumber") &&
        session.contains("skuiId") &&
        session.contains("cartId")
    ) {
      exec(http("plutus eligibility")
        .post("https://pps.myntra.com/plutus/v3/eligibility/at")
        .headers(paymentsHeaders)
        .body(ElFileBody("hlt-test-data/plutusEligibilityV2.json")).asJson
        .check(status.is(200)))
    }

    val resolveConflict: ChainBuilder = doIf(
      _ => util.getAPIStatus("Payments.resolveConflict")
    ) {
      exec(http("resolveConflict")
        .post("/v1/cart/default/resolveConflict")
        .headers(paymentsHeaders)
        .check(status.is(200)))
    }

    val verifyCaptcha: ChainBuilder = doIf(
      _ => util.getAPIStatus("Payments.verifyCaptcha")
    ) {
      exec(http("verifyCaptcha")
        .post("https://www.myntra.com/cod_verification.php")
        .headers(paymentsHeaders)
        .header("Accept-Encoding", "gzip, deflate, br")
        .header("Cookie" , "at=${at};rt=${rt};ilgim=true")
        .body(ElFileBody("hlt-test-data/verifyCaptcha.json")).asJson
        .check(status.is(200)))
    }

    val paymentInstruments: ChainBuilder = doIf(
      session => util.getAPIStatus("Payments.paymentInstruments") &&
        session.contains("cartId")
    ) {
      exec(http("paymentInstruments")
        .post("https://pps.myntra.com/myntra-payment-plan-service/v3/paymentInstruments")
        .headers(paymentsHeaders)
        .header("User-Agent", "phonepe-webvie")
        .header("Origin", "myntra.com")
        .header("X-Forwarded-For" , "10.10.10.10")
        .header("client" , "myprecious")
        .header("version" , "1.0")
        .header("xMetaApp" , "deviceId=123456")
        .body(ElFileBody("hlt-test-data/paymentInstruments.json")).asJson
        .check(status.is(200))
        .check(bodyString.saveAs("paymentInstrumentResponse"))
        .check(jsonPath("$.csrfToken").saveAs("csrfToken"))
        .check(jsonPath("$.formSubmitUrl").saveAs("formSubmitUrl")))
    }

    val makeCardPayment: ChainBuilder = doIf(
      session => util.getAPIStatus("Payments.makeCardPayment") &&
        session.contains("addressId") &&
        session.contains("cardNumber") &&
        session.contains("cartId") &&
        session.contains("csrfToken")
    ) {
      exec(http("makePayment")
        .post("https://pps.myntra.com/myntra-payment-plan-service/v2/paynowform")
        .headers(paymentsHeaders)
        .header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36")
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header("Accept-Language" , "en-GB,en-US;q=0.9,en;q=0.8")
        .header("Origin" , "null")
        .header("upgrade-insecure-requests" , "1")
        .formParamMap(makePaymentFormDataMap)
        .formParamMap(creditCardPaymentFormDataMap)
        .check(status.is(200)))
    }

    val makeWalletPayment: ChainBuilder = doIf(
      session => util.getAPIStatus("Payments.makeWalletPayment") &&
        session.contains("bagAmount") &&
        session.contains("addressId") &&
        session.contains("cartId") &&
        session.contains("csrfToken")
    ) {
      exec(http("makeWalletPayment")
        .post("https://pps.myntra.com/myntra-payment-plan-service/v2/paynowform")
        .header("Cookie", "at=${at}")
        .header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36")
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header("Origin" , "myntra.com")
        .header("upgrade-insecure-requests" , "1")
        .formParamMap(makePaymentFormDataMap)
        .formParamMap(walletPaymentFormDataMap).disableFollowRedirect
        .check(status.is(303))
        .check(bodyString.saveAs("walletPaynowResponse")))

        .exec(session => {
          val str = session("walletPaynowResponse").as[String]

          session.set("returnUrl", extractWalletCallbackUrl(str))
        })
        .exec(flushCookieJar)
        .exec(http("walletCallback")
          .post(session => session("returnUrl").as[String])
          .header("Origin", "myntra.com")
          .header("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36")
          .header("Content-Type", "application/x-www-form-urlencoded")
          .header("Cookie", "at=${at}")
          .disableFollowRedirect
          .check(status.is(303))
          .check(headerRegex("location", "transaction_status=(.*)&errorCode=(.*)#payment").ofType[(String, String)])
          .check(header("location").saveAs("result")))
    }

    val makeCODPayment: ChainBuilder = doIf(
      session => util.getAPIStatus("Payments.makeCODPayment") &&
        session.contains("addressId") &&
        session.contains("cartId") &&
        session.contains("csrfToken")
    ) {
      exec(http("makeCODPayment")
        .post("https://pps.myntra.com/myntra-payment-plan-service/v2/buyform")
        .header("cookie", "at=${at}")
        .header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36")
        .header("Content-Type", "application/x-www-form-urlencoded")
        .header("Origin" , "myntra.com")
        .header("upgrade-insecure-requests" , "1")
        .formParam("pm","cod")
        .formParamMap(makePaymentFormDataMap).disableFollowRedirect
        .check(status.is(303))
        .check(bodyString.saveAs("codPaymentResponse")))
    }

    val postPayment: ChainBuilder = doIf(
      _ => util.getAPIStatus("Payments.postPayment")
    ) {
      exec(http("postPayment")
        .post("${postPaymentUrl}")
        .headers(paymentsHeaders)
        .header("User-Agent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
        .header("Content-Type", "application/x-www-form-urlencoded")
        .formParamMap(makePaymentFormDataMap)
        .check(status.is(200)))
    }

  }

  private def extractWalletCallbackUrl(response: String) = {
    val doc = Jsoup.parseBodyFragment(response)
    val body = doc.body
    val data = body.select("form[name=subFrm]").first
    println("callback url is " + data.attr("action"))
    val url = data.attr("action")
    if (url.contains("http:"))
      url.replaceAllLiterally("http:", "https:")
  }

}
