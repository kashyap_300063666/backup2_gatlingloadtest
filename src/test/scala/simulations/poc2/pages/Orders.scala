package simulations.poc2.pages

import com.myntra.commons.util.HLTUtility
import com.myntra.hlt.HLTOrdersUtil
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

class Orders {
  val util: HLTUtility = new HLTUtility()
  val ordersUtil: HLTOrdersUtil = new HLTOrdersUtil()

  val orderHeaders = Map(
    "authorization" -> "Basic YWJjOjEyMzQ=",
    "cache-control" -> "no-cache",
    "content-type" -> "application/json"
  )

  object orderAPIs {
    val cancelOrder: ChainBuilder = doIf(
      session =>
        util.getAPIStatus("Orders.cancelOrder") &&
          session.contains("orderId") &&
          session.contains("uidx")
    ) {
      exec(http("Order Cancellation")
        .get("/myntra-oms-service/v2/oms/owner/2297/buyer/2297/storeOrder/${orderId}")
        .headers(orderHeaders)
        .body(StringBody(
          """{"actionCode":"CANCEL_ORDER","cancellationReasonId": "38",
            | "comment": "Free item cancellation", "login": "${uidx}","userId":"${uidx}"}""".stripMargin)).asJson
        .check(status.saveAs("cancelStatus")))

        .exec(session=>{
          /* Save Order to Redis according to cancellation status code */
          if (session("cancelStatus").as[Int] == 200) {
            ordersUtil.saveCancelledCODOrder(session("orderId").as[String], session("uidx").as[String])
          } else {
            ordersUtil.saveToCODRetry(session("orderId").as[String], session("uidx").as[String])
          }
          session
        })
    }

    val myOrders: ChainBuilder = doIf(
      _ => util.getAPIStatus("Orders.myOrders")
    ) {
      exec(http("Get My Orders")
        .get("https://www.myntra.com/my/orders")
        .header("at", "${at}")
        .header("Cookie", "at=${at}; rt=${rt}; ilgim=true")
        .check(status.in(200, 302)))
    }
  }

}
