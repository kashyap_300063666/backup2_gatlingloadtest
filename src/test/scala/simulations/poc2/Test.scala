package simulations.poc2

import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import scala.concurrent.duration._

class Test extends BaseSimulation {

	val httpConf = http.baseUrl("http://www.google.co.in").disableWarmUp


	val googlepage = scenario("google page")
		.exec(http("google page")
			.get("/")
			.headers(headers)
			.check(status.is(200))
		)


	setUp(
		googlepage.inject(
			rampUsersPerSec(System.getProperty("ramp_up_rate.start").toInt) to System.getProperty("ramp_up_rate.end").toInt during(getLongProperty("duration.seconds") seconds)
		)
	).maxDuration(maxDurationInSec).protocols(httpConf)
}