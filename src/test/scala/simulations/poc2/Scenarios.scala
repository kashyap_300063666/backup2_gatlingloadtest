package simulations.poc2

import com.myntra.commons.util.PropertyLoader
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import simulations.poc2.pages.{AddressPage, CartPage, EngagementPage, HomePage, ListPage, Login, Orders, Payments, PdpPage, SessionSetUp, UserAPIs, WebPage}

class Scenarios extends PropertyLoader{

  val feeders = new Feeders()

  /* Initialize Page Objects */
  private val sessionSetUp = new SessionSetUp().SessionObj.sessionFromRedis
  private val loginApis = new Login().LoginAPIs
  private val homePage = new HomePage().HomePageAPIs
  private val listPage = new ListPage().Search
  private val pdpPageCalls =  new PdpPage().StyleDetails
  private val cartPage = new CartPage().CartApis
  private val addressPage=new AddressPage().Address
  private val userWishlistAPIs = new UserAPIs().WishList
  private val payments = new Payments().PaymentRelatedAPIs
  private val engagementPage = new EngagementPage().EngagementApis
  private val webPage = new WebPage().WebPageAPIs
  private val ordersPage = new Orders().orderAPIs
  private val repeat = getIntProperty("repeat", "1")

  /* Create various scenarios */
  val allAPIs: ScenarioBuilder =
    scenario("Full User Path")
      .exec(sessionSetUp)
      .feed(feeders.stylesFeeder)
      .feed(feeders.queryParamFeeder)
      .feed(feeders.filterQueryFeeder)
      .exec(
        homePage.home, homePage.topnav, homePage.recommended,
        listPage.paginationMTV2, listPage.filtersV2,
        pdpPageCalls.stylePdp, pdpPageCalls.similarStyles,
        cartPage.clearCart,cartPage.addToCart, cartPage.checkOutCart
      )

  val landingPage: ScenarioBuilder =
    scenario("landingPage")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          homePage.home, homePage.topnav
        )
      }

  val landingPageBiro: ScenarioBuilder =
    scenario("landingPageBiro")
      .exec(sessionSetUp)
      .repeat(repeat){
        exec(
          homePage.biro
        )
      }

  val landingPageMen: ScenarioBuilder =
    scenario("landingPageMen")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          homePage.men
        )
      }

  val landingPageWomen: ScenarioBuilder =
    scenario("landingPageWomen")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          homePage.women
        )
      }

  val landingPageSmashbox: ScenarioBuilder =
    scenario("landingPageSmashbox")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          homePage.smashbox
        )
      }

  val landingPageMall: ScenarioBuilder =
    scenario("landingPageMall")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          homePage.mall
        )
      }

  val landingPageRecommended: ScenarioBuilder =
    scenario("landing Page - Recommended")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          homePage.recommended
        )
      }

  val landingPageGetLoyaltyBalance: ScenarioBuilder =
    scenario("landing Page - GetLoyaltyBalance")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          homePage.getLoyaltyBalance
        )
      }

      
  val searchCallAutoSuggest: ScenarioBuilder =
    scenario("search auto suggest")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.queryParamFeeder)
          .exec(
            listPage.autoSuggest
          )
      }
      
  val searchCallPaginationsV2: ScenarioBuilder =
    scenario("search Call for Paginations")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.queryParamFeeder)
          .feed(feeders.listPageNosFeederCircular)
          .exec(
            listPage.paginationMTV2
          )
      }
      
  val searchpaginationMTV1: ScenarioBuilder =
    scenario("search paginationMTV1")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.queryParamFeeder)
          .feed(feeders.listPageNosFeederCircular)
          .exec(
            listPage.paginationMTV1
          )
      }
      
  val searchClusterV2: ScenarioBuilder =
    scenario("Search clusterV2")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.queryParamFeeder)
          .feed(feeders.filterQueryFeeder)
          .feed(feeders.listPageNosFeeder)
          .exec(
            listPage.clusterV2
          )
      }
      
  val searchFiltersV2: ScenarioBuilder =
    scenario("Search filtersV2")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.queryParamFeeder)
          .feed(feeders.filterQueryFeeder)
          .exec(
            listPage.filtersV2
          )
      }

      
  val searchFiltersV1: ScenarioBuilder =
    scenario("Search filtersV1")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.queryParamFeeder)
          .feed(feeders.filterQueryFeeder)
          .feed(feeders.listPageNosFeeder)
          .exec(
            listPage.filtersV1
          )
      }

  val checkDelivery: ScenarioBuilder =
    scenario("Check Delivery")
      .exec(sessionSetUp)
      .repeat(repeat) {
          exec(
            pdpPageCalls.checkDeliveryOptions
          )
      }

  val checkDeliveryV2: ScenarioBuilder =
    scenario("Check DeliveryV2")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          pdpPageCalls.checkDeliveryOptionsV2
        )
      }

  val checkDeliveryLego: ScenarioBuilder =
    scenario("Check Delivery Lego")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          pdpPageCalls.checkDeliveryOptionsLego
        )
      }

  val checkDeliveryV3: ScenarioBuilder =
    scenario("Check DeliveryV3")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          pdpPageCalls.checkDeliveryOptionsV3
        )
      }

  val myOrders: ScenarioBuilder =
    scenario("My Orders")
      .exec(sessionSetUp)
      .repeat(repeat){
        exec(
          ordersPage.myOrders
        )
      }

  val searchAllCalls: ScenarioBuilder =
    scenario("all Search Calls")
      .exec(sessionSetUp)
      .feed(feeders.queryParamFeeder)
      .feed(feeders.filterQueryFeeder)
      .feed(feeders.listPageNosFeeder)
      .exec(
        listPage.autoSuggest, listPage.clusterV2,
        listPage.filtersV1, listPage.filtersV2,
        listPage.paginationMTV1, listPage.paginationMTV2
      )

  val pdpPageAllCalls: ScenarioBuilder =
    scenario("pdp Page all APIs")
      .exec(sessionSetUp)
      .feed(feeders.stylesFeeder)
      .exec(
        pdpPageCalls.stylePdp, pdpPageCalls.stylePdpV2, pdpPageCalls.stylePdpLego,
        pdpPageCalls.clickForOffer, pdpPageCalls.clickForOfferV2, pdpPageCalls.clickForOfferLego,
        pdpPageCalls.similarStyles, pdpPageCalls.similarStylesV2, pdpPageCalls.similarStylesLego,
        pdpPageCalls.crossSell, pdpPageCalls.crossSellV2, pdpPageCalls.crossSellLego,
        pdpPageCalls.styleLooksV2, pdpPageCalls.styleShoppableLooks,
        pdpPageCalls.checkDeliveryOptions, pdpPageCalls.checkDeliveryOptionsV2, pdpPageCalls.checkDeliveryOptionsV3,
        pdpPageCalls.checkDeliveryOptionsLego
      )
      
  val pdpSimilarStylesLego: ScenarioBuilder =
    scenario("pdp similarStylesLego")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            pdpPageCalls.similarStylesLego
          )
      }
      
  val pdpcrossSellLego: ScenarioBuilder =
    scenario("pdp crossSellLego")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            pdpPageCalls.crossSellLego
          )
      }
      
     //PDP - pdpPageCalls.similarStylesV2
      
  val pdpsimilarStylesV2: ScenarioBuilder =
    scenario("pdp similarStylesV2")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            pdpPageCalls.similarStylesV2
          )
      }
    
    //PDP - pdpPageCalls.crossSellV2
  val pdpcrossSellV2: ScenarioBuilder =
    scenario("pdp crossSellV2")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            pdpPageCalls.crossSellV2
          )
      }
    
    //PDP - pdpPageCalls.crossSell
  val pdpcrossSell: ScenarioBuilder =
    scenario("pdp crossSell")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            pdpPageCalls.crossSell
          )
      }
    
    //PDP - pdpPageCalls.clickForOffer

  val pdpStyleOffers: ScenarioBuilder =
    scenario("pdp clickForOffer")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            pdpPageCalls.clickForOffer,
          )
      }

  val stylePdpV2OfferV2SizeRecommendSizeProfileInsiderPoints: ScenarioBuilder =
    scenario("stylePdpV2OfferV2SizeRecommendSizeProfileInsiderPoints")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            pdpPageCalls.stylePdpV2,
            pdpPageCalls.clickForOfferV2,
            pdpPageCalls.sizeRecommend,
            pdpPageCalls.sizeProfile,
            pdpPageCalls.insiderPoints
          )
          .repeat(60) {
            exec(
              pdpPageCalls.sizeRecommend,
              pdpPageCalls.sizeProfile
            )
          }
      }

  val stylePdpLegoOfferLegoShoppableLooks: ScenarioBuilder =
    scenario("stylePdpLegoOfferLegoShoppableLooks")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            pdpPageCalls.stylePdpLego,
            pdpPageCalls.clickForOfferLego,
            pdpPageCalls.styleShoppableLooks
          )
      }
      
  val pdpStyleData: ScenarioBuilder =
    scenario("pdp Style Data")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            pdpPageCalls.stylePdp
          )
      }

  val pdpStyleV2: ScenarioBuilder =
    scenario("pdpStyleV2")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            pdpPageCalls.stylePdpV2
          )
      }

  val pdpStyleLego: ScenarioBuilder =
    scenario("stylePdpLego")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            pdpPageCalls.stylePdpLego
          )
      }

  val pdpSimilarStyles: ScenarioBuilder =
    scenario("pdp Similar Styles")
      .exec(sessionSetUp)
      .feed(feeders.stylesFeeder)
      .exec(
        pdpPageCalls.similarStyles,
      )

  val styleLegoOffersLegoShoppableLooksAddCheckoutApplyCouponRemoveCartFiller: ScenarioBuilder =
    scenario("styleLegoOffersLegoShoppableLooksAddCheckoutRemoveCartFiller")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .feed(feeders.couponsFeeder)
          .exec(
            pdpPageCalls.stylePdpLego,
            pdpPageCalls.clickForOfferLego,
            pdpPageCalls.styleShoppableLooks,
            cartPage.addToCartWithCheckoutApplyCoupon,
            cartPage.cartFiller,
            cartPage.removefromcart,
          )
      }
      
  val UserAPIsgetWishListV2: ScenarioBuilder =
    scenario("UserAPIs - getWishListV2")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            userWishlistAPIs.getWishListV2,
          )
      }

  val wishListSummary: ScenarioBuilder =
    scenario("UserAPIs - wishListSummary")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            userWishlistAPIs.wishListSummary,
          )
      }

  val addToWishListUsingStylePdpLego: ScenarioBuilder =
    scenario("addToWishListUsingStylePdpLego")
      .exec(sessionSetUp)
      .feed(feeders.stylesFeeder)
      .exec(
        pdpPageCalls.stylePdpLego,
        userWishlistAPIs.addToWishList
      )

  val styleLegoOfferLegoAddCheckoutApplyCouponRemoveFromCartCartFiller: ScenarioBuilder =
    scenario("styleLegoOfferLegoAddCheckoutRemoveFromCartCartFiller")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .feed(feeders.couponsFeeder)
          .exec(
            pdpPageCalls.stylePdpLego,
            pdpPageCalls.clickForOfferLego,
            cartPage.addToCartWithCheckoutApplyCoupon,
            cartPage.cartFiller,
            cartPage.removefromcart
          )
      }

  val styleV2OfferLegoAddCheckoutApplyCouponRemoveFromCartCartFiller: ScenarioBuilder =
    scenario("styleV2OfferLegoAddCheckoutRemoveFromCartCartFiller")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .feed(feeders.couponsFeeder)
          .exec(
            pdpPageCalls.stylePdpV2,
            pdpPageCalls.clickForOfferLego,
            cartPage.addToCartWithCheckoutApplyCoupon,
            cartPage.cartFiller,
            cartPage.removefromcart
          )
      }
      
  val cartFillerAPI: ScenarioBuilder =
    scenario("Cart page cartFiller")
      .exec(sessionSetUp)
      .feed(feeders.stylesFeeder)
      .exec(
        cartPage.clearCart,
        pdpPageCalls.stylePdp,
        cartPage.cartFiller
      )
      
  val cartDefaultApply: ScenarioBuilder =
    scenario("cart page cartDefaultApply")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            cartPage.cartDefaultApply
          )
      }
      
//  val applyCouponAPI: ScenarioBuilder =
//    scenario("Cart page applyCoupon")
//      .exec(sessionSetUp)
//      .repeat(repeat) {
//        feed(feeders.stylesFeeder)
//          .feed(feeders.couponsFeeder)
//          .exec(
//              cartPage.applyCoupon // applyCoupon
//            )
//      }

  val applyCouponAPIBrowseOnlyflow: ScenarioBuilder =
    scenario("Cart applyCoupon Browse")
      .exec(sessionSetUp)
      .feed(feeders.stylesFeeder)
      .feed(feeders.couponsFeeder)
      .exec(
        cartPage.clearCart,
        pdpPageCalls.stylePdpLego,
        cartPage.addToCartBrowseFlow, cartPage.checkOutCart,cartPage.getAllCoupons,cartPage.applyCoupon   //getAllCoupons and applyCoupon
      )

  val checkoutCartNodeAPI: ScenarioBuilder =
    scenario("Cart page checkoutCartNode")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            cartPage.checkoutCartNode
          )
      }

  val styleV2OfferLegoAddCheckoutToCartApplyCoupon: ScenarioBuilder =
    scenario("styleV2OfferLegoAddAndCheckoutToCart")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .feed(feeders.couponsFeeder)
          .exec(
            pdpPageCalls.stylePdpV2,
            pdpPageCalls.clickForOfferLego,
            cartPage.addToCartWithCheckoutApplyCoupon
          )
      }

  val checkoutCartGetAllCoupons: ScenarioBuilder =
    scenario("checkoutCartGetAllCoupons")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .exec(
            cartPage.checkOutCart,
            cartPage.getAllCoupons
          )
      }

  val giftCardApply: ScenarioBuilder =
    scenario("giftCardApply")
      .exec(sessionSetUp)
      .repeat(repeat){
        exec(
          cartPage.giftcardPayment
        )
      }
      
      //addressPage.checkoutAddress
      
  val addresscheckoutAddress: ScenarioBuilder =
    scenario("Addresspage checkoutAddress")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          addressPage.checkoutAddress
        )
      }

  val addNewAddress: ScenarioBuilder =
    scenario("addNewAddress")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          addressPage.addNewAddressAddress
        )
      }
      
      
      //addressPage.checkoutAddressNode
      
  val addresscheckoutAddressNode: ScenarioBuilder =
    scenario("Addresspage checkoutAddressNode")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          addressPage.checkoutAddressNode
        )
      }
      
      
      //AddressPage:isPinCodeServicable
      
  val addresspinCodeService: ScenarioBuilder =
    scenario("Addresspage pinCodeService")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.pincodeFeeder)
          .exec(
            addressPage.pinCodeService
          )
      }
      
      //AddressPage:selectAddress
      
  val checkoutAndselectAddress: ScenarioBuilder =
    scenario("Addresspage selectAddress")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          addressPage.checkoutAddress, addressPage.selectAddress
        )
      }
      
      //EngagementPage.EngagementAccountSummary
      
  val engagementAccountSummary: ScenarioBuilder =
    scenario("EngagementPage - EngagementAccountSummary")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          engagementPage.EngagementAccountSummary
        )
      }
      
      
      //EngagementPage.EngagementRewards
      
  val engagementRewards: ScenarioBuilder =
    scenario("EngagementPage - EngagementRewards")
      .exec(sessionSetUp)
      .repeat(repeat) {
        exec(
          engagementPage.EngagementRewards
        )
      }
      
      
      //WebPage - Myntraweb
      
  val Myntraweb: ScenarioBuilder =
    scenario("WebPage - Myntraweb")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .feed(feeders.queryParamWebAndPwaCircular)
          .exec(
            webPage.Myntraweb, webPage.MyntrawebWithQueryParam, webPage.MyntrawebWithStyleID
          )
      }
      
      //WebPage - Myntrapwa
      
  val Myntrapwa: ScenarioBuilder =
    scenario("WebPage - Myntrapwa")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
          .feed(feeders.queryParamWebAndPwaCircular)
          .exec(
            webPage.Myntrapwa, webPage.MyntrapwaWithQueryParam, webPage.MyntrapwaWithStyleID
          )
      }
      
      //PdpPage.looks
  
  val looks: ScenarioBuilder =
    scenario("looks")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
        .exec(
          pdpPageCalls.looks
        )
      }
  
  //PdpPage.looksPlayground
  
  val looksPlayground: ScenarioBuilder =
    scenario("looksPlayground")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
        .exec(
          pdpPageCalls.looksPlayground
        )
      }
  
  //pdpPageCalls.looksV2Lego
  
  val looksV2Lego: ScenarioBuilder =
    scenario("looksV2Lego")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
        .exec(
          pdpPageCalls.looksV2Lego
        )
      }
  
  // HomePage.GetScratchCard
  
  val GetScratchCard: ScenarioBuilder =
    scenario("GetScratchCard")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
        .exec(
          homePage.GetScratchCard
        )
      }
  
  // HomePage.Referralcode
  
  val Referralcode: ScenarioBuilder =
    scenario("Referralcode")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
        .exec(
          homePage.Referralcode
        )
      }
  
  
  // HomePage.fetchSlot
  
  val fetchSlot: ScenarioBuilder =
    scenario("fetchSlot")
      .exec(sessionSetUp)
      .repeat(repeat) {
        feed(feeders.stylesFeeder)
        .exec(
          homePage.fetchSlot
        )
      }
 
 val browseFlow01: ScenarioBuilder =
    scenario("Browse flow 01")
      .exec(sessionSetUp)
      .feed(feeders.stylesFeeder)
      .feed(feeders.listPageNosFeeder)
      .feed(feeders.filterQueryFeeder)
      .feed(feeders.queryParamFeeder)
      .exec(
        homePage.biro, homePage.getLoyaltyBalance, homePage.home, homePage.topnav,
        listPage.autoSuggest, listPage.paginationMTV1, listPage.clusterV2, listPage.filtersV1,
        pdpPageCalls.stylePdp, pdpPageCalls.similarStyles, pdpPageCalls.crossSell, pdpPageCalls.clickForOffer,
        cartPage.addToCartBrowseFlow, cartPage.cartCountDefault, cartPage.checkOutCart, cartPage.checkoutCartNode,
      )

  val browseFlow02: ScenarioBuilder =
    scenario("Browse flow 02")
      .exec(sessionSetUp)
      .feed(feeders.stylesFeeder)
      .feed(feeders.listPageNosFeeder)
      .feed(feeders.filterQueryFeeder)
      .feed(feeders.queryParamFeeder)
      .exec(
        homePage.home, homePage.topnav,
        listPage.autoSuggest, listPage.paginationMTV2, listPage.clusterV2, listPage.filtersV2,
        pdpPageCalls.stylePdpV2, pdpPageCalls.similarStylesV2, pdpPageCalls.crossSellV2, pdpPageCalls.clickForOfferV2,
        cartPage.cartCountDefault, cartPage.checkOutCart, cartPage.checkoutCartNode,
      )

  val browseFlow03: ScenarioBuilder =
    scenario("Browse flow 03")
      .exec(sessionSetUp)
      .feed(feeders.stylesFeeder)
      .feed(feeders.listPageNosFeeder)
      .feed(feeders.filterQueryFeeder)
      .feed(feeders.queryParamFeeder)
      .exec(
        loginApis.tokenApi, loginApis.loginApi,
        homePage.biro, homePage.home, homePage.topnav, homePage.recommended,
        listPage.autoSuggest, listPage.paginationMTV2, listPage.clusterV2, listPage.filtersV2,
        pdpPageCalls.stylePdpLego, pdpPageCalls.similarStylesLego, pdpPageCalls.crossSellLego, pdpPageCalls.clickForOfferLego,
        userWishlistAPIs.addToWishList, userWishlistAPIs.getWishListV2,
        cartPage.addToCartBrowseFlow, cartPage.cartCountDefault, cartPage.checkOutCart, cartPage.checkoutCartNode, cartPage.removefromcart,
        addressPage.checkoutAddress, addressPage.checkoutAddressNode
      )

  val codPaymentsFlow: ScenarioBuilder =
    scenario("codPaymentsFlow")
      .exec(sessionSetUp)
      .feed(feeders.stylesFeeder)
      .feed(feeders.listPageNosFeeder)
      .feed(feeders.filterQueryFeeder)
      .feed(feeders.queryParamFeeder)
      .exec(
        homePage.biro, homePage.getLoyaltyBalance, homePage.home, homePage.topnav,
        listPage.autoSuggest, listPage.paginationMTV1, listPage.clusterV2, listPage.filtersV1,
        pdpPageCalls.stylePdp, pdpPageCalls.similarStyles, pdpPageCalls.crossSell, pdpPageCalls.clickForOffer,
        cartPage.addToCartBrowseFlow, cartPage.cartCountDefault, cartPage.checkOutCart, cartPage.checkoutCartNode,
        addressPage.checkoutAddress,
        payments.paymentInstruments, payments.getCaptcha, payments.verifyCaptcha, payments.makeCODPayment
      )

  val walletPaymentsFlow: ScenarioBuilder =
    scenario("walletPaymentsFlow")
      .exec(sessionSetUp)
      .feed(feeders.stylesFeeder)
      .feed(feeders.listPageNosFeeder)
      .feed(feeders.filterQueryFeeder)
      .feed(feeders.queryParamFeeder)
      .exec(
        homePage.biro, homePage.getLoyaltyBalance, homePage.home, homePage.topnav,
        listPage.autoSuggest, listPage.paginationMTV1, listPage.clusterV2, listPage.filtersV1,
        pdpPageCalls.stylePdp, pdpPageCalls.similarStyles, pdpPageCalls.crossSell, pdpPageCalls.clickForOffer,
        cartPage.addToCartBrowseFlow, cartPage.cartCountDefault, cartPage.checkOutCart, cartPage.checkoutCartNode,
        addressPage.checkoutAddress,
        payments.paymentInstruments, payments.makeWalletPayment
      )
}
