package simulations.growth

import java.text.SimpleDateFormat
import java.util.{Calendar, TimeZone}
import io.gatling.core.Predef._
import simulations.BaseSimulation
import com.github.phisgr.gatling.grpc.Predef._
import com.github.phisgr.gatling.grpc.protocol.GrpcProtocol
import io.grpc.ManagedChannelBuilder
import com.github.phisgr.gatling.pb._
import com.myntra.scratchCardService._
import io.gatling.core.session.Expression
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import com.myntra.scratchCardService.scratchCardService.{getAllRewardsRequest, scratchCardServiceGrpc, scratchCardServiceRequest}
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef.status
class ScratchCard extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "growthhack"
  /* Name of File to be downloaded */
  val fileName = "uidxlistcs.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val growthUrl = System.getProperty("growth") //Pass the complete url of single server if req
  protected var grpcConf = grpc(
    ManagedChannelBuilder
      .forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.GROWTH.toString,""), grpcPortNum)
      .usePlaintext()
    )
  if(growthUrl != null) {
    grpcConf = grpc(
      ManagedChannelBuilder
        .forAddress(growthUrl, grpcPortNum)
    )
  }
  val uidxFeeder: BatchableFeederBuilder[String]#F = csv(filePath).random
  val format = new SimpleDateFormat("yyyy-MM-dd")
  format.setTimeZone(TimeZone.getTimeZone("IST"))
  val dateString: String = format.format(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime())

  var scGetAllReq: Expression[getAllRewardsRequest] = getAllRewardsRequest(uidx = "${uidx}").updateExpr(_.uidx :~ $("uidx"))
  var scClaimRewardReq: Expression[scratchCardServiceRequest] =
    scratchCardServiceRequest(uidx = "${uidx}", orderID = "${uidx}")
      .updateExpr(_.uidx :~ $("uidx"))
      .updateExpr(_.orderID :~ "${uidx}" + "-" +dateString)
  val scRewards: ScenarioBuilder = scenario("growth: daily scratch card rewards")
    .feed(uidxFeeder)
    .exec(grpc("Growth - Scratch Card Get All")
      .rpc(scratchCardServiceGrpc.METHOD_GET_ALL_REWARDS)
      .payload(scGetAllReq)
      .check()
    )
    .exec(grpc("Growth - Scratch Card Claim Reward")
      .rpc(scratchCardServiceGrpc.METHOD_CLAIM_REWARD)
      .payload(scClaimRewardReq)
      .check()
    )
    setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec)
      .protocols(grpcConf)
}
// java -Denvironment=stage -Duse.constant_users=true -Dduration.seconds=5 -jar build/libs/myntra-gatling-load-tests-all.jar -s simulations.growth.ScratchCard
//https://scaleit.myntra.com/runs/zMGTgE7x25scaleitmO#details