package simulations.insider.masterclass

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef.scenario
import io.gatling.http.Predef._
import simulations.BaseSimulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class UserQuestionAndAnswer extends BaseSimulation {
  private val masterclassBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.MASTERCLASS.toString)).disableWarmUp

  val userId = Map("userid" -> "4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0")
  val timeslotId = "5eb28b4eb1c35c6165263778"

  // 1) /question/get-all-questionIds
  private val getAllQuestions =
    scenario("get all questions").
      exec(http("getAllQuestions").
        get("/api/masterclass/v1/question/get-all-questionIds?timeslotId=" + timeslotId).headers(userId).
        check(status.is(200)))

  // 2) /question/get-qna-by-ids
  private val getQuestionsByIds =
    scenario("get questions by id").
      exec(http("getQuestionsByIds").
        post("/api/masterclass/v1/question/get-qna-by-ids").headers(userId).body(
        StringBody("""{"timeslotId":"5eb28b4eb1c35c6165263778","questionIds":["5ece2c98b1c35c38f2421d13","5ece1ae2b1c35c38f2421d08","5ece193f9c78b2567c8ec209","5ecdf64ab85c95fc76b613b7","5ecd31aeb1c35c38f2421cca","5ecc28b2b85c95fc76b61373","5ecb322fb1c35c38f2421c11","5eca059db1c35c38f2421bc6","5ec9ae24b85c95fc76b61318","5ec7a2f7b85c95fc76b612dc"]}""")).
        asJson.check(status.is(200)))

  // 3) /question/get-questions
  private val getQuestions =
    scenario("get questions").
      exec(http("getQuestions").
        get("/api/masterclass/v1/question/get-questions?timeslotId=" + timeslotId).headers(userId).
        check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(masterclassBaseURL)
}
