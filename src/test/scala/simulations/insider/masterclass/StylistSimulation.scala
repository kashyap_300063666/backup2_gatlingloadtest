package simulations.insider.masterclass

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.http.Predef.http
import simulations.BaseSimulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class StylistSimulation extends BaseSimulation {
  private val masterclassBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.MASTERCLASS.toString)).disableWarmUp

  val userId = Map("userid" -> "4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0")
  val stylistId = "5de74baab85c951c9628a1ff"

  // 1) /stylist/get-one
  private val getOne =
    scenario("get stylist").
      exec(http("getOne").
        get("/api/masterclass/v1/stylist/get-one?id=" + stylistId).headers(userId).
        check(status.is(200)))

  // 2) /stylist/get-all
  private val getAll =
    scenario("get all stylist").
      exec(http("getAll").
        get("/api/masterclass/v1/stylist/get-all").headers(userId).
        check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(masterclassBaseURL)
}
