package simulations.insider.masterclass

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef.scenario
import io.gatling.http.Predef._
import simulations.BaseSimulation
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class EventSimulation extends BaseSimulation {
  private val masterclassBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.MASTERCLASS.toString)).disableWarmUp

  val userId = Map("userid" -> "4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0")
  val eventId = "5de74f58b85c951c9628a200"
  val stylistId = "5de74baab85c951c9628a1ff"
  val timeslotId = "5eb28b4eb1c35c6165263778"

  // 1) /event/page-state
  private val pageState =
    scenario("page state").
      exec(http("pageState").
        get("/api/masterclass/v1/event/page-state?eventId=" + eventId + "&timeSlotId=" + timeslotId).headers(userId).
        check(status.is(200)))

  // 2) /event/dashboard-all
  private val dashboardAll =
    scenario("dashboard all").
      exec(http("dashboardAll").
        get("/api/masterclass/v1/event/dashboard-all").headers(userId).
        check(status.is(200)))

  // 3) /event/dashboard-vod-all
  private val dashboardVodAll =
    scenario("dashboard vod all").
      exec(http("dashboardVodAll").
        get("/api/masterclass/v1/event/dashboard-vod-all").headers(userId).
        check(status.is(200)))

  // 4) /event/all-slots
  private val allSlots =
    scenario("all slots by event").
      exec(http("allSlots").
        get("/api/masterclass/v1/event/all-slots?eventId=" + eventId).headers(userId).
        check(status.is(200)))

  // 5) /event/all-by-stylist
  private val allEventByStylist =
    scenario("all event by stylist").
      exec(http("allEventByStylist").
        get("/api/masterclass/v1/event/all-by-stylist?stylistId=" + stylistId).headers(userId).
        check(status.is(200)))

  // 6) /event/list-all-redeemed
  private val listAllRedeemed =
    scenario("list all redeemed").
      exec(http("listAllRedeemed").
        get("/api/masterclass/v1/event/list-all-redeemed").headers(userId).
        check(status.is(200)))

  // 7) /event/markers-by-timeslot
  private val markersByTimeslot =
    scenario("markers by timeslot").
      exec(http("markersByTimeslot").
        get("/api/masterclass/v1/event/markers-by-timeslot?timeSlotId=" + timeslotId).headers(userId).
        check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(masterclassBaseURL)
}
