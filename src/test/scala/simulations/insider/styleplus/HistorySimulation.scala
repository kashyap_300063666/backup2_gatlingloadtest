package simulations.insider.styleplus

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class HistorySimulation extends BaseSimulation {
  private val styleplusBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.STYLE_PLUS_BACKEND.toString)).disableWarmUp

  // Create a user with userid LoadTestUser before starting the simulation
  // also assign loyalty points to that user, credit orderLine points and engagement points
  // create entry in redeem table for this user and use the respective redeem id
  val userId = Map("userid" -> "4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0")
  val redeemId = "9089224"

  // 1) /history/pointsHistory
  private val pointsHistory =
    scenario("user exists").
      exec(http("pointsHistory").
        get("/api/styleplus/v1/history/pointsHistory").headers(headers).headers(userId).
          check(status.is(200)))

  // 2) /history/claimedRewards
  private val claimedRewards =
    scenario("claimed rewards").
      exec(http("claimedRewards").
        get("/api/styleplus/v1/history/claimedRewards").headers(headers).headers(userId).
          check(status.is(200)))

  // 3) /history/delivered-orders
  private val deliveredOrders =
    scenario("delivered orders").
      exec(http("deliveredOrders").
        get("/api/styleplus/v1/history/delivered-orders").headers(headers).headers(userId).
          check(status.is(200)))

  // 4) /history/redeem/{{redeemId}}
  private val redeemDetails =
    scenario("redeem detals").
      exec(http("redeemDetails").
        get("/api/styleplus/v1/history/redeem/" + redeemId).headers(headers).headers(userId).
        check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(styleplusBaseUrl)
}
