package simulations.insider.styleplus

import java.util.UUID.randomUUID

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class OfferSimulation extends BaseSimulation {
  private val styleplusBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.STYLE_PLUS_BACKEND.toString)).disableWarmUp

  // create a dummy partner, dummy group
  // create a dummy offer with infinite validity ,0 points , with above partner and group and use its offer id here
  // assign this offer_id to LoadTestUser
  // also create dummy coupon mapped to dummy offer and coupon id here

  val offerId = "2116"
  val couponId = "65016746"
  val userId = Map("userid" -> "4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0")
  private val uuidFeeder = Iterator.continually(Map("uuid" -> randomUUID().toString))

  // 1) /offers/{{id}}
  private val getOfferById =
    scenario("offer by id").
      exec(http("getOfferById").
        get("/api/styleplus/v1/offer/" + offerId).headers(headers).
        check(status.is(200)))

  // 3) /offers/{offid}/coupon/{couponId}
  private val claimedCoupon =
    scenario("claimed coupon").
      exec(http("claimedCoupon").
        get("/api/styleplus/v1/offer/" + offerId + "/coupon/" + couponId).headers(headers).headers(userId).
          check(status.is(200)))

  // 4) /offers/rewards
  private val offerRewards =
    scenario("offer rewards").
      exec(http("offerRewards").
        get("/api/styleplus/v1/offers/rewards").headers(headers).headers(userId).
        check(status.is(200)))

  // 5) /offers/source-points
  private val sourcePoints =
    scenario("source points").
      exec(http("sourcePoints").
        get("/api/styleplus/v1/offers/source-points?sourceName=test").headers(headers).headers(userId).
        check(status.is(200)))

  // 6) /offers/list-redeem-perks
  private val redeemperks =
    scenario("redeem perks").
      exec(http("redeemperks").
        get("/api/styleplus/v1/offers/list-redeem-perks").headers(headers).headers(userId).
        check(status.is(200)))

  // 8) /offers/by-category
  private val offerByCategory =
    scenario("offer by category").
      exec(http("offerByCategory").
        get("/api/styleplus/v1/offers/by-category?offCatIds=4,1").headers(headers).headers(userId).
        check(status.is(200)))

  // 9)  /offers/by-ids
  private val offerByIds =
    scenario("offer by Ids").
      exec(http("offerByIds").
        get("/api/styleplus/v1/offers/by-ids?ids=2116").headers(headers).headers(userId).
        check(status.is(200)))

  // 10) /offers/v2/credit-points
  private val creditPoints =
    scenario("creditPoints").
      feed(uuidFeeder).
      exec(http("creditPoints")
        .post("/api/styleplus/v1/offers/v2/credit-points")
        .headers(headers)
        .body(StringBody("""{"uidx":"4dfb22e5.8760.4573.99f2.ba0f70387e63BAG9yw3CY0","source":"map",
    "points": 10,"message": "Credit points for testing","expiryDate":"2019-06-06", "sourceKey": "${uuid}",
    "sourceName":"test"}""")).asJson
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(styleplusBaseUrl)
}
