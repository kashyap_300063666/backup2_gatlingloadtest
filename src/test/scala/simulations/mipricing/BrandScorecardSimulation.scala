package simulations.mipricing

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import MiPricingUtils._
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import simulations.BaseSimulation

class BrandScorecardSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "vorta"

  private val serviceUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MIPRICING.toString)

  def forceRefresh: Boolean = getProperty("CACHE_REFRESH", "false").toBoolean

  val filePath1: String = feederUtil.getFileDownloadedPath(containerName, "brandscorecardsimulation_bs_roadster_1.json")
  val filePath2: String = feederUtil.getFileDownloadedPath(containerName, "brandscorecardsimulation_bs_roadster_2.json")
  val filePath3: String = feederUtil.getFileDownloadedPath(containerName, "brandscorecardsimulation_bs_roadster_3.json")
  val filePath4: String = feederUtil.getFileDownloadedPath(containerName, "brandscorecardsimulation_bs_roadster_4.json")



  private val httpProtocol = http
    .baseUrl(serviceUrl)
    .header("Accept", "application/json")
    .header("userId", "gaurav.raj@myntra.com")
    .header("Content-Type", "application/json")
    .header("X-Myntra-forceRefreshCache", forceRefresh.toString())
    .disableWarmUp


  private val scn = scenario("BrandScorecardSimulation")
    .exec(getBrandReportMetadata("CAMEY"))
    .pause(2 seconds)
    .exec(getBrandReportMetadata("ROADSTER"))
    .pause(2)
    .exec(getBrandScorecard(filePath1))
    .pause(4)
    .exec(getBrandScorecard(filePath2))
    .pause(4)
    .exec(getBrandScorecard(filePath3))
    .pause(4)
    .exec(getBrandScorecard(filePath4))
    .pause(4)

  setUp(scenarios map (e => e.inject(step)) toList)
    .maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}

