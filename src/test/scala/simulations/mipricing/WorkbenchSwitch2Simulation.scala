package simulations.mipricing

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import MiPricingUtils._
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import simulations.BaseSimulation

class WorkbenchSwitch2Simulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the conatiner Name as shown on ui , url: https://scaleit.myntra.com/feeder */
  val containerName = "vorta"

  val filePath2: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0002_request.json")
  val filePath13: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0013_request.json")
  val filePath14: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0014_request.json")
  val filePath15: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0015_request.json")
  val filePath16: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0016_request.json")
  val filePath17: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0017_request.json")
  val filePath19: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0019_request.json")
  val filePath20: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0020_request.json")
  val filePath22: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0022_request.json")
  val filePath23: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0023_request.json")
  val filePath24: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0024_request.json")
  val filePath25: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0025_request.json")
  val filePath26: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0026_request.json")
  val filePath27: String = feederUtil.getFileDownloadedPath(containerName, "workbenchswitchsimulation2_0027_request.json")


  private val serviceUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MIPRICING.toString)

  def forceRefresh: Boolean = getProperty("CACHE_REFRESH", "false").toBoolean

  private val httpProtocol = http
    .baseUrl(serviceUrl)
    .header("Accept", "application/json")
    .header("userId", "gaurav.raj@myntra.com")
    .header("Content-Type", "application/json")
    .header("X-Myntra-forceRefreshCache", forceRefresh.toString())
    .disableWarmUp

  private val scn = scenario("Workbench Switch Scenario 2")
    .exec(getBrandsMetadata(filePath2))
    .exec(getCategoryMetadata("Men-Tshirts"))
    .pause(500 milliseconds)
    .exec(getGraphData(filePath13))
    .exec(getGraphData(filePath14))
    .exec(getGraphData(filePath15))
    .exec(getGraphData(filePath16))
    .exec(getGraphData(filePath17))
    .exec(getGraphData(filePath19))
    .exec(getGraphData(filePath20))
    .exec(getGraphData(filePath22))
    .exec(getGraphData(filePath23))
    .exec(getGraphData(filePath24))
    .exec(getGraphData(filePath25))
    .exec(getGraphData(filePath26))
    .exec(getGraphData(filePath27))
    .pause(4)

  setUp(scenarios map (e => e.inject(step)) toList)
    .maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}
