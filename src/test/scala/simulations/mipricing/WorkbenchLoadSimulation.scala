package simulations.mipricing

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import MiPricingUtils._
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import simulations.BaseSimulation

class WorkbenchLoadSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "vorta"

  private val serviceUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MIPRICING.toString)

  def forceRefresh: Boolean = getProperty("CACHE_REFRESH", "false").toBoolean

  val filePath8: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0008_request.json")
  val filePath10: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0010_request.json")
  val filePath29: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0029_request.json")
  val filePath30: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0030_request.json")
  val filePath31: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0031_request.json")
  val filePath32: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0032_request.json")
  val filePath33: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0033_request.json")
  val filePath34: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0034_request.json")
  val filePath35: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0035_request.json")
  val filePath36: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0036_request.json")
  val filePath37: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0037_request.json")
  val filePath38: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0038_request.json")
  val filePath39: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0039_request.json")
  val filePath40: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0040_request.json")
  val filePath41: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0041_request.json")
  val filePath42: String = feederUtil.getFileDownloadedPath(containerName, "workbenchloadsimulation_0042_request.json")

  private val httpProtocol = http
    .baseUrl(serviceUrl)
    .header("Accept", "application/json")
    .header("userId", "mi_partner_2@myntra.com")
    .header("Content-Type", "application/json")
    .header("X-Myntra-forceRefreshCache", forceRefresh.toString())
    .disableWarmUp

  private val scn = scenario("Workbench Load Scenario")
    .exec(getWorkspaceFilters())
    .exec(getSystemDashboardFilters())
    .exec(getBrandsMetadata(filePath10))
    .exec(getDashboards("SYSTEM", filePath8))
    .pause(2 seconds)
    .exec(getCategoryMetadata("Women-Dresses"))
    .pause(1 second)
    .exec(getGraphData(filePath29))
    .exec(getGraphData(filePath30))
    .exec(getGraphData(filePath31))
    .exec(getGraphData(filePath32))
    .exec(getGraphData(filePath33))
    .exec(getGraphData(filePath34))
    .exec(getGraphData(filePath35))
    .exec(getGraphData(filePath36))
    .exec(getGraphData(filePath37))
    .exec(getGraphData(filePath38))
    .exec(getGraphData(filePath39))
    .exec(getGraphData(filePath40))
    .exec(getGraphData(filePath41))
    .exec(getGraphData(filePath42))
    .pause(2)

  setUp(scenarios map (e => e.inject(step)) toList)
    .maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}

