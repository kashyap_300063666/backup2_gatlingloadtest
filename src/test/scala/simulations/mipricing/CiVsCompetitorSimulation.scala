package simulations.mipricing

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}

import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import simulations.mipricing.MiPricingUtils.getCiData

class CiVsCompetitorSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "vorta"
  /* Name of File to be downloaded */
  val filePath2: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0002_request.json")
  val filePath3: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0003_request.json")
  val filePath11: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0011_request.json")
  val filePath13: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0013_request.json")
  val filePath14: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0014_request.json")
  val filePath15: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0015_request.json")
  val filePath16: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0016_request.json")
  val filePath17: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0017_request.json")
  val filePath18: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0018_request.json")
  val filePath19: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0019_request.json")
  val filePath20: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0020_request.json")
  val filePath21: String = feederUtil.getFileDownloadedPath(containerName, "consumerinsightssimulation_0021_request.json")


  private val serviceUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MIPRICING.toString)

  def forceRefresh: Boolean = getProperty("CACHE_REFRESH", "false").toBoolean

  private val httpProtocol = http
    .baseUrl(serviceUrl)
    .header("Accept", "application/json")
    .header("userId", "mi_partner_2@myntra.com")
    .header("tenantId", "spectrum")
    .header("Content-Type", "application/json")
    .header("X-Myntra-forceRefreshCache", forceRefresh.toString())
    .disableWarmUp

  val scn = scenario("Ci RR and Competitor Analysis")
    .exec(getCiData(filePath2))
    .exec(getCiData(filePath3))
    .pause(2 seconds)
    .exec(getCiData(filePath11))
    .exec(getCiData(filePath13))
    .exec(getCiData(filePath14))
    .exec(getCiData(filePath15))
    .exec(getCiData(filePath16))
    .exec(getCiData(filePath17))
    .exec(getCiData(filePath18))
    .exec(getCiData(filePath19))
    .exec(getCiData(filePath20))
    .exec(getCiData(filePath21))


  setUp(scenarios map (e => e.inject(step)) toList)
    .maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}
