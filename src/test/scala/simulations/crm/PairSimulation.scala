package simulations.crm

import simulations.BaseSimulation
import com.myntra.commons.ServiceType
import io.gatling.core.Predef.configuration
import io.gatling.core.Predef.findCheckBuilder2ValidatorCheckBuilder
import io.gatling.core.Predef.scenario
import io.gatling.core.Predef.stringToExpression
import io.gatling.core.Predef.value2Expression
import io.gatling.core.Predef._
import io.gatling.http.Predef.status
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}

import scala.io.Source

class PairSimulation extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "crm"
  /* Name of File to be downloaded */
  val fileNamePhoneList = "phoneList.txt"
  val fileNameOrderIds = "order_ids.txt"
  val fileNameReturnIds = "return_ids.txt"
  val fileNameItemids = "item_ids.txt"
  val fileNamePacketIds = "packet_ids.txt"




  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromFile(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }

  
  //val phoneFileLocation = System.getProperty("phoneFileLocation", "test-data/phoneList.txt").toString
  val phoneFileLocation = System.getProperty("phoneFileLocation", fileNamePhoneList).toString
  val filePathPhoneList: String = feederUtil.getFileDownloadedPath(containerName, phoneFileLocation)

  //val orderIdsFileLocation = System.getProperty("phoneFileLocation", "test-data/order_ids.txt").toString
  val orderIdsFileLocation = System.getProperty("phoneFileLocation", fileNameOrderIds).toString
  val filePathOrderIds: String = feederUtil.getFileDownloadedPath(containerName, orderIdsFileLocation)

  //val returnIdsFileLocation = System.getProperty("phoneFileLocation", "test-data/return_ids.txt").toString
  val returnIdsFileLocation = System.getProperty("phoneFileLocation", fileNameReturnIds).toString
  val filePathReturnIds: String = feederUtil.getFileDownloadedPath(containerName, returnIdsFileLocation)

  //val itemIdsFileLocation = System.getProperty("phoneFileLocation", "test-data/item_ids.txt").toString
  val itemIdsFileLocation = System.getProperty("phoneFileLocation", fileNameItemids).toString
  val filePathItemids: String = feederUtil.getFileDownloadedPath(containerName, itemIdsFileLocation)

  //val packetIdsFileLocation = System.getProperty("phoneFileLocation", "test-data/packet_ids.txt").toString
  val packetIdsFileLocation = System.getProperty("phoneFileLocation", fileNamePacketIds).toString
  val filePathPacketIds: String = feederUtil.getFileDownloadedPath(containerName, packetIdsFileLocation)

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.PAIR.toString)
  private val pairServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp

  val phoneLines = readFile(filePathPhoneList)
  val orderIdLines = readFile(filePathOrderIds)
  val returnIdLines = readFile(filePathReturnIds)
  val itemIdLines = readFile(filePathItemids)
  val packetIdLines = readFile(filePathPacketIds)

  private val poheaders = Map("cache-control" -> "no-cache", "accept" -> "application/json", "Authorization" -> "Basic Og==", "Content-Type" -> "application/json")
  val r = scala.util.Random

  private val ivrWorkflowServiceLoad =
    scenario("pair ivr workflow service")
      .exec(session => session.set("phone", phoneLines(r.nextInt(2000))))
      .exec(http("pair ivr workflow service")
        .get("/myntra-pair-service/workflow/2297/phone/${phone}/ivr/1")
        .headers(headers)
        .headers(poheaders)
        .check(status.is(200))
      )

  private val saOrderWorkflowServiceLoad =
    scenario("pair sa order workflow service")
      .exec(session => session.set("orderId", orderIdLines(r.nextInt(10000))))
      .exec(http("pair sa order workflow service")
        .get("/myntra-pair-service/v2/workflow/2297/orderId/${orderId}/sa/orderView/1")
        .headers(headers)
        .headers(poheaders)
        .check(status.is(200))
      )

  private val saReturnWorkflowServiceLoad =
    scenario("pair sa return workflow service")
      .exec(session => session.set("returnId", returnIdLines(r.nextInt(10000))))
      .exec(http("pair sa return workflow service")
        .get("/myntra-pair-service/v2/workflow/2297/returnId/${returnId}/sa/returnView/1")
        .headers(headers)
        .headers(poheaders)
        .check(status.is(200))
      )

  private val saItemWorkflowServiceLoad =
    scenario("pair sa item workflow service")
      .exec(session => session.set("itemId", itemIdLines(r.nextInt(10000))))
      .exec(http("pair sa item workflow service")
        .get("/myntra-pair-service/v2/workflow/2297/itemId/${itemId}/sa/itemView/1")
        .headers(headers)
        .headers(poheaders)
        .check(status.is(200))
      )

  private val saPacketWorkflowServiceLoad =
    scenario("pair sa packet workflow service")
      .exec(session => session.set("packetId", packetIdLines(r.nextInt(10000))))
      .exec(http("pair sa packet workflow service")
        .get("/myntra-pair-service/v2/workflow/2297/packetId/${packetId}/sa/packetView/1")
        .headers(headers)
        .headers(poheaders)
        .check(status.is(200))
      )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(pairServiceBaseUrl)

}