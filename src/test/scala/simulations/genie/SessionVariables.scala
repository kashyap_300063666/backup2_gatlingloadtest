package simulations.genie

object SessionVariables {
  val PICKLIST_BARCODE = "PicklistBarcode"
  val SELLER_PID = "SellerPId"
  val WH_ID = "WhId"
  val CONSOLIDATION_GROUP_KEYS = "ConsolidationGroupKeys"
  val SKU_ID = "SkuId"
  val SELLER_PACKET_ITEMS_BY_SELLER_PACKET = "SellerPacketItemsBySellerPacket"
  val SELLER_PACKET_AND_ITEM_TUPLE_LIST = "SellerPacketAndItemTupleList"
  val SELLER_PACKET_AND_ITEM_TUPLE_LIST_SIZE = "SellerPacketAndItemTupleListSize"
  val CURRENT_SELLER_PACKET_IDX = "CurrentSellerPacketIdx"
}
