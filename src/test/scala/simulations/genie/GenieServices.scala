package simulations.genie

import java.util.concurrent.TimeUnit

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.genie.GenieUtil._
import simulations.genie.SessionVariables._

import scala.concurrent.duration.FiniteDuration

object GenieServices {


  val createPicklistWithCriteria = {
    exec(http("CreatePicklistWithCriteria")
      .put("/genie-service/genie/v2/orderPicklist/createWithCriteria")
      .requestTimeout(FiniteDuration(100000, TimeUnit.MILLISECONDS))
      .headers(serviceHeaders)
      .body(StringBody(session =>
        """{"quantity": 30, "orderType": "BOTH", "sellerPartnerId": """ + getSessionValAsStr(session, SELLER_PID) +
          """ "warehouseId": """ + getSessionValAsStr(session, WH_ID) +
          """ "storePartnerIds": [ 2297], "priority": false}""".stripMargin)
      )
      .asJson
      .check(status.is(200))
      .check(jsonPath("$.status.statusType").is("SUCCESS"))
    )
  }

  val createPicklistWithOrders = {
    exec(http("CreatePicklistWithOrders")
      .put("/genie-service/genie/v2/orderPicklist/createWithOrders")
      .requestTimeout(FiniteDuration(100000, TimeUnit.MILLISECONDS))
      .headers(serviceHeaders)
      .body(StringBody(session =>
        """{"uniqueGroupKeys":[""" + getSessionValAsStr(session, CONSOLIDATION_GROUP_KEYS) + """ ]""" +
          ""","sellerPartnerId":""" + getSessionValAsStr(session, SELLER_PID) +
          ""","warehouseId":""" + getSessionValAsStr(session, WH_ID) +
          ""","priority": false}""".stripMargin)
      )
      .asJson
      .check(status.is(200))
      .check(jsonPath("$.status.statusType").is("SUCCESS"))
      .check(jsonPath("$.data[0].barcode").saveAs(PICKLIST_BARCODE))
    )
  }

  val searchCreatedOrdersWithSameSkus = {
    exec(http("SearchCreatedOrdersWithSameSku")
      .get("/genie-service/genie/v2/fulfilmentOrder/search")
      .requestTimeout(FiniteDuration(100000, TimeUnit.MILLISECONDS))
      .queryParamMap(
        Map("q" -> ("sellerPartnerId.eq:${" + SELLER_PID + "}___sourceWarehouseId.eq:${" + WH_ID + "}___skuId.eq:${" + SKU_ID + "}___status.eq:CREATED"),
          "fetchSize" -> "30",
          "f" -> "orderId,sellerPartnerId,sourceWarehouseId"
        )
      )
      .headers(serviceHeaders)
      .asJson
      .check(status.is(200))
      .check(jsonPath("$.status.statusType").is("SUCCESS"))
      .check(jsonPath("$.data[*].orderId").findAll.transform(strSeq => strSeq.toSet.mkString(",")).notNull.saveAs(CONSOLIDATION_GROUP_KEYS))
      .check(jsonPath("$.data[0].sellerPartnerId").notNull.saveAs(SELLER_PID))
      .check(jsonPath("$.data[0].sourceWarehouseId").notNull.saveAs(WH_ID))
    )
  }

  val pickInitiateAndDownload = {
    exec(http("PickInitiateAndDownload")
      .put(session => "/genie-service/genie/v2/orderPicklist/pickInitiateAndGetDocument/" + session(PICKLIST_BARCODE).as[String])
      .requestTimeout(FiniteDuration(100000, TimeUnit.MILLISECONDS))
      .headers(serviceHeaders)
      .check(status.is(200))
    )
  }

  val getPicklistDetails = exec(http("GetPicklistDetails")
    .get(session => "/genie-service/genie/v2/orderPicklist/findOrderPicklistByBarcode/" + session(PICKLIST_BARCODE).as[String])
    .requestTimeout(FiniteDuration(100000, TimeUnit.MILLISECONDS))
    .headers(serviceHeaders)
    .transformResponse((session, response) => transformToActiveSellerPacketWithItems(session, response))
    .check(status.is(200))
    .check(bodyString.saveAs(SELLER_PACKET_ITEMS_BY_SELLER_PACKET))
  )


  val qcDone = {
    repeat("${" + SELLER_PACKET_AND_ITEM_TUPLE_LIST_SIZE + "}", CURRENT_SELLER_PACKET_IDX) {
      exec(http("QcDone")
        .put(BaseUrlConstructor.getBaseUrl(ServiceType.PACKMAN_PLAY.toString) + "/packman-service/packman/sellerpacketitem/markItemsQCPass")
        .body(StringBody(session => {
          val currentSellerPacketAndItems = getCurrentSellerPacketAndItems(session)
          """{"sellerPacketId": """ + currentSellerPacketAndItems._1 + """ ,"clientId": "FulfilmentOrder", "warehouseId": """ +
            session(WH_ID).as[String] + """ ,"packetItems": """ + GenieUtil.toJsonStr(currentSellerPacketAndItems._2) + """}""".stripMargin
        }
        ))
        .asJson
        .requestTimeout(FiniteDuration(100000, TimeUnit.MILLISECONDS))
        .headers(serviceHeaders)
        .check(status.is(200))
      )
    }
  }

  val RTD = {
    repeat("${" + SELLER_PACKET_AND_ITEM_TUPLE_LIST_SIZE + "}", CURRENT_SELLER_PACKET_IDX) {
      exec(session => {
        val currentSellerPacketAndItems = getCurrentSellerPacketAndItems(session)
        session.set("currentSellerPacketId", currentSellerPacketAndItems._1).set("currentSellerPacketItems", currentSellerPacketAndItems._2)
      })
        .exec(http("RTD")
          .put(session => BaseUrlConstructor.getBaseUrl(ServiceType.PACKMAN_PLAY.toString)
            + "/packman-service/packman/sellerpacket/markReadyToDispatch/" + getSessionValAsStr(session, "currentSellerPacketId"))
          .body(StringBody(session => {
            """{"sellerPacketId": """ + getSessionValAsStr(session, "currentSellerPacketId") +
              """ ,"clientId": "FulfilmentOrder","packetItems": """ +
              toJsonStr(session("currentSellerPacketItems").as[Object]) + """}""".stripMargin
          }))
          .asJson
          .requestTimeout(FiniteDuration(100000, TimeUnit.MILLISECONDS))
          .headers(serviceHeaders)
          .check(status.is(200))
        )
    }
  }
}
