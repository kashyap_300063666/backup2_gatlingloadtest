package simulations.genie

import java.nio.charset.Charset
import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import io.gatling.core.Predef.{Session, exec, _}
import io.gatling.http.response.{Response, StringResponseBody}
import io.netty.handler.codec.http.HttpResponseStatus
import simulations.genie.SessionVariables._

import scala.collection.mutable.ListBuffer

object GenieUtil {
  val objectMapper = new ObjectMapper()

  val serviceHeaders = Map("accept" -> "application/json", "content-type" -> "application/json", "Authorization" -> "Basic Z2VuaWVMVDpnZW5pZUxU")

  val getSessionValAsStr: (Session, String) => String = (session, value) => session.attributes.get(value).map(_.toString).orNull
  val getSessionValAsInt: (Session, String) => Int = (session, value) => session.attributes.get(value).map(v => v.toString.toInt).getOrElse(-1)
  val toJsonStr: Object => String = (obj) => objectMapper.writeValueAsString(obj)

  def toObjFromJsonStr[T](json: String, typ: Class[T]): T = objectMapper.readValue(json, typ)

  val transformToActiveSellerPacketWithItems: (Session, Response) => Response = (session, response) => {
    var newResponse = response
    if (response.status.code() == 200) {
      val respBodyJsonStr = response.body.string
      val jsonResponseDataMap: util.Map[String, Object] = toObjFromJsonStr(respBodyJsonStr, classOf[util.Map[String, Object]])
      val activePacketItemsBySellerPacket: util.Map[String, util.List[Object]] = new util.HashMap();
      val statusType = jsonResponseDataMap.get("status").asInstanceOf[util.Map[String, Object]].get("statusType").asInstanceOf[String]
      if (statusType != null && "SUCCESS".equals(statusType)) {
        val responseData = jsonResponseDataMap.get("data").asInstanceOf[util.List[util.Map[String, util.List[util.Map[String, Object]]]]]
        if (responseData != null && responseData.size() > 0) {
          responseData.get(0).get("orderPicklistLines").forEach(picklistLine => {
            if (picklistLine.get("sellerPacketId") != null && picklistLine.get("isActive").asInstanceOf[Boolean].equals(true)) {
              val sellerPacketId = picklistLine.get("sellerPacketId").asInstanceOf[java.lang.Integer].toString
              val foId = picklistLine.get("fulfilmentOrderId").asInstanceOf[java.lang.Integer].toString
              val activePacketItems = activePacketItemsBySellerPacket.getOrDefault(sellerPacketId, new util.ArrayList[Object]())
              val packetItem: java.util.Map[String, String] = new util.HashMap()
              packetItem.put("refId", foId)
              packetItem.put("qcDeskCode", "59")
              packetItem.put("userLogin", "BT113605844")
              activePacketItems.add(packetItem)
              activePacketItemsBySellerPacket.put(sellerPacketId, activePacketItems)
            }
          })
        }
        newResponse = response.copy(body = new StringResponseBody(toJsonStr(activePacketItemsBySellerPacket), Charset.defaultCharset()))
      } else {
        newResponse = response.copy(status = HttpResponseStatus.INTERNAL_SERVER_ERROR)
      }
    }
    newResponse
  }

  val injectSellerPacketAndItemTuplesInSession = {
    exec(session => {
      val packetItemsBySellerPacketJson = getSessionValAsStr(session, SELLER_PACKET_ITEMS_BY_SELLER_PACKET)
      val packetItemsBySellerPacket: util.Map[String, Object] = toObjFromJsonStr(packetItemsBySellerPacketJson, classOf[util.Map[String, Object]])
      val scalaList: ListBuffer[(String, Object)] = ListBuffer()
      packetItemsBySellerPacket.forEach((sellerPacketId, packetItems) => scalaList.append((sellerPacketId, packetItems)))
      session.set(SELLER_PACKET_AND_ITEM_TUPLE_LIST_SIZE, packetItemsBySellerPacket.size()).set(SELLER_PACKET_AND_ITEM_TUPLE_LIST, scalaList)
    }
    )
  }

  val getCurrentSellerPacketAndItems: (Session) => (String, Object) = (session) => {
    val currentSellerPacketIdx = getSessionValAsInt(session, CURRENT_SELLER_PACKET_IDX)
    val sellerPacketAndItems = session(SELLER_PACKET_AND_ITEM_TUPLE_LIST).as[ListBuffer[(String, Object)]]
    sellerPacketAndItems(currentSellerPacketIdx)
  }
}
