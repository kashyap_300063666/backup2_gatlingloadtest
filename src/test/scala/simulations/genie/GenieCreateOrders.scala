package simulations.genie

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import simulations.BaseSimulation

class GenieCreateOrders extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "wms"
  /* Name of File to be downloaded */
  val fileNameEmail = "emailAddress.csv"
  val fileNameCart = "genieCartPayloads.csv"

  val httpConf: HttpProtocolBuilder = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.API.toString)).disableWarmUp

 // val emailCsvFile: String = System.getProperty("emailAddress.csv", "test-data/emailAddress.csv")
  val emailCsvFile: String = System.getProperty("emailAddress.csv", fileNameEmail)
  val filePathEmail: String = feederUtil.getFileDownloadedPath(containerName, emailCsvFile)

  //val cartCsvFile: String = System.getProperty("cartPayloads.csv", "test-data/genieCartPayloads.csv")
  val cartCsvFile: String = System.getProperty("cartPayloads.csv", fileNameCart)
  val filePathCart: String = feederUtil.getFileDownloadedPath(containerName, cartCsvFile)

  val emailAddressFeeder: BatchableFeederBuilder[String]#F = csv(filePathEmail).circular

  val cartPayloadFeeder: BatchableFeederBuilder[String]#F = separatedValues(filePathCart, '?').circular

  private val createOrder =
    scenario("Get User Token and login")
      .feed(emailAddressFeeder)
      .feed(cartPayloadFeeder)
      .exec(http("knuthToken")
        .get("/auth/v1/token")
        .header("UserAgent", "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)")
        .header("clientid", "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61")
        .header("x-meta-app", "appFamily=MyntraRetailAndroid")
        .header("Authorization", "Basic YTpi")
        .header("origin", "myntra.com")
        .check(status.is(200))
        .check(header("At").exists.saveAs("at"))
        .check(header("Xid").exists.saveAs("xid"))
      )
      .doIf(session => session.contains("at") && session.contains("xid")) {
        exec(http("login using gen token ")
          .post("/auth/v1/login")
          .header("at", "${at}")
          .header("xid", "${xid}")
          .header("x-myntra-app", "appFamily=MyntraRetailAndroid")
          .body(StringBody("""{"userId":"${emailId}","accessKey":"${emailId}"}""")).asJson
          .check(status.is(200))
          .check(header("At").exists.saveAs("at"))
        )
          .doIf(session => session.contains("at")) {
            exec(http("Clear Cart")
              .delete("/v1/cart/default/clear")
              .header("at", "${at}")
              .header("Authorization", "Basic YTpi")
              .header("xid", "${xid}")
              .header("x-myntra-app", "appFamily=MyntraRetailAndroid; appVersion=20.2007.2")
              .body(StringBody("""{"userId":"${emailId}","accessKey":"${emailId}"}""")).asJson
              .check(status.is(200))
            )
              .doIf(session => session.contains("at")) {
                exec(http("Add to  Cart")
                  .post("/v1/cart/default/add")
                  .header("at", "${at}")
                  .header("Authorization", "Basic YTpi")
                  .header("xid", "${xid}")
                  .header("x-myntra-app", "appFamily=MyntraRetailAndroid; appVersion=20.2007.2")
                  .body(StringBody("""${cartPayload}""")).asJson
                  .transformResponse((session, response) => {
                    println("Response = " + response.body.string)
                    response
                  })
                  .check(status.is(200))
                  .check(jsonPath("$.id").saveAs("cartId"))
                )
                  .doIf(session => session.contains("at") && session.contains("cartId")) {
                    exec(http("PayNowPps call")
                      .post(BaseUrlConstructor.getBaseUrl(ServiceType.PPSSERVICE.toString) + "/myntra-payment-plan-service/v2/paynowform")
                      .header("Cookie", "at=${at}")
                      .header("UserAgent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36")
                      .formParam("csrf_token", "123456")
                      .formParam("cartContext", "DEFAULT")
                      .formParam("cartId", "${cartId}")
                      .formParam("clientCode", "responsive")
                      .formParam("profile", "www.myntra.com")
                      .formParam("address", "${addressId}")
                      .formParam("email", "${emailId}")
                      .formParam("mobile", "1234567890")
                      .formParam("userGroup", "normal")
                      .formParam("at", "${at}")
                      .formParam("pm", "cod")
                      .formParam("userinputcaptcha", "testcheckout")
                      .transformResponse((session, response) => {
                        println("Response = " + response.body.string)
                        response
                      })
                      .check(status.is(200))
                    )
                  }
              }
          }
      }
  setUp(createOrder.inject(step)).maxDuration(maxDurationInSec).protocols(httpConf)
}