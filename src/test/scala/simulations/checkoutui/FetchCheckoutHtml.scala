package simulations.checkoutui

import io.gatling.core.Predef.configuration
import io.gatling.core.Predef.findCheckBuilder2ValidatorCheckBuilder
import io.gatling.core.Predef.scenario
import io.gatling.core.Predef.stringToExpression
import io.gatling.core.Predef.value2Expression
import io.gatling.http.Predef.http
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import com.myntra.commons.util.BaseUrlConstructor
import scala.io.Source
import io.gatling.http.Predef.{http, status, _}


class FetchCheckoutHtml extends BaseSimulation{
  
  var agentHeader = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36 Edg/81.0.416.77".toString()
  
  val agent = System.getProperty("agent", "firefox").toString

  val url = System.getProperty("baseUrl", "https://www.myntra.com/").toString;

  val cookie = System.getProperty("cookie", "").toString;

  val poHeaders = Map("cache-control" -> "no-cache", "cookie" -> cookie);

  val orderId = System.getProperty("orderid", "").toString;
  
  if (agent.equals("mobile")) {
   agentHeader = "Mozilla/5.0 (Linux; Android 8.0.0; Pixel 2 XL Build/OPD1.170816.004) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36".toString()
  }
  
  val httpProtocol = http
    .baseUrl(url)
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader(agentHeader).disableWarmUp
  
   private val nodeapp = scenario("nodeapp")
      .exec(http("cart")
      .get("/checkout/cart")
      .headers(poHeaders))
      .pause(1)
      .exec(http("address")
      .get("/checkout/address")
      .headers(poHeaders)
      .check(status.is(200)))
      .pause(1)
      .exec(http("payment")
      .get("/checkout/payment")
      .headers(poHeaders)
      .check(status.is(200)))
      .pause(1)
      .exec(http("confirm")
      .get("/checkout/confirm?orderid=" + orderId)
      .headers(poHeaders)
      .check(status.is(200)))
      .pause(1)
 
    
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}