package simulations.charges

import com.github.phisgr.gatling.grpc.Predef.grpc
import io.gatling.core.Predef._
import simulations.BaseSimulation
import io.grpc.ManagedChannelBuilder
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import com.myntra.charge.dto.charge._
import com.myntra.charge.enums.charge_enum.ChargeType
import io.grpc.Metadata

class GiftWrapChargeSimulation extends BaseSimulation{

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "systems"
  /* Name of File to be downloaded */
  val fileName = "charges_giftwrap.csv"
  
  private val baseUrl = System.getProperty("singleServerURL")//Pass the complete url of single server if req
  var grpcConf = grpc(ManagedChannelBuilder.forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.CHARGES.toString, ""), grpcPortNum).usePlaintext())
  
     if(baseUrl != null) {
    grpcConf = grpc(ManagedChannelBuilder.forAddress(baseUrl, grpcPortNum).usePlaintext())
    println("Updated BaseUrl:" + baseUrl)
  }
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
  val styleIdFeeder = csv(filePath).circular
  //val styleIdFeeder = csv("test-data/charges_giftwrap.csv").circular
  
  protected val noOfInputContext = System.getProperty("noOfInputContext", "2").toInt
  
  def seqOfItemInputContext(session:Session) : Seq[ItemInputContext]= {
    var seq:Seq[ItemInputContext] = Seq[ItemInputContext]()
     
    for(i <- 1 to noOfInputContext){
    val eachitem=ItemInputContext( 
       Option(ItemContext.of(
             quantity=Some(1.toInt),
             styleId=Option(session("styleId"+i).as[Long]),
             mrp=Option(2500),
             sellerPartnerId=Option(session("sellerPartnerId"+i).as[Long]),
             discountedPrice=Option(400),
             sellerId=Option(session("sellerId"+i).as[Long]),
             skuId=Option(session("skuId"+i).as[Long]),
             articleTypeId=Option(90),
             tax=Option(90),
             tryNBuyOpted=Option(false) 
         )))
         
         seq = seq :+ eachitem 
      
    }
    seq
 }
  
  val giftwrapcharge = scenario("GiftWrapCharge - Apply charge")
    .feed(styleIdFeeder,noOfInputContext)
    .exec(grpc("GiftWrapCharge - applyCharge")
    .rpc(ChargeServiceGrpc.METHOD_APPLY_CHARGE)
      .payload(session => 
      ChargeInputContext(
            giftWrapOpted =Option(true),
            coverFeeOpted=Option(false),
            cartLevelCharges=Seq(ChargeType.giftwrap),
            totalValueWithBagDiscount=Option(500),
            totalValueWithoutBagDiscount=Option(700),  
            itemDetails=seqOfItemInputContext(session)))
                
    .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))("storeid=2297")
 )
     
      
//  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
//    .protocols(grpcConf)
  setUp(giftwrapcharge.inject(
    nothingFor(5),
    constantUsersPerSec(noOfUsers) during (DurationJLong(getLongProperty("duration.seconds")) seconds)
  ).protocols(grpcConf)
  ).maxDuration(maxDurationInSec)
}