package simulations.coreservice.mordor

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class UserSlots extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "apifydevapi"
  /* Name of File to be downloaded */
  val fileName = "mordorData.csv"

  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MORDOR.toString)
  private val defaultContext = "demo";
  //val csvFile = System.getProperty("testData", "test-data/mordorData.csv").toString
  val csvFile = System.getProperty("testData", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)
  val mordorBaseUrl = System.getProperty("baseUrl", defaultUrl)
  val slotContext = System.getProperty("slotContext", defaultContext)


  val httpProtocol = http
    .baseUrl(mordorBaseUrl)

  //val mordorDataReader = csv(csvFile).circular
  val mordorDataReader = csv(filePath).circular

  private val slot =
    scenario("slot")
      .feed(mordorDataReader)
      .exec(session => {
        session.set("userSlotContext", slotContext)
      })
      .exec(
        //slotApis.assignSlot,
        slotApis.getSlot
        //slotApis.updateSlot

      )

    object slotApis {
      val assignSlot: ChainBuilder = doIf(
        session =>
          session.contains("userSlotContext")
      ) {
        exec(http("assignSlot")
          .post("/admission/context/${userSlotContext}/user")
          .header("m-uidx", "${uidx}")
          .check(status.is(201))
        )
      }


      val updateSlot: ChainBuilder = doIf(
        session =>
          session.contains("userSlotContext") && session.contains("availableSlot")
      ) {
        exec(http("updateSlot")
          .put("/admission/context/${userSlotContext}/user")
          .header("m-uidx", "${uidx}")
          .body(StringBody("""{"id":"${availableSlot}","context":"${userSlotContext}"}""")).asJson
          .check(status.is(200))
        )
      }

      val getSlot: ChainBuilder = doIf(
        session =>
          session.contains("userSlotContext")
      ) {
        exec(http("getSlot")
          .get("/admission/context/${userSlotContext}/user/slots")
          .header("m-uidx", "${uidx}")
          .check(status.is(200))
          //.check(jsonPath("$..availableSlots[?($.assignedSlot.id != @.id && @.isAvailable == true)].id").find(0).saveAs("availableSlot"))
        )
      }
    }
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpProtocol)
}