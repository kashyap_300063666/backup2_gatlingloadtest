package simulations.coreservice.rollouts

import java.util.UUID.randomUUID

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Rollouts extends BaseSimulation {

  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.ROLLOUTS.toString)
  private val rolloutsHeaders = Map("accept-encoding" -> "gzip", "Content-Type" ->"application/json");

  val rolloutsBaseUrl = System.getProperty("baseUrl", defaultUrl)

  val httpProtocol = http
    .baseUrl(rolloutsBaseUrl)

  private val rolloutsDefaultVersion =
    scenario("rolloutDefaultVersion")
      .exec(http("rolloutDefaultVersion")
        .get("packages/android-jsbundle-4.2012.0/111/version")
        .headers(rolloutsHeaders)
        .asJson.check(status.is(200))
      )

  private val rolloutsVersion =
    scenario("rolloutVersion")
      .exec(http("rolloutVersion")
        .get("packages/android-jsbundle-4.2012.0/1.0.0")
        .headers(rolloutsHeaders)
        .asJson.check(status.is(200))
      )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpProtocol)
}