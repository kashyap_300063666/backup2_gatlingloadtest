package simulations.coreservice.gateway

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Address extends BaseSimulation{
  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid;",
    "clientid" -> "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61",
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "at"-> "${at}",
    "m-uidx"-> "${uidx}",
    "x-mynt-ctx"-> "storeid=2297;uidx=${uidx};nidx=${xid}")

  object addressApis {
    val getAddress: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("getAddress")
        .get("/v2/addresses")
        .headers(defaultHeader)
        .check(status.is(200))
        .check(jsonPath("$..addresses[*].id").find(0).saveAs("addressId"))

      )
    }

    val getAddressById: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("GetAddressById")
        .get("/v2/addresses/${addressId}")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val getLocality: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("getLocality")
        .get("/v1/address/locality?pincode=560068")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }
  }
}
