package simulations.coreservice.gateway

import com.myntra.commons.util.FeederUtil
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Pdp extends BaseSimulation{

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "apifydevapi"
  /* Name of File to be downloaded */
  val fileNameV2 = "serviceabilityV2.json"
  val filePathV2: String = feederUtil.getFileDownloadedPath(containerName, fileNameV2)
  val fileNameV3 = "serviceabilityV3.json"
  val filePathV3: String = feederUtil.getFileDownloadedPath(containerName, fileNameV3)


  //val csvFile = System.getProperty("testData", "test-data/PDPstyles.csv").toString


  //val styleData = csv(csvFile).circular

  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid;",
    "clientid" -> "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61",
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "at"-> "${at}",
    "m-uidx"-> "${uidx}",
    "x-mynt-ctx"-> "storeid=2297;uidx=${uidx};nidx=${xid}")

  object pdpApis {
    val pdpV1: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("xid") && session.contains("styleId") && session.contains("uidx")
    ) {
      exec(http("pdpV1")
        .get("/v1/product/${styleId}")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val pdpV2: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") && session.contains("styleId") || session.contains("xid")
    ) {
      exec(http("pdpV2")
        .get("/v2/product/${styleId}")
        .headers(defaultHeader)
        .check(status.is(200))
        .check(jsonPath("$..sizes[?(@.available==true)].skuId").find(0).saveAs("skuId"))
        .check(jsonPath("$..sizes[?(@.available==true)].sizeSellerData[?(@.availableCount>=1)].sellerPartnerId").find(0).saveAs("sellerPartnerId"))
      )
    }

    val pdpV2Related: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") && session.contains("styleId") || session.contains("xid")
    ) {
      exec(http("pdpV2Related")
        .get("/v2/product/${styleId}/related")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val pdpV2CrossSell: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") && session.contains("styleId") || session.contains("xid")
    ) {
      exec(http("pdpV2CrossSell")
        .get("/v2/product/${styleId}/cross-sell")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val pdpV2Offers: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") && session.contains("styleId") && session.contains("sellerPartnerId") || session.contains("xid")
    ) {
      exec(http("pdpV2Offers")
        .get("/v2/product/${styleId}/offers/${sellerPartnerId}")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val serviceabilityV2: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    ) {
      exec(http("serviceabilityV2")
        .post("/v2/serviceability/check")
        .headers(defaultHeader)
        .body(ElFileBody(filePathV2)).asJson
        .check(status.is(200))
      )
    }

    val serviceabilityV3: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    ) {
      exec(http("serviceabilityV3")
        .post("/v3/serviceability/check")
        .headers(defaultHeader)
        .body(ElFileBody(filePathV3)).asJson
        .check(status.is(200))
      )
    }

    val getColorShades: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") && session.contains("styleId") || session.contains("xid")
    ) {
      exec(http("GetColorShades")
        .get("/v2/product/${styleId}/getColorShades")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    //todo
    val productList: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") && session.contains("styleId") || session.contains("xid")
    ) {
      exec(http("ProductList")
        .post("/v1/product/list")
        .body(StringBody("""{"styleIds":[${styleId}]""")).asJson
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }
  }
}
