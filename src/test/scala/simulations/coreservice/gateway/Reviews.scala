package simulations.coreservice.gateway

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Reviews extends BaseSimulation{
  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid;",
    "clientid" -> "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61",
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "at"-> "${at}",
    "m-uidx"-> "${uidx}",
    "x-mynt-ctx"-> "storeid=2297;uidx=${uidx};nidx=${xid}")

  object reviewsApis {
    val getReviewImagesByStyle: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("GetReviewImagesByStyle")
        .get("/v1/reviews/product/${styleId}/images?page=1&size=10")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val getReviewsByStyle: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("GetReviewsByStyle")
        .get("/v1/reviews/product/${styleId}?size=10&sort=1&rating=0&page=1&includeMetaData=true")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val getReviewsByUser: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("GetReviewsByUser")
        .get("/v1/reviews/user?page=1&size=10")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }
  }
}
