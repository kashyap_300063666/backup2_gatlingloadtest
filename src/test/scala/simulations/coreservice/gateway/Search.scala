package simulations.coreservice.gateway

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Search extends BaseSimulation {

  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid;",
    "clientid" -> "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61",
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "at" -> "${at}",
    "m-uidx" -> "${uidx}",
    "x-mynt-ctx" -> "storeid=2297;uidx=${uidx};nidx=${xid}")

  object searchApis {
    val searchV1: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") || session.contains("xid")
    ) {
      exec(http("SearchV1")
        .get("/v1/search/nike?rows=48")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val searchV2: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("xid") && session.contains("uidx")
    ) {
      exec(http("SearchV2")
        .get("/v2/search/nike?rows=48")
        .headers(defaultHeader)
        .check(status.is(200))
        .check(jsonPath("$..products[?(@.inventoryInfo[?(@.available == true)])].productId").find(0).saveAs("styleId"))
      )
    }

    val autoSuggest: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("xid") && session.contains("uidx")
    ) {
      exec(http("AutoSuggest")
        .get("/v1/search/autosuggest?q=nike")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }
  }
}
