package simulations.coreservice.gateway

import com.myntra.commons.util.FeederUtil
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Cart extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "apifydevapi"
  /* Name of File to be downloaded */
  val fileNameCartfiller = "cartFiller.json"
  val filePathCartfiller: String = feederUtil.getFileDownloadedPath(containerName, fileNameCartfiller)
  val fileNameApplycoupon = "applyCoupon.json"
  val filePathApplycoupon: String = feederUtil.getFileDownloadedPath(containerName, fileNameApplycoupon)
  val fileNameApplGiftWrap = "applyGiftWrap.json"
  val filePathApplGiftWrap: String = feederUtil.getFileDownloadedPath(containerName, fileNameApplGiftWrap)

  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid;",
    "clientid" -> "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61",
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "at"-> "${at}",
    "m-uidx"-> "${uidx}",
    "x-mynt-ctx"-> "storeid=2297;uidx=${uidx};nidx=${xid}")

  object cartApis {

    val clearCart: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("ClearCart")
        .delete("/v1/cart/default/clear")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val resolveConflict: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("ResolveConflict")
        .post("/v1/cart/default/resolveConflict")
        .headers(defaultHeader)
        .check(status.is(200))
      )

    }

    val getCart: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("GetCart")
        .get("/v1/cart/default?cm=true")
        .headers(defaultHeader)
        .check(status.is(200))
      )

    }

    val addCart: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") && session.contains("styleId") && session.contains("skuId") && session.contains("sellerPartnerId") || session.contains("xid")
    )
    {
      exec(http("AddToCart")
        .post("/v1/cart/default/add")
        .headers(defaultHeader)
        .body(StringBody("""{"id":${styleId}, "skuId": ${skuId}, "quantity":1, "sellerPartnerId": ${sellerPartnerId}}""".stripMargin)).asJson
        .check(status.is(200))
        .check(jsonPath("$.id").find(0).saveAs("cartId"))
      )

    }
    val cartSummary: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("CartSummary")
        .get("/v1/cart/default/summary")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val cartFiller: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("CartFiller")
        .post("/v1/cart/cartFiller")
        .headers(defaultHeader)
        .body(ElFileBody(filePathCartfiller)).asJson
        .check(status.is(200))
      )
    }

    val applyGiftWrap: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("ApplyGiftWrap")
        .put("/v1/cart/default/giftwrap/apply")
        .headers(defaultHeader)
        .body(ElFileBody(filePathApplGiftWrap)).asJson
        .check(status.is(200))
      )
    }

    val removeGiftWrap: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("RemoveGiftWrap")
        .put("/v1/cart/default/giftwrap/remove")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val applyCoverFee: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("ApplyCoverFee")
        .post("/v1/cart/default/coverfee/apply")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val removeCoverFee: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("RemoveCoverFee")
        .post("/v1/cart/default/coverfee/apply")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val applyLoyaltyPoints: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("ApplyLoyaltyPoints")
        .put("/v1/cart/default/loyaltypoints/apply")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val removeLoyaltyPoints: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("RemoveLoyaltyPoints")
        .put("/v1/cart/default/loyaltypoints/apply")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val cartServiceability: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("CartServiceability")
        .get("/v1/cart/default?pincode=560068&serviceability=true")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val applyCoupon: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("ApplyCoupon")
        .put("/v1/cart/default/coupon/apply")
        .headers(defaultHeader)
        .body(ElFileBody(filePathApplycoupon)).asJson
        .check(status.is(200))
      )
    }

    val removeCoupon: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("RemoveCoupon")
        .put("/v1/cart/default/coupon/remove")
        .headers(defaultHeader)
        .body(ElFileBody(filePathApplycoupon)).asJson
        .check(status.is(200))
      )
    }

    val applyGiftCard: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("ApplyLoyaltyPoints")
        .put("/v1/cart/default/giftcard/apply")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val removeGiftCard: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("RemoveLoyaltyPoints")
        .put("/v1/cart/default/giftcard/remove")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val applyShippingSdd: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("applyShippingSdd")
        .put("/v1/cart/default/shipping/sdd/apply")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val removeShippingSdd: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("RemoveShippingSdd")
        .put("/v1/cart/default/shipping/sdd/remove")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val applyShippingNormal: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("ApplyShippingNormal")
        .put("/v1/cart/default/shipping/normal/apply")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val removeShippingNormal: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("RemoveShippingNormal")
        .put("/v1/cart/default/shipping/normal/remove")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val applyValueShippingNormal: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("ApplyValueShippingNormal")
        .put("/v1/cart/default/shipping/valueshipping/apply")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val removeValueShippingNormal: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("RemoveValueShippingNormal")
        .put("/v1/cart/default/shipping/valueshipping/remove")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val applyExpressShippingNormal: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("ApplyExpressShippingNormal")
        .put("/v1/cart/default/shipping/expressshipping/apply")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val removeExpressShippingNormal: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx")  || session.contains("xid")
    )
    {
      exec(http("RemoveExpressShippingNormal")
        .put("/v1/cart/default/shipping/expressshipping/remove")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }
  }
}
