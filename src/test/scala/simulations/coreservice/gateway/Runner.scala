package simulations.coreservice.gateway

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random

class Runner extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "apifydevapi"
  /* Name of File to be downloaded */
  val fileName = "styleids.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.API.toString)
  val baseUrl = System.getProperty("baseUrl", defaultUrl)

  val styleDataFeeder = csv(filePath).random

  val httpProtocol = http
    .baseUrl(baseUrl)

  val count: Int = 1599999

  private val authApis = new Auth().authApis
  private val biroApis = new Biro().biroApis
  private val lgpApis = new Lgp().biroApis
  private val searchApis = new Search().searchApis
  private val pdpApis = new Pdp().pdpApis
  private val cartApis = new Cart().cartApis
  private val addressApis = new Address().addressApis
  private val notificationApis = new Notification().notificationApis
  private val giftCardApis = new GiftCard().giftCardApis
  private val userApis = new Users().usersApis
  private val wishlistApis = new Wishlist().wishlistApis
  private val orderApis = new Orders().orderApis
  private val ratingApis = new Ratings().ratingApis
  private val reviewApis = new Reviews().reviewsApis
  private val abTestApis = new Abtest().abtestApis
  private val mliveTestApis = new Mlive().mliveAis

  val testify: ScenarioBuilder = scenario("gatewayAll")
    .exec(session => {
      session.set("emailId", (Random.nextInt(count) + 1).toString + "@myntra360.com")
    })
    .exec(
      authApis.guestTokenApi, authApis.loggedUserApi,
      abTestApis.abtest,
      biroApis.biro,
      lgpApis.streamApi,
      searchApis.searchV1, searchApis.searchV2,
      searchApis.autoSuggest,
      pdpApis.pdpV1,
      pdpApis.pdpV2, pdpApis.pdpV2Related, pdpApis.pdpV2CrossSell, pdpApis.pdpV2Offers,
      pdpApis.serviceabilityV2, pdpApis.serviceabilityV3,
      pdpApis.getColorShades,
      //pdpApis.productList,
      ratingApis.getStyleRating,
      reviewApis.getReviewImagesByStyle,
      reviewApis.getReviewsByStyle,
      reviewApis.getReviewsByUser,
      cartApis.resolveConflict,
      cartApis.getCart, cartApis.addCart,
      cartApis.cartSummary,
      cartApis.applyCoverFee, cartApis.removeCoverFee,
      cartApis.applyGiftWrap, cartApis.removeGiftWrap,
      cartApis.applyLoyaltyPoints, cartApis.removeLoyaltyPoints,
      cartApis.applyCoupon, cartApis.removeCoupon,
      cartApis.applyGiftCard, cartApis.removeGiftCard,
      cartApis.applyShippingSdd, cartApis.removeShippingSdd,
      cartApis.applyShippingNormal, cartApis.removeShippingNormal,
      cartApis.applyValueShippingNormal, cartApis.removeValueShippingNormal,
      cartApis.applyExpressShippingNormal, cartApis.removeValueShippingNormal,
      cartApis.cartServiceability,
      cartApis.cartFiller,
      cartApis.clearCart,
      wishlistApis.getWishlist,
      addressApis.getAddress, addressApis.getAddressById,
      addressApis.getLocality,
      notificationApis.getNotification,
      notificationApis.getNotificationActiveCount,
      notificationApis.getNotificationBeaconCount,
      userApis.getProfile,
      userApis.userAttributes,
      userApis.userLoyaltypoints,
      userApis.userMyntraCreditBalance,
      userApis.userMyntraCreditTransactionLogs,
      userApis.userStatusLA,
      //userApis.userReturnsAll,
      userApis.cityList,
      userApis.userLocationContext,
      userApis.specialCouponSignUp,
      //userApis.skuCode,
      giftCardApis.GetAllGiftCards,
      giftCardApis.GetTransactions,
      orderApis.orders,
      //orderApis.orderById,
      // orderApis.usePacketIsCancellationAllowed,
      orderApis.cancellationReasons,
      orderApis.archivedOrders,
      //orderApis.archivedOrderById
    )
    .exec (session =>{
      println(session)
      session
    })

  val gatewayPdp: ScenarioBuilder = scenario("gatewayPdp")
    .exec(session => {
      session.set("emailId", (Random.nextInt(count) + 1).toString + "@myntra360.com")
    })
    .exec(
      authApis.guestTokenApi, authApis.loggedUserApi,
      pdpApis.pdpV1,
      pdpApis.pdpV2, pdpApis.pdpV2Related, pdpApis.pdpV2CrossSell, pdpApis.pdpV2Offers,
      pdpApis.serviceabilityV2, pdpApis.serviceabilityV3
    )

  val gatewayCart: ScenarioBuilder = scenario("gatewayCart")
    .exec(session => {
      session.set("emailId", (Random.nextInt(count) + 1).toString + "@myntra360.com")
    })
    .exec(
      authApis.guestTokenApi, authApis.loggedUserApi,
      searchApis.searchV2,
      pdpApis.pdpV2,
      cartApis.resolveConflict,
      cartApis.getCart, cartApis.addCart,
      cartApis.cartSummary,
      cartApis.applyCoverFee, cartApis.removeCoverFee,
      cartApis.applyGiftWrap, cartApis.removeGiftWrap,
      cartApis.applyLoyaltyPoints, cartApis.removeLoyaltyPoints,
      cartApis.applyCoupon, cartApis.removeCoupon,
      cartApis.applyGiftCard, cartApis.removeGiftCard,
      cartApis.applyShippingSdd, cartApis.removeShippingSdd,
      cartApis.applyShippingNormal, cartApis.removeShippingNormal,
      cartApis.applyValueShippingNormal, cartApis.removeValueShippingNormal,
      cartApis.applyExpressShippingNormal, cartApis.removeValueShippingNormal,
      cartApis.cartServiceability,
      cartApis.cartFiller,
      cartApis.clearCart
    )

  val gatewayOrder: ScenarioBuilder = scenario("gatewayOrder")
    .exec(session => {
      session.set("emailId", (Random.nextInt(count) + 1).toString + "@myntra360.com")
    })
    .exec(
      authApis.guestTokenApi, authApis.loggedUserApi,
      orderApis.orders, orderApis.orderById,
      // orderApis.usePacketIsCancellationAllowed,
      orderApis.cancellationReasons,
      //orderApis.archivedOrders, orderApis.archivedOrderById
    )

  val mlive: ScenarioBuilder = scenario("mlivePostComment")
    .exec(session => {
      session.set("emailId", (Random.nextInt(count) + 1).toString + "@myntra360.com")
    })
    .exec(
      authApis.guestTokenApi, authApis.loggedUserApi,
      mliveTestApis.postComment
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(httpProtocol)
}
