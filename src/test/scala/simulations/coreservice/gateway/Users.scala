package simulations.coreservice.gateway

import com.myntra.commons.util.FeederUtil
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Users extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "apifydevapi"
  /* Name of File to be downloaded */
  val fileName = "locationContext.json"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid;",
    "clientid" -> "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61",
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "at"-> "${at}",
    "m-uidx"-> "${uidx}",
    "x-mynt-ctx"-> "storeid=2297;uidx=${uidx};nidx=${xid}")

  object usersApis {
    val getProfile: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("getProfile")
        .get("/v1/user/profile")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val userAttributes: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("userAttributes")
        .get("/v1/user/attributes")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val userReturnsAll: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("userReturnsAll")
        .get("/v1/user/return/getall?start=1&fetchSize=8")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val userMyntraCreditBalance: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("userMyntraCreditBalance")
        .get("/v1/user/profile/getMyntraCreditBalance")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val userMyntraCreditTransactionLogs: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("userMyntraCreditTransactionLogs")
        .get("/v1/user/profile/getMyntraCreditTransactionLogs?isExpired=true&offset=1&limit=4")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val userLoyaltypoints: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("userLoyaltypoints")
        .get("/v1/user/profile/loyaltypoints")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val userStatusLA: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("userStatusLA")
        .get("/v1/user/status")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val userLocationContext: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") && session.contains("styleId") && session.contains("skuId") || session.contains("xid")
    )
    {
      exec(http("UserLocationContext")
        .post("/v1/user/locationContext")
        .headers(defaultHeader)
        .body(ElFileBody(filePath)).asJson
        .check(status.is(200))
      )
    }

    val cityList: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("CityList")
        .get("/city/list?search=mum")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val skuCode: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("SkuCode")
        .get("/product/skuCode/MSCSDRSS00002488")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val specialCouponSignUp: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    )
    {
      exec(http("SpecialCouponSignUp")
        .get("/coupons/specialCoupon/signup")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }
  }
}
