package simulations.coreservice.gateway

import com.myntra.commons.util.FeederUtil
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Mlive extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "ilabs"
  /* Name of File to be downloaded */
  val fileName = "livetrak.csv"

  //val csvFile = System.getProperty("livetrakUidxFile", "test-data/livetrak.csv")
  val csvFile = System.getProperty("livetrakUidxFile", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)
  //val csvfeeder = csv(csvFile).circular
  val csvfeeder = csv(filePath).circular

  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid;",
    "clientid" -> "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61",
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "at"-> "${at}",
    "m-uidx"-> "${uuidx}",
    "x-mynt-ctx"-> "storeid=2297;uidx=${uidx};nidx=${unidx}")

  object mliveAis {
    val postComment: ChainBuilder = doIf(
      session =>
        session.contains("at")
    ) {
      feed(csvfeeder)
        .exec(http("mlive_post_comment")
        .post("/v1/mlive/event/${eventid}/useraction")
        .headers(defaultHeader)
        .body(StringBody(
          """{
		    "uidx":"${uuidx}",
		    "actionType":"COMMENT",
		    "data":[{
		        "key":"comment",
		        "value": "${comment}"
		        }]}""")).asJson
        .check(status.is(200))
      )
    }
  }

}
