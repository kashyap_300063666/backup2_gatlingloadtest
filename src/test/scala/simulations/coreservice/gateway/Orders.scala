package simulations.coreservice.gateway

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Orders extends BaseSimulation {

  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid;",
    "clientid" -> "myntra-02d7dec5-8a00-4c74-9cf7-9d62dbea5e61",
    "UserAgent" -> "MyntraRetailAndroid/1.2.1 (Phone, 320dpi)",
    "at"-> "${at}",
    "m-uidx"-> "${uidx}",
    "m-storeid"-> "2297",
    "x-mynt-ctx"-> "storeid=2297;uidx=${uidx};nidx=${xid}")

  object orderApis {
    val orders: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") || session.contains("xid")
    )
    {
      exec(http("GetAllOrders")
        .post("/v2/user/orders")
        .headers(defaultHeader)
        .body(ElFileBody("hlt-test-data/userOrders.json")).asJson
        .check(status.is(200))
       // .check(jsonPath("$..orders[*].storeOrderId").find(0).saveAs("storeOrderId")))
        )
    }

    val orderById: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") && session.contains("storeOrderId") || session.contains("xid")
    )
    {
      exec(http("GetOrderById")
        .post("/v2/user/order/${storeOrderId}")
        .headers(defaultHeader)
        .body(ElFileBody("hlt-test-data/userOrders.json")).asJson
        .check(status.is(200))
      )
    }

    val archivedOrders: ChainBuilder = doIf(
      session =>
        session.contains("at") || session.contains("uidx") && session.contains("xid")
    ) {
      exec(http("GetAllArchivedOrder")
        .post("/v2/user/archivedOrders")
        .headers(defaultHeader)
        .body(ElFileBody("hlt-test-data/userOrders.json")).asJson
        .check(status.is(200))
        //.check(jsonPath("$..orders[*].storeOrderId").find(0).saveAs("archivedStoreOrderId"))
      )
    }

    val archivedOrderById: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") && session.contains("archivedStoreOrderId") ||  session.contains("xid")
    ) {
      exec(http("GetArchivedOrderById")
        .post("/v2/user/archivedOrder/${archivedStoreOrderId}")
        .headers(defaultHeader)
        .body(ElFileBody("hlt-test-data/userOrders.json")).asJson
        .check(status.is(200))
      )
    }

    //todo
    val usePacketIsCancellationAllowed: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") && session.contains("packetId") || session.contains("xid")
    ) {
      exec(http("UsePacketIsCancellationAllowed")
        .get("/v1/user/packet/storePacket/${packetId}/isCancellationAllowed")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }

    val cancellationReasons: ChainBuilder = doIf(
      session =>
        session.contains("at") && session.contains("uidx") || session.contains("xid")
    ) {
      exec(http("CancellationReasons")
        .get("/v1/user/order/getCancellationReasons?context=FULL_ORDER_CANCELLATION")
        .headers(defaultHeader)
        .check(status.is(200))
      )
    }
  }
}
