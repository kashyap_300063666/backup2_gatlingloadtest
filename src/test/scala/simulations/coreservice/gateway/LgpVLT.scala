package simulations.coreservice.gateway

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class LgpVLT extends BaseSimulation {

  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.LEGO.toString)

  private val defaultHeader = Map(
    "Content-Type" -> "application/json",
    "x-meta-app" -> "appFamily=MyntraRetailAndroid;")

  val httpProtocol = http
    .baseUrl(defaultUrl)

  val randomUidxFeeder = Array("8c5c408a.fddf.4648.8247.67045c0eafbaXXho9Mx4DC",
    "7ff8d2b0.9ba2.43f6.8884.0f6a5a50090cOStsv53rA9",
    "c531378f.0a76.4637.892e.4970e36f84a0yqSSyurEb0",
    "b1d02b92.53f2.4e62.82cd.724b06c124e45OBpYpCDRM").map(e => Map("uidx" -> e)).array.random

  //val randomPageNameFeeder = Array("home", "sstest").map(e => Map("pageName" -> e)).array.random

  private val lgpStream =
      scenario("lgp-stream")
      .feed(randomUidxFeeder)
      .exec(http("lgp-stream")
      .get("/lgp/v2.9/stream")
      .headers(defaultHeader)
      .header("x-mynt-ctx", "storeid=2297; uidx=$uidx")
      .check(status.is(200))
      )
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpProtocol)
}
