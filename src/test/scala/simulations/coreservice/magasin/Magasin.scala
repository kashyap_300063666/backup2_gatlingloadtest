package simulations.coreservice.magasin

import java.util.UUID.randomUUID

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class Magasin extends BaseSimulation {

  private val defaultUrl = BaseUrlConstructor.getBaseUrl(ServiceType.MAGASIN.toString)
  private val magasinHeaders = Map("x-magasin-key" -> "43fa61a5cc532a7bf70efb5c0e7c7907");
  //val csvFile = System.getProperty("testData", "test-data/magasinData.csv").toString

  val magasinBaseUrl = System.getProperty("baseUrl", defaultUrl)

  val httpProtocol = http
    .baseUrl(magasinBaseUrl)

  //val magasinDataReader = csv(csvFile).circular
  private val uuidFeeder = Iterator.continually(Map("uuid" -> randomUUID().toString))


  private val magasinServiceLoad =
    scenario("update-magasin")
      .feed(uuidFeeder)
      .exec(http("updateMagasin")
        .put("/magasin/installation/MagasinV2_${uuid}")
        .headers(magasinHeaders)
        .body(StringBody("""{
                    "OSVersion": "Android 5.1 (API 22) Build/LMY47D",
                    "advertisingId": "12bfd756-82d2-4b87-8a9d-24ae1ef18221",
                     "accountsLoggedInFrom": ["MagasinV2_${uuid}"],
                     "appBuildNumber": 80110364,
                     "appIdentifier": "com.myntra.android",
                     "appName": "Myntra",
                     "appVersion": "4.2008.1",
                     "deviceBuildNumber": "vbox86p",
                     "deviceId": "16fcb0befef4963f",
                     "deviceManufacturer": "unknown",
                     "deviceName": "unknown Google",
                     "deviceScreenResolution": "768x1184",
                     "deviceTimeZone": "America/New_York",
                     "deviceType": "android",
                     "gcmSenderId": "787245060481",
                     "installationId": "MagasinV2_${uuid}",
                     "isRooted": true,
                     "limitAdTracking": false,
                     "preBurn": false,
                     "pushType": "fcm",
                     "userAgentString": "MyntraRetailAndroid/4.2008.1-QA-0 (Phone, 320dpi); MyntraAndroid/4.2008.1-QA-0 (Phone, 320dpi)",
                     "userId": "MagasinV2_${uuid}"}"""))
        .asJson.check(status.is(200))
      )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpProtocol)
}