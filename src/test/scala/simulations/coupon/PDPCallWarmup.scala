package simulations.coupon

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class PDPCallWarmup extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "inboundplanningbuying"
  /* Name of File to be downloaded */
  val fileName = "lt_users.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
  val fileNamePdpCall = "PDPCall.csv"
  val filePathPdpCall: String = feederUtil.getFileDownloadedPath(containerName, fileNamePdpCall)

  val couponBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PP_COUPON_SERVICE.toString)).disableWarmUp
  val couponheaders = Map ("accept" -> "application/json", "Content-Type" -> "application/json")
  val couponpdpFeeder = csv(filePathPdpCall).random
  //val uidxFeeder = csv("test-data/lt_users.csv").circular
  val uidxFeeder = csv(filePath).circular

  val clickforoffer = scenario("Click For Offer")
    .feed(couponpdpFeeder)
    .feed(uidxFeeder)
    .exec(http("click for offer")
      .get("/coupons/pdp/${styleId}/${sellerId}")
      .header("x-mynt-ctx" , "storeid=2297;uidx="+"${login}")
      .headers(couponheaders)
      .check(status.is(200))
    )
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(couponBaseUrl)

}
