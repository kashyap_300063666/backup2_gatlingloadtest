package simulations.coupon

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class ApplyCouponWith20StyleFalse extends BaseSimulation{

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "scmfinancialplanning"
  /* Name of File to be downloaded */
  val fileName = "ApplyCoupon20.csv"
  val fileName1 = "ApplyCoupon20Style.json"

  val applyCouponData = System.getProperty("applyCouponData", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, applyCouponData)
  val filePath1: String = feederUtil.getFileDownloadedPath(containerName, fileName1)

  val couponpdpFeeder= csv(filePath).circular

  val couponBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PP_COUPON_SERVICE.toString)).disableWarmUp
  val couponheaders = Map ("accept" -> "application/json", "Content-Type" -> "application/json", "x-mynt-ctx" -> "storeid=2297")
  /*val applyCouponData = System.getProperty("applyCouponData", "ApplyCoupon20.csv").toString
  val couponpdpFeeder = csv("test-data/"+applyCouponData).circular*/

  val clickforoffer = scenario("Apply coupon 20 style with fetchbestcombination false")
    .feed(couponpdpFeeder)
    .exec(http("Apply Coupon With FetchBestCombination True")
      .put("/coupons/myntcoupon/apply")
      .headers(couponheaders)
      .body(ElFileBody(filePath1)).asJson
      .check(status.is(200))
    )
  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(couponBaseUrl)

}
