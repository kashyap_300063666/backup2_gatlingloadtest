package simulations.referral

import com.myntra.commons.util.BaseUrlConstructor
import com.myntra.commons.ServiceType
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class ReferralLoadTestGETAPIs extends BaseSimulation {

  val httpConf = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.REFERRAL.toString)).disableWarmUp


  val GetReferralCode = scenario("1. Get Referral Code")
    .exec(http("1. Get Referral Code")
      .get("/referralcode/71ad1d5a.1334.4090.bbc4.1d2b7133af34995R3SaxIz")
      .headers(headers)
      .check(status.is(200))
      .check(jsonPath("$..success").ofType[Boolean].is(true))
    )

  val GetrewardsbyUDIX = scenario("4. Get rewards by UDIX  *CRITICAL*")
    .exec(http("4. Get rewards by UDIX  *CRITICAL*")
      .get("/rewards/526a78d7.e9e9.4760.a8e0.06247305b0fc9Dqw8qETJG")
      .header("x-mynt-ctx", "storeid=2297")
      .headers(headers)
      .check(status.is(200))
      .check(jsonPath("$..success").ofType[Boolean].is(true))
    )


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpConf)
}