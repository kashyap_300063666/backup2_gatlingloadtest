package simulations.atp

import io.gatling.http.Predef.{http, status}
import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.Console.println


class AtpUtil5 {

  val atpHeaders = Map(
    "accept" -> "application/json",
    "content-type"-> "application/xml",
    "Authorization"->"Basic YWJjOjEyMw=="
  )

  object AtpApis {



    val portalInventory = {
      exec(http("portalInventory")
        .put("/myntra-atp-service/atpV2/inventory/v2/portalInventory/v2")
        .headers(atpHeaders)
        .body(StringBody("""<?xml version="1.0" encoding="UTF-8"?>
      		<portalListingInventoryRequest>
      		   <data>
      		      <portalListingInventory>
      		         <sellerPartnerId>${sellerId1}</sellerPartnerId>
      		         <skuId>${skuId1}</skuId>
      		         <storePartnerId>2297</storePartnerId>
      		      </portalListingInventory>
      		      <portalListingInventory>
                     <sellerPartnerId>${sellerId2}</sellerPartnerId>
                     <skuId>${skuId2}</skuId>
                     <storePartnerId>2297</storePartnerId>
                  </portalListingInventory>
                  <portalListingInventory>
                     <sellerPartnerId>${sellerId3}</sellerPartnerId>
                     <skuId>${skuId3}</skuId>
                     <storePartnerId>2297</storePartnerId>
                  </portalListingInventory>
                   <portalListingInventory>
                     <sellerPartnerId>${sellerId4}</sellerPartnerId>
                     <skuId>${skuId4}</skuId>
                     <storePartnerId>2297</storePartnerId>
                  </portalListingInventory>
                   <portalListingInventory>
                     <sellerPartnerId>${sellerId5}</sellerPartnerId>
                     <skuId>${skuId5}</skuId>
                     <storePartnerId>2297</storePartnerId>
                  </portalListingInventory>
      		   </data>
      		</portalListingInventoryRequest>""")).asXml
        .check(status.is(200))
      )
    }
    val orderInventory ={
      exec(http("orderInventory")
        .put("/myntra-atp-service/atpV2/inventory/v2/orderInventory/")
        .basicAuth("22", "22")
        .headers(atpHeaders)
        .body(StringBody("""<?xml version="1.0" encoding="UTF-8"?>
      		<orderListingInventoryRequest>
      		   <data>
      		      <orderListingInventory>
      		         <sellerPartnerId>${sellerId1}</sellerPartnerId>
      		         <skuId>${skuId1}</skuId>
      		         <storePartnerId>2297</storePartnerId>
      		         <supplyType>ON_HAND</supplyType>
      		      </orderListingInventory>
      		      <orderListingInventory>
                     <sellerPartnerId>${sellerId2}</sellerPartnerId>
                     <skuId>${skuId2}</skuId>
                     <storePartnerId>2297</storePartnerId>
                     <supplyType>ON_HAND</supplyType>
                  </orderListingInventory>
                  <orderListingInventory>
                     <sellerPartnerId>${sellerId3}</sellerPartnerId>
                     <skuId>${skuId3}</skuId>
                     <storePartnerId>2297</storePartnerId>
                     <supplyType>ON_HAND</supplyType>
              </orderListingInventory>
              <orderListingInventory>
                     <sellerPartnerId>${sellerId4}</sellerPartnerId>
                     <skuId>${skuId4}</skuId>
                     <storePartnerId>2297</storePartnerId>
                     <supplyType>ON_HAND</supplyType>
              </orderListingInventory>
              <orderListingInventory>
                     <sellerPartnerId>${sellerId5}</sellerPartnerId>
                     <skuId>${skuId5}</skuId>
                     <storePartnerId>2297</storePartnerId>
                     <supplyType>ON_HAND</supplyType>
              </orderListingInventory>
      		   </data>
      		</orderListingInventoryRequest>
      		""")).asXml
        .check(status.is(200))
      )
    }

    val blockInventory = {
      exec(http("blockInventory")
        .put("/myntra-atp-service/atpV2/inventory/v2/blockInventory")
        .basicAuth("qq", "11")
        .headers(atpHeaders)
        .body(StringBody("""<blockListingInventoryRequest>
      		   <data>
      		       <blockListingInventory>
      		          <quantity>1</quantity>
      		          <sellerPartnerId>${sellerId1}</sellerPartnerId>
      		          <skuId>${skuId1}</skuId>
      		          <storePartnerId>2297</storePartnerId>
      		          <supplyType>ON_HAND</supplyType>
      		       </blockListingInventory>
      		       <blockListingInventory>
                        <quantity>1</quantity>
                        <sellerPartnerId>${sellerId2}</sellerPartnerId>
                        <skuId>${skuId2}</skuId>
                        <storePartnerId>2297</storePartnerId>
                        <supplyType>ON_HAND</supplyType>
                    </blockListingInventory>
                    <blockListingInventory>
                        <quantity>1</quantity>
                        <sellerPartnerId>${sellerId3}</sellerPartnerId>
                        <skuId>${skuId3}</skuId>
                        <storePartnerId>2297</storePartnerId>
                        <supplyType>ON_HAND</supplyType>
                    </blockListingInventory>
                    <blockListingInventory>
                        <quantity>1</quantity>
                        <sellerPartnerId>${sellerId4}</sellerPartnerId>
                        <skuId>${skuId4}</skuId>
                        <storePartnerId>2297</storePartnerId>
                        <supplyType>ON_HAND</supplyType>
                    </blockListingInventory>
                    <blockListingInventory>
                        <quantity>1</quantity>
                        <sellerPartnerId>${sellerId5}</sellerPartnerId>
                        <skuId>${skuId5}</skuId>
                        <storePartnerId>2297</storePartnerId>
                        <supplyType>ON_HAND</supplyType>
                    </blockListingInventory>
      		    </data>
      		 </blockListingInventoryRequest>""")).asJson
        .check(status.is(200))
      )
    }

  }
}



