package simulations.example.singleServer


import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

class SingleServerLT extends BaseSimulation {

/*
  To run on a single server for HTTP
  Just Pass -DsingleServerUrl = "http://singleserver.myntra.com/abc"
  Doing this will override the httpUrl i.e(ServiceType.ADDRESS_SERVICE.toString)
  with the passed url("http://singleserver.myntra.com/abc")
*/

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "systems"
  /* Name of File to be downloaded */
  val fileName = "address.csv"

  private val httpUrl = BaseUrlConstructor.getBaseUrl(ServiceType.ADDRESS_SERVICE.toString)

  private val addressServiceBaseUrl = http.baseUrl(httpUrl).disableWarmUp

/*  val csvFile = System.getProperty("addressUidxFile", "test-data/address.csv")

  val csvfeeder = csv(csvFile).circular*/
  val csvFile = System.getProperty("addressUidxFile", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)
  val csvfeeder = csv(filePath).circular

  //reference for feeder stratergies-https://gatling.io/docs/current/session/feeder

  val getAllAddress =scenario("Single server LT changes")
    .feed(csvfeeder)
    .exec(http("get all address test")
      .get("/myntra-address-service/v3/address")
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("15002"))
    )

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(addressServiceBaseUrl)
}
