package simulations.example.injections

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

class StairCaseInjection extends BaseSimulation{

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "systems"
  /* Name of File to be downloaded */
  val fileName = "address.csv"

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.ADDRESS_SERVICE.toString)
  private val addressServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp
  /*val csvFile = System.getProperty("addressUidxFile", "test-data/address.csv")

  val csvfeeder = csv(csvFile).circular*/
  val csvFile = System.getProperty("addressUidxFile", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)
  val csvfeeder = csv(filePath).circular

  //reference for feeder stratergies-https://gatling.io/docs/current/session/feeder

  def ApiLoad(step: Int, pacing: Int, duration: Int) =
    scenario(s"API Scenario $step")//.during(duration) {
      //pace(pacing)
        .feed(csvfeeder)
        .exec(http("get all address test")
          .get("/myntra-address-service/v3/address")
          .headers(headers)
          .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
          .check(status.is(200))
          .check(jsonPath("$.status.statusCode").is("15002"))
        )


  val numberOfSteps = getIntProperty("staircase.steps")

  val steps = 1 to numberOfSteps map { i => {
    ApiLoad(i, getIntProperty("staircase.pacing"), getIntProperty("staircase.duration")).inject(
      //nothingFor(getIntProperty("staircase.wait")),
      rampUsersPerSec(i * getLongProperty("staircase.ramp_up_rate.start")) to i * getLongProperty("staircase.ramp_up_rate.end") during (DurationJLong(getLongProperty("staircase.ramptime")) seconds),
      constantUsersPerSec(i * getIntProperty("staircase.constantuser")) during (DurationJLong(getLongProperty("staircase.holdtime")) seconds)
    )
  }
  }

  setUp(steps:_*).maxDuration(maxDurationInSec).protocols(addressServiceBaseUrl)

}
