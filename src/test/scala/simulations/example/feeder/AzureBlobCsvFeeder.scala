package simulations.example.feeder


import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import com.myntra.commons.util.FeederUtil
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

class AzureBlobCsvFeeder extends BaseSimulation{

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.ADDRESS_SERVICE.toString)
  private val addressServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "systems"
  /* Name of File to be downloaded */
  val fileName = "address.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  //val addressFeederGatling: BatchableFeederBuilder[String] = feederUtil.getFeeder(containerName, fileName).random

  val addressFeederGatling: BatchableFeederBuilder[String] = csv(filePath).random

  //reference for feeder stratergies-https://gatling.io/docs/current/session/feeder

  val getAllAddress =scenario("Csv Feeder")
    .feed(addressFeederGatling)
    .exec(http("get all address test")
      .get("/myntra-address-service/v3/address")
      .headers(headers)
      .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("15002"))
    )


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(addressServiceBaseUrl)

}

