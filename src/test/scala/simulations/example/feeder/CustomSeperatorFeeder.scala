package simulations.example.feeder

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.http.Predef._
import simulations.BaseSimulation

class CustomSeperatorFeeder extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "systems"
  /* Name of File to be downloaded */
  val fileName = "addressLT.csv"

  val csvFile = System.getProperty("addressUidxFile", fileName).toString
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.ADDRESS_SERVICE.toString)
  private val addressServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp
  val customSeparatorFeeder = separatedValues(filePath, ',')
  /*val csvFile = System.getProperty("addressUidxFile", "test-data/addressLT.csv")

  val customSeparatorFeeder = separatedValues(csvFile, ',')*/ // use your own separator ex - #,$,@ etc

  //reference for feeder stratergies- https://gatling.io/docs/current/session/feeder

  val getAllAddress =scenario("Custom Feeder")
    .feed(customSeparatorFeeder)
      .exec(http("get all address test")
        .get("/myntra-address-service/v3/address")
        .headers(headers)
        .header("x-mynt-ctx", "storeid=2297;uidx=${uidx};nidx=075525fe-6fd9-11e9-8b78-000d3af27f0e;")
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("15002"))
      )


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(addressServiceBaseUrl)

}
