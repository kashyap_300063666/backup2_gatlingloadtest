package simulations.example.redisFeeder

import com.myntra.commons.{FormIdeaUserJson, ServiceType}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation
import com.myntra.commons.util.{BaseUrlConstructor, HLTUtility, RedisUtil}

class RedisFeeder extends BaseSimulation {
  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.IDEA_SERVICE.toString)
  private val ideaServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp
//  private val redisHost = System.getProperty("redis.Host", "localhost")
//  private val redisPort = System.getProperty("redis.port", "6379").toInt
  private val counterKeyName = System.getProperty("counter.key", "counter")
  private val waitTimeToSetCounterVal = System.getProperty("wait.time", "5").toInt
  //val redisUtil = new RedisUtil(redisHost, redisPort)
  val util = new HLTUtility()

  before {
    util.redisUtil.flushdb()
    util.redisUtil.set(counterKeyName, "0")
    }

  private val loginIdea =
    scenario("SaveUserToken in Redis")
      .exec(session => {session.set("emailId", util.redisUtil.increamentCounter(counterKeyName) + "@myntra360.com")})
      .exec(
        http("idea login test")
          .post("/idea/opt/auth/email")
          .body(StringBody("""{"emailId":"${emailId}","accessKey":"${emailId}","appName":"myntra"}""")).asJson
          .headers(headers)
          .check(status.is(200))
          .check(jsonPath("$.status.statusCode").is("3"))
          .check(jsonPath("$.tokenEntry.at").saveAs("at"))
          .check(jsonPath("$.tokenEntry.rt").saveAs("rt"))
      )
      .exec(sess => {
        util.redisUtil.set(sess.apply("emailId").as[String], FormIdeaUserJson.formJson(
          sess.apply("at").as[String], sess.apply("rt").as[String]))
        sess
    })

  setUp( loginIdea.inject(
    nothingFor(waitTimeToSetCounterVal),
    constantUsersPerSec(noOfUsers) during (DurationJLong(getLongProperty("duration.seconds")) seconds)
  ).protocols(ideaServiceBaseUrl)
  ).maxDuration(maxDurationInSec)
}
