package simulations.lms.vanguard

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import simulations.BaseSimulation

class FetchSdaDetailsSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "lms"
  /* Name of File to be downloaded */
  val fileName = "trackingNumbersForSdaDetails.csv"

  val baseUrl: HttpProtocolBuilder = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.VANGUARD.toString)).disableWarmUp
  //val csvFile: String = System.getProperty("pincodes", "test-data/lms/vanguard/trackingNumbersForSdaDetails.csv")
  val csvFile = System.getProperty("pincodes", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)
  val trackingNumbers: BatchableFeederBuilder[String]#F = csv(fileName = filePath).circular


  val singleTrackingNumber: ScenarioBuilder = scenario("Get SDA Details (Single Tracking number)").feed(trackingNumbers)
    .exec(http("singleTrackingNumber")
      .get("/lms-vanguard-service/sdaDetails/tenantId/4019/trackingNumber/${trackingNumbers}")
      .basicAuth("system~system", "Windows@123")
      .header("x-myntra-req-id", "abc")
      .headers(headers)
      .check(status.is(200))
    )


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(baseUrl)

}