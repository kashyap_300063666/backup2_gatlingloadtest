package simulations.partners.terms

import simulations.BaseSimulation
import java.io.FileInputStream
import java.io.File
import java.util.Properties
import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor

import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}

class PartnerTermsSimAzure extends BaseSimulation {

  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.PARTNER_TERMS_SERVICE.toString)

  private val partnerTermsBaseUrl = http.baseUrl(baseUrl).disableWarmUp

  private def loadPropertiesFile( file: File ) : java.util.Properties = {
    val properties = new java.util.Properties()
    properties.load(new FileInputStream(file))
    properties
  }

  private val scenarioProperties = loadPropertiesFile(new File("term-scenario-config.properties"))

  val statusFeeder = Array(200).map(e => Map("status" -> e)).array.circular

  val entityIdFeeder = Array(22264).map(e => Map("entity_id" -> e)).array.circular
  val when = Array(2019-10-22).map(e => Map("when" -> e)).array.circular

  private val activeContractInfoTermBulkSearch = scenario("active contract info bulk search for 3 contracts")
    .feed(entityIdFeeder)
    .feed(when)
    .feed(statusFeeder)
    .exec(http("active contract info bulk search for 3 contracts")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody(
        """[ {"_when": "${when}", "_type":"contractinfo", "__entityId":"${entity_id}"}, {"_when": "${when}", "_type":"contractinfo", "__entityId": 21844}, {"_when": "${when}", "_type":"contractinfo", "__entityId": 22072}]""")).asJson
      .check(status.is("${status}")))
    .inject(constantUsersPerSec( Integer.parseInt(scenarioProperties.getProperty("activeTermBulkSearch_rampup_user_startrate")))
      during ( Integer.parseInt(scenarioProperties.getProperty("duration"))))

  val _entityIdFeeder = Array(9227).map(e => Map("_entity_id" -> e)).array.circular
  private val activeContractInfoTermBulkSearchSecond = scenario("active contract info bulk search for 2 apis")
    .feed(_entityIdFeeder)
    .feed(statusFeeder)
    .exec(http("active contract info bulk search for 2 apis")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody("""[ {"_when": "2019-07-01", "_type":"contractinfo", "__entityId":"${_entity_id}"}, {"_when": "2019-07-01", "_type":"contractinfo", "__entityId": 20100}]""")).asJson
      .check(status.is("${status}")))
    .inject(constantUsersPerSec( Integer.parseInt(scenarioProperties.getProperty("activeTermBulkSearch_rampup_user_startrate")))
      during ( Integer.parseInt(scenarioProperties.getProperty("duration"))))

  val agreementTypeFeeder = Array("JIT_MARKETPLACE", "JIT_MARKETPLACE", "JIT_MARKETPLACE", "JIT_MARKETPLACE", "JIT_MARKETPLACE").map(e => Map("agreementType" -> e)).array.circular
  val masterCategoryFeeder = Array("FOOTWEAR", "APPAREL", "APPAREL", "ACCESSORIES", "ACCESSORIES").map(e => Map("masterCategory" -> e)).array.circular
  val articleTypeFeeder = Array("", "", "", "", "").map(e => Map("articleType" -> e)).array.circular
  val genderFeeder = Array("", "", "ALL", "ALL", "ALL").map(e => Map("gender" -> e)).array.circular
  val brandsFeeder = Array("RPIR", "TRRY", "FLMW", "ATUN", "DNSN").map(e => Map("brand" -> e)).array.circular
  val additionalClassificationFeeder = Array("NOT_APPLICABLE", "NOT_APPLICABLE", "NOT_APPLICABLE", "NOT_APPLICABLE", "NOT_APPLICABLE").map(e => Map("additionalClassification" -> e)).array.circular
  val contractIdFeeder = Array(9658, 20739, 8161, 7830, 8854).map(e => Map("contractId" -> e)).array.circular

  private val activeTermBulkSearchPo = scenario("active term bulk search PO")
    .feed(agreementTypeFeeder)
    .feed(masterCategoryFeeder)
    .feed(articleTypeFeeder)
    .feed(genderFeeder)
    .feed(brandsFeeder)
    .feed(contractIdFeeder)
    .feed(additionalClassificationFeeder)
    .feed(statusFeeder)
    .exec(http("active term bulk search PO")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody("""[ {"_contractId": "${contractId}", "_type":"po", "brand":"${brand}", "agreementType":"${agreementType}", "masterCategory":"${masterCategory}", "articleType":"${articleType}", "gender":"${gender}", "additionalClassification":"${additionalClassification}"}, {"_contractId": "${contractId}", "_type":"po", "brand":"ALL", "agreementType":"", "masterCategory":"ALL", "articleType":"ALL", "gender":"ALL", "additionalClassification":"NOT_APPLICABLE"} ]""")).asJson
      .check(status.is("${status}")))
    .inject(constantUsersPerSec( Integer.parseInt(scenarioProperties.getProperty("activeTermBulkSearch_rampup_user_startrate")))
      during ( Integer.parseInt(scenarioProperties.getProperty("duration"))))

  private val activeCommssionTermBulkSearch = scenario("active commission bulk search for contracts")
    .feed(entityIdFeeder)
    .feed(when)
    .feed(statusFeeder)
    .exec(http("active commission bulk search for contracts")
      .post("/search/bulk")
      .headers(headers)
      .body(StringBody(
        """[ {"_when": "2019-10-22", "_type": "commission", "_entityId": 2297, "model" : "COMBINED_COMMISSION", "gender" : "ALL", "articleType" : "ALL", "brand" : "PUMA"}, {"_when": "2018-11-01", "_type": "commission", "_entityId": 2297, "model" : "COMBINED_COMMISSION", "gender" : "ALL", "articleType" : "ALL", "brand" : "SJYA"},
          {"_when": "2018-12-19", "_type": "commission", "_entityId": 2297, "model" : "COMBINED_COMMISSION", "gender" : "ALL", "articleType" : "ALL", "brand" : "GRSN"},
          {"_when": "2018-12-19", "_type": "commission", "_entityId": 2297, "model" : "COMBINED_COMMISSION", "gender" : "ALL", "articleType" : "ALL", "brand" : "SIUT"}]""")).asJson
      .check(status.is("${status}")))
    .inject(constantUsersPerSec( Integer.parseInt(scenarioProperties.getProperty("activeTermBulkSearch_rampup_user_startrate")))
      during ( Integer.parseInt(scenarioProperties.getProperty("duration"))))

  setUp(List(activeContractInfoTermBulkSearch, activeContractInfoTermBulkSearchSecond, activeTermBulkSearchPo, activeCommssionTermBulkSearch))
    .protocols(partnerTermsBaseUrl).maxDuration(maxDurationInSec)

}
