package simulations.partners.dms

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

class PartnerDMSSimulation extends BaseSimulation {

  private val dmsBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PARTNER_DMS_SERVICE.toString)).disableWarmUp

  private val brandName = Array("Puma").map(e => Map("brandName" -> e)).array.circular
  private val vendorId = Array(1082).map(e => Map("vendorId" -> e)).array.circular
  private val storeId = Array(2297).map(e => Map("storeId" -> e)).array.circular

  private val getStylesByBrandName =
    scenario("get styles by brand name")
      .feed(vendorId)
      .feed(storeId)
      .feed(brandName)
      .exec(http("get styles by brand name")
        .get("/partner-discount-management-service/partnerdms/vendor/${vendorId}/styles/${storeId}/param?brand=${brandName}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3")))

  private val getCategoryHierarchy =
    scenario("get category hierarchy by vendorId and storeId")
      .feed(vendorId)
      .feed(storeId)
      .exec(http("get category hierarchy by vendorId and storeId")
        .get("/partner-discount-management-service/partnerdms/vendor/${vendorId}/categoryHierarchy/${storeId}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3")))

  private val getAllTheEvents =
    scenario("get all the events by vendorId and storeId")
      .feed(vendorId)
      .feed(storeId)
      .exec(http("get all the events by vendorId and storeId")
        .get("/partner-discount-management-service/partnerdms/vendor/${vendorId}/event/${storeId}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3")))

  private val eventId = Array(1, 136, 130, 10, 15).map(e => Map("eventId" -> e)).array.circular

  private val getEventByEventId =
    scenario("get particular event by event id")
      .feed(vendorId)
      .feed(storeId)
      .feed(eventId)
      .exec(http("get particular event by event id")
        .get("/partner-discount-management-service/partnerdms/vendor/${vendorId}/event/${storeId}/${eventId}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3")))

  private val getAllStyleForEvent =
    scenario("get all style for event by vendor id and store id")
      .feed(vendorId)
      .feed(storeId)
      .exec(http("get all style for event by vendor id and store id")
        .get("/partner-discount-management-service/partnerdms/vendor/${vendorId}/event/${storeId}/style")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3")))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(dmsBaseUrl)
}