package simulations.partners.settlementutil

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

class SettlementUtilSimAzure extends BaseSimulation {

  private val settlementUtilBaseUrl = http.
    baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.SETTLEMENT_UTIL.toString)).disableWarmUp

  private val orderLineId = Array("4084731278", "4084731282", "4084731283", "4084731285", "4084731286")
    .map(e => Map("orderLineId" -> e)).array.circular


  private val getSettlementAmountSplit =
    scenario("get settlement amount split")
      .feed(orderLineId)
      .exec(http("get settlement amount split")
        .get("/settlement-util/settlement/v1/amount/split/${orderLineId}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.status.statusMessage").is("Success"))
        .check(jsonPath("$.data[*]").count.gte(1))
        .check(jsonPath("$.data[0].paymentMethodEntries.COD").count.gte(1))
        .check(jsonPath("$.data[0].paymentMethodEntries.ONLINE").count.gte(1)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(settlementUtilBaseUrl)
}
