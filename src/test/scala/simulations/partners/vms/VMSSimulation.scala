package simulations.partners.vms

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, status, _}
import simulations.BaseSimulation

class VMSSimulation extends BaseSimulation {

  private val vmsBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.VMS_SERVICE.toString)).disableWarmUp

  private val sourceSystemFeeder = Array("SELLER", "STORE").map(e => Map("source_system_name" -> e)).array.circular
  private val sourceSytemIdFeeder = Array(18, 1, 20, 3, 22, 5).map(e => Map("source_system_id" -> e)).array.circular

  private val getSourceSystemByNameAndId =
    scenario("get partner by source system")
      .feed(sourceSystemFeeder)
      .feed(sourceSytemIdFeeder)
      .exec(http("get partner by source system")
        .get("/myntra-vms-service/vendorService/partner/${source_system_name}/${source_system_id}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3")))

  private val idFeeder = Array(200, 210, 240, 350, 400, 500, 4000, 4555, 5266, 6379).map(e => Map("id" -> e)).array.circular

  private val getVendorsInfoById =
    scenario("get vendor info by id")
      .feed(idFeeder)
      .exec(http("get vendor info by id")
        .get("/myntra-vms-service/vendorService/partner/info/${id}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.partnerEntries[*]").count.is(1)))


  private val vendorIdFeeder = Array(2001, 210, 2201, 300, 400, 500, 111, 223, 4567, 1).map(e => Map("vendorId" -> e)).array.circular

  private val getVendorsBrandById =
    scenario("get vendor's brand by id")
      .feed(vendorIdFeeder)
      .exec(http("get vendor's brand by id")
        .get("/myntra-vms-service/vms/vendors/brand/search?q=vendor.id.eq:${vendorId}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.vendorBrandResponse.status.statusCode").is("1608")))

  private val sellerIdFeeder = Array(3138, 4024, 4027, 4028).map(e => Map("sellerIdFeeder" -> e)).array.circular
  private val storeIdFeeder = Array(2297, 4603).map(e => Map("storeIdFeeder" -> e)).array.circular

  private val getFacilityForB2CSeller =
    scenario("get facility of seller using B2CSellerId and storeId")
      .feed(sellerIdFeeder)
      .feed(storeIdFeeder)
      .exec(http("get facility of seller using B2CSellerId and storeId")
        .get("/myntra-vms-service/vendorService/facility/seller/${sellerIdFeeder}/store/${storeIdFeeder}" +
          "?supplyType=ON_HAND&shippingType=SHIPPING_FROM_ADDRESSES")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.data[*]").count.gte(1)))

  private val sellerIdFeederV2 = Array(4027, 4024, 4028, 4036).map(e => Map("sellerIdFeeder" -> e)).array.circular
  private val storeIdFeederV2 = Array(2297, 4603).map(e => Map("storeIdFeeder" -> e)).array.circular

  private val getFacilityV2ForB2CSeller =
    scenario("get facility V2 of seller using B2CSellerId and storeId")
      .feed(sellerIdFeederV2)
      .feed(storeIdFeederV2)
      .exec(http("get facility V2 of seller using B2CSellerId and storeId")
        .get("/myntra-vms-service/vendorService/v2/facility/seller/${sellerIdFeeder}/store/${storeIdFeeder}" +
          "?supplyType=ON_HAND&shippingType=SHIPPING_FROM_ADDRESSES")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.data[*]").count.gte(1)))

  private val sellerIdFeederB2B = Array(4254, 5620, 6480, 5628).map(e => Map("sellerIdFeeder" -> e)).array.circular
  private val buyerIdFeederV2 = Array(3974, 4602).map(e => Map("buyerIdFeeder" -> e)).array.circular

  private val getFacilityV2ForB2BSeller =
    scenario("get facility V2 of seller using B2BSellerId and buyerId")
      .feed(sellerIdFeederB2B)
      .feed(buyerIdFeederV2)
      .exec(http("get facility V2 of seller using B2BSellerId and buyerId")
        .get("/myntra-vms-service/vendorService/v2/facility/seller/${sellerIdFeeder}/buyer/${buyerIdFeeder}" +
          "?supplyType=JIT&shippingType=SHIPPING_TO_ADDRESSES")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.data[*]").count.gte(1)))

  private val _vendorIdFeeder = Array(948, 1212, 1283, 4542, 4629).map(e => Map("id" -> e)).array.circular
  private val warehouseIdFeeder = Array(20, 17, 1, 197, 121).map(e => Map("warehouseId" -> e)).array.circular

  private val getPartnerWarehouseAddByIdAndWarehouseId =
    scenario("get vendor warehouse address by partner id and warehouse id")
      .feed(_vendorIdFeeder)
      .feed(warehouseIdFeeder)
      .exec(http("get vendor warehouse address by partner id and warehouse id")
        .get("/myntra-vms-service/vms/vendors/partner/address/${id}/${warehouseId}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.vendorResponse.status.statusCode").is("1605")))

  private val kycSignatureVendorIdFeeder = Array(3465, 1167, 4028, 4542, 5442).map(e => Map("id" -> e)).array.circular

  private val getPartnerSignatureFileById =
    scenario("get partner signature file by partner id")
      .feed(kycSignatureVendorIdFeeder)
      .exec(http("get partner signature file by partner id")
        .get("/myntra-vms-service/vendorService/partner/kyc/partnerSignatureFile/${id}")
        .headers(headersAcceptOctetStream)
        .check(status.is(200)))

  private val warehouseId = Array(125, 220, 360, 216, 36, 309, 28, 235).map(e => Map("warehouseId" -> e)).circular
  private val buyerId = Array(3974, 4602).map(e => Map("buyerId" -> e)).circular

  private val getPartnerContactAddByWarehouseId =
    scenario("get partner contact add by warehouse id")
      .feed(warehouseId)
      .feed(buyerId)
      .exec(http("get partner contact add by warehouse id")
        .get("/myntra-vms-service/vendorService/partnerContactAddress/partner/${buyerId}/warehouse?warehouseIds=${warehouseId}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("4107"))
        .check(jsonPath("$.data[*]").count.is(1)))

  private val kycDocEntityIdFeeder = Array(4024, 6420, 3000, 4603, 5442).map(e => Map("id" -> e)).array.circular

  private val searchKYCDocByEntityId =
    scenario("search partner KYC by entity id")
      .feed(kycDocEntityIdFeeder)
      .exec(http("search partner KYC by entity id")
        .get("/myntra-vms-service/vendorService/partner/kyc/search?q=entityId.eq:${id}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("4100")))

  private val getContractBySourceNameAndType =
    scenario("get Contract By Source Name And Type")
      .feed(sourceSystemFeeder)
      .feed(sourceSytemIdFeeder)
      .exec(http("get Contract By Source Name And Type")
        .get("/myntra-vms-service/vendorService/contract/partner/${source_system_name}/${source_system_id}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3")))

  private val partner1Id = Array(3974, 2297, 4602, 4603, 3854, 4816).map(e => Map("partner1id" -> e)).array.circular
  private val partner1Role = Array("BUYER", "STORE").map(e => Map("partner1Role" -> e)).array.circular
  private val partner2Id = Array(6336, 6320, 6228, 1672, 4891, 4216, 6384, 6321, 6376, 6278, 4854, 4024,
    5262, 6319, 4640, 4, 4860, 4027).map(e => Map("partner2id" -> e)).array.circular
  private val partner2Role = Array("B2B_SELLER", "B2C_SELLER").map(e => Map("partner2Role" -> e)).array.circular

  private val getContractByPartnersIdAndPartnersRole =
    scenario("get Contract By Partner1 Id & Role and Partner2 Id & Role")
      .feed(partner1Id)
      .feed(partner1Role)
      .feed(partner2Id)
      .feed(partner2Role)
      .exec(http("get Contract By Partner1 Id & Role and Partner2 Id & Role")
        .get("/myntra-vms-service/vendorService/contract/info/${partner1id}/${partner1Role}/${partner2id}/${partner2Role}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3")))


  private val configCategoryFeeder = Array("SELLER_API", "CART", "ORDER_MANAGEMENT", "SELLER_SETTLEMENT")
    .map(e => Map("configCategoryFeeder" -> e)).array.circular

  private val getConfigurationSearchByPartnersIdAndPartnersRole =
    scenario("get Configuration search By Partner1 Id & Role and Partner2 Id & Role")
      .feed(storeIdFeeder)
      .feed(sellerIdFeeder)
      .feed(configCategoryFeeder)
      .exec(http("get Configuration search By Partner1 Id & Role and Partner2 Id & Role")
        .get("/myntra-vms-service/vendorService/configuration/search/contract/${storeIdFeeder}/STORE/${sellerIdFeeder}" +
          "/B2C_SELLER/${configCategoryFeeder}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("1653"))
        .check(jsonPath("$.data[*]").count.gte(1)))

  private val getContractV2ByPartnersIdAndPartnersRole =
    scenario("get Contract V2 By Partner1 Id & Role and Partner2 Id & Role")
      .feed(partner1Id)
      .feed(partner1Role)
      .feed(partner2Id)
      .feed(partner2Role)
      .exec(http("get Contract V2 By Partner1 Id & Role and Partner2 Id & Role")
        .get("/myntra-vms-service/vendorService/contract/v2/${partner1id}/${partner1Role}/${partner2id}/${partner2Role}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3")))

  private val getPartnersById =
    scenario("get partner by id")
      .feed(idFeeder)
      .exec(http("get partner by id")
        .get("/myntra-vms-service/vendorService/partner/${id}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("4102"))
        .check(jsonPath("$.partnerEntries[*]").count.is(1)))

  private val getPartnerContactByPartnerId =
    scenario("get partner contact using partner id")
      .feed(idFeeder)
      .exec(http("get partner contact using partner id")
        .get("/myntra-vms-service/vendorService/partnerContact/search?q=enabled.eq:true___partnerId.eq:${id}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("1656"))
        .check(jsonPath("$.data[*]").count.is(1)))

  private val searchPartnerContactAddress =
    scenario("search partner contact address")
      .feed(idFeeder)
      .exec(http("search partner contact address")
        .get("/myntra-vms-service/vendorService/partnerContactAddress/search?q=partnerType.eq:VENDOR___partnerId.eq:${id}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("4109"))
        .check(jsonPath("$.data[*]").count.gte(1)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(vmsBaseUrl)

}