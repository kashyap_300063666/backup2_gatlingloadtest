package simulations.partners.vms

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class VMSConstRampComboSimulation extends BaseSimulation{
  private val vmsBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.VMS_SERVICE.toString)).disableWarmUp

  private val sourceSystemFeeder = Array("SELLER", "STORE").map(e => Map("source_system_name" -> e)).array.circular
  private val sourceSytemIdFeeder = Array(18, 1, 20, 3, 22, 5).map(e => Map("source_system_id" -> e)).array.circular

  private val getSourceSystemByNameAndId =
    scenario("get partner by source system")
      .feed(sourceSystemFeeder)
      .feed(sourceSytemIdFeeder)
      .exec(http("get partner by source system")
        .get("/myntra-vms-service/vendorService/partner/${source_system_name}/${source_system_id}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3")))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getSourceSystemByNameAndIdCUSR")
        during ((duration * getIntProperty("getSourceSystemByNameAndIdDuration"))/100),
        constantUsersPerSec(getIntProperty("getSourceSystemByNameAndIdCUSR"))
          during ((duration * (100 - (getIntProperty("getSourceSystemByNameAndIdDuration"))))/100) )

  private val idFeeder = Array(200, 210, 240, 350, 400, 500, 4000, 4555, 5266, 6379).map(e => Map("id" -> e)).array.circular

  private val getVendorsInfoById =
    scenario("get vendor info by id")
      .feed(idFeeder)
      .exec(http("get vendor info by id")
        .get("/myntra-vms-service/vendorService/partner/info/${id}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.partnerEntries[*]").count.is(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getVendorsInfoByIdCUSR")
        during ((duration * getIntProperty("getVendorsInfoByIdCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getVendorsInfoByIdCUSR"))
          during ((duration * (100 - (getIntProperty("getVendorsInfoByIdCUSRDuration"))))/100) )

  private val vendorIdFeeder = Array(2001, 210, 2201, 300, 400, 500, 111, 223, 4567, 1).map(e => Map("vendorId" -> e)).array.circular

  private val getVendorsBrandById =
    scenario("get vendor's brand by id")
      .feed(vendorIdFeeder)
      .exec(http("get vendor's brand by id")
        .get("/myntra-vms-service/vms/vendors/brand/search?q=vendor.id.eq:${vendorId}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.vendorBrandResponse.status.statusCode").is("1608")))
      .inject(rampUsersPerSec(0)
        to getLongProperty("getVendorsBrandByIdCUSR")
        during ((duration * getIntProperty("getVendorsBrandByIdCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getVendorsBrandByIdCUSR"))
          during ((duration * (100 - (getIntProperty("getVendorsBrandByIdCUSRDuration"))))/100) )

  private val sellerIdFeeder = Array(3138, 4024, 4027, 4028).map(e => Map("sellerIdFeeder" -> e)).array.circular
  private val storeIdFeeder = Array(2297, 4603).map(e => Map("storeIdFeeder" -> e)).array.circular

  private val sellerIdFeederV2 = Array(4027, 4024, 4028, 4036).map(e => Map("sellerIdFeeder" -> e)).array.circular
  private val storeIdFeederV2 = Array(2297, 4603).map(e => Map("storeIdFeeder" -> e)).array.circular

  private val getFacilityV2ForB2CSeller =
    scenario("get facility V2 of seller using B2CSellerId and storeId")
      .feed(sellerIdFeederV2)
      .feed(storeIdFeederV2)
      .exec(http("get facility V2 of seller using B2CSellerId and storeId")
        .get("/myntra-vms-service/vendorService/v2/facility/seller/${sellerIdFeeder}/store/${storeIdFeeder}" +
          "?supplyType=ON_HAND&shippingType=SHIPPING_FROM_ADDRESSES")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getFacilityV2ForB2CSellerCUSR")
        during ((duration * getIntProperty("getFacilityV2ForB2CSellerCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getFacilityV2ForB2CSellerCUSR"))
          during ((duration * (100 - (getIntProperty("getFacilityV2ForB2CSellerCUSRDuration"))))/100) )

  private val sellerIdFeederB2B = Array(4254, 5620, 6480, 5628).map(e => Map("sellerIdFeeder" -> e)).array.circular
  private val buyerIdFeederV2 = Array(3974, 4602).map(e => Map("buyerIdFeeder" -> e)).array.circular

  private val getFacilityV2ForB2BSeller =
    scenario("get facility V2 of seller using B2BSellerId and buyerId")
      .feed(sellerIdFeederB2B)
      .feed(buyerIdFeederV2)
      .exec(http("get facility V2 of seller using B2BSellerId and buyerId")
        .get("/myntra-vms-service/vendorService/v2/facility/seller/${sellerIdFeeder}/buyer/${buyerIdFeeder}" +
          "?supplyType=JIT&shippingType=SHIPPING_TO_ADDRESSES")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getFacilityV2ForB2BSellerCUSR")
        during ((duration * getIntProperty("getFacilityV2ForB2BSellerCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getFacilityV2ForB2BSellerCUSR"))
          during ((duration * (100 - (getIntProperty("getFacilityV2ForB2BSellerCUSRDuration"))))/100) )

  private val _vendorIdFeeder = Array(948, 1212, 1283, 4542, 4629).map(e => Map("id" -> e)).array.circular
  private val warehouseIdFeeder = Array(20, 17, 1, 197, 121).map(e => Map("warehouseId" -> e)).array.circular

  private val getPartnerWarehouseAddByIdAndWarehouseId =
    scenario("get vendor warehouse address by partner id and warehouse id")
      .feed(_vendorIdFeeder)
      .feed(warehouseIdFeeder)
      .exec(http("get vendor warehouse address by partner id and warehouse id")
        .get("/myntra-vms-service/vms/vendors/partner/address/${id}/${warehouseId}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.vendorResponse.status.statusCode").is("1605")))
      .inject(rampUsersPerSec(0)
        to getLongProperty("getPartnerWarehouseAddByIdAndWarehouseIdCUSR")
        during ((duration * getIntProperty("getPartnerWarehouseAddByIdAndWarehouseIdCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getPartnerWarehouseAddByIdAndWarehouseIdCUSR"))
          during ((duration * (100 - (getIntProperty("getPartnerWarehouseAddByIdAndWarehouseIdCUSRDuration"))))/100) )

  private val kycSignatureVendorIdFeeder = Array(3465, 1167, 4028, 4542, 5442).map(e => Map("id" -> e)).array.circular

  private val warehouseId = Array(125, 220, 36, 81, 28, 120, 104, 328, 125, 121, 309).map(e => Map("warehouseId" -> e)).circular
  private val buyerId = Array(3974, 4602, 4024, 4027, 6420, 4036, 6419, 6206, 6626, 3920, 4854).map(e => Map("buyerId" -> e)).circular

  private val getPartnerContactAddByWarehouseId =
    scenario("get partner contact add by warehouse id")
      .feed(warehouseId)
      .feed(buyerId)
      .exec(http("get partner contact add by warehouse id")
        .get("/myntra-vms-service/vendorService/partnerContactAddress/partner/${buyerId}/warehouse?warehouseIds=${warehouseId}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("4107"))
        .check(jsonPath("$.data[*]").count.is(1)))
      .inject(rampUsersPerSec(0)
        to getLongProperty("getPartnerContactAddByWarehouseIdCUSR")
        during ((duration * getIntProperty("getPartnerContactAddByWarehouseIdCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getPartnerContactAddByWarehouseIdCUSR"))
          during ((duration * (100 - (getIntProperty("getPartnerContactAddByWarehouseIdCUSRDuration"))))/100) )

  private val kycDocEntityIdFeeder = Array(4024, 6420, 3000, 4603, 5442).map(e => Map("id" -> e)).array.circular

  private val getContractBySourceNameAndType =
    scenario("get Contract By Source Name And Type")
      .feed(sourceSystemFeeder)
      .feed(sourceSytemIdFeeder)
      .exec(http("get Contract By Source Name And Type")
        .get("/myntra-vms-service/vendorService/contract/partner/${source_system_name}/${source_system_id}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3")))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getContractBySourceNameAndTypeCUSR")
        during ((duration * getIntProperty("getContractBySourceNameAndTypeCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getContractBySourceNameAndTypeCUSR"))
          during ((duration * (100 - (getIntProperty("getContractBySourceNameAndTypeCUSRDuration"))))/100) )

  private val partner1Id = Array(3974, 2297, 4602, 4603, 3854, 4816).map(e => Map("partner1id" -> e)).array.circular
  private val partner1Role = Array("BUYER", "STORE").map(e => Map("partner1Role" -> e)).array.circular
  private val partner2Id = Array(6336, 6320, 6228, 1672, 4891, 4216, 6384, 6321, 6376, 6278, 4854, 4024,
    5262, 6319, 4640, 4, 4860, 4027).map(e => Map("partner2id" -> e)).array.circular
  private val partner2Role = Array("B2B_SELLER", "B2C_SELLER").map(e => Map("partner2Role" -> e)).array.circular

  private val configCategoryFeeder = Array("SELLER_API", "CART", "ORDER_MANAGEMENT", "SELLER_SETTLEMENT")
    .map(e => Map("configCategoryFeeder" -> e)).array.circular

  private val getConfigurationSearchByPartnersIdAndPartnersRole =
    scenario("get Configuration search By Partner1 Id & Role and Partner2 Id & Role")
      .feed(storeIdFeeder)
      .feed(sellerIdFeeder)
      .feed(configCategoryFeeder)
      .exec(http("get Configuration search By Partner1 Id & Role and Partner2 Id & Role")
        .get("/myntra-vms-service/vendorService/configuration/search/contract/${storeIdFeeder}/STORE/${sellerIdFeeder}" +
          "/B2C_SELLER/${configCategoryFeeder}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("1653"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getConfigurationSearchByPartnersIdAndPartnersRoleCUSR")
        during ((duration * getIntProperty("getConfigurationSearchByPartnersIdAndPartnersRoleCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getConfigurationSearchByPartnersIdAndPartnersRoleCUSR"))
          during ((duration * (100 - (getIntProperty("getConfigurationSearchByPartnersIdAndPartnersRoleCUSRDuration"))))/100) )

  private val searchPartnerContactAddress =
    scenario("search partner contact address")
      .feed(idFeeder)
      .exec(http("search partner contact address")
        .get("/myntra-vms-service/vendorService/partnerContactAddress/search?q=partnerType.eq:VENDOR___partnerId.eq:${id}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("4109"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(0)
        to getLongProperty("searchPartnerContactAddressCUSR")
        during ((duration * getIntProperty("searchPartnerContactAddressCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("searchPartnerContactAddressCUSR"))
          during ((duration * (100 - (getIntProperty("searchPartnerContactAddressCUSRDuration"))))/100) )

  private val _partner_id = Array(6420, 6419).map(e => Map("_partner_id" -> e)).array.circular
  private val b2c_seller_id = Array(4036, 4024, 4036, 4214, 4036, 4036).map(e => Map("b2c_seller_id" -> e)).array.circular
  private val brand_code = Array("UCFB", "NIKE", "KLNJ", "RDTP", "RLPH", "SPKR").map(e => Map("brand_code" -> e)).array.circular
  private val searchV2BrandContract=
    scenario("search v2 brand contract")
      .feed(_partner_id)
      .feed(b2c_seller_id)
      .feed(brand_code)
      .exec(http("search v2 brand contract")
        .get("/myntra-vms-service/vendorService/contract/v2/brand/contract/${_partner_id}/" +
          "B2B_SELLER/BUYER?b2cSellerId=${b2c_seller_id}&brandCode=${brand_code}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("searchV2BrandContractCUSR")
        during ((duration * getIntProperty("searchV2BrandContractCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("searchV2BrandContractCUSR"))
          during ((duration * (100 - (getIntProperty("searchV2BrandContractCUSRDuration"))))/100) )

  private val searchContractBasedOnPartnerRole=
    scenario("search contract partner based on role")
      .feed(partner1Role)
      .exec(http("search contract partner based on role")
        .get("/myntra-vms-service/vendorService/contract/partner/${partner1Role}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.partnerEntries[*]").count.gte(1)))
      .inject(rampUsersPerSec(0)
        to getLongProperty("searchContractBasedOnPartnerRoleCUSR")
        during ((duration * getIntProperty("searchContractBasedOnPartnerRoleCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("searchContractBasedOnPartnerRoleCUSR"))
          during ((duration * (100 - (getIntProperty("searchContractBasedOnPartnerRoleCUSRDuration"))))/100) )

  private val partner1IdCont = Array(3974, 2297, 4602, 4603, 3854, 4816).map(e => Map("partner1IdCont" -> e)).array.circular
  private val partner1RoleCont = Array("BUYER", "STORE").map(e => Map("partner1RoleCont" -> e)).array.circular
  private val partner2IdCont = Array(6336, 6320, 6228, 1672, 4891, 4216, 6384, 6321, 6376, 6278, 4854, 4024,
    5262, 6319, 4640, 4, 4860, 4027).map(e => Map("partner2IdCont" -> e)).array.circular
  private val partner2RoleCont = Array("B2B_SELLER", "B2C_SELLER").map(e => Map("partner2RoleCont" -> e)).array.circular

  private val getContractByPartnersIdAndPartnersRole =
    scenario("get Contract By Partner1 Id & Role and Partner2 Id & Role")
      .feed(partner1IdCont)
      .feed(partner1RoleCont)
      .feed(partner2IdCont)
      .feed(partner2RoleCont)
      .exec(http("get Contract By Partner1 Id & Role and Partner2 Id & Role")
        .get("/myntra-vms-service/vendorService/contract/info/${partner1IdCont}/${partner1RoleCont}/${partner2IdCont}/${partner2RoleCont}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(0)
        to getLongProperty("getContractByPartnersIdAndPartnersRoleCUSR")
        during ((duration * getIntProperty("getContractByPartnersIdAndPartnersRoleCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getContractByPartnersIdAndPartnersRoleCUSR"))
          during ((duration * (100 - (getIntProperty("getContractByPartnersIdAndPartnersRoleCUSRDuration"))))/100) )

  private val partner1IdV2 = Array(3974, 2297, 4602, 4603, 3854, 4816).map(e => Map("partner1IdV2" -> e)).array.circular
  private val partner1RoleV2 = Array("BUYER", "STORE").map(e => Map("partner1RoleV2" -> e)).array.circular
  private val partner2IdV2 = Array(6336, 6320, 6228, 1672, 4891, 4216, 6384, 6321, 6376, 6278, 4854, 4024,
    5262, 6319, 4640, 4, 4860, 4027).map(e => Map("partner2IdV2" -> e)).array.circular
  private val partner2RoleV2 = Array("B2B_SELLER", "B2C_SELLER").map(e => Map("partner2RoleV2" -> e)).array.circular

  private val getContractV2ByPartnersIdAndPartnersRole =
    scenario("get Contract V2 By Partner1 Id & Role and Partner2 Id & Role")
      .feed(partner1IdV2)
      .feed(partner1RoleV2)
      .feed(partner2IdV2)
      .feed(partner2RoleV2)
      .exec(http("get Contract V2 By Partner1 Id & Role and Partner2 Id & Role")
        .get("/myntra-vms-service/vendorService/contract/v2/${partner1IdV2}/${partner1RoleV2}/${partner2IdV2}/${partner2RoleV2}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(0)
        to getLongProperty("getContractV2ByPartnersIdAndPartnersRoleCUSR")
        during ((duration * getIntProperty("getContractV2ByPartnersIdAndPartnersRoleCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getContractV2ByPartnersIdAndPartnersRoleCUSR"))
          during ((duration * (100 - (getIntProperty("getContractV2ByPartnersIdAndPartnersRoleCUSRDuration"))))/100) )

  private val partner1IdEnrich = Array(3974, 2297, 4602, 4603, 3854, 4816).map(e => Map("partner1IdEnrich" -> e)).array.circular
  private val partner1RoleEnrich = Array("BUYER", "STORE").map(e => Map("partner1RoleEnrich" -> e)).array.circular
  private val partner2IdEnrich = Array(6336, 6320, 6228, 1672, 4891, 4216, 6384, 6321, 6376, 6278, 4854, 4024,
    5262, 6319, 4640, 4, 4860, 4027).map(e => Map("partner2IdEnrich" -> e)).array.circular
  private val partner2RoleEnrich = Array("B2B_SELLER", "B2C_SELLER").map(e => Map("partner2RoleEnrich" -> e)).array.circular

  private val getEnrichContractByPartnersIdAndPartnersRole =
    scenario("get enrich contract By Partner1 Id & Role and Partner2 Id & Role")
      .feed(partner1IdEnrich)
      .feed(partner1RoleEnrich)
      .feed(partner2IdEnrich)
      .feed(partner2RoleEnrich)
      .exec(http("get enrich contract By Partner1 Id & Role and Partner2 Id & Role")
        .get("/myntra-vms-service/vendorService/contract/enrich/${partner1IdEnrich}/${partner1RoleEnrich}/${partner2IdEnrich}/${partner2RoleEnrich}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getEnrichContractByPartnersIdAndPartnersRoleCUSR")
        during ((duration * getIntProperty("getEnrichContractByPartnersIdAndPartnersRoleCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getEnrichContractByPartnersIdAndPartnersRoleCUSR"))
          during ((duration * (100 - (getIntProperty("getEnrichContractByPartnersIdAndPartnersRoleCUSRDuration"))))/100) )

  private val partner_id_ = Array(3974, 4602, 3854).map(e => Map("partner_id_" -> e)).array.circular
  private val brand_code_ = Array("UCFB", "NIKE", "KLNJ", "RDTP", "RLPH", "SPKR").map(e => Map("brand_code_" -> e)).array.circular

  private val searchBrandContract =
    scenario("get contract using brand")
      .feed(partner_id_)
      .feed(brand_code_)
      .exec(http("get contract using brand")
        .get("/myntra-vms-service/vendorService/contract/brand/contract/${partner_id_}" +
          "/B2B_SELLER/BUYER?brandCode=${brand_code_}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(0)
        to getLongProperty("searchBrandContractCUSR")
        during ((duration * getIntProperty("searchBrandContractCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("searchBrandContractCUSR"))
          during ((duration * (100 - (getIntProperty("searchBrandContractCUSRDuration"))))/100) )

  private val address_id = Array(24012, 24004, 24132, 24126, 24124, 24112).map(e => Map("address_id" -> e)).array.circular
  private val getPartnerContactAdddressV2ById =
    scenario("get partner contact address V2 by id")
      .feed(address_id)
      .exec(http("get partner contact address V2 by id")
        .get("/myntra-vms-service/vendorService/v2/partnerContactAddress/${address_id}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("4109"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(0)
        to getLongProperty("searchBrandContractCUSR")
        during ((duration * getIntProperty("getPartnerContactAdddressV2ByIdCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("searchBrandContractCUSR"))
          during ((duration * (100 - (getIntProperty("getPartnerContactAdddressV2ByIdCUSRDuration"))))/100) )

  private val getPartnerContactAdddressV2ByPartnerId =
    scenario("get partner contact address V2 by partner id")
      .feed(partner2IdEnrich)
      .exec(http("get partner contact address V2 by partner id")
        .get("/myntra-vms-service/vendorService/v2/partnerContactAddress/partner/${partner2IdEnrich}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("4109"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(0)
        to getLongProperty("getPartnerContactAdddressV2ByPartnerIdCUSR")
        during ((duration * getIntProperty("getPartnerContactAdddressV2ByPartnerIdCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getPartnerContactAdddressV2ByPartnerIdCUSR"))
          during ((duration * (100 - (getIntProperty("getPartnerContactAdddressV2ByPartnerIdCUSRDuration"))))/100) )

  private val getPartnerContactAdddressV2ByPartnerIdAddressType =
    scenario("get partner contact address V2 by partner id and address type")
      .feed(partner2IdEnrich)
      .exec(http("get partner contact address V2 by partner id and address type")
        .get("/myntra-vms-service/vendorService/v2/partnerContactAddress/partner/${partner2IdEnrich}/addressType/SHIPPING")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("4109"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(0)
        to getLongProperty("getPartnerContactAdddressV2ByPartnerIdAddressTypeCUSR")
        during ((duration * getIntProperty("getPartnerContactAdddressV2ByPartnerIdAddressTypeCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getPartnerContactAdddressV2ByPartnerIdAddressTypeCUSR"))
          during ((duration * (100 - (getIntProperty("getPartnerContactAdddressV2ByPartnerIdAddressTypeCUSRDuration"))))/100) )

  private val _partnerId_ = Array(7368, 7364, 7362).map(e => Map("partnerId" -> e)).array.circular
  private val warehouse_id = Array(28,36, 125).map(e => Map("warehouseId" -> e)).array.circular
  private val getPartnerContactAdddressV2ByPartnerIdAndWarehouseId =
    scenario("get partner contact address V2 by partner id and warehouse id")
      .feed(_partnerId_)
      .feed(warehouse_id)
      .exec(http("get partner contact address V2 by partner id and warehouse id")
        .get("/myntra-vms-service/vendorService/v2/partnerContactAddress/partner/${partnerId}/warehouse/${warehouseId}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("4109"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getPartnerContactAdddressV2ByPartnerIdAndWarehouseIdCUSR")
        during ((duration * getIntProperty("getPartnerContactAdddressV2ByPartnerIdAndWarehouseIdCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getPartnerContactAdddressV2ByPartnerIdAndWarehouseIdCUSR"))
          during ((duration * (100 - (getIntProperty("getPartnerContactAdddressV2ByPartnerIdAndWarehouseIdCUSRDuration"))))/100) )

  private val _partnerId1 = Array(7368, 7364, 7362, 3974, 4602, 3854, 6419, 6420, 6750).map(e => Map("partnerId1" -> e)).array.circular
  private val getStoreForPartner =
    scenario("get store for partner")
      .feed(_partnerId1)
      .exec(http("get store for partner")
        .get("/myntra-vms-service/vendorService/partner/store/${partnerId1}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.partnerEntries").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getStoreForPartnerCUSR")
        during ((duration * getIntProperty("getStoreForPartnerCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getStoreForPartnerCUSR"))
          during ((duration * (100 - (getIntProperty("getStoreForPartnerCUSRDuration"))))/100) )

  private val getConfigurationV2SearchP1AndP2 =
    scenario("get Configuration V2 search By Partner1 Id & Role and Partner2 Id & Role")
      .feed(storeIdFeeder)
      .feed(sellerIdFeeder)
      .feed(configCategoryFeeder)
      .exec(http("get Configuration V2 search By Partner1 Id & Role and Partner2 Id & Role")
        .get("/myntra-vms-service/vendorService/configuration/v2/search/contract/${storeIdFeeder}/STORE/${sellerIdFeeder}" +
          "/B2C_SELLER/${configCategoryFeeder}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("1653"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getConfigurationV2SearchByPartnersIdAndPartnersRoleCUSR")
        during ((duration * getIntProperty("getConfigurationV2SearchByPartnersIdAndPartnersRoleCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getConfigurationV2SearchByPartnersIdAndPartnersRoleCUSR"))
          during ((duration * (100 - (getIntProperty("getConfigurationV2SearchByPartnersIdAndPartnersRoleCUSRDuration"))))/100) )

  private val getContactInfoV2ByPartnerIdAndWarehouseId =
    scenario("get Contract info V2 By Partner1 Id & Role and Partner2 Id & Role")
      .feed(partner1IdCont)
      .feed(partner1RoleCont)
      .feed(partner2IdCont)
      .feed(partner2RoleCont)
      .exec(http("get Contract info V2 By Partner1 Id & Role and Partner2 Id & Role")
        .get("/myntra-vms-service/vendorService/contract/v2/info/${partner1IdCont}/${partner1RoleCont}/${partner2IdCont}/${partner2RoleCont}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getContractInfoV2ByPartnersIdAndPartnersRoleCUSR")
        during ((duration * getIntProperty("getContractInfoV2ByPartnersIdAndPartnersRoleCUSRDuration"))/100),
        constantUsersPerSec(getIntProperty("getContractInfoV2ByPartnersIdAndPartnersRoleCUSR"))
          during ((duration * (100 - (getIntProperty("getContractInfoV2ByPartnersIdAndPartnersRoleCUSRDuration"))))/100) )

  private val _buyerId = Array(3974, 6420, 6752, 6751, 6750, 6419).map(e => Map("buyerId" -> e)).array.circular
  private val brandCode = Array("PUMA", "LEVI", "HGBS", "BIBA", "WDCT", "CLKN").map(e => Map("brandCode" -> e)).array.circular
  private val getSellerBuyerContractUsingContractNameAndBrandCode =
    scenario("get Seller Buyer Contract using contract name and brand code")
      .feed(_buyerId)
      .feed(brandCode)
      .exec(http("get Seller Buyer Contract using contract name and brand code")
        .get("/myntra-vms-service/vendorService/contract/${buyerId}/B2B_SELLER/BUYER/SELLER_BUYER/brand/${brandCode}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.preferredPartnerEntries[*]").count.gte(1))
        .check(jsonPath("$.preferredPartnerEntries[*].brands[0].brandCode").is("${brandCode}"))
        .check(jsonPath("$.partnerEntries[*]").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getSellerBuyerContractUsingBrandCode")
        during ((duration * getIntProperty("getSellerBuyerContractUsingBrandCodeDuration"))/100),
        constantUsersPerSec(getIntProperty("getSellerBuyerContractUsingBrandCode"))
          during ((duration * (100 - (getIntProperty("getSellerBuyerContractUsingBrandCodeDuration"))))/100) )

  private val _buyerId_ = Array(3974, 6420, 6752, 6751, 6750, 6419).map(e => Map("buyerId" -> e)).array.circular
  private val _brandCode_ = Array("PUMA", "LEVI", "HGBS", "BIBA", "WDCT", "CLKN").map(e => Map("brandCode" -> e)).array.circular
  private val getBuyerContractUsingBrandCode =
    scenario("get Buyer Contract using brand code")
      .feed(_buyerId_)
      .feed(_brandCode_)
      .exec(http("get Buyer Contract using brand code")
        .get("/myntra-vms-service/vendorService/contract/${buyerId}/B2B_SELLER/BUYER/brand/${brandCode}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.preferredPartnerEntries[*]").count.gte(1))
        .check(jsonPath("$.preferredPartnerEntries[*].brands[0].brandCode").is("${brandCode}"))
        .check(jsonPath("$.partnerEntries[*]").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getBuyerContractUsingBrandCode")
        during ((duration * getIntProperty("getBuyerContractUsingBrandCodeDuration"))/100),
        constantUsersPerSec(getIntProperty("getBuyerContractUsingBrandCode"))
          during ((duration * (100 - (getIntProperty("getBuyerContractUsingBrandCodeDuration"))))/100) )

  private val brandCode_ = Array("ADDS", "LEVI", "HGBS", "BIBA", "WDCT", "CLKN", "PUMA", "RBOK").map(e => Map("brandCode" -> e)).array.circular
  private val getPreferredBuyerUsingBrandCode =
    scenario("get preferred buyer using brand code")
      .feed(brandCode_)
      .exec(http("get preferred buyer using brand code")
        .get("/myntra-vms-service/vendorService/partner/IS_BETA_SELLER/brand/${brandCode}")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.status.statusCode").is("3"))
        .check(jsonPath("$.partnerEntries[*]").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getPreferredBuyerUsingBrandCode")
        during ((duration * getIntProperty("getPreferredBuyerUsingBrandCodeDuration"))/100),
        constantUsersPerSec(getIntProperty("getPreferredBuyerUsingBrandCode"))
          during ((duration * (100 - (getIntProperty("getPreferredBuyerUsingBrandCodeDuration"))))/100) )

  val styleId1 = Array(12705380, 12703688, 12701380, 12700860, 12699678).map(e => Map("styleId1" -> e)).array.circular
  val styleId2 = Array(12705378, 12703684, 12700886, 12700858, 12699676).map(e => Map("styleId2" -> e)).array.circular
  val vendorIds = Array(3236, 1951, 4557, 6462, 4901).map(e => Map("vendorId" -> e)).array.circular
  private val getVIMEntriesForVendorUsingVendorId  = scenario("get VIM entries for vendor using vendorId")
      .feed(styleId1)
      .feed(styleId2)
      .feed(vendorIds)
      .exec(http("get VIM entries for vendor using vendorId")
        .post("/myntra-vms-service/vendorService/vendorItemMaster/vendor/${vendorId}/search")
        .headers(headers)
        .body(StringBody(
        """{"styleIds": [${styleId1}, ${styleId2}]}""".stripMargin)).asJson
        .check(status.is(200))
        .check(jsonPath("$.data[*]").count.gte(1)))
      .inject(rampUsersPerSec(1)
        to getLongProperty("getVIMEntriesForVendorUsingVendorId")
        during ((duration * getIntProperty("getVIMEntriesForVendorUsingVendorIdDuration"))/100),
        constantUsersPerSec(getIntProperty("getVIMEntriesForVendorUsingVendorId"))
          during ((duration * (100 - (getIntProperty("getVIMEntriesForVendorUsingVendorIdDuration"))))/100) )

  val skuId = Array(44397110,43370468,35082807,45583838,43370468,35082807,44388814,43370406,35081874,44381912,43371274,39435540,44380520,43371802,35083473).map(e => Map("skuId" -> e)).array.circular
  val commercialType = Array("JIT", "OUTRIGHT", "SOR").map(e => Map("commercialType" -> e)).array.circular
  private val getVIMEntriesForVendorUsingStoreId  = scenario("get VIM entries for vendor using storeId")
    .feed(skuId)
    .feed(commercialType)
    .exec(http("get VIM entries for vendor using storeId")
      .post("/myntra-vms-service/vendorService/vendorItemMaster/store/2297/search")
      .headers(headers)
      .body(StringBody(
      """[ {"skuId" : "${skuId}","commercialType" : ["${commercialType}"]},{"skuId" : ${skuId}, "commercialType" : ["${commercialType}"]}, {"skuId" : ${skuId}, "commercialType" : ["${commercialType}"]}]""".stripMargin)).asJson
      .check(status.is(200))
      .check(jsonPath("$.status.statusCode").is("3"))
      .check(jsonPath("$.data[*]").count.gte(1)))
    .inject(rampUsersPerSec(1)
      to getLongProperty("getVIMEntriesForVendorUsingStoreId")
      during ((duration * getIntProperty("getVIMEntriesForVendorUsingStoreIdDuration"))/100),
      constantUsersPerSec(getIntProperty("getVIMEntriesForVendorUsingStoreId"))
        during ((duration * (100 - (getIntProperty("getVIMEntriesForVendorUsingStoreIdDuration"))))/100) )

  setUp(List(getSourceSystemByNameAndId, getVendorsInfoById, getVendorsBrandById, getFacilityV2ForB2CSeller,
    getFacilityV2ForB2BSeller, getPartnerWarehouseAddByIdAndWarehouseId, getPartnerContactAddByWarehouseId,
    getConfigurationSearchByPartnersIdAndPartnersRole, searchPartnerContactAddress, searchV2BrandContract,
    searchContractBasedOnPartnerRole, getContractByPartnersIdAndPartnersRole, getContractV2ByPartnersIdAndPartnersRole,
    getEnrichContractByPartnersIdAndPartnersRole, searchBrandContract, getPartnerContactAdddressV2ById,
    getPartnerContactAdddressV2ByPartnerId, getPartnerContactAdddressV2ByPartnerIdAddressType,
    getPartnerContactAdddressV2ByPartnerIdAndWarehouseId, getStoreForPartner, getConfigurationV2SearchP1AndP2,
    getContactInfoV2ByPartnerIdAndWarehouseId, getSellerBuyerContractUsingContractNameAndBrandCode, getBuyerContractUsingBrandCode,
    getPreferredBuyerUsingBrandCode, getContractBySourceNameAndType, getVIMEntriesForVendorUsingVendorId,
    getVIMEntriesForVendorUsingStoreId))
    .maxDuration(maxDurationInSec)
    .protocols(vmsBaseUrl)

}
