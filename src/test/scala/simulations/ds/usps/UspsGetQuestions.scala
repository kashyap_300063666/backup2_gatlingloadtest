package simulations.ds.usps


import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation


class UspsGetQuestions extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "datascience"
  /* Name of File to be downloaded */
  val fileName = "usps_gats.json"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val uspsBaseUrl = http.baseUrl  (BaseUrlConstructor.getBaseUrl(ServiceType.USPS_DS_SERVICE.toString)).disableWarmUp

  private val uspsheaders = Map( "content-type" -> "application/json")

  private var feederMap = jsonFile(filePath).circular

  private val usps_get_questions_api =
    scenario("usps get questions service")
        .feed(feederMap)
        .exec(http("usps get questions service")
        .post("/v1/recommendations/apparel/questions/${gender}/${articleType}")
        .headers(uspsheaders)
        .body(StringBody("""{"uidx":"${uidx}","pidx":"${pidx}","gender":"${gender}","articleType":"${articleType}"}""")).asJson
        .check(status.is(200)))

}