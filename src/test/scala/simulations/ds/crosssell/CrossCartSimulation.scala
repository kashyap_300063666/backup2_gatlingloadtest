package simulations.ds.crosssell


import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random


class CrossCartSimulation extends BaseSimulation {
	private val feederUtil = new FeederUtil()
	/* Name of the team Name */
	val containerName = "datascience"
	/* Name of File to be downloaded */
	val fileName = "crosscart_feeder_loader.csv"


val httpConf = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.CS_DS_SERVICE.toString)).disableWarmUp
private val csheaders = Map( "content-type" -> "application/json")
//val csvFile = "test-data/crosscart_feeder_loader.csv"
val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
val csvfeeder = csv(filePath).circular

val crosscart = scenario(scenarioName =  "crosscart feeder")
                .feed(csvfeeder)
		.exec(http(requestName = "all requests")
		.post("${endpoint}")
		.headers(csheaders)
		.body(StringBody("""${payload}""")).asJson
		.check(status.is(200))
	)


setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(httpConf)
}
