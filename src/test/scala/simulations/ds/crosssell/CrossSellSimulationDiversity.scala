package simulations.ds.crosssell


import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random


class CrossSellSimulationDiversity extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "datascience"
  /* Name of File to be downloaded */
  val fileName = "cs_style_uidx.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val csBaseUrl = http.baseUrl  (BaseUrlConstructor.getBaseUrl(ServiceType.CS_DS_SERVICE.toString)).disableWarmUp

  private val csheaders = Map( "content-type" -> "application/json","x-myntra-abtest" -> "pdp.crosssell=business_diversity")

  private val styleIdListMyntra= CSVUtils.readCSV2(filePath, false).map(e => e.apply(0))

  private val uidxListMyntra= CSVUtils.readCSV2(filePath, false).map(e => e.apply(1))

  private val feederMap = (styleIdListMyntra zip uidxListMyntra).map(e => Map("styleId"->e._1, "uidx"->e._2)).circular


  private val cs_mix_user =
    scenario("mix_users")
      .feed(feederMap)
      .exec(http("mix_users")
        .post("/api/v1/cross_sell")
        .headers(csheaders)
        .body(StringBody("""{"items":[{"styleId": "${styleId}"}],"prod_page": "pdp","uidx":"${uidx}"}""")).asJson
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(csBaseUrl)

}
