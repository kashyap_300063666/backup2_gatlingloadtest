package simulations.ds.crosssell


import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random


class CrossSellSimulationHeader extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "datascience"
  /* Name of File to be downloaded */
  val fileName = "cs_style_uidx_header.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val csBaseUrl = http.baseUrl  (BaseUrlConstructor.getBaseUrl(ServiceType.CS_DS_SERVICE.toString)).disableWarmUp


  //val csvFile = "test-data/cs_style_uidx_header.csv"
  //val csvfeeder = csv(csvFile).circular
  val csvfeeder = csv(filePath).circular


  private val cs_mix_user =
    scenario("mix_users")
      .feed(csvfeeder)
      .exec(http("mix_users")
        .post("/api/v1/cross_sell")
        .headers(Map( "content-type" -> "application/json","x-myntra-abtest" -> "${header}"))
        .body(StringBody("""{"items":[{"styleId": "${style_id}"}],"prod_page": "pdp","uidx":"${uidx}"}""")).asJson
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(csBaseUrl)

}
