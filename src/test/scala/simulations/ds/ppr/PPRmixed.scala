package simulations.ds.ppr


import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation


class PPRmixed extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "datascience"
  /* Name of File to be downloaded */
  val fileName = "ppr_mixed.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val pprBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.PPR_MULTI.toString)).disableWarmUp

  private val pprheaders = Map( "content-type" -> "application/json")

  //val csvFile = "test-data/ppr_mixed.csv"
  val csvfeeder = csv(filePath).circular

  private val ppr_mix_users =
    scenario("bnb")
      .feed(csvfeeder)
      .exec(http("bnb")
        .post("/pprservice/v1/recommendations")
        .headers(pprheaders)
        .body(StringBody("""{ "styleIds": [${styleId}],"recommendationType": ["${recommendationType}"], "storeId": 2297, "uidx": "${uidx}", "category": "${category}","gender":"${gender}" }""")).asJson
        .check(status.is(202)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(pprBaseUrl)

}
