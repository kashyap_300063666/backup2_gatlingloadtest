package simulations

import com.myntra.commons.util.{GatlingPropertiesLoader, LoggerTrait}
import io.gatling.core.Predef.{DurationJLong, Simulation, atOnceUsers, constantUsersPerSec, rampUsers, rampUsersPerSec}
import io.gatling.core.controller.inject.open.OpenInjectionStep
import io.gatling.core.structure.ScenarioBuilder

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import org.slf4j.LoggerFactory
import ch.qos.logback.classic.{Level, LoggerContext}

class BaseSimulation extends Simulation with LoggerTrait with GatlingPropertiesLoader {

  protected val headers = Map("Authorization" -> "Basic U3JlZW5pdmFzdWx1fnNyZWVuaX46YWJjZA==", "accept" -> "application/json", "Content-Type" -> "application/json")

  protected val headersAcceptOctetStream = Map("authorization" -> "Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==", "accept" -> "application/octet-stream")

  protected val noOfUsers = System.getProperty("users.count", "1").toInt

  protected val duration = DurationJLong(getLongProperty("duration.seconds")) seconds

  protected val toFilterScenarios = getProperty("filter.scenarios").split(",") map (e => e.trim.toLowerCase) toList

  protected val maxDurationInSec = DurationJLong(getLongProperty("duration.seconds") +
    getLongProperty("extraDuration.seconds")) seconds

  protected val grpcPortNum = System.getProperty("grpc.port", "8080").toInt

  //Setting Log level
  protected val httpLogLevel = getProperty("log.level.http").toUpperCase
  protected val grpcLogLevel = getProperty("log.level.grpc").toUpperCase

  val context: LoggerContext = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]
  context.getLogger("io.gatling.http.engine.response").setLevel(Level.valueOf(httpLogLevel))
  context.getLogger("com.github.phisgr.gatling.grpc").setLevel(Level.valueOf(grpcLogLevel))

  protected var step: OpenInjectionStep = null;
  if (getBooleanProperty("use.constant_users")) {
    LOG.info(s"use.constant_users -> ${noOfUsers} users over ${getLongProperty("duration.seconds")} seconds")
    step = constantUsersPerSec(noOfUsers) during (DurationJLong(getLongProperty("duration.seconds")) seconds)
  } else if (getBooleanProperty("use.ramp_up")) {
    LOG.info(s"use.ramp_up -> ${getIntProperty("ramp_up.users_count")} users during ${getLongProperty("duration.seconds")} seconds")
    step = rampUsers(getIntProperty("ramp_up.users_count")) during (DurationJLong(getLongProperty("duration.seconds")) seconds)
  } else if (getBooleanProperty("use.ramp_up_rate")) {
    LOG.info(s"use.ramp_up_rate -> ${getLongProperty("ramp_up_rate.start")} to ${getLongProperty("ramp_up_rate.end")}")
    step = rampUsersPerSec(getLongProperty("ramp_up_rate.start")) to getLongProperty("ramp_up_rate.end") during (DurationJLong(getLongProperty("duration.seconds")) seconds)
  } else if (getBooleanProperty("use.incrementUserPerSec")){
    step = incrementUsersPerSec(getLongProperty("incrementUserPerSec"))
      .times(getIntProperty("times"))
      .eachLevelLasting(getLongProperty("eachLevelLasting") seconds)
      .separatedByRampsLasting(getLongProperty("separatedByRampsLasting") seconds)
      .startingFrom(getLongProperty("startingFrom"))
  }
  else{
    LOG.info(s"at once -> ${noOfUsers}")
    step = atOnceUsers(noOfUsers)
  }

  protected def scenarios = {
    
    this.getClass.getDeclaredFields.filter(e => e.getType.getName.contains("ScenarioBuilder")) map (e => {
      e.setAccessible(true)
      val value = e.get(this).asInstanceOf[ScenarioBuilder]
      e.setAccessible(false)
      value
    }) filter (filterScenariosByName) toList
  }

  protected def filterScenariosByName(e: ScenarioBuilder) = {
    if (toFilterScenarios != null && !toFilterScenarios.isEmpty) {
      toFilterScenarios find (name => e.name.toLowerCase.contains(name)) isDefined
    } else {
      true
    }
  }
}

