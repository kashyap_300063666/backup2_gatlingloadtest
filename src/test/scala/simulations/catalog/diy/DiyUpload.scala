package simulations.catalog.diy

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random

class DiyUpload extends BaseSimulation {
  private val baseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.CMS_DIY_SERVICE.toString)

  private val diyServiceBaseUrl = http.baseUrl(baseUrl).disableWarmUp

  def van() = Random.nextInt(1000)

  val partnerIdFeeder = Array(1082).map(e => Map("partnerId" -> e)).array.circular
  val brandFeeder = Array("Puma", "Nike", "Reebok", "Adidas").map(e => Map("brand" -> e)).array.circular
  val vanFeeder = Array(van()).map(e => Map("van" -> e)).random
  val colorFeeder = Array("Red", "Blue", "Green").map(e => Map("color" -> e)).array.circular
  val buyingEntityIdFeeder = Array(3974, 4602).map(e => Map("buyingEntityId" -> e)).array.circular

  val imagesFeeder = Array(Map("imageUrl" -> Array("https://myntrawebimages.s3.amazonaws.com/diypartnerimages/cjjxz42si0019jntv0cjhesie/1/.jpg",
    "https://myntrawebimages.s3.amazonaws.com/diypartnerimages/cjjxz42si0019jntv0cjhesie/2/.jpg").map(e => "{\"imageUrl\": \"" + e + "\"}").mkString(","))).array.circular


  private val uploadImage = scenario("uploadImage")
    .feed(partnerIdFeeder)
    .feed(brandFeeder)
    .feed(vanFeeder)
    .feed(colorFeeder)
    .feed(buyingEntityIdFeeder)
    .feed(imagesFeeder)
    .exec(http("uploadImage")
      .post("/myntra-diy-service/style/upload")
      .headers(headers)
      .body(StringBody("""{"partnerId":${partnerId},"brand":"${brand}","van":"${van}","color":"${color}","buyingEntityId":${buyingEntityId},"images":[${imageUrl}]}""")).asJson)

  setUp(scenarios map (e => e.inject(step) ) toList).maxDuration(maxDurationInSec)
    .protocols(diyServiceBaseUrl)

}