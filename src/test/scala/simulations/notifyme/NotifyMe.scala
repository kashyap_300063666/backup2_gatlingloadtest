package simulations.notifyme

import java.text.SimpleDateFormat
import java.util.Calendar

import io.gatling.core.Predef._
import simulations.BaseSimulation
import com.github.phisgr.gatling.grpc.Predef._
import com.github.phisgr.gatling.grpc.protocol.GrpcProtocol
import io.grpc.ManagedChannelBuilder
import com.github.phisgr.gatling.pb._
import com.myntra.notifyme._
import io.gatling.core.session.Expression
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import com.myntra.commons.ServiceType
import com.myntra.notifyme.notifyme._
import io.gatling.core.feeder.BatchableFeederBuilder
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef.status
import io.grpc.{ManagedChannelBuilder, Metadata}


class NotifyMe extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "growthhack"
  /* Name of File to be downloaded */
  val fileNameDeviceId = "deviceID.csv"
  val filePathDeviceId: String = feederUtil.getFileDownloadedPath(containerName, fileNameDeviceId)
  val fileNameUidx = "uidxlistcs.csv"
  val filePathUidx: String = feederUtil.getFileDownloadedPath(containerName, fileNameUidx)

  private val notifymeUrl = System.getProperty("notifyme") //Pass the complete url of single server if req
  protected var grpcConf = grpc(
    ManagedChannelBuilder
      .forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.NOTIFYME.toString,""), grpcPortNum)
      .usePlaintext()
    )
  if(notifymeUrl != null) {
    grpcConf = grpc(
      ManagedChannelBuilder
        .forAddress(notifymeUrl, grpcPortNum)
    )
  }
  val uidxFeeder: BatchableFeederBuilder[String]#F = csv(filePathUidx).random
  //val deviceIdFeeder = csv("test-data/deviceID.csv").circular
  val deviceIdFeeder = csv(filePathDeviceId).circular

  
  var setNotifymeReq: Expression[SetNotifymeRequest] =
    SetNotifymeRequest(emailId = "${emailId}",source="PDP")
      .updateExpr(_.emailId :~ $("uidx"))

  
  val setNotifyMeScenario: ScenarioBuilder = scenario("NotifyMe: Set Notifyme")
    .feed(uidxFeeder)
    .feed(deviceIdFeeder)
    .exec(grpc("NotifyMe - Set Notifyme")
      .rpc(NotifymeServiceGrpc.METHOD_SET_NOTIFYME)
      .payload(setNotifymeReq)
      .header(Metadata.Key.of("x-meta-app", Metadata.ASCII_STRING_MARSHALLER))("deviceID=${deviceid};uidx=${uidx};")
      .header(Metadata.Key.of("x-mynt-ctx", Metadata.ASCII_STRING_MARSHALLER))("storeid=2297")
      .check()
    )

    setUp(scenarios map (e => e.inject(step))).maxDuration(maxDurationInSec)
      .protocols(grpcConf)
}
// java -Denvironment=stage -Duse.constant_users=true -Dduration.seconds=5 -jar build/libs/myntra-gatling-load-tests-all.jar -s simulations.notifyme.NotifyMe
//https://scaleit.myntra.com/runs/zMGTgE7x25scaleitmO#details