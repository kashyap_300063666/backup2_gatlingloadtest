package simulations.cms.sizing

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.http.Predef._
import io.gatling.core.Predef._
import simulations.BaseSimulation

import scala.util.Random

class SizingSimulation extends BaseSimulation {

private val sizingServiceBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.CMS_SIZING_SERVICE.toString)).disableWarmUp

  var articleTypes = Array("accessories","apparel","footwear","personal_care","sporting_goods","bapk","clth","hdbg","belt","cflk","sgls","glov","caps","scrf","mflr","scks","stle","ties","ubrl","wllt","wtch","cpri","jens","legg","shor","skrt","swmw","trpt","trst","trsr","drss","blzr","coat","jckt","krta","krti","shrt","swsh","swtr","tops","tsht","tnic","sndl","cash","fosh","spsh","dedt","hrcr","tlkt","swma","wabl","woac","wrbd","crbl","glbl","tnbl","dfbg","hbnd","flfp","shlc","spol","shac","waco","dupa","bref","vest","boxr","cmsl","lngr","chdr","stoc","neck","bang","braa","ticu","laba","pfbm","trac","wapo","ruba","mopo","trba","tigh","lngp","sspn","dhot","lhnc","pjpj","ptla","acgs","pend","earr","frgs","free_items","prmv","lnsh","ipod","ipad","nidr","bots","rmpr","brct","brch","jmst","dngr","hatt","home","cscv","fctl","hdtl","bhtl","tlst","btrb","ring","hrac","ankl","jwst","lpst","lpln","lpgs","lpce","cnlr","fdpr","eysd","mscr","nlps","dvst","jgng","clst","frgf","skwr","hrpt","spwr","robe","sldp","bwsb","jprp","lppl","kepl","rnjk","rntr","shrg","heels","flats","lunt","spsd","mgbg","mkrm","hrbh","hclr","tonr","fcwh","fmnc","snsr","eycm","cmpt","crst","lgst","btay","trnk","nspn","drmt","crdt","ptdp","bdst","bdcr","crtn","cshn","pils","plcr","vass","dtbl","ormt","cdls","cnhd","hmfg","hgst","frms","clks","btmt","btst","kycn","eber","tmtp","tmbt","pdbf","flkt","btac","domt","flmt","hdfc","hdcs","dtcr","tent","slmt","cstr","tbmt","slbg","wldr","mgst","tray","aprn","ktls","ovgl","npst","tlct","tbsl","bddt","srbl","hbcb","bdmt","hwsr","spcr","mngk","nles","sncs","btgs","srac","suit","boxx","plbg","mscb","hdph","spac","spst","byst","shwl","bbgs","cush","peco","mbac","srwi","lgas","pksq","brqs","ctfb","smws","fsbs","esdd","rgdd","ptdd","bedd","btdd","bbdl","ntst","slsh","slss","esgd","rggd","ptgd","begd","btgd","blkt","crpt","bhtw","hlmt","whgs","whog","tbps","tbcs","tbls","tbns","aecs","knts","kngs","gift_cards","egcd","bccm","mkgs","mkkt","mpbs","fser","mkpl","fsgl","blch","scgs","trmr","eptr","hrae","hrol","hcmk","hgsy","hrsm","byln","byol","bwes","bsso","sges","sbrr","bdmc","pens","psne","negd","slps","tshr","sshr","irhr","hshr","pchr","plzs","ptfm","wdns","wlsl","kyhd","mirr","tpat","atwk","wdcm","ptor","amol","amod","hfst","hdaf","atpl","vafl","swps","fnts","boxes","dtmg","phab","knse","lnbg","llls","rsws","kass","kthp","stjy","tsmt","jkmt","ssmt","ctbs","ftbs","btbs","vybs","bnrs","bnss","tsrs","tsks","ttbs","ttks","mspt","pwpt","tmst","sars","hkhd","sthr","bmas","bles","blcr","nngd","nndd","hdjw","tbwe","hngr","rgnr","bins","khtl","srwr","bdke","bcps","bgny","ckwe","bkwe","rnst","spkr","nhjk","toys_and_games","ladt","atag","stad","tyvh","afps","cnty","msty","rdot","nrpl","smtp","smbt","swct","pltr","nacd","bibs","fdes","runs","mtns","sidr","sles","fsel","pgsf","lcmo","fmhg","speq","sqbl","sqrq","crbk","cthb").map(e => Map("articleTypes" -> e)).random.circular

  private val getAllScalesByArticletype =
    scenario("getAllScalesByArticletype")
     .feed(articleTypes)
      .exec(http("getAllScalesByArticletype")
        .get("/myntra-sizingengine-service/scalemapping/getAllScalesByArticletype/${articleTypes}")
        .headers(headers)
        .check(status.is(200)))

  private val getAllUnifiedScalesByArticletype =
    scenario("getAllUnifiedScalesByArticletype")
     .feed(articleTypes)
      .exec(http("getAllUnifiedScalesByArticletype")
        .get("/myntra-sizingengine-service/scalemapping/getAllUnifiedScalesByArticletype/${articleTypes}")
        .headers(headers)
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(sizingServiceBaseUrl)
}