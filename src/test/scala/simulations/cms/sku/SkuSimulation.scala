package simulations.cms.sku

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.{Random, Try}



class SkuSimulation extends BaseSimulation{
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "cms"
  /* Name of File to be downloaded */
  val fileName = "m_skus.csv"
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)
  val fileNameSkuCode = "skucodes.csv"
  val filePathSkuCode: String = feederUtil.getFileDownloadedPath(containerName, fileNameSkuCode)
  val fileNameVans = "vans.csv"
  val filePathVans: String = feederUtil.getFileDownloadedPath(containerName, fileNameVans)

  private val skuServiceBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.SKU_SERVICE.toString)).disableWarmUp

  var bvcsFeeder = Iterator.continually(Map("bvcsPayload" -> Random.shuffle(CSVUtils.readCSV2(filePathVans, false).map(e => e.apply(0)).toList.map(m => """{"brandName":"Puma", "vendorArticleNo":""""+m+"""", "remarks":"Red", "size":"S"}""")).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString(",")))
  var skuIdsMyntraFeeder = Iterator.continually(Map("skuIds" ->  Random.shuffle(CSVUtils.readCSV2(filePath, false).map(e => e.apply(0)).toList).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString(",")))
  var skuCodesMyntraFeeder = Iterator.continually(Map("skuCodes" ->  Random.shuffle(CSVUtils.readCSV2(filePathSkuCode, false).map(e => e.apply(0)).toList).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString(",")))

  private val customizeSkuSearchList =
    scenario("customizeSkuSearchList")
      .feed(bvcsFeeder)
      .exec(http("customizeSkuSearchList")
        .post("/sku-service/v2/skus/customizeSkuSearchList")
        .headers(headers)
        .body(StringBody("""{"data":[${bvcsPayload}]}""")).asJson
        .check(status.is(200)))

  private val getSkusCache =
    scenario("getSkusCache")
      .feed(skuIdsMyntraFeeder)
      .exec(http("getSkusCache")
        .post("/sku-service/v2/skus/getSkusCache")
        .headers(headers)
        .body(StringBody("""{"data":[${skuIds}]}""")).asJson
        .check(status.is(200)))

  private val skuSearchBySkuCodes =
    scenario("skuSearchBySkuCodes")
      .feed(skuCodesMyntraFeeder)
      .exec(http("skuSearchBySkuCodes")
        .get("/sku-service/v2/skus/search?q=code.in:${skuCodes}")
        .headers(headers)
        .check(status.is(200)))

  private val skuSearchBySkuId =
    scenario("skuSearchBySkuId")
      .feed(skuIdsMyntraFeeder)
      .exec(http("skuSearchBySkuId")
        .get("/sku-service/v2/skus/search?q=id.eq:${skuIds}")
        .headers(headers)
        .check(status.is(200)))

  private val skuSearchBySkuIds =
    scenario("skuSearchBySkuIds")
      .feed(skuIdsMyntraFeeder)
      .exec(http("skuSearchBySkuIds")
        .get("/sku-service/v2/skus/search?q=id.in:${skuIds}")
        .headers(headers)
        .check(status.is(200)))


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(skuServiceBaseUrl)
}
