package simulations.cms.sim

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.http.Predef._
import simulations.BaseSimulation
import io.gatling.core.Predef._

import scala.util.{Random, Try}

class SIMSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "cms"
  /* Name of File to be downloaded */
  val fileNameSkuCodes = "j_skucodes.csv"
  val filePathSkuCodes: String = feederUtil.getFileDownloadedPath(containerName, fileNameSkuCodes)
  val fileNameSku = "j_skus.csv"
  val filePathSku: String = feederUtil.getFileDownloadedPath(containerName, fileNameSku)
  val fileNameMSkuCodes = "m_skucodes.csv"
  val filePathMSkuCodes: String = feederUtil.getFileDownloadedPath(containerName, fileNameMSkuCodes)
  val fileNameSimSku = "sim-skus.csv"
  val filePathSimSku: String = feederUtil.getFileDownloadedPath(containerName, fileNameSimSku)
  val fileNameSimStyles = "sim_styles.csv"
  val filePathSimStyles: String = feederUtil.getFileDownloadedPath(containerName, fileNameSimStyles)

  private val simBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.SELLER_SERVICE.toString)).disableWarmUp

  private val myntraStore = Array(2297).map(e => Map("store" -> e)).random
  private val jabongStore = Array(4603).map(e => Map("store" -> e)).random
  private val commonSkuIds1 = Array(11552154, 11552155, 11552156, 11552157).map(e => Map("commonSkuIds1" -> e)).random
  private val commonSkuIds2 = Array(11552154, 11552155, 11552156, 11552157).map(e => Map("commonSkuIds2" -> e)).random
  private val myntraSkuCode = Array(32697519,32697520,32697521,32697522,32697525,32697509,32697512,32697513,32697514,32697515,32697517,32697518,32697510,32697548,32697549,32697550,32697551,32697552,32697553,32697506,32697542).map(e => Map("skuCode" -> e)).random
  private val jabongSkuCode = Array(16542469,16542470,16542471,16542472,16542473,16542474,16542477,16542479,16542481,16542482,16542483,16542484,16542485,16542486,16542487,16542488,16542489,16542490,16542491,16542492,16542526,16542528,16542530,16542570,16542572,16542573,16542576,16542574,16542578,16542579,16542581,16542582,16542583,16542584).map(e => Map("skuCode" -> e)).random
  private val userId = Map("userId" -> "user")
  private val listingStatus = getProperty("getListingStatus");


  var myntraSkuIdsFeeder = Iterator.continually(
    Map("skuIds" ->  Random.shuffle(CSVUtils.readCSV2(filePathSimSku, false).map(e => e.apply(0)).toList).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString("},{\"skuId\":")
    ))

  var jabongSkuIdsFeeder = Iterator.continually(
    Map("skuIds" ->  Random.shuffle(CSVUtils.readCSV2(filePathSku, false).map(e => e.apply(0)).toList).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString("},{\"skuId\":")
    ))

  var myntraSkuCodesFeeder = Iterator.continually(
    Map("skuCodes" ->  Random.shuffle(CSVUtils.readCSV2(filePathMSkuCodes, false).map(e => e.apply(0)).toList).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString("},{\"skuCode\":")
    ))

  var jabongSkuCodesFeeder = Iterator.continually(
    Map("skuCodes" ->  Random.shuffle(CSVUtils.readCSV2(filePathSkuCodes, false).map(e => e.apply(0)).toList).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString("},{\"skuCode\":")
    ))

  var listingStatusFeeder = Iterator.continually(Map("payload" ->
    Random.shuffle(CSVUtils.readCSV2(filePathSimStyles, false).map(e => e.apply(0))
      .toList.map(m => """{"styleId":"""+m+""", "soldByPartnerId":6005, "soldToPartnerId":2297, "listingStatus":""""+listingStatus+"""", "listingComments":"through load test"}""")).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString(",")))

  var sellerMrpFeeder = Iterator.continually(Map("payload" ->
    Random.shuffle(CSVUtils.readCSV2(filePathSimStyles, false).map(e => e.apply(0))
      .toList.map(m => """{"styleId":"""+m+""", "soldByPartnerId":6005, "soldToPartnerId":2297, "sellerMrp":"""+Random.nextInt(50000)+"""}""")).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString(",")))

  private val getSIMGivenSkuAndStoreMyntra =
    scenario("getSIMGivenSkuAndStoreMyntra")
      .feed(myntraStore)
      .feed(myntraSkuIdsFeeder)
      .exec(http("getSIMGivenSkuAndStoreMyntra")
        .post("/myntra-seller-service/sellerItemMaster/v2/${store}/sellerItemsbySkuIds/")
        .headers(headers)
        .body(StringBody("""{"data": [{"skuId":${skuIds}}]}""")).asJson
        .check(status.is(200)))

  private val getSIMGivenSkuAndStoreJabong =
    scenario("getSIMGivenSkuAndStoreJabong")
      .feed(jabongStore)
      .feed(jabongSkuIdsFeeder)
      .exec(http("getSIMGivenSkuAndStoreJabong")
        .post("/myntra-seller-service/sellerItemMaster/v2/${store}/sellerItemsbySkuIds/")
        .headers(headers)
        .body(StringBody("""{"data": [{"skuId":${skuIds}}]}""")).asJson
        .check(status.is(200)))

  private val getSIMGivenSkuForAllStores =
    scenario("getSIMGivenSkuForAllStores")
      .feed(myntraSkuIdsFeeder)
      .exec(http("getSIMGivenSkuForAllStores")
        .post("/myntra-seller-service/sellerItemMaster/v2/sellerItemsbySkuIdsForAllStores/")
        .headers(headers)
        .body(StringBody("""{"data": [{"skuId":${skuIds}}]}""")).asJson
        .check(status.is(200)))

  private val getStyleGivenStoreAndSellerMyntra =
    scenario("getStyleGivenStoreAndSellerMyntra")
      .feed(myntraStore)
      .exec(http("getStyleGivenStoreAndSellerMyntra")
        .get("/myntra-seller-service/sellerItemMaster/v2/${store}/styleIds?sellerId=21&start=0&fetchSize=100")
        .headers(headers)
        .check(status.is(200)))

  private val getStyleGivenStoreAndSellerJabong =
    scenario("getStyleGivenStoreAndSellerJabong")
      .feed(jabongStore)
      .exec(http("getStyleGivenStoreAndSellerJabong")
        .get("/myntra-seller-service/sellerItemMaster/v2/${store}/styleIds?sellerId=21&start=0&fetchSize=100")
        .headers(headers)
        .check(status.is(200)))

  private val getSIMGivenSkuSellerStoreMyntra =
    scenario("getSIMGivenSkuSellerStoreMyntra")
      .feed(myntraStore)
      .feed(myntraSkuIdsFeeder)
      .exec(http("getSIMGivenSkuSellerStoreMyntra")
        .post("/myntra-seller-service/sellerItemMaster/v2/21/${store}/sellerItemsbySkuId/")
        .headers(headers)
        .body(StringBody("""{"data": [{"skuId":${skuIds}}]}""")).asJson
        .check(status.is(200)))

  private val getSIMGivenSkuSellerStoreJabong =
    scenario("getSIMGivenSkuSellerStoreJabong")
      .feed(jabongStore)
      .feed(jabongSkuIdsFeeder)
      .exec(http("getSIMGivenSkuSellerStoreJabong")
        .post("/myntra-seller-service/sellerItemMaster/v2/21/${store}/sellerItemsbySkuId/")
        .headers(headers)
        .body(StringBody("""{"data": [{"skuId":${skuIds}}]}""")).asJson
        .check(status.is(200)))

  private val getSIMGivenStoreAndSkuCodeMyntra =
    scenario("getSIMGivenStoreAndSkuCodeMyntra")
      .feed(myntraStore)
      .feed(myntraSkuCode)
      .exec(http("getSIMGivenStoreAndSkuCodeMyntra")
        .get("/myntra-seller-service/sellerItemMaster/v2/29/${store}/sellerSku/${skuCode}")
        .headers(headers)
      .check(status.is(200)))

  private val getSIMGivenStoreAndSkuCodeJabong =
    scenario("getSIMGivenStoreAndSkuCodeJabong")
      .feed(jabongStore)
      .feed(jabongSkuCode)
      .exec(http("getSIMGivenStoreAndSkuCodeJabong")
        .get("/myntra-seller-service/sellerItemMaster/v2/29/${store}/sellerSku/${skuCode}")
        .headers(headers)
        .check(status.is(200)))

  //omnichannel apis

  private val getSIMGivenSellerIdStoreIdSellerSkuCodeMyntra =
    scenario("getSIMGivenSellerIdStoreIdSellerSkuCodeMyntra")
      .feed(myntraStore)
      .feed(myntraSkuCode)
      .exec(http("getSIMGivenSellerIdStoreIdSellerSkuCodeMyntra")
        .get("/myntra-seller-service/sellerItemMaster/v2/29/${store}/sellerItemsbySellerIdandSellerSku/${skuCode}")
        .headers(headers)
        .check(status.is(200)))


  private val getSIMGivenSellerIdStoreIdSellerSkuCodeJabong =
    scenario("getSIMGivenSellerIdStoreIdSellerSkuCodeJabong")
      .feed(jabongStore)
      .feed(jabongSkuCode)
      .exec(http("getSIMGivenSellerIdStoreIdSellerSkuCodeJabong")
        .get("/myntra-seller-service/sellerItemMaster/v2/29/${store}/sellerItemsbySellerIdandSellerSku/${skuCode}")
        .headers(headers)
        .check(status.is(200)))

  private val getSellerItemsbySkuIdandSellerIdMyntra =
    scenario("getSellerItemsbySkuIdandSellerIdMyntra")
      .feed(myntraStore)
      .feed(myntraSkuCode)
      .exec(http("getSellerItemsbySkuIdandSellerIdMyntra")
        .get("/myntra-seller-service/sellerItemMaster/v2/${store}/${skuCode}/29/sellerItemsbySkuIdandSellerId")
        .headers(headers)
        .check(status.is(200)))

  private val getSellerItemsbySkuIdandSellerIdJabong =
    scenario("getSellerItemsbySkuIdandSellerIdJabong")
      .feed(jabongStore)
      .feed(jabongSkuCode)
      .exec(http("getSellerItemsbySkuIdandSellerIdJabong")
        .get("/myntra-seller-service/sellerItemMaster/v2/${store}/${skuCode}/29/sellerItemsbySkuIdandSellerId")
        .headers(headers)
        .check(status.is(200)))

  private val getsellerItemsbySkuIdForAllSellers =
    scenario("getsellerItemsbySkuIdForAllSellers")
      .feed(myntraStore)
      .feed(myntraSkuIdsFeeder)
      .exec(http("getsellerItemsbySkuIdForAllSellers")
        .get("/myntra-seller-service/sellerItemMaster/v2/${store}/${skuId}/sellerItemsbySkuIdForAllSellers")
        .headers(headers)
        .check(status.is(200)))

  private val getSellerItemsBySkuidsMyntraV2 =
    scenario("getSellerItemsBySkuidsMyntraV2")
      .feed(myntraStore)
      .feed(myntraSkuIdsFeeder)
      .exec(http("getSellerItemsBySkuidsMyntraV2")
        .post("/myntra-seller-service/sellerItemMaster/v2/{store}/sellerItemsbySkuIds/")
        .headers(headers)
        .body(StringBody("""{"data": [{"skuId":${skuIds}}]}""")).asJson
        .check(status.is(200)))

  private val getSellerItemsBySkuidsJabongV2 =
    scenario("getSellerItemsBySkuidsJabongV2")
      .feed(jabongStore)
      .feed(jabongSkuIdsFeeder)
      .exec(http("getSellerItemsBySkuidsJabongV2")
        .post("/myntra-seller-service/sellerItemMaster/v2/{store}/sellerItemsbySkuIds/")
        .headers(headers)
        .body(StringBody("""{"data": [{"skuId":${skuIds}}]}""")).asJson
        .check(status.is(200)))

  private val sellerItemsbySellerIdandSellerSkuMyntraV2 =
    scenario("sellerItemsbySellerIdandSellerSkuMyntraV2")
      .feed(myntraStore)
      .feed(myntraSkuCode)
      .exec(http("sellerItemsbySellerIdandSellerSkuMyntraV2")
        .get("/myntra-seller-service/sellerItemMaster/v2/21/{store}/sellerItemsbySellerIdandSellerSku/{sellerSku}")
        .headers(headers)
        .check(status.is(200)))

  private val sellerItemsbySellerIdandSellerSkuJabongV2 =
    scenario("sellerItemsbySellerIdandSellerSkuJabongV2")
      .feed(jabongStore)
      .feed(jabongSkuCode)
      .exec(http("sellerItemsbySellerIdandSellerSkuJabongV2")
        .get("/myntra-seller-service/sellerItemMaster/v2/21/{store}/sellerItemsbySellerIdandSellerSku/{sellerSku}")
        .headers(headers)
        .check(status.is(200)))


  private val updateListingStatus =
    scenario("updateListingStatus")
    .feed(listingStatusFeeder)
      .exec(http("updateListingStatus")
        .post("/myntra-seller-service/sellerItemMaster/v2/bulkUpdate/listingStatus")
        .headers(headers)
        .headers(userId)
        .body(StringBody("""{"data": [${payload}]}""")).asJson
        .check(status.is(200)))



  private val updateSellerMrp =
    scenario("updateSellerMrp")
    .feed(sellerMrpFeeder)
      .exec(http("updateSellerMrp")
        .post("/myntra-seller-service/sellerItemMaster/v2/bulkUpdate/sellerMrp?isUpdatingBundleStyle=false")
        .headers(headers)
        .body(StringBody("""{"data": [${payload}]}""")).asJson
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(simBaseUrl)
}