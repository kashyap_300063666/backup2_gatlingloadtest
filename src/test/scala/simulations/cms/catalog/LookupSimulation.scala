package simulations.cms.catalog

import com.myntra.commons.ServiceType
import com.myntra.commons.util.BaseUrlConstructor
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random

class LookupSimulation extends BaseSimulation {
  private val catalogBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.CMS_CATALOG_SERVICE.toString)).disableWarmUp

  def styleSQLId() = Random.nextInt(1000)

  def skuSQLId() = Random.nextInt(1000)

  private val findbyidstylelookup =
    scenario("findByIdStyleLookup")
      .exec(http("findByIdStyleLookup")
        .get("/myntra-catalog-service/lookup/style/" + styleSQLId())
        .headers(headers)
        .check(status.is(200)))

  private val findbyidskulookup =
    scenario("findByIdSkuLookup")
      .exec(http("findByIdSkuLookup")
        .get("/myntra-catalog-service/lookup/sku/" + skuSQLId())
        .headers(headers)
        .check(status.is(200)))

  var platformStyleId = Array(2515287, 2515284, 2515280, 2515262, 2515263, 2515290, 2532557, 2532558, 2532561, 2532563, 2532559, 2532567, 2532577, 2532581).map(e => Map("platformStyleId" -> e)).random.circular
  var stylePartnerIdForPartner = Array(2297, 2297, 2297, 2297, 2297, 2297, 4603, 4603, 4603, 4603, 4603, 4603, 4603, 4603).map(e => Map("partnerId" -> e)).random.circular

  private val findpartnerstyleid =
    scenario("findpartnerstyleid")
      .feed(platformStyleId)
      .feed(stylePartnerIdForPartner)
      .exec(http("findpartnerstyleid")
        .get("/myntra-catalog-service/lookup/style/platform/${platformStyleId}/partnerid/${partnerId}")
        .headers(headers)
        .check(status.is(200)))

  var partnerStyleId = Array(2515262, 2515263, 2515290, 2515326, 2515398, 2515397, 2516740, 2516736, 2516746, 2516747, 2516744, 2516745, 2516709, 2516706).map(e => Map("partnerStyleId" -> e)).random.circular
  val partnerId = 2297

  private val findplatformstyleid =
    scenario("findplatformstyleid ")
      .feed(partnerStyleId)
      .exec(http("findplatformstyleid")
        .get("/myntra-catalog-service/lookup/style/partner/${partnerStyleId}/partnerid/" + partnerId)
        .headers(headers)
        .check(status.is(200)))

  var platformSkuId = Array(15790929, 15790930, 15790931, 15790932, 15790912, 15790914, 15790916, 15790917, 15790919, 15790920, 15790921, 15790923, 15790924, 15790926).map(e => Map("platformSkuId" -> e)).random.circular

  private val findpartnerskuid =
    scenario("findpartnerskuid")
      .feed(platformSkuId)
      .exec(http("findpartnerskuid")
        .get("/myntra-catalog-service/lookup/sku/platform/${platformSkuId}/partnerid/" + partnerId)
        .headers(headers)
        .check(status.is(200)))

  var partnerSkuId = Array(15791953, 15791954, 15791955, 15791956, 15791957, 15791958, 15791959, 15791960, 15791961, 15791962, 15791963, 15791964, 15791965, 15791966).map(e => Map("partnerSkuId" -> e)).random.circular

  private val findplatformskuid =
    scenario("findplatformskuid")
      .feed(partnerSkuId)
      .exec(http("findplatformskuid")
        .get("/myntra-catalog-service/lookup/sku/partner/${partnerSkuId}/partnerid/" + partnerId)
        .headers(headers)
        .check(status.is(200)))

  var partnerSecondaryStyleId = Array(1000023, 1000025, 1000029, 1000125, 1000127, 1000129, 1000133, 1000135, 1000137, 1000139, 1000141, 1000219, 1000221, 1000225).map(e => Map("partnerSecondaryStyleId" -> e)).random.circular
  val jabongPartnerId = 4603

  private val findplatformstyleidusingpartnersecondarystyleId =
    scenario("findplatformstyleidusingpartnersecondarystyleId")
      .feed(partnerSecondaryStyleId)
      .exec(http("findplatformstyleidusingpartnersecondarystyleId")
        .get("/myntra-catalog-service/lookup/sku/partner/${partnerSecondaryStyleId}/partnerid/" + jabongPartnerId)
        .headers(headers)
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(catalogBaseUrl)
}