package simulations.cms.catalog

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random
import scala.util.Try


class CatalogSimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "cms"
  /* Name of File to be downloaded */
  val fileNameSku = "j_skus.csv"
  val filePathSku: String = feederUtil.getFileDownloadedPath(containerName, fileNameSku)
  val fileNameStyles = "j_styles.csv"
  val filePathStyles: String = feederUtil.getFileDownloadedPath(containerName, fileNameStyles)
  val fileNameMSku = "m_skus.csv"
  val filePathMSku: String = feederUtil.getFileDownloadedPath(containerName, fileNameMSku)
  val fileNameMStyles = "m_styles.csv"
  val filePathMStyles: String = feederUtil.getFileDownloadedPath(containerName, fileNameMStyles)
  val fileNamePriceTag = "price-tag-update-styles.csv"
  val filePathPriceTag: String = feederUtil.getFileDownloadedPath(containerName, fileNamePriceTag)


  private val catalogBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.CMS_CATALOG_SERVICE.toString)).disableWarmUp
  val myntraStoreHeader = Map("storeId" -> "2297")
  val multiFormHeader = Map("storeId" -> "3974", "Content-Type" -> "multipart/form-data")
  val gzipHeader = Map("Accept-Encoding" -> "application/gzip")
  val jabongStoreHeader = Map("storeId" -> "4603")
  var path = System.getProperty("user.dir");
  var articleTypeId =  Array(43,44,45,46,47,48,49,50,51,52,54,56,59,60,61,62,63,64,65,66,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,97,98,99,100,101,102,104,105,106,107,108,110,111,112,113,114,120,121,127,129,130,132,133,134,135,137,138,139,140,141,142,143,144,145,148,149,151,152,153,154,155,156,158,160,162,164,165,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,189,192,195,197,198,199,201,202,204,205,206,207,208,209,211,214,215,216,217,218,219,220,221,222,223,224,225,226,227,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,251,252,254,255,256,257,258,259,260,261,262,267,268,269,271,272,273,279,280,281,282,283,284,285,286,287,289,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,310,311,312,313,314,315,317,318,319,321,322,323,324,325,326,327,328,329,331,332,333,334,335,336,337,338,339,340,344,345,346,347,348,349,350,352,353,354,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,375,376,377,378,379,380,381,382,383,384,385,386,387,390,391,392,393,394,395,396,398,399,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,441,442,444,445,447,448,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,481,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,550,551,552,553,554,555,556,557,558,559,560,561,562,563,565,566,567,568,569,570,571,572,573,575,576,577,578,579,580,581,583,584,585,586,587,588,589,592,593,594,595,596,597,598,599,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621).map(e => Map("articleTypeId" -> e)).random

  var styleIdListMyntra= CSVUtils.readCSV2(filePathMStyles, false).map(e => e.apply(0)).map(e => Map("styleIds" -> e)).random
  var styleIdListJabong= CSVUtils.readCSV2(filePathStyles, false).map(e => e.apply(0)).map(e => Map("styleIds" -> e)).random
  val randomTag = Random.alphanumeric.take(10).mkString

  var styleIdsMyntraFeeder = Iterator.continually(Map("styleIds" ->  Random.shuffle(CSVUtils.readCSV2(filePathMStyles, false).map(e => e.apply(0)).toList).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString(",")))
  var styleIdsJabongFeeder =Iterator.continually(Map("styleIds" ->  Random.shuffle(CSVUtils.readCSV2(filePathStyles, false).map(e => e.apply(0)).toList).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString(",")))
  var skuIdsMyntraFeeder = Iterator.continually(Map("skuIds" ->  Random.shuffle(CSVUtils.readCSV2(filePathMSku, false).map(e => e.apply(0)).toList).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString(",")))
  var skuIdsJabongFeeder = Iterator.continually(Map("skuIds" ->  Random.shuffle(CSVUtils.readCSV2(filePathSku, false).map(e => e.apply(0)).toList).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString(",")))

  var tagStylesFeeder = Iterator.continually(Map("styleIds" ->  Random.shuffle(CSVUtils.readCSV2(filePathPriceTag, false).map(e => e.apply(0)).toList).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString(",")))
  var priceUpdateFeeder = Iterator.continually(Map("styleIds" -> Random.shuffle(CSVUtils.readCSV2(filePathPriceTag, false).map(e => e.apply(0)).toList.map(m => """{"styleId":"""+m+""","price":"""+Random.nextInt(50000)+"""}""")).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString(",")))
  var statusUpdateFeeder = Iterator.continually(Map("styleIds" ->  Random.shuffle(CSVUtils.readCSV2(filePathPriceTag, false).map(e => e.apply(0)).toList).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).mkString(",")))

  val userIdHeader = Map("userId" -> "mrp-engine")
//  val randomStyleFeeder = Iterator.continually(Map("styleIds" -> (8000000 + Random.nextInt(( 8100000 - 8000000) + 1)).toString))
//  val randomSkuFeeder = Iterator.continually(Map("skuIds" -> Stream.continually(44417396 - Random.nextInt((1500000) + 1)).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).toList.mkString(",")))
  val isPoMrpUpdate = getProperty("isPoMrpUpdate");
  var upperBound = Try(getProperty("getRandomStyleMax").toInt).getOrElse(8100000)
  var lowerBound = Try(getProperty("getRandomStyleMin").toInt).getOrElse(8000000)
  var countStyle = Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)
  val randomStyleFeeder = Iterator.continually(Map("styleIds" -> Stream.continually(2*( (lowerBound/2)+ Random.nextInt((upperBound/2) - (lowerBound/2) + 1))).take(countStyle).toList.mkString(",")));
  val randomSkuFeeder = Iterator.continually(Map("skuIds" -> Stream.continually(44417396 - Random.nextInt((300000) + 1)).take(Try(getProperty("getCountOfStylesOrSkus").toInt).getOrElse(1)).toList.mkString(",")))
  val clientIdHeader = Map("x-myntra-client-id" -> "catalogLoadTest")

  private val findbyidMyntra =
    scenario("findbyidMyntra")
      .feed(styleIdListMyntra)
      .exec(http("findbyidMyntra")
        .get("/myntra-catalog-service/product/${styleIds}")
        .headers(headers)
        .check(status.is(200)))

  private val findbyidDBMyntra =
    scenario("findbyidDBMyntra")
      .feed(randomStyleFeeder)
      .exec(http("findbyidDBMyntra")
        .get("/myntra-catalog-service/product/${styleIds}")
        .headers(headers)
        .check(status.is(200)))

  private val findbyidJabong =
    scenario("findbyidJabong")
      .feed(styleIdListJabong)
      .exec(http("findbyidJabong")
        .get("/myntra-catalog-service/product/${styleIds}")
        .headers(headers)
        .headers(jabongStoreHeader)
        .check(status.is(200)))


  var skuId = Array(109241, 173633, 173634, 173635, 113635, 123635, 133635, 134635, 135635).map(e => Map("skuId" -> e)).random

  private val solrSearch =
    scenario("solrSearch")
      .feed(skuId)
      .exec(http("solrSearch")
        .get("/myntra-catalog-service/v2/product/search?q=productOptions.sku.skuId.in:${skuId},${skuId},${skuId},${skuId},${skuId},${skuId},${skuId},${skuId},${skuId},${skuId}")
        .headers(headers)
        .headers(myntraStoreHeader)
        .check(status.is(200)))


  private val findbycsvMyntra =
    scenario("findbycsvMyntra")
      .feed(styleIdsMyntraFeeder)
      .exec(http("findbycsvMyntra")
        .get("/myntra-catalog-service/product/ids?productIdsCSV=${styleIds}")
        .headers(headers)
        .headers(myntraStoreHeader)
        .check(status.is(200)))

  private val findbycsvDBMyntra =
    scenario("findbycsvDBMyntra")
      .feed(randomStyleFeeder)
      .exec(http("findbycsvDBMyntra")
        .get("/myntra-catalog-service/product/${styleIds}")
        .headers(headers)
        .check(status.is(200)))

  private val findbycsvJabong =
    scenario("findbycsvJabong")
      .feed(styleIdsJabongFeeder)
      .exec(http("findbycsvJabong")
        .get("/myntra-catalog-service/product/ids?productIdsCSV=${styleIds}")
        .headers(headers)
        .headers(jabongStoreHeader)
        .check(status.is(200)))

   private val v2searchMyntra =
      scenario("v2searchMyntra")
        .feed(randomSkuFeeder)
        .exec(http("v2searchMyntra")
          .get("/myntra-catalog-service/v2/product/search?q=productOptions.sku.skuId.in:${skuIds}")
          .headers(headers)
          .headers(myntraStoreHeader)
          .check(status.is(200)))

  private val v2searchJabong =
    scenario("v2searchJabong")
      .feed(skuIdsJabongFeeder)
      .exec(http("v2searchJabong")
        .get("/myntra-catalog-service/v2/product/search?q=productOptions.sku.skuId.in:${skuIds}")
        .headers(headers)
        .headers(jabongStoreHeader)
        .check(status.is(200)))

  private val v2searchCursorMyntra =
    scenario("v2searchCursorMyntra")
      .feed(randomSkuFeeder)
      .exec(http("v2searchCursorMyntra")
        .get("/myntra-catalog-service/v2/product/search/paging?q=productOptions.sku.skuId.in:${skuIds}&sortBy=productId")
        .headers(headers)
        .headers(myntraStoreHeader)
        .check(status.is(200)))

  private val v2searchCursorJabong =
    scenario("v2searchCursorJabong")
      .feed(skuIdsJabongFeeder)
      .exec(http("v2searchCursorJabong")
        .get("/myntra-catalog-service/v2/product/search/paging?q=productOptions.sku.skuId.in:${skuIds}&sortBy=productId")
        .headers(headers)
        .headers(jabongStoreHeader)
        .check(status.is(200)))

  private val getbyskusMyntra =
    scenario("getbyskusMyntra")
      .feed(skuIdsMyntraFeeder)
      .exec(http("getbyskusMyntra")
        .post("/myntra-catalog-service/v2/product/getBySkus")
        .headers(headers)
        .body(StringBody("""{"soldByPartnerId":1234,"soldToPartnerId":2297,"list":[${skuIds}]}""")).asJson
        .check(status.is(200)))


  private val getbyskusJabong =
    scenario("getbyskusJabong")
      .feed(skuIdsJabongFeeder)
      .exec(http("getbyskusJabong")
        .post("/myntra-catalog-service/v2/product/getBySkus")
        .headers(headers)
        .body(StringBody("""{"soldByPartnerId":1234,"soldToPartnerId":4603,"list":[${skuIds}]}""")).asJson
        .check(status.is(200)))

  private val skujobtracker =
    scenario("skujobtracker")
      .exec(http("skujobtracker")
        .post("/myntra-catalog-service/product/import/v2/StyleSKUBulkUploader?styleCreationSource=oi&sourceID=test")
        .headers(headers)
        .headers(multiFormHeader)
        .formUpload("file", path + "/src/test/scala/simulations/cms/catalog/DIYVIMMT.xlsx")
        .check(status.is(200)))


   private val productCategory =
    scenario("productCategory")
      .feed(articleTypeId)
      .exec(http("productCategory")
        .get("/myntra-catalog-service/productcategory/${articleTypeId}")
        .headers(headers)
        .check(status.is(200)))

   private val productCategoryAll =
     scenario("productCategoryAll")
      .exec(http("productCategoryAll")
        .get("/myntra-catalog-service/productcategory/0")
        .headers(headers)
        .headers(gzipHeader)
        .check(status.is(200)))


  private val findByIdStyle = scenario("findByIdStyle")
    .feed(styleIdListMyntra)
    .exec(http("findByIdStyle")
      .get("/myntra-catalog-service/style/${styleIds}")
      .headers(headers)
      .check(status.is(200)))


  private val priceUpdate =
    scenario("priceUpdate")
    .feed(priceUpdateFeeder)
      .exec(http("priceUpdate")
        .post("/myntra-catalog-service/style/priceDbUpdate")
        .headers(headers)
        .headers(userIdHeader)
        .body(StringBody("""{"inputPriceEntries":[${styleIds}],"user":"load test user", "poMrpUpdate":""""+isPoMrpUpdate+"""","storeId":2297}""")).asJson
        .check(status.is(200)))


  private val tagUpdate =
    scenario("tagUpdate")
      .feed(tagStylesFeeder)
      .exec(http("tagUpdate")
        .post("/myntra-catalog-service/style/tagDbUpdate")
        .headers(headers)
        .body(StringBody("""{"tag":""""+randomTag+"""", "inputStyleIds":[${styleIds}], "comment":"loadtest tag", "user":"load test user", "action":"add_tag", "storeId":2297}""")).asJson
        .check(status.is(200)))

  private val statusUpdate =
    scenario("statusUpdate")
      .feed(statusUpdateFeeder)
      .exec(http("statusUpdate")
        .post("/myntra-catalog-service/style/statusDbUpdate")
        .headers(headers)
        .body(StringBody("""{"status":""""+getProperty("getStyleStatus")+"""", "inputStyleIds":[${styleIds}], "comment":"loadtest listStyles", "user":"load test user", "storeId":2297}""")).asJson
        .check(status.is(200)))

  private val findbyidMyntraWithRateLimit =
    scenario("findbyidMyntraWithRateLimit")
      .feed(styleIdListMyntra)
      .exec(http("findbyidMyntraWithRateLimit")
        .get("/myntra-catalog-service/product/${styleIds}")
        .headers(headers)
          .headers(clientIdHeader)
        .check(status.is(200)))

  private val v2searchMyntraWithRateLimit =
    scenario("v2searchMyntraWithRateLimit")
      .feed(randomSkuFeeder)
      .exec(http("v2searchMyntraWithRateLimit")
        .get("/myntra-catalog-service/v2/product/search?q=productOptions.sku.skuId.in:${skuIds}")
        .headers(headers)
        .headers(myntraStoreHeader)
        .headers(clientIdHeader)
        .check(status.is(200)))

  private val findbycsvMyntraWithRateLimit =
    scenario("findbycsvMyntraWithRateLimit")
      .feed(styleIdsMyntraFeeder)
      .exec(http("findbycsvMyntraWithRateLimit")
        .get("/myntra-catalog-service/product/ids?productIdsCSV=${styleIds}")
        .headers(headers)
        .headers(myntraStoreHeader)
        .headers(clientIdHeader)
        .check(status.is(200)))

  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec).protocols(catalogBaseUrl)

}
