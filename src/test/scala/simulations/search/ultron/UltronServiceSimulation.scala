package simulations.search.ultron

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, HLTUtility}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random

class UltronServiceSimulation extends BaseSimulation {

  private val ultronBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.ULTRON.toString)).disableWarmUp
  private val commonHeaders = Map("storeId" -> "2297", "cache-control" -> "no-cache", "accept" -> "application/json",
    "Content-Type" -> "application/json")

  /* Steps to be executed before running load tests */
  private val util = new HLTUtility()
  /* Download all prod StyleIds to a file */
  util.downloadFile("styles.csv")
  private val csvData: List[String] = CSVUtils.readCSV2("styles.csv", isIncludeHeader = false).map(e => e.apply(0)).toList
  private val numberOfStyles: Int = csvData.length
  val styleIdsFeeder: Iterator[Map[String, String]] =
    Iterator.continually(
      Map(
        "styleId" -> csvData.apply(Random.nextInt(numberOfStyles))
      )
    )

  // Improve by using offset and limit dynamically in header
  private val looksAPICall =
    scenario("get looks recommendations")
      .feed(styleIdsFeeder)
      .exec(http("fetch looks recommendations")
        .get("/ultron-service/ultron/v2/looks/")
        .headers(commonHeaders)
        .queryParam("id", "${styleId}")
        .check(status.is(200)))


  // Improve by calling style service to get touch points and call ultron only if touch points exists
  private val shoppableLooksAPICall = scenario("get shoppable looks recommendations")
    .feed(styleIdsFeeder)
    .exec(http("fetch shoppable looks recommendations")
      .post("/ultron-service/ultron/shoppableLooks")
      .headers(commonHeaders)
      .body(StringBody("""[ { "styleId": ${styleId}, "type": "SECONDARY" } ]""")).asJson
      .check(status.is(200)))

  // Improve by using dynamic data form a file, For now using static data for vlt
  private val cartFillerAPICall =
    scenario("get cart-filler recommendations")
      .feed(styleIdsFeeder)
      .exec(http("fetch cart-filler recommendations")
        .post("/ultron-service/ultron/cart-filler/recommendations")
        .headers(commonHeaders)
        .body(StringBody(
          """{
                "uidx": "",
                "items": [
                    {
                        "styleId": 1991268,
                        "skuId": 123,
                        "price": 1234.53,
                        "brand": "Roadster",
                        "articleType": "Tshirts",
                        "gender": "Women",
                        "size": "XL",
                        "addedTime": 1523524443,
                        "wareHouseId": 2
                    },
                    {
                        "styleId": 871386,
                        "skuId": 123,
                        "price": 1234.53,
                        "brand": "Roadster",
                        "articleType": "Tshirts",
                        "gender": "Women",
                        "size": "XL",
                        "addedTime": 1523524443,
                        "wareHouseId": 2
                    }
                ],
                "cartPrice": 2324.5,
                "freeShippingThreshold": 1233.3,
                "coupons": [
                    ""
                ]
		      }""")).asJson
        .check(status.is(200)))


  setUp(scenarios map (e => e.inject(step)) toList).maxDuration(maxDurationInSec)
    .protocols(ultronBaseUrl)
}
