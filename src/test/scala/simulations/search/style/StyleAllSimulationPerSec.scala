package simulations.search.style

import com.github.phisgr.gatling
import com.github.phisgr.gatling.grpc.Predef.{$, grpc, statusCode}
import com.github.phisgr.gatling.pb.{EpxrLens, value2ExprUpdatable}
import com.google.protobuf.any.Any.defaultInstance.is
import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, HLTUtility}
import com.myntra.style.style.{GetPdpDataBulkRequest, GetPdpDataRequest, GetProjectedEntriesRequest, StyleResponse, StyleServiceGrpc}
import io.gatling.core.Predef._
import io.gatling.core.session.Expression
import io.grpc.{ManagedChannelBuilder, Status}
import simulations.BaseSimulation

import scala.util.Random

class StyleAllSimulationPerSec extends BaseSimulation {
  private var grpcConf : com.github.phisgr.gatling.grpc.protocol.StaticGrpcProtocol = null

  private val pdpDataStep = constantUsersPerSec(System.getProperty("pdp.users", "1").toInt) during (DurationJLong(getLongProperty("duration.seconds")) seconds)
  private val pdpDataBulkStep = constantUsersPerSec(System.getProperty("pdpBulk.users", "1").toInt) during (DurationJLong(getLongProperty("duration.seconds")) seconds)
  private val projectedEntriesStep = constantUsersPerSec(System.getProperty("projected.users", "1").toInt) during (DurationJLong(getLongProperty("duration.seconds")) seconds)
  private val pdpBulkSkipDiscount = System.getProperty("pdpBulk.skipDiscount", "false").toBoolean
  private val projectedEntriesSkipDiscount = System.getProperty("projected.skipDiscount", "false").toBoolean
  private val baseUrl = System.getProperty("singleServerURL")//Pass the complete url of single server if req

  if(baseUrl != null && !baseUrl.isEmpty) {
    grpcConf = grpc(ManagedChannelBuilder.forAddress(baseUrl, grpcPortNum).usePlaintext())
    println("Updated BaseUrl:" + baseUrl)
  } else {
    grpcConf = grpc(ManagedChannelBuilder.forAddress(BaseUrlConstructor.getBaseUrl(ServiceType.STYLE.toString,""), grpcPortNum).usePlaintext())
  }

  /* Download all prod StyleIds to a file */
  private val util = new HLTUtility()
  private val styleUtil = new StyleUtil()
  util.downloadFile("styles.csv")

  private val csvData: List[String] = CSVUtils.readCSV2("styles.csv", isIncludeHeader = false).map(e => e.apply(0)).toList
  private val numberOfStyles: Int = csvData.length

  val styleIdFeeder: Iterator[Map[String, Long]] =
    Iterator.continually(
      Map(
        "styleId" -> csvData.apply(Random.nextInt(numberOfStyles)).toLong
      )
    )

  val styleIdListFeederGetPdpDataBulk: Iterator[Map[String, Seq[Long]]] =
    Iterator.continually(
      Map(
        "styleIdList" -> Random
          .shuffle(csvData)
          .take(styleUtil
            .getBatchSizePdpDataBulk(Random.nextInt(100)))
          .map(str => str.toLong)
      )
    )

  val styleIdListFeederGetProjectedEntries: Iterator[Map[String, Seq[Long]]] =
    Iterator.continually(
      Map(
        "styleIdList" -> Random
          .shuffle(csvData)
          .take(styleUtil
            .getBatchSizeProjectedEntries(Random.nextInt(100)))
          .map(str => str.toLong)
      )
    )

  val dynamicRequestPayloadGetPdpData: Expression[GetPdpDataRequest] = GetPdpDataRequest(clientId = "style-vlt",
    styleId = 1)
    .updateExpr(
      _.styleId :~ $("styleId")
    )

  val dynamicRequestPayloadGetPdpDataBulk: Expression[GetPdpDataBulkRequest] = GetPdpDataBulkRequest(clientId = "style-vlt",
    styleId = null, skipDiscountCall = pdpBulkSkipDiscount)
    .updateExpr(
      _.styleId :~ $("styleIdList")
    )

  val dynamicRequestPayloadGetProjectedEntries: Expression[GetProjectedEntriesRequest] = GetProjectedEntriesRequest(clientId = "style-vlt",
    styleId = null, skipDiscountCall = projectedEntriesSkipDiscount)
    .updateExpr(
      _.styleId :~ $("styleIdList")
    )

  val getProjectedEntries = scenario("getProjectedEntries")
    .feed(styleIdListFeederGetProjectedEntries)
    .forever(exec(grpc("getProjectedEntriest")
      .rpc(StyleServiceGrpc.METHOD_GET_PROJECTED_ENTRIES)
      .payload(dynamicRequestPayloadGetProjectedEntries)))

  val getPdpDataBulk = scenario("getPdpDataBulk")
    .feed(styleIdListFeederGetPdpDataBulk)
    .forever(exec(grpc("getPdpDataBulk")
      .rpc(StyleServiceGrpc.METHOD_GET_PDP_DATA_BULK)
      .payload(dynamicRequestPayloadGetPdpDataBulk)))

  val getPdpData = scenario("getPdpData")
    .feed(styleIdFeeder)
    .forever(exec(grpc("getPdpData")
      .rpc(StyleServiceGrpc.METHOD_GET_PDP_DATA)
      .payload(dynamicRequestPayloadGetPdpData)))

  setUp(
    getPdpData.inject(pdpDataStep),
    getPdpDataBulk.inject(pdpDataBulkStep),
    getProjectedEntries.inject(projectedEntriesStep))
    .maxDuration(maxDurationInSec)
    .protocols(grpcConf)
}
