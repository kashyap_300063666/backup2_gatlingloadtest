package simulations.search.style

class StyleUtil {

  def getBatchSizePdpDataBulk(randInt:Int) : Int = {
    if (randInt < 40) return 1
    else if (randInt < 40 + 11) return 2
    else if (randInt < 40 + 11 + 8) return 20
    else if (randInt < 40 + 11 + 8 + 5) return 3
    else if (randInt < 40 + 11 + 8 + 5 + 5) return 4
    else if (randInt < 40 + 11 + 8 + 5 + 5 + 3) return 6
    else if (randInt < 40 + 11 + 8 + 5 + 5 + 3 + 3) return 8
    else if (randInt < 40 + 11 + 8 + 5 + 5 + 3 + 3 + 3 + 2) return 5
    else if (randInt < 40 + 11 + 8 + 5 + 5 + 3 + 3 + 3 + 2 + 2) return 10
    else return 7
  }

  def getBatchSizeProjectedEntries(randInt:Int) : Int = {
    if (randInt < 44) return 15
    else if (randInt < 44 + 20) return 48
    else if (randInt < 44 + 20 + 12) return 20
    else if (randInt < 44 + 20 + 12 + 6) return 50
    else if (randInt < 44 + 20 + 12 + 6 + 3) return 1
    else if (randInt < 44 + 20 + 12 + 6 + 3 + 2) return 5
    else if (randInt < 44 + 20 + 12 + 6 + 3 + 2 + 2) return 2
    else return 2
  }

}
