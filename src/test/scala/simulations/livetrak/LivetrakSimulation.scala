package simulations.livetrak

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

class LivetrakSimulation extends BaseSimulation {

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "ilabs"
  /* Name of File to be downloaded */
  val fileName = "livetrak.csv"

  private val livetrakBaseURL = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.LIVETRAK.toString)).disableWarmUp

  protected val livetrakheaders = Map("Accept" -> "application/json", "Content-Type" -> "application/json")

  //val csvFile = System.getProperty("livetrakUidxFile", "test-data/livetrak.csv")
  val csvFile = System.getProperty("livetrakUidxFile", fileName)
  val filePath: String = feederUtil.getFileDownloadedPath(containerName, csvFile)

  //val csvfeeder = csv(csvFile).circular
  val csvfeeder = csv(filePath).circular

  val remindereventid="600e4aa1eb850c693837d16e"
  // 1) AudiencegetAll
  private val getAll =
    scenario("Audience_GetAllEvents")
      .feed(csvfeeder)
      .exec(http("Audience_GetAllEvents")
        .get("/livetrak/v1/event/all")
        .headers(livetrakheaders)
        .header("m-uidx", "${uuidx}")
        .header("x-mynt-ctx", "storeid=2297;uidx=${uuidx};nidx=${unidx};")
        .check(status.is(200)))

  //Audience getalleventbycategory
  val audiencegetalleventbycategory = scenario("Audience_GetAllEventsByCategory")
    .feed(csvfeeder)
    .exec(http("Audience_GetAllEventsByCategory")
      .get("/livetrak/v1/event/category/SUBMITTED")
      .headers(livetrakheaders)
      .header("m-uidx", "${uuidx}")
      .header("x-mynt-ctx", "storeid=2297;uidx=${uuidx};nidx=${unidx};")
      .queryParam("pagenum", "1")
      .queryParam("pagesize", "10")
      .check(status.is(200))
    )

  //Audience geteventbyid
  val Audiencegeteventbyid = scenario("Audience_GetEventById")
    .feed(csvfeeder)
    .exec(http("Audience_GetEventById")
      .get("/livetrak/v1/event/${eventid}")
      .headers(livetrakheaders)
      .header("m-uidx", "${uuidx}")
      .header("x-mynt-ctx", "storeid=2297;uidx=${uuidx};nidx=${unidx};")
      .check(status.is(200))
    )

  //Host GetAllevents
  val HostGetAllevents = scenario("Host_GetAllEvents")
    .feed(csvfeeder)
    .exec(http("Host_GetAllEvents")
      .get("/livetrak/v1/event/host/${huidx}")
      .headers(livetrakheaders)
      .header("m-uidx", "${huidx}")
      .header("x-mynt-ctx", "storeid=2297;uidx=${huidx};nidx=${hnidx};")
      .check(status.is(200))
    )

  //Host geteventsbycategory
  val Hostgeteventsbycategory = scenario("Host_GetAllEventsByCategory")
    .feed(csvfeeder)
    .exec(http("Host_GetAllEventsByCategory")
      .get("/livetrak/v1/event/host/${huidx}/category/SUBMITTED")
      .headers(livetrakheaders)
      .header("m-uidx", "${huidx}")
      .header("x-mynt-ctx", "storeid=2297;uidx=${huidx};nidx=${hnidx};")
      .queryParam("pagenum", "1")
      .queryParam("pagesize", "10")
      .check(status.is(200))
    )

  //Host geteventbyID
  val HostgeteventbyID = scenario("Host_GetEventById")
    .feed(csvfeeder)
    .exec(http("Host_GetEventById")
      .get("/livetrak/v1/event/${eventid}/host/${huidx}")
      .header("m-uidx", "${huidx}")
      .header("x-mynt-ctx", "storeid=2297;uidx=${huidx};nidx=${hnidx};")
      .check(status.is(200))
    )

/* Removed temporarily
  //Host getMetrics
  val HostgetMetrics = scenario("Host_GetMetrics")
    .feed(csvfeeder)
    .exec(http("Host_GetMetrics")
      .get("/livetrak/v1/event/${eventid}/host/${huidx}/metrics")
      .header("m-uidx", "${huidx}")
      .header("x-mynt-ctx", "storeid=2297;uidx=${huidx};nidx=${hnidx};")
      .check(status.is(200))
    )
 */


  //comment
  val livetrakcomment = scenario("Livetrak_Comment")
    .feed(csvfeeder)
    .exec(http("Livetrak_Comment")
      .post("/livetrak/v1/event/${eventid}/useraction")
      .headers(livetrakheaders)
      .header("m-uidx", "${uuidx}")
      .header("x-mynt-ctx", "storeid=2297;uidx=${uuidx};nidx=${unidx};")
      .body(StringBody(
        """{
		    "uidx":"${uuidx}",
		    "actionType":"COMMENT",
		    "data":[{
		        "key":"comment",
		        "value":"${comment}"
		        }]}""")).asJson
      .check(status.is(200))
    )

  //livetrak comment non loggedin
  val livetrakcommentnonloggedin = scenario("Livetrak_Comment_Nonloggedin")
    .feed(csvfeeder)
    .exec(http("Livetrak_Comment_Nonloggedin")
      .post("/livetrak/v1/event/${eventid}/useraction")
      .headers(livetrakheaders)
      .header("m-uidx", "")
      .header("x-mynt-ctx", "storeid=2297;nidx=${unidx};")
      .body(StringBody(
        """{
		    "actionType":"COMMENT",
		    "data":[{
		        "key":"comment",
		        "value":"${comment}"
		        }]}""")).asJson
      .check(status.is(200))
    )

  //livetrak useraction
  val livetrakuseraction = scenario("Livetrak_Useraction")
    .feed(csvfeeder)
    .exec(http("Livetrak_Useraction")
      .post("/livetrak/v1/event/${eventid}/useraction")
      .headers(livetrakheaders)
      .header("m-uidx", "")
      .header("x-mynt-ctx", "storeid=2297;uidx=${uuidx};nidx=${unidx};")
      .body(StringBody(
        """{
		    "actionType":"${action}",
		    "data":[{}]}""")).asJson
      .check(status.is(200))
    )

  //livetrak user event reminder
  val livetrakuserreminder = scenario("Livetrak_User_reminder")
    .feed(csvfeeder)
    .exec(http("Livetrak_User_reminder")
      .post("/livetrak/v1/event/"+remindereventid+"/reminder")
      .headers(livetrakheaders)
      .header("m-uidx", "")
      .header("x-mynt-ctx", "storeid=2297;uidx=${uuidx};nidx=${unidx};deviceID=${unidx};")
      .check(status.is(200))
    )

  //Audience geteventbyid
  val ReminderAudiencegeteventbyid = scenario("Reminder_Audience_GetEventById")
    .feed(csvfeeder)
    .exec(http("Reminder_Audience_GetEventById")
      .get("/livetrak/v1/event/"+remindereventid)
      .headers(livetrakheaders)
      .header("m-uidx", "${uuidx}")
      .header("x-mynt-ctx", "storeid=2297;uidx=${uuidx};nidx=${unidx};deviceID=${unidx};")
      .check(status.is(200))
    )

  setUp(scenarios map (e => e.inject(rampUsersPerSec(getLongProperty("ramp_up_rate.start"))
    to getLongProperty("ramp_up_rate.end")
    during getIntProperty("ramp_duration"),
    constantUsersPerSec(getIntProperty("hold_rate"))
      during getIntProperty("hold_duration"))))
    .maxDuration(maxDurationInSec)
    .protocols(livetrakBaseURL)
}