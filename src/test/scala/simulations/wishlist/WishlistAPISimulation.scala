package simulations.wishlist

import com.myntra.commons.ServiceType
import com.myntra.commons.util.{BaseUrlConstructor, CSVUtils, FeederUtil, HLTUtility}
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import simulations.BaseSimulation

import scala.util.Random

class WishlistAPISimulation extends BaseSimulation {
  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "Users.csv"

  val filePath: String = feederUtil.getFileDownloadedPath(containerName, fileName)

  private val wishlistBaseUrl = http.baseUrl(BaseUrlConstructor.getBaseUrl(ServiceType.WISHLIST.toString)).disableWarmUp

  private val csvData: List[String] = CSVUtils.readCSV2(filePath, isIncludeHeader = false).map(e => e.apply(0)).toList
    private val numberOfUsers: Int = csvData.length
    val uidxFeeder: Iterator[Map[String, String]] =
      Iterator.continually(
        Map(
          "uidx" -> csvData.apply(Random.nextInt(numberOfUsers))
        )
      )

  val getWishlist = scenario("get_wishlist")
     .feed(uidxFeeder)
     .exec(http("get_wishlist")
     .get("/v2/${uidx}/lists/wishlist?offset=1&size=5")
     .headers(headers)
     .header("x-mynt-ctx", "storeid=2297; uidx=$uidx")
     .check(status.is(200)))

  val addToWishlist = scenario("add_to_wishlist")
     .feed(uidxFeeder)
     .exec(http("add_to_wishlist")
     .post("/v2/${uidx}/lists/wishlist/add")
     .body(StringBody("""{
                           "styleId": 432,
                           "skuId" : 688
                       }""")).asJson
     .headers(headers)
     .header("x-mynt-ctx", "storeid=2297; uidx=$uidx")
     .check(status.is(200)))

  val removeFromWishlist = scenario("remove_from_wishlist")
     .feed(uidxFeeder)
     .exec(http("remove_from_wishlist")
     .post("/v2/${uidx}/lists/wishlist/delete")
     .body(StringBody("""{
                           "styleId" : 432
                       }""")).asJson
     .headers(headers)
     .header("x-mynt-ctx", "storeid=2297; uidx=$uidx")
     .check(status.is(200)))

  val getWishlistsSummary = scenario("get_wishlists_summary")
     .feed(uidxFeeder)
     .exec(http("get_wishlists_summary")
     .get("/v2/${uidx}/lists/wishlist/summary")
     .headers(headers)
     .header("x-mynt-ctx", "storeid=2297; uidx=$uidx")
     .check(status.is(200)))

  val addGetAndRemoveProduct = scenario("add_get_and_remove_product")
      .exec(addToWishlist)
      .exec(getWishlist)
      .exec(getWishlistsSummary)
      .exec(removeFromWishlist)

  setUp(addGetAndRemoveProduct.inject(step)).maxDuration(maxDurationInSec).protocols(wishlistBaseUrl)

}
