package simulations.wishlist

import com.myntra.commons.util.{CSVUtils, FeederUtil}
import io.gatling.core.Predef._
import io.gatling.http.Predef.{http, _}
import simulations.BaseSimulation

import scala.util.Random

object CaseExamples1 extends BaseSimulation{

  private val csvData: List[String] = CSVUtils.readCSV("test-data/Users.csv", isIncludeHeader = false).map(e => e.apply(0)).toList

  private val feederUtil = new FeederUtil()
  /* Name of the team Name */
  val containerName = "sfplatform"
  /* Name of File to be downloaded */
  val fileName = "addressIds.csv"


  def readFile(filename: String): Seq[String] = {
    val bufferedSource = scala.io.Source.fromResource(filename)
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close
    lines
  }


  def convertBooleanToStringMessage(bool: Boolean): String = bool match {

    case true => {
      print("1st")
      print("\n")
      "you said true"
    }
    case false => {
      println("2nd")
      print("\n")
      "you said false"
    }
  }

  def main(args: Array[String]) {

    val addressIdFileLocation = System.getProperty("addressIdFileLocation", fileName)
    val filePath: String = feederUtil.getFileDownloadedPath(containerName, addressIdFileLocation)
    val fileLoc2 = "/Users/300063666/git_New/scaleit/myntra-gatling-load-tests/SFPlatform_addressIds.csv"

    val uidxFileLocation = System.getProperty("uidxFileLocation", "test-data/linkedAccountsUidx.csv")
    val fileLoc = "/Users/300063666/git_New/scaleit/myntra-gatling-load-tests/src/test/resources/test-data/m_styles.csv"
    val fileLocRelative = "test-data/m_styles.csv"
    val readAbsolutePath = CSVUtils.readCSV2(fileLoc2)
    val uidxFeeder: Iterator[Map[String, String]] = {
      Iterator.continually(
        Map(
          "uidx" -> csvData.apply(Random.nextInt(10))
        )
      )
    }

    val getWishlist = scenario("get_wishlist")
      .feed(uidxFeeder)
      .exec(http("get_wishlist")
        .post("/v2/${uidx}/lists/wishlist?offset=1&size=5")
        .headers(headers)
        .body(ElFileBody(fileLocRelative)).asJson
        .check(status.is(200)))
    //val elfPath = ElFileBody(fileLoc2)
    val readRelativePath = CSVUtils.readCSV(fileLoc2)
    //val linesUidx = readFile(fileLoc)

    val classpathStr = System.getProperty("java.class.path")
    println(classpathStr)
    /*val numberOfUsers: Int = csvData.length
    val uidxFeeder: Iterator[Map[String, String]] = {
      Iterator.continually(
        Map(
          "uidx" -> csvData.apply(Random.nextInt(numberOfUsers))
        )
      )
    }
    println(uidxFeeder)

    val getWishlist = scenario("get_wishlist")
      .feed(uidxFeeder)
      .exec(http("get_wishlist")
        .get("/v2/${uidx}/lists/wishlist?offset=1&size=5")
        .headers(headers)
        .check(status.is(200)))

    System.setProperty("isDockinsEnabled", "true")
    System.setProperty("dockEnvName", "styleplus2")
    System.setProperty("environment", "stage")
    val wishlistBaseUrl = BaseUrlConstructor.getBaseUrl(ServiceType.WISHLIST.toString)

    println(wishlistBaseUrl)
    val a = convertBooleanToStringMessage(true);
    println(a)
*/
  }

}
