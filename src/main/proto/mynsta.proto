syntax = "proto3";

package mynsta;

option java_multiple_files = false;
option java_package = "com.myntra.mynsta";
option java_outer_classname = "MynstaProto";


//import "google/api/annotations.proto";

message Empty {

}

message GenericStringIdRequest {
  string id = 1;
}

message GenericStringUidxRequest {
  string uidx = 1;
}

message GenericStringIdArrayRequest {
  repeated string ids = 1;
}

message GenericUidxArrayRequest {
  repeated string uidxs = 1;
}

message GenericDeleteResponse {
  bool success = 1;
}

message GenericStringRequest {
  string value = 1;
}

message GenericStringAutoCompleteRequest {
  string value = 1;
  Pagination pagination = 2;
}

message GenericBooleanResponse {
  bool value = 1;
}

enum FollowOption {
  FOLLOW_AUTHOR = 0;
  UNFOLLOW_AUTHOR = 1;
  FOLLOW_TOPIC = 2;
  UNFOLLOW_TOPIC = 3;
}

message FollowRequest {
  FollowOption option = 2;
  repeated string ids = 4;
}

message FollowResponse {
  bool success = 1;
  repeated string failedIds = 2;
}

message Pagination {
  uint64 pageNumber = 1;
  uint64 nPerPage = 2;
}

message FeedRequest {

  // Both id & loggedInUidx fields are mandatory, without which system may either not work,
  // or will fallback to DefaultFeed, if possible

  // Value of id should be uidx for UserFeed, authorUidx for AuthorFeed, topicName for TopicFeed and empty for DefaultFeed.
  oneof id {
    string uidx = 1;
    string authorUidx = 2;
    string topicId = 3;
    string tagName = 4;
    bool default = 5;
    Reaction reaction = 9;
  }
  // This field has uidx/deviceId, to show personalised content (liked posts, etc) for the user.
  // "loggedInUidx" needed for any kind of feed fetched for logged in user.
  // "deviceId" can be used for personalized feed for non logged in users, once DS integration is done.
  oneof userId {
    string loggedInUidx = 6;
    string deviceId = 7;
  }
  Pagination pagination = 8;
}

message SmartStoreFeedRequest {
  repeated string topicIds = 1;
  repeated int64 postIds = 2;
  PostType type = 3;
  int32 minPosts = 4;
  int32 maxPosts = 5;
}

message FeedResponse {
  repeated EnrichedPost enrichedPosts = 1;
  repeated int64 postIds = 2;
  Pagination pagination = 3;
}

message UserInteraction {
  int64 postId = 1;
  Reaction reaction = 2;
}

message User {
  string uidx = 1;

  // As most of the times user is fetched, these values won't be needed to be enriched
  // We are returning IDs instead, bulk fetch APIs are available to fetch their details
  repeated UserInteraction userInteractions = 2;
  repeated string authorsFollowing = 3;
  repeated string topicsFollowing = 4;
}

enum PostType {
  POST_IMAGE = 0;
  POST_VIDEO = 1;
  POST_LISTICLE = 2;
  POST_STORY = 3;
  POST_POLL = 4;
  POST_QUIZ = 5;
}

enum PostStatus {
  POST_PENDING = 0; // When post is created initially, before moderator action.
  POST_APPROVED = 1; // When post is created with approval, or approved by moderator.
  POST_PUBLISHED = 2; // When post is created with approval, or approved by moderator.
  POST_ON_HOLD = 3; // If post is kept on hold.
  POST_REJECTED = 4; // If post is rejected.
  POST_DELETED = 5; // If post has been deleted.
  POST_DRAFT = 6; // If post has been saved as draft.
  POST_SCHEDULED = 7; // If post has been scheduled to publish.
}

enum AuthorType {
  BRAND = 0;
  INFLUENCER = 1;
  MYNTRA = 2;
}

// AuthorStatus is "pending" on creation/after update.
// If it has to be changed to INCOMPLETE/PENDING, an explicit call to ChangeAuthorStatus is needed.
enum AuthorStatus {
  AUTHOR_PROFILE_INCOMPLETE = 0; // When author has filled his profile, but needs more details before moderator approval. (this is default status)
  AUTHOR_PENDING = 1; // When author has registered initially, and ready for moderator approval.
  AUTHOR_APPROVED = 2; // When author is approved by moderator.
  AUTHOR_BLACK_LISTED = 3; // If an author is black-listed by moderator.
  AUTHOR_DELETED = 4; // If an author is deleted (soft delete).
}

enum TopicType {
  TOPIC = 0;
  TAG = 1;
}

enum MediaType {
  MEDIA_IMAGE = 0; // a still image
  MEDIA_VIDEO = 1; // video content
}

message Author {
  string uidx = 1;
  string uniqueHandle = 2; // Eg: tushar2708
  string name = 3; // Eg: Tushar Dwivedi
  string description = 4; // Eg. Just another guy, likes to code

  AuthorType type = 5;
  AuthorStatus status = 6;

  Media profileImage = 7;
  Media coverImage = 8;

  int64 followerCount = 9;
  int64 postsCount = 10;

  repeated int32 promotedStyles = 11;
  History history = 12;
}

message EnrichedAuthor {
  Author author = 1;
  UserFollowInteraction userAuthorInteraction = 2;
}

message AuthorTopicRequest {
  string authorUidx = 1;
  repeated string topics = 2;
}

message Image {
  string imageUrl = 1;
  float aspectRatio = 2;
  string resolution = 4;
}

enum VideoPlatform {
  ALL = 0; // "ALL" has intentionally been kept 0, to keep it the default behaviour
  BRIGHTCOVE = 1;
  MATRIX = 2;
  VIDEO_PLATFORM_NONE = 3;
}

message BrightCoveVideoFields {
  string videoId = 1; // brightcove video ID
  string videoJobId = 2; // brightcove video job ID
  string thumbnailJobId = 3; // brightcove thumbnail image's job ID
}

message MatrixVideoFields {
  string videoId = 1; // matrix video ID
  string videoJobId = 2; // matrix video job ID
  // Available video urls (here keys can be "hls", "dash", and values being respective video URLs)
  // In future more formats may be added
  map<string, string> videoUrls = 3;
}

enum VideoStatus {
  VIDEO_NONE = 0;
  VIDEO_PENDING = 1;
  VIDEO_DONE = 2;
  VIDEO_FAILED = 3;
  VIDEO_DELETED = 4;
}

message VideoEncodeStatus {
  VideoStatus status = 1;
  int32 retries = 2;
}

message UrlEntry {
  string url = 1;
  string key = 2;
}

message UrlArray {
  repeated UrlEntry urlEntries = 1;
}

message Video {
  int32 videoPlatformId = 1; // to decide between brightcove & internal video platform

  string videoBcId = 2; // for brightcove videos
  string videoBcJobId = 3; // for brightcove videos
  string thumbnailBcJobId = 4; // for brightcove videos

  string videoId = 5; // for internal videos platform
  string videoUrl = 6; // for internal videos platform
  string videoJobId = 7; // for internal videos platform

  string thumbnailJobId = 8; // for internal videos platform

  Image thumbnailImage = 9;
  int32 videoDuration = 10;

  map<string, UrlArray> videoUrls = 11;
}

message MultiVideo {
  VideoPlatform videoPlatformId = 1; // to decide between brightcove (1), matrix (2) and all(0) platform

  BrightCoveVideoFields bcvFields = 2;
  MatrixVideoFields mtxFields = 3;

  Image thumbnailImage = 4;
  int32 videoDuration = 5;
}

message Media {
  MediaType type = 1; // type can be enum. (video/image etc.)
  oneof content {
    Image image = 2;
    Video video = 3;
  }
  string id = 4;
  // more fields to be added
}

message PostMedia {
  string authorUidx = 1;
  Media media = 2;
}

message VideoCreationRequest {
  string authorUidx = 1;
  string videoAzureBlobId = 2;
  string mediaId = 3; // Set this field, if the call is being made to retry a video upload.
  VideoPlatform videoPlatformNeeded = 4; // to decide between brightcove (1), matrix (2) and all(0) platform
}

message ImageMediaRequest {
  string authorUidx = 1;
  Image image = 2;
}

message VideoMediaResponse {
  PostMedia media = 1;
}

message MediaFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  repeated string mediaIds = 2;
}

message MediaTypeFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  MediaType mediaType = 2;
}

// MediasFilter
message MediasFilter {
  MediaFilter mediaFilter = 1;
  AuthorFilter authorFilter = 2;
  MediaTypeFilter mediaTypeFilter = 3;
}

message MediasRequest {
  MediasFilter filter = 1;
  Pagination pagination = 2;
}

// MediasResponse has multiple media objects
message MediasResponse {
  repeated Media medias = 1;
  int64 totalCount = 2;
}

message VideoStatusResponse {
  VideoEncodeStatus bcvStatus = 1;
  VideoEncodeStatus mtxStatus = 2;
  PostMedia video = 3;
}

message ImageMediaResponse {
  PostMedia media = 1;
}

message Listicle {
  string title = 1;
  string description = 2;
  Media media = 3;
  repeated ListicleEntry listicleEntries = 4;
}

message ListicleEntry {
  string title = 1;
  string description = 2;
  repeated Media medias = 3;
  repeated int32 styleIds = 4;
}

message UserPostInteraction {
  Reaction reaction = 1;
  bool bookmarked = 2;
}

message UserFollowInteraction {
  FollowOption followState = 1;
}

message StatusChangeEntry {
  int32 status = 1;
  int64 changedAt = 2;
  string moderatorUidx = 3;
  string comment = 4;
}

message History {
  repeated StatusChangeEntry changeHistory = 1;
  int64 createdAt = 2;
  int64 lastModifiedAt = 3;
  int64 lastPublishedAt = 4;
}

message ReactionCounts {
  int32 like = 1;
  int32 love = 2;
  int32 laugh = 3;
  int32 wow = 4;
  int32 angry = 5;
  int32 sad = 6;
}

message Banner {
  int64 id = 1;
  EnrichedAuthor enrichedAuthor = 2;
  PostType type = 3;
  PostStatus status = 4;

  string text = 5;
  repeated Media medias = 6;
}

message EnrichedBanner {
  Banner banner = 1;
  History history = 2;
}

message EnrichedPost {

  EnrichedPostData enrichedPostData = 1;
  ReactionCounts reactionCounts = 2;
  History history = 3;

  // this field is only useful, when post(s) are being fetched in context of a uidx
  UserPostInteraction userPostInteraction = 4;
  string scheduleId = 5;
  int64 scheduleTime = 6;
}

message AuthorFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  repeated string authorUidxs = 2;
}

message PostFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  repeated int64 postIds = 2;
}

message StoryFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  repeated string storyIds = 2;
}

message AuthorStatusFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  repeated AuthorStatus statuses = 2;
}

message AuthorTypeFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  repeated AuthorType types = 2;
}

message PostStatusFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  repeated PostStatus statuses = 2;
}

message PostTypeFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  repeated PostType types = 2;
}

message MetaTagFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  repeated string metaTagIds = 2;
}

message TopicFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  repeated string topicIds = 2;
}

message TopicGroupFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  repeated string topicGroupIds = 2;
}

message TimeRangeFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  int64 startTime = 2;
  int64 endTime = 3;
}

message PersonaFilter {
  bool selected = 1; // "selected" should be true for the filter to be applied
  repeated string personaTags = 2;
}

// AuthorsFilter
message AuthorsFilter {
  AuthorFilter authorFilter = 1;
  AuthorStatusFilter authorStatusFilter = 2;
  AuthorTypeFilter authorTypeFilter = 3;
}

// TopicsFilter
message TopicsFilter {
  TopicFilter topicFilter = 1;
}

// MetaTagsFilter
message MetaTagsFilter {
  MetaTagFilter metaTagFilter = 1;
}

// TopicGroupsFilter
message TopicGroupsFilter {
  TopicGroupFilter topicGroupFilter = 1;
  TopicFilter topicFilter = 2;
  PersonaFilter personaFilter = 3;
}

message PostsFilter {
  PostFilter postFilter = 1;
  AuthorFilter authorFilter = 2;
  PostStatusFilter postStatusFilter = 3;
  TopicFilter topicFilter = 4;
  TimeRangeFilter timeRangeFilter = 5; // Works on createdAt (epoch nanosecond values expected)
  PostTypeFilter postTypeFilter = 6;
  MetaTagFilter metaTagFilter = 7;
}

message StoriesFilter {
  StoryFilter storyFilter = 1;
  AuthorFilter authorFilter = 2;
  PostStatusFilter postStatusFilter = 3;
  TopicFilter topicFilter = 4;
  TimeRangeFilter timeRangeFilter = 5; // Works on createdAt (epoch nanosecond values expected)
}

message MetaTagsRequest {
  MetaTagsFilter filter = 1;
  Pagination pagination = 2;
}

message TopicsRequest {
  TopicsFilter filter = 1;
  Pagination pagination = 2;
}

message TopicGroupsRequest {
  TopicGroupsFilter filter = 1;
  Pagination pagination = 2;
}

message PostsRequest {
  PostsFilter filter = 1;
  Pagination pagination = 2;
}

message StoriesRequest {
  StoriesFilter filter = 1;
  Pagination pagination = 2;
}

message Story {
  int64 id = 1;
  string authorUidx = 2;
  PostType type = 3;
  PostStatus status = 4;

  repeated string topicIds = 5;
  repeated string tags = 6;

  string text = 7;
  repeated Media medias = 8;
  repeated int32 styles = 9;
}

message PostData {
  int64 id = 1;
  //If mynsta provide all author info within posts otherwise just the authorUidx.
  string authorUidx = 2;
  PostType type = 3;
  PostStatus status = 4;

  repeated string topicIds = 5; //This field contains string Object IDs for topics
  repeated string tags = 6; //This field contains string names for tags

  string text = 7;
  repeated Media medias = 8;
  //    repeated string mediaIds = 8;
  repeated int32 styles = 9;

  Listicle listicle = 10;
  repeated string metaTags = 11; //This field contains names for meta-tags
  PollContent pollContent = 12;
  QuizContent quizContent = 13;
}

message UserRequest {
  string uidx = 1;
  Pagination pagination = 2;
}

message AuthorsRequest {
  AuthorsFilter filter = 1;
  Pagination pagination = 2;
}

message EnrichedStoryData {
  int64 id = 1;
  EnrichedAuthor enrichedAuthor = 2;
  PostType type = 3;
  PostStatus status = 4;

  repeated EnrichedTopic enrichedTopics = 5;
  repeated Tag tags = 6;

  string text = 7;
  repeated Media medias = 8;
  repeated int32 styles = 9;
}

message EnrichedPostData {
  int64 id = 1;
  EnrichedAuthor enrichedAuthor = 2;
  PostType type = 3;
  PostStatus status = 4;

  repeated EnrichedTopic enrichedTopics = 5;
  repeated Tag tags = 6;

  string text = 7;
  repeated Media medias = 8;
  repeated int32 styles = 9;

  Listicle listicle = 10;
  repeated MetaTag metaTags = 11; //This field contains names for meta-tags
  EnrichedPollContent pollContent = 12;
  EnrichedQuizContent quizContent = 13;
}

message RelatedContentRequest {
  int64 postId = 1;
  Pagination page = 2;
}

message PostPreview {
  int64 id = 1;
  PostType type = 2;
  Media media = 3;
}

message VideoPlaylistItem {
  int64 postId = 1;
  Video video = 2;
  string authorName = 3;
  string title = 4;
  string description = 5;
  UserPostInteraction userPostInteraction = 6;
}

message VideoPlaylist {
  string title = 1;
  repeated VideoPlaylistItem videoItems = 2;
  int32 videosCount = 3;
}

message EnrichedStory {
  EnrichedStoryData enrichedStoryData = 1;
  History history = 2;
}

message AuthorStoryThumb {
  EnrichedAuthor author = 1;
  int32 storyCount = 2;
}

message StoryPanel {
  repeated AuthorStoryThumb storyThumbs = 1;
}

message AuthorsStories {
  repeated AuthorStoriesCarousel authorStoriesCarousel = 1;
}

message AuthorStoriesCarousel {
  EnrichedAuthor author = 1;
  repeated EnrichedStory enrichedStories = 2;
  uint32 indexTillSeen = 3;
}

message AuthorList {
  repeated EnrichedAuthor enrichedAuthors = 1;
}

message TopicList {
  repeated EnrichedTopic enrichedTopics = 1;
}

message AuthorPostsCarousel {
  EnrichedAuthor enrichedAuthor = 1;
  repeated PostPreview postPreviews = 2;
}

message SmartStorePosts {
  repeated PostPreview postPreviews = 1;
}

message TopicPostsCarousel {
  EnrichedTopic enrichedTopic = 1;
  repeated PostPreview postPreviews = 2;
}

message EnrichPostsRequest {
  string uidx = 1;
  repeated int64 postIds = 2;
}

message EnrichedPostsResponse {
  repeated EnrichedPost enrichedPosts = 1;
  int64 totalCount = 2;
}

message EnrichedStoriesResponse {
  repeated EnrichedStory enrichedStories = 1;
  int64 totalCount = 2;
}

// EnrichedAuthorsResponse has one author and it's posts
message EnrichedAuthorsResponse {
  repeated EnrichedAuthor enrichedAuthors = 1;
  int64 totalCount = 2;
}

// EnrichedTopicsResponse has one topic and it's posts
message EnrichedTopicsResponse {
  repeated EnrichedTopic enrichedTopics = 1;
  int64 totalCount = 2;
}

message EnrichedTagsResponse {
  repeated Tag tags = 1;
}

message EnrichedMetaTagsResponse {
  repeated MetaTag metaTags = 1;
  int64 totalCount = 2;
}

message EnrichedUsersResponse {
  repeated User users = 1;
}

message PostByIdRequest {
  int64 id = 1;
}

// For now, only NONE & LIKE are to be used, in future, others can be used too
enum Reaction {
  NONE = 0;
  LIKE = 1;
  LOVE = 2;
  LAUGH = 3;
  WOW = 4;
  ANGRY = 5;
  SAD = 6;
  BOOKMARK = 7;
}

message ReactRequest {
  int64 postId = 1;
  Reaction reaction = 3;
}

// This is general message for success/failure response with message
message ReactResponse {
  bool success = 1;
  uint32 reactionCount = 2;
}

message SeenPostRequest {
  repeated int64 postIds = 1;
}

// This is general message for success/failure response with message
message SeenPostResponse {
  bool success = 1;
}

message PostRequest {
  PostData postData = 1;
}

message PostStatusChangeRequest {

  int64 postId = 1;
  PostStatus newStatus = 2;
  // moderatorUidx value is ignored if status is "PENDING",
  // but in other cases non-empty "moderatorUidx" would mean that action is made by moderator
  string moderatorUidx = 3;
  string moderatorComment = 4;
  int64 scheduleTime = 5;
}

message AuthorStatusChangeRequest {
  string authorUidx = 1;
  AuthorStatus newStatus = 2;
  // moderatorUidx value is ignored if status is "PENDING",
  // but in other cases non-empty "moderatorUidx" would mean that action is made by moderator
  string moderatorUidx = 3;
  string moderatorComment = 4;
}

// This is general message for success/failure response for post status change request
message StatusChangeResponse {
  bool success = 1;
}

message PostResponse {
  EnrichedPost enrichedPost = 1;
}

message PostsResponse {
  repeated EnrichedPost enrichedPosts = 1;
  string cursorObjectId = 2;
  int64 count = 3;
  Pagination pagination = 4;
}

message Tag {
  string id = 1;
  string name = 2;
  int64 postsCount = 3;
}

message MetaTag {
  string id = 1;
  string name = 2;
  int64 postsCount = 3;
}

message Topic {
  string id = 1;
  string name = 2;
  Media imageData = 3;
  int64 followerCount = 4;
  int64 postsCount = 5;
}

message EnrichedTopic {
  Topic topic = 1;
  UserFollowInteraction userTopicsInteraction = 2;
}

enum EnvelopeType {
  ENVELOPE_NONE = 0; // none, default
  ENVELOPE_AUTHOR = 1; // author
  ENVELOPE_TOPIC = 2; // topic
  ENVELOPE_TAG = 3; // tag
}

enum FeedEnvelopType {
  ENVELOPE_FEED_NONE = 0; // none, default
  ENVELOPE_AUTHOR_POST_CAROUSEL = 1;
  ENVELOPE_TOPIC_POST_CAROUSEL = 2;
  ENVELOPE_AUTHOR_LIST = 3;
  ENVELOPE_TOPIC_LIST = 4;
  ENVELOPE_POST_BANNER = 5;
  ENVELOPE_ENRICHED_POST = 6;
  ENVELOPE_SMART_STORE_FEED = 7;
}

message GenericEnvelopeItem {
  EnvelopeType type = 1;
  oneof item {
    EnrichedAuthor enrichedAuthor = 2;
    EnrichedTopic enrichedTopic = 3;
    Tag tag = 4;
  }
}

message GenericFeedItem {
  FeedEnvelopType type = 1;
  oneof item {
    AuthorPostsCarousel authorPostsCarousel = 2;
    TopicPostsCarousel topicPostsCarousel = 3;
    AuthorList authorList = 4;
    TopicList topicList = 5;
    EnrichedPost enrichedPost = 6;
    EnrichedBanner enrichedBanner = 7;
    GenericEnvelopeItem genericEnvelopeItem = 8;
    SmartStorePosts smartStorePosts = 9;
  }
}

message SearchResponse {
  repeated GenericEnvelopeItem items = 1;
}

message GenericFeedResponse {
  repeated GenericFeedItem items = 1;
}

message PromoteTagRequest {
  string tagId = 1;
  Media imageData = 2;
}

message TagsResponse {
  repeated Tag tags = 1;
}

message TopicGroup {
  string id = 1;
  string name = 2;
  repeated string topicIds = 3;
  Media media = 4;
  int32 score = 5;
  repeated string personaTags = 6;
}

message EnrichedTopicGroup {
  string id = 1;
  string name = 2;
  repeated EnrichedTopic enrichedTopics = 3;
  Media media = 4;
  int32 score = 5;
  repeated string personaTags = 6;
}

message EnrichedTopicGroupsResponse {
  repeated EnrichedTopicGroup enrichedTopicGroups = 1;
  int64 totalCount = 2;
}

message TopicGridResponse {
  repeated EnrichedTopicGroup enrichedTopicGroups = 1;
  repeated string topicGroupIds = 2;
}

message AuthorPromotedStylesRequest {
  string authorUidx = 1;
  Pagination pagination = 2;
}

message AuthorPromotedStylesResponse {
  repeated int32 styles = 1;
  int32 totalCount = 2;
}

message AuthorsResponse {
  repeated EnrichedAuthor enrichedAuthors = 1;
}

message UsersResponse {
  repeated User users = 1;
}

message AuthorRecommendationResponse {
  repeated EnrichedAuthor enrichedAuthors = 1;
}

message TopicRecommendationResponse {
  repeated EnrichedTopic enrichedTopics = 1;
}

message AuthorTopicRecommendationResponse {
  repeated EnrichedAuthor enrichedAuthors = 1;
  repeated EnrichedTopic enrichedTopics = 2;
}

message OnboardingRequest {
  oneof id {
    string uidx = 1; // For DS based persona creation, based on user affinity
    string personaTag = 2; // For non-DS filtering (this value can be man, woman, etc)
  }
}

service FeedService {

  // GetDefaultFeed gives a default & non-personalised feed for non-logged in users
  // This API should only be called for non-logged in users.
  rpc GetDefaultFeed (FeedRequest) returns (FeedResponse) {

  }

  // GetUserFeed gives feed based on topics & authors you have followed. (For on-boarded users)
  // For logged-in, but non-onboarded users, it will internally fallback to GetDefaultFeed,
  // unless the user has 3 followed topics & 3 authors
  rpc GetUserFeed (FeedRequest) returns (FeedResponse) {

  }

  // GetBookmarkedFeed gives bookmarked posts' feed
  rpc GetBookmarkedFeed (FeedRequest) returns (FeedResponse) {

  }

  // GetUserRecoFeed gives feed based on topics & authors you are recommended to follow. (For on-boarded users)
  // These won't include feed from topics or authors that you already follow
  rpc GetUserRecoFeed (FeedRequest) returns (FeedResponse) {

  }

  // GetAuthorFeed gives feed for a specific author, sorted by recency/popularity (TBD)
  rpc GetAuthorFeed (FeedRequest) returns (FeedResponse) {

  }

  // GetTopicFeed gives feed for a specific topic, sorted by recency/popularity (TBD)
  rpc GetTopicFeed (FeedRequest) returns (FeedResponse) {

  }

  // GetTagFeed gives feed for a specific tag, sorted by recency/popularity (TBD)
  rpc GetTagFeed (FeedRequest) returns (FeedResponse) {

  }

  // GetFollowingCarousels gives following carousel feed
  rpc GetFollowingCarousels (FeedRequest) returns (GenericFeedResponse) {

  }

  // GetSuggestedCarousels gives suggested carousel feed
  rpc GetSuggestedCarousels (FeedRequest) returns (GenericFeedResponse) {

  }

  // GetSmartStorePosts gives smart-store carousel feed
  rpc GetSmartStorePosts (SmartStoreFeedRequest) returns (GenericFeedResponse) {

  }
}

service PostService {

  rpc CreateUpdatePost (PostRequest) returns (PostResponse) {

  }

  // whenever a post is created or updated, it goes back to PENDING state
  // Irrespective of the state sent in this rpc
  rpc ChangePostStatus (PostStatusChangeRequest) returns (StatusChangeResponse) {

  }

  rpc GetPostById (PostByIdRequest) returns (PostResponse) {

  }

  rpc GetFilteredPosts (PostsRequest) returns (EnrichedPostsResponse) {

  }

  rpc GetAllPosts (Pagination) returns (EnrichedPostsResponse) {

  }

  rpc GetEnrichedPosts (EnrichPostsRequest) returns (EnrichedPostsResponse) {

  }

}


service TopicService {

  rpc GetTopicById (GenericStringIdRequest) returns (EnrichedTopic) {

  }

  rpc GetEnrichedTopics (GenericStringIdArrayRequest) returns (EnrichedTopicsResponse) {

  }

  rpc GetFilteredTopics (TopicsRequest) returns (EnrichedTopicsResponse) {

  }

  rpc GetTopicByPartialName (GenericStringRequest) returns (EnrichedTopicsResponse) {

  }

  // AddTopic will take care of add/update for topic/tag.
  rpc AddTopic (Topic) returns (Topic) {

  }

  // UpdateTopic updates the entry identified by Topic.id, with content of Topic
  rpc UpdateTopic (Topic) returns (Topic) {

  }

  // Will take care of add/update for tag.
  rpc DeleteTopic (Topic) returns (GenericDeleteResponse) {

  }
}

service TopicGroupService {

  rpc GetTopicGroupById (GenericStringIdRequest) returns (EnrichedTopicGroup) {

  }

  // Will take care of add/update for topic group
  rpc AddTopicGroup (TopicGroup) returns (EnrichedTopicGroup) {

  }

  // UpdateTopicGroup updates the entry identified by TopicGroup.id, with content of Topic Group
  rpc UpdateTopicGroup (TopicGroup) returns (EnrichedTopicGroup) {

  }

  // Will take care of add/update for topic group
  rpc DeleteTopicGroup (TopicGroup) returns (GenericDeleteResponse) {

  }

  rpc GetFilteredTopicGroups (TopicGroupsRequest) returns (EnrichedTopicGroupsResponse) {

  }

  rpc GetEnrichedTopicGroups (GenericStringIdArrayRequest) returns (EnrichedTopicGroupsResponse) {

  }

  rpc GetTopicGridToFollow (OnboardingRequest) returns (TopicGridResponse) {

  }
}

service TagService {

  rpc GetTagById (GenericStringIdRequest) returns (Tag) {

  }

  rpc GetEnrichedTags (GenericStringIdArrayRequest) returns (EnrichedTagsResponse) {

  }

  rpc GetTagByPartialName (GenericStringRequest) returns (TagsResponse) {

  }

  // Will take care of add/update for tag.
  rpc AddTag (Tag) returns (Tag) {

  }

  // Will take care of add/update for tag.
  rpc UpdateTag (Tag) returns (Tag) {

  }

  // Will take care of add/update for tag.
  rpc DeleteTag (Tag) returns (GenericDeleteResponse) {

  }

  // Will take care of add/update for tag.
  rpc PromoteTagToTopic (PromoteTagRequest) returns (Topic) {

  }
}

service MetaTagService {

  rpc GetMetaTagById (GenericStringIdRequest) returns (MetaTag) {

  }

  rpc GetEnrichedMetaTags (GenericStringIdArrayRequest) returns (EnrichedMetaTagsResponse) {

  }

  rpc GetMetaTagByPartialName (GenericStringAutoCompleteRequest) returns (EnrichedMetaTagsResponse) {

  }

  // Will take care of add/update for tag.
  rpc AddMetaTag (MetaTag) returns (MetaTag) {

  }

  // Will take care of add/update for tag.
  rpc UpdateMetaTag (MetaTag) returns (MetaTag) {

  }

  // Will take care of add/update for tag.
  rpc DeleteMetaTag (MetaTag) returns (GenericDeleteResponse) {

  }
}

service RecommendationService {

  rpc GetAuthorsAndTopicsToFollow (UserRequest) returns (AuthorTopicRecommendationResponse) {

  }

  rpc GetAuthorsToFollow (UserRequest) returns (AuthorRecommendationResponse) {

  }

  rpc GetTopicsToFollow (UserRequest) returns (TopicRecommendationResponse) {

  }

  rpc SearchPartialAuthorTopicTags (GenericStringAutoCompleteRequest) returns (SearchResponse) {

  }

  rpc GetRecommendedVideos (RelatedContentRequest) returns (VideoPlaylist) {

  }

  rpc GetRecommendedPosts (RelatedContentRequest) returns (EnrichedPostsResponse) {

  }

}

service StoryService {

  rpc CreateUpdateStory (Story) returns (EnrichedStory) {

  }

  // whenever a story is created or updated, it goes back to PENDING state
  // Irrespective of the state sent in this rpc
  rpc ChangeStoryStatus (PostStatusChangeRequest) returns (StatusChangeResponse) {

  }

  rpc GetStoryById (PostByIdRequest) returns (EnrichedStory) {

  }

  rpc GetFilteredStories (StoriesRequest) returns (EnrichedStoriesResponse) {

  }

  rpc GetStoryPanel (UserRequest) returns (StoryPanel) {

  }

  rpc GetAuthorStory (StoriesFilter) returns (AuthorsStories) {

  }
}

service AuthorService {

  rpc GetAuthorByUidx (GenericStringUidxRequest) returns (EnrichedAuthor) {

  }

  rpc GetAuthorFollowStatus (GenericStringUidxRequest) returns (UserFollowInteraction){

  }

  rpc GetAuthorByHandle (GenericStringRequest) returns (EnrichedAuthor) {

  }

  rpc GetAuthorByPartialHandle (GenericStringRequest) returns (AuthorsResponse) {

  }

  rpc GetFilteredAuthors (AuthorsRequest) returns (EnrichedAuthorsResponse) {

  }

  // Will take care of add for author.
  rpc AddAuthor (Author) returns (Author) {

  }

  // whenever an author is created or updated, it goes back to PENDING state
  // Irrespective of the state sent in this rpc
  rpc ChangeAuthorStatus (AuthorStatusChangeRequest) returns (StatusChangeResponse) {

  }

  // UpdateAuthor updates the entry identified by Author.id, with content of Author
  rpc UpdateAuthor (Author) returns (Author) {

  }

  // Will take care of add topic for author.
  rpc AddTopicsToAuthor (AuthorTopicRequest) returns (Author) {

  }

  // Will take care of giving promoted styles for an author
  rpc GetAuthorPromotedStyles (AuthorPromotedStylesRequest) returns (AuthorPromotedStylesResponse) {

  }
}

service UserService {

  rpc GetUserByUidx (GenericStringUidxRequest) returns (User) {

  }

  rpc GetEnrichedUsers (GenericUidxArrayRequest) returns (EnrichedUsersResponse) {

  }

  // OnboardUser creates/updates the user, with onboarding status set to true
  rpc OnboardUser (User) returns (GenericBooleanResponse) {

  }

  rpc IsUserOnboarded (Empty) returns (GenericBooleanResponse) {

  }

  // Will take care of add/update for tag.
  rpc DeleteUser (GenericStringUidxRequest) returns (GenericDeleteResponse) {

  }

  // Follow would handle a user following/un-following an author/topic
  rpc Follow (FollowRequest) returns (FollowResponse) {

  }

  // ReactToPost would handle a user reacting (like/unlike) to a post
  rpc ReactToPost (ReactRequest) returns (ReactResponse) {

  }

  // MarkPostsAsSeen would handle storing a user's last X(decided by application) seen posts
  rpc MarkPostsAsSeen (SeenPostRequest) returns (SeenPostResponse) {

  }

  // BookmarkPost would handle a user bookmarking a post
  rpc BookmarkPost (ReactRequest) returns (ReactResponse) {

  }
}

message BrightcoveCallbackRequest {
  string entity = 1;
  string entityType = 2;
  string version = 3;
  string action = 4;
  string jobId = 5;
  string videoId = 6;
  string dynamicRenditionId = 7;
  string language = 8;
  string variant = 9;
  string accountId = 10;
  string status = 11;
}

message VideoObject {
  string id = 1;
  string name = 2;
  int64 created = 3;
  int64 lastModified = 4;
  string akamaiHlsUrl = 5;
  string akamaiDashUrl = 6;
  string akamaiVideoFrameUrl = 7;
  string description = 8;
  string posterUrl = 9;
  string thumbnailUrl = 10;
  string webVttUrl = 11;
  string rawVideoBlobId = 12;
  string distributionFolderBlobId = 13;
  string dashPlaylistBlobId = 14;
  string hlsPlaylistBlobId = 15;
  string encKey = 16;
  string videoFrameBlobId = 17;
  string encodingProfile = 18;
}

message MatrixCallbackRequest {
  string jobId = 1;
  string jobStatus = 2;
  VideoObject videoContentEntity = 3;
}

message ThumbnailUpdateRequest {
  string mediaId = 1;
  Image thumbnailImage = 2;
}

service MediaService {

  rpc GetMediaById (GenericStringIdRequest) returns (PostMedia) {

  }

  rpc GetFilteredMedias (MediasRequest) returns (MediasResponse) {

  }

  rpc CreateImage (ImageMediaRequest) returns (ImageMediaResponse) {

  }

  rpc UpdateImageMedia (Media) returns (ImageMediaResponse) {

  }

  rpc CreateVideo (VideoCreationRequest) returns (VideoMediaResponse) {

  }

  rpc UpdateVideoThumbnail (ThumbnailUpdateRequest) returns (VideoMediaResponse) {

  }

  rpc CheckVideoStatus (GenericStringIdRequest) returns (VideoStatusResponse) {

  }

  rpc DeleteMedia (GenericStringIdRequest) returns (GenericDeleteResponse) {

  }

  rpc HandleBrightCoveCallback (BrightcoveCallbackRequest) returns (GenericBooleanResponse) {

  }

  rpc HandleMatrixCallback (MatrixCallbackRequest) returns (GenericBooleanResponse) {

  }
}

message MyssScheduleRequest {
  string payload = 1;
  int64 scheduleTime = 2;
  string relativeURL = 3;
}

message MyssScheduleResponse {
  int64 statusCode = 1;
  string statusMessage = 2;
  string statusType = 3;
  string scheduleId = 4;
}

message MyssDeleteScheduleRequest {
  string scheduleId = 1;
}

message MyssCallbackRequest {
  string payload = 1;
}

message PollOptionData {
  string text = 1;
  Media imageData = 2;
}

message PollOption {
  PollOptionData pollOptionData = 1;
  /*
  no use of this "votes" field while creating a poll,
  but when fetching, it will have vote counts for a given option
  */
  int32 votes = 2;
}

message EnrichedPollOption {
  PollOption pollOption = 1;
  float percentage = 2;
}

message PollContent {
  int64 pollId = 1;
  repeated PollOption pollOptions = 2;
}

message PollRequest {
  PollContent pollContent = 1;
}

message QuizRequest {
  QuizContent quizContent = 1;
}

message UserPollInteraction {
  int32 pollOptionSelected = 1;
}

message EnrichedPollContent {
  repeated EnrichedPollOption enrichedPollOptions = 1;
  int32 pollTaken = 2; // number of times this poll has been taken
  UserPollInteraction userPollInteraction = 3; // earlier vote casted by the user (-1, if no vote casted)
}

message QuizOption {
  // Option text
  string text = 1;
  Media imageData = 2;
  /*
  // "relatedCategory" is a category that is related to result category
   (eg. Mango Cold Brew tea means you are Joe)
   It will be -1 for quizzes with fixed answers
  */
  int32 relatedCategory = 3;
}

message AnswerFeedback {
  string correctAnswerMessage = 1;
  Media correctAnswerImage = 2;
  string incorrectAnswerMessage = 3;
  Media incorrectAnswerImage = 4;
}

enum QuizType {
  None = 0;
  ScoreQuiz = 1;
  PreferenceQuiz = 2;
}

message QuizQuestion {
  // Question test/statement (Don't forget the question-mark. We don't add it, if you don't ask)
  string text = 1;
  Media imageData = 2;
  repeated QuizOption quizOptions = 3;
  /* "correctOption" tells which option is correct for the given question. Value will be -1, for */
  int32 correctOption = 4;
  // message to show if the answer is correct, only useful for "ScoreQuiz"
  string correctAnswerMessage = 5;
  // sorry, wrong answer Again, only useful for "ScoreQuiz"
  string incorrectAnswerMessage = 6;
}

message QuizResultCategory {
  string title = 1;
  string description = 2;
  Media imageData = 3;
  /* promoted style IDs to be shown for each category */
  repeated int32 styleIds = 4;
}

message QuizContent {
  int64 quizId = 1;
  // Which kind of quiz is this ?
  QuizType type = 2;
  /*
  List of categories, that the person could belong to, based on responses
  */
  repeated QuizResultCategory resultCategories = 3;
  /* this map is useful in auiz of type "ScoreQuiz",
   to decide which category you fall in based on final score */
  map<int32, int32> scoreToResultCategoryMap = 4;
  // The real thing - actual questions
  repeated QuizQuestion quizQuestions = 5;
  // some fancy text to show along with the quiz results
  string resultText = 6;
}

message EnrichedQuizContent {
  QuizContent quizContent = 1;
  int32 questionsCount = 2;
  int32 quizTaken = 3; // number of times this quiz has been taken
}

message QuizQuestionRequest {
  int64 postId = 1;
  int32 questionId = 2;
}

message OptionSelectRequest {
  int64 postId = 1;
  int32 questionId = 2;
  int32 optionId = 3;
}

message QuizAnswer {
  int32 questionId = 1;
  int32 selectedOption = 2;
}

message QuizEvaluateRequest {
  int64 postId = 1;
  repeated QuizAnswer answers = 2;
}

message QuizEvaluationResponse {
  int64 postId = 1;
  string quizTitle = 2;
  QuizResultCategory category = 3;
  int32 totalQuestions = 4;
  int32 correctQuestions = 5;
  QuizType type = 6;
}

message FetchQuizQuestionResponse{
  int64 postId = 1;
  EnrichedQuizContent enrichedQuizContent = 2;
}

message QuizVoteSelectResponse {
  int64 postId = 1;
  EnrichedQuizContent newState = 2;
}

message QuizAnswerSelectResponse {
  int64 postId = 1;
  EnrichedQuizContent newState = 2;
  QuizQuestion nextQuestion = 3;
}

service MyssIntegrationService {
  rpc MyssSchedule (MyssScheduleRequest) returns (MyssScheduleResponse) {

  }

  rpc MyssDelete (MyssDeleteScheduleRequest) returns (MyssScheduleResponse) {

  }

  rpc MyssCallback (MyssCallbackRequest) returns (GenericBooleanResponse) {

  }
}

service PollQuizService {

  /*
  rpc CreateUpdatePoll (PollRequest) returns (PollContent) {
      option (google.api.http) = {
          post: "/v1.0/poll/create"
          body: "*"
      };
  }

  rpc CreateUpdateQuiz (QuizRequest) returns (QuizContent) {
      option (google.api.http) = {
          post: "/v1.0/quiz/create"
          body: "*"
      };
  }
  */

  rpc CastPollVote (OptionSelectRequest) returns (GenericBooleanResponse) {

  }

  rpc FetchQuizQuestion (QuizQuestionRequest) returns (FetchQuizQuestionResponse) {

  }

  rpc EvaluateQuizAnswers (QuizEvaluateRequest) returns (QuizEvaluationResponse) {

  }
}
