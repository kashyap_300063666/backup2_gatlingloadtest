package com.myntra.commons.util

import java.io._
import java.net.{HttpURLConnection, URL}

import com.myntra.hlt.ApiConfig

class HLTUtility {
  /* Redis variables */
  private val redisHost: String = System.getProperty("redis.Host", "localhost")
  private val redisPort: Int = System.getProperty("redis.port", "6379").toInt
  val redisUtil = new RedisUtil(redisHost, redisPort)

  /* API Status variables */
  private val apiStatusKey = "ApiEnableDisable"
  private val apiStatus: Iterator[Product2[String, String]] = ApiConfig.apiStatusConfig

  /* Fetching Prod Styles related variables */
  private val solrURI = "http://pas-solr8:8984/solr/sprod/select?fl=styleid&fq=count_options_availbale:[1%20TO%20*]&_route_=2297&fq=styletype:P&q=*:*&rows=1000000&wt=csv"

  /**
    * Set status of all the APIs in Redis key "ApiEnableDisable"
    * @return : Returns if Setting redis key value is successful or not
    */
  def setAPIStatuses(): Boolean = {
    redisUtil.hmset(apiStatusKey, apiStatus.toIterable)
  }


  /**
    * Get if API is enabled or disabled
    * @param name : Name of the API
    * @return     : Status of API as Boolean
    */
  def getAPIStatus(name: String): Boolean = {
    try {
      /* Return the API status stored in Redis */
      redisUtil.hget(apiStatusKey, name).toBoolean
    } catch {
      /* Return False if API status not found */
      case _: Throwable =>
        println("Failed to get API status of :: " + name)
        false
    }
  }

  /**
    * Function to get all available prod styles
    * @param fileToDownload : Name of the file to be saved
    */
  def downloadFile(fileToDownload: String): Unit = {
    try {
      println("Creating Styles file !!!!")
      val url = new URL(solrURI)
      val connection = url.openConnection().asInstanceOf[HttpURLConnection]
      connection.setRequestMethod("GET")
      val in: InputStream = connection.getInputStream
      /* Do not change the below path */
      var fileToDownloadAs = new java.io.File(fileToDownload)
      if (!fileToDownloadAs.exists()){
        val fileCreated = fileToDownloadAs.createNewFile()
        println(("File creation :: " + fileCreated))
      }
      val out: OutputStream = new BufferedOutputStream(new FileOutputStream(fileToDownloadAs))
      val byteArray = Stream.continually(in.read).takeWhile(-1 !=).map(_.toByte).toArray
      out.write(byteArray)
      in.close()
      out.flush()
      out.close()
    } catch {
      case e: FileNotFoundException =>
        println("Couldn't find that file.")
        e.printStackTrace()
        System.exit(0)
      case e: IOException =>
        println("Got an IOException!")
        e.printStackTrace()
        System.exit(0)
      case _: Throwable =>
        println("Failed to get Styles before Simulation")
        System.exit(0)
    }
  }
}
