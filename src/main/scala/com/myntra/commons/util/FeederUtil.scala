package com.myntra.commons.util

import java.io.File

import com.microsoft.azure.storage.CloudStorageAccount
import com.microsoft.azure.storage.blob.{CloudBlobClient, CloudBlobContainer, CloudBlockBlob}
import com.myntra.commons.util.BaseUrlConstructor.getProperty
import io.gatling.core.Predef._
import io.gatling.core.feeder.BatchableFeederBuilder

class FeederUtil extends ConfigurationManager{
  //val storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=scaleit;AccountKey=/T5+Dft3slq/vIUzTeh4acKGTs1clHruFKNDo3zHO24RO+4ioGYn1JiWMuO9EBqVN5DhQr4wN9u+SxbOGab26Q==;EndpointSuffix=core.windows.net"
  val storageConnectionString = getProperty("storageConnectionString")
  val storageAccount: CloudStorageAccount = CloudStorageAccount.parse(storageConnectionString)
  val blobClient: CloudBlobClient = storageAccount.createCloudBlobClient
  //val container: CloudBlobContainer = blobClient.getContainerReference("feeder-data")

  def getFileDownloadedPath(containerName: String, fileName: String): String = {
    val container: CloudBlobContainer = blobClient.getContainerReference(containerName)
    /* Create blob reference */
    val blob: CloudBlockBlob = container.getBlockBlobReference(fileName)
    /* Create File object and download to the same */
    val downloadedFile = new File(containerName + "_" + fileName)
    blob.downloadToFile(downloadedFile.getAbsolutePath)
    downloadedFile.getAbsolutePath
  }
}
