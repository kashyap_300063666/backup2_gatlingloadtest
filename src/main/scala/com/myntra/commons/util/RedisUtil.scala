package com.myntra.commons.util

import com.redis.RedisClientPool
import com.redis.serialization.{Format, Parse}

class RedisUtil(val host: String, val port: Int) {

  val clients = new RedisClientPool(host, port)

  // lpush
  def lp(key: String, msgs: List[String]): Unit = {
    clients.withClient {
      client => {
        msgs.foreach(client.lpush(key, _))
      }
    }
  }

  // rpush
  def rp(key: String, msgs: List[String]): Unit = {
    clients.withClient {
      client => {
        msgs.foreach(client.rpush(key, _))
      }
    }
  }

  def lpop(key: String): Option[String] = {
    clients.withClient {
      client => {
        client.lpop(key)
      }
    }
  }

  def rpop(key: String): Option[String] = {
    clients.withClient {
      client => {
        client.rpop(key)
      }
    }
  }

  // set
  def set(key: String, msgs: String): Boolean = {
    if(key==null && msgs==null || key.isEmpty) return false
      clients.withClient {
        client => {
          client.set(key, msgs)
        }
      }
  }

  //increament value
  def increamentCounter(key: String): Int = {
    clients.withClient {
      client => {
         client.incr(key).get.toInt
      }
    }
  }

  def getAll(pattern:Any ): Option[List[Option[String]]] = {
    clients.withClient {
      client => {
        client.keys(pattern)
      }
    }
  }

  def get(key:Any): String = {
    clients.withClient {
      client => {
        client.get(key).get
      }
    }
  }

  def del(key: Any, keys: Any*): Option[Long] = {
    clients.withClient {
      client => {
        client.del(key,keys)
      }
    }
  }

  def flushdb(): Boolean = {
    clients.withClient {
      client => {
        client.flushdb
      }
    }
  }

  def decreamentCounter(key: String): Int = {
    clients.withClient {
      client => {
        client.decr(key).get.toInt
      }
    }
  }

  def hmset(key: Any, map: Iterable[Product2[Any, Any]])(implicit format: Format): Boolean = {
    clients.withClient {
      client => {
        client.hmset(key, map)
      }
    }
  }

  def hmget[K, V](key: Any, fields: K*)(implicit format: Format, parseV: Parse[V]): Option[Map[Seq[K], V]] = {
    clients.withClient {
      client => {
        client.hmget(key, fields)
      }
    }
  }

  def hget[A](key: Any, field: Any)(implicit format: Format, parse: Parse[A]): A ={
    clients.withClient {
      client => {
        client.hget(key, field).get
      }
    }
  }

}
