package com.myntra.commons.util

object GatlingSimulationLauncher extends GatlingPropertiesLoader {

  def main(args: Array[String]): Unit = {
    loadGatlingProperties
    io.gatling.app.Gatling.main(args)
  }
}
