package com.myntra.commons.util

trait ConfigurationManager extends PropertyLoader {

  setProperties("default.properties", "config.properties")

}