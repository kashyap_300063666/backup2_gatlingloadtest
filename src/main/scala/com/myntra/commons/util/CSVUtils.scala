package com.myntra.commons.util

import java.io.{File, FileInputStream, FileReader, InputStreamReader}

import com.opencsv.CSVReader

import scala.collection.JavaConverters._

object CSVUtils {
  /* CSV files from resources */
  def readCSV(filePath: String, isIncludeHeader: Boolean = true): Array[Array[String]] = {
    val reader = new CSVReader(new InputStreamReader(FileUtils.getInputStream(filePath)))
    val data = reader.readAll()
    if(!isIncludeHeader) {
      data.remove(0)
    }
    return data.asScala.toArray
  }
  /* CSV files with absolute path */
  def readCSV2(filePath: String, isIncludeHeader: Boolean = true): Array[Array[String]] = {
    val reader = new CSVReader(new FileReader(filePath))
    val data = reader.readAll()
    if(!isIncludeHeader) {
      data.remove(0)
    }
    return data.asScala.toArray
  }
}