package com.myntra.commons.util

trait PropertyLoader extends LoggerTrait {

  val properties = new java.util.Properties

  protected def setProperties(files: String*) = {
    if (files != null && files.length > 0) {
      files.map(e => e.trim).foreach(e => properties.putAll(FileUtils.readProperties(e)))
    }
  }

  protected def getLongProperty(key: String, default: String = "0L") = {
    getProperty(key, default).toLong
  }

  protected def getIntProperty(key: String, default: String = "0") = {
    getProperty(key, default).toInt
  }

  protected def getFloatProperty(key: String, default: String = "0F") = {
    getProperty(key, default).toFloat
  }

  protected def getDoubleProperty(key: String, default: String = "0D") = {
    getProperty(key, default).toDouble
  }

  protected def getBooleanProperty(key: String, default: String = "false") = {
    getProperty(key, default).toBoolean;
  }

  def getProperty(key: String, default: String = null): String = {
    val value = if (!isEmptyOrNull(System.getProperty(key))) System
      .getProperty(key)
    else properties.getProperty(key)
    if (value == null) default else value
  }

  private def isEmptyOrNull(str: String) = str == null || str.trim.isEmpty


}
