package com.myntra.commons.util

trait GatlingPropertiesLoader extends PropertyLoader {

  setProperties("/gatling.properties")

  def loadGatlingProperties(): Unit = {
    System.setProperty("gatling.http.ahc.requestTimeout", getProperty("request.timeout_in_ms"))
    System.setProperty("gatling.http.ahc.connectTimeout", getProperty("connection.timeout_in_ms"))
    System.setProperty("gatling.http.ahc.handshakeTimeout", getProperty("handshake.timeout_in_ms"))
  }

}
