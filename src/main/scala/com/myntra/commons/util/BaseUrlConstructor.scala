package com.myntra.commons.util

import java.net.{HttpURLConnection, URL}

import com.jayway.jsonpath.JsonPath
import net.minidev.json.JSONArray
import scala.language.postfixOps

import scala.collection.Map
import scala.collection.mutable.ListBuffer
import scala.io.Source
import scala.xml.XML

object BaseUrlConstructor extends ConfigurationManager {

  private var services: List[String] = List.empty
  private var prodUrls: Map[String, String] = Map.empty

  def getHost(service: String, port: String = "8080", protocol: String = "http"): String = {
    var host = getBaseUrl(service)
    host = host.replace(protocol+"://", "")
    host = host.replace(":"+port, "")
    host
  }

  def getBaseUrl(service: String, protocol: String = "http"): String = {

    val singleServer = getProperty("singleServerUrl")
    if(singleServer != null && !singleServer.isEmpty)
    {
      LOG.info(s"baseurl => ${singleServer}")
      return singleServer
    }

    if (isEmpty(service)) {
      LOG.error("service name cannot be null")
      return null
    }
    val env = getProperty("environment").trim.toLowerCase
    val isDockinsEnabled = getBooleanProperty("isDockinsEnabled")
    val dockerInstanceName = getProperty("dockEnvName")

    LOG.debug(s"environment -> ${env}")
    LOG.debug(s"isDockinsEnabled -> ${isDockinsEnabled}")
    LOG.debug(s"dockerInstanceName -> ${dockerInstanceName}")

    val _service = service.trim

    if (_service.startsWith("http")) {
      return _service
    }

    var url: String = null

    env match {
      case "prod" => {
        if (prodUrls == null || prodUrls.isEmpty) {
          val stream = FileUtils.getInputStream("prodUrls.xml")
          if (stream == null) {
            LOG.error("could not load prodUrls.xml")
          } else {
            val source = XML.load(stream)
            prodUrls = (source \\ "service").map(e => ((e \ "@servicename" toString) -> (e \ "@baseurl" toString))).toMap
          }
        }
        url = prodUrls.getOrElse(service, null)
        if (isEmpty(url)) {
          LOG.error(s"baseurl for service '${_service}' not found in prodUrls.xml")
        }
      }
      case _ => {
        if (isDockinsEnabled) {
          if (services == null || services.isEmpty) {
            services = getDockerServices(dockerInstanceName)
          }
          url = if (services.contains(_service)) s"${protocol}://${dockerInstanceName}-${service}.dockins.myntra.com" else formUrl(_service, protocol, env)
        } else {
          url = formUrl(_service, protocol, env)
        }
      }
    }

    if(url != null && !url.isEmpty && url.startsWith("://")){
      url = url.replaceFirst("://","")
    }
    if (url != null && !url.isEmpty) {
      LOG.info(s"baseurl => ${url}")
    } else {
      LOG.error("error occurred while forming base url")
    }
    url
  }

  private def formUrl(service: String, protocol: String = "http", env: String): String = {
    if (isEmpty(service) || isEmpty(env)) {
      return null
    }
    env.trim.toLowerCase match {
      case "m7" => return s"${protocol}://${env}${service}.mpreprod.myntra.com"
      case _ => return s"${protocol}://${service}.${env}.myntra.com"
    }
  }

  private def isEmpty(str: String) = str == null || str.trim.isEmpty

  private def getDockerServices(instance: String): List[String] = {
    if (isEmpty(instance)) {
      return null
    }
    val _instance = instance.trim;
    val dockinsUrl = getProperty("dockinsurl").trim
    val url = new URL(if (dockinsUrl.endsWith("/")) s"${dockinsUrl}${_instance}" else s"${dockinsUrl}/${_instance}")
    val connection = url.openConnection.asInstanceOf[HttpURLConnection]
    HttpURLConnection.setFollowRedirects(true)
    connection.setConnectTimeout(10000)
    connection.setReadTimeout(10000)
    try {
      if (connection != null) {
        connection.connect
        if (200 == connection.getResponseCode) {
          val response = Source.fromInputStream(connection.getInputStream).mkString
          val context = JsonPath.parse(response)
          val arr: JSONArray = context.read("$.[*].name")
          var services = ListBuffer[String]()
          arr.forEach(e => services += e.toString)
          LOG.info(s"services added in dockins instance '${_instance}' -> ${services} - falling back to stage for other services")
          return services toList
        } else {
          LOG.error(s"received http status code '${connection.getResponseCode}' when connecting to GET ${url}")
        }
      }
    } catch {
      case e: Throwable => LOG.error(s"${e.getClass} occurred while calling getDockerServices('${instance}')")
    } finally {
      if (connection != null) {
        connection.disconnect
      }
    }
    List.empty
  }

}