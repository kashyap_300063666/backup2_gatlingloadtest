package com.myntra.commons.util

import org.slf4j.LoggerFactory

trait LoggerTrait {
  val LOG = LoggerFactory.getLogger(this.getClass)
}