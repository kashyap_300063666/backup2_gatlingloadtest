package com.myntra.commons.util

import java.io.InputStream
import java.util.Properties

object FileUtils extends LoggerTrait {

  def readProperties(file: String) = {
    val props = new Properties
    if (file == null || file.trim.isEmpty) {
      LOG.error("file cannot be null or empty")
    } else {
      var stream = getInputStream(file.trim);
      if (stream != null) {
        props.load(stream)
      }
    }
    props
  }

  def getInputStream(file: String): InputStream = {
    if (file == null || file.trim.isEmpty) {
      LOG.error("file cannot be null or empty")
      return null
    }
    val _file = file.trim
    var stream = this.getClass.getResourceAsStream(_file)
    if (stream != null) {
      return stream
    } else {
      if (!_file.startsWith("/")) {
        stream = this.getClass.getResourceAsStream('/' + _file)
        if (stream != null) {
          return stream
        }
      }
    }
    return null
  }

}