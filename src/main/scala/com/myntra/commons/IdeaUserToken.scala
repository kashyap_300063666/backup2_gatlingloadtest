package com.myntra.commons

import com.google.gson.Gson

case class IdeaUserToken(at:String, rt:String)
case class GenTokenAndLoginUser(at:String, rt:String, uidx:String)

object FormIdeaUserJson {

  def formJson(at:String, rt:String): String = {
    val obj = IdeaUserToken(at, rt)
    // create a JSON string from the Person, then print it
    val gson = new Gson
    val jsonString = gson.toJson(obj)
    jsonString
  }
}

object FormGenTokenAndLoginUser {

  def formJson(at:String, rt:String, uidx:String): String = {

    if(at.isEmpty || at == null || rt.isEmpty || rt == null || 
      uidx.isEmpty || uidx == null) return null

    val obj = GenTokenAndLoginUser(at, rt, uidx)
    // create a JSON string from the Person, then print it
    val gson = new Gson
    val jsonString = gson.toJson(obj)
    jsonString
  }
}