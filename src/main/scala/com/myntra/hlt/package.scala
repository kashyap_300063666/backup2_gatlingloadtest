package com.myntra

package object hltScenarioParams {

  /* Common Parameters for all scenarios */

  val rampUpDuration = "rampUpDuration"
  val maxLoadHoldDuration = "maxLoadHoldDuration"

  val peakLoadDuration = "peakLoadDuration"
  val peakLoadAsTimesOfMaxLoad = "peakLoadAsTimesOfMaxLoad"

  val maxHltLoad = "maxHltLoad"

  /* Scenario specific Parameters */

  val landingPage = "landingPage_maxLoad"
  val landingPageRecommended = "landingPageRecommended_maxLoad"
  val addToCartAPI = "addToCartAPI_maxLoad"
  val cartFillerAPI="cartFillerAPI_maxLoad"
  val cartCountDefaultAPI="cartCountDefaultAPI_maxLoad"
  val applyCouponAPI="applyCouponAPI_maxLoad"
  val checkoutCartNodeAPI="checkoutCartNodeAPI_maxLoad"
  val removefromcartAPI="removefromcartAPI_maxLoad"
  val addressAPI="addressAPI_maxLoad"
  val addressPincodeAPI="addressPincodeAPI_maxLoad"
  val pdpStyleData = "pdpStyleData_maxLoad"
  val pdpPageAllCalls = "pdpPageAllCalls_maxLoad"
  val searchAllCalls = "searchAllCalls_maxLoad"

  val browseFlow01 = "browseFlow01_maxLoad"
  val browseFlow02 = "browseFlow02_maxLoad"
  val browseFlow03 = "browseFlow03_maxLoad"
  val codPaymentsFlow = "codPaymentsFlow_maxLoad"
  val walletPaymentsFlow = "walletPaymentsFlow_maxLoad"
  
  // Conversion ratio propertyName
  val landingPageRatio = "landingPageRatio";
  val landingPageRecommendedRatio = "landingPageRecommendedRatio";
  val landingPageBiroRatio = "landingPageBiroRatio";
  val landingPageMenRatio = "landingPageMenRatio";
  val landingPageWomenRatio = "landingPageWomenRatio";
  val landingPageSmashboxRatio = "landingPageSmashboxRatio";
  val landingPageMallRatio = "landingPageMallRatio";
  val landingPageGetLoyaltyBalanceRatio = "landingPageGetLoyaltyBalanceRatio";

  
  val searchCallAutoSuggestRatio = "searchCallAutoSuggestRatio";
  val searchCallPaginationsV2Ratio = "searchCallPaginationsRatio";
  val searchpaginationMTV1Ratio = "searchpaginationMTV1Ratio";
  val searchClusterV2Ratio = "searchClusterV2Ratio";
  val searchFiltersV2Ratio = "searchFiltersV2Ratio";
  val searchFiltersV1Ratio = "searchFiltersV1Ratio";
  
  val pdpSimilarStylesLegoRatio = "pdpSimilarStylesLegoRatio"; 
  val pdpcrossSellLegoRatio = "pdpcrossSellLegoRatio"
  val pdpSimilarStylesRatio = "pdpSimilarStylesRatio"
  val pdpsimilarStylesV2Ratio = "pdpsimilarStylesV2Ratio"
  val pdpcrossSellV2Ratio = "pdpcrossSellV2Ratio"
  val pdpcrossSellRatio = "pdpcrossSellRatio"
  val pdpStyleOffersRatio = "pdpStyleOffersRatio"
  val pdpStyleDataRatio = "pdpStyleDataRatio"
  val pdpStyleV2Ratio = "pdpStyleV2Ratio"
  val pdpStyleLegoRatio = "pdpStyleLegoRatio"
  val stylePdpV2OfferV2SizeRecommendSizeProfileInsiderPointsRatio = "stylePdpV2OfferV2SizeRecommendSizeProfileInsiderPointsRatio"
  val stylePdpLegoOfferLegoShoppableLooksRatio = "stylePdpLegoOfferLegoShoppableLooksRatio"
  val styleLegoOffersLegoShoppableLooksAddCheckoutApplyCouponRemoveCartFillerRatio = "styleLegoOffersLegoShoppableLooksAddCheckoutApplyCouponRemoveCartFillerRatio"

  val checkDeliveryRatio = "checkDeliveryRatio"
  val checkDeliveryV2Ratio = "checkDeliveryV2Ratio"
  val checkDeliveryLegoRatio = "checkDeliveryLegoRatio"
  val checkDeliveryV3Ratio = "checkDeliveryV3Ratio"

  val addresscheckoutAddressRatio = "addresscheckoutAddressRatio"
  val addNewAddressRatio = "addNewAddressRatio"
  val addresscheckoutAddressNodeRatio = "addresscheckoutAddressNodeRatio"
  val addresspinCodeServiceRatio = "addresspinCodeServiceRatio"
  val checkoutAndselectAddressRatio = "checkoutAndselectAddressRatio"
  
  val MyntrawebRatio="MyntrawebRatio"
  val MyntrapwaRatio="MyntrapwaRatio"
  
  val UserAPIsgetWishListV2Ratio="UserAPIsgetWishListV2Ratio"
  val wishListSummaryRatio="wishListSummaryRatio"

  val addToWishListUsingStylePdpLegoRatio = "addToWishListUsingStylePdpLegoRatio"
  
  val applyCouponAPIRatio = "applyCouponAPIRatio";

  val styleLegoOfferLegoAddCheckoutApplyCouponRemoveFromCartCartFillerRatio = "styleLegoOfferLegoAddCheckoutApplyCouponRemoveFromCartCartFillerRatio"
  val styleV2OfferLegoAddCheckoutApplyCouponRemoveFromCartCartFillerRatio = "styleV2OfferLegoAddCheckoutApplyCouponRemoveFromCartCartFillerRatio"
  val styleV2OfferLegoAddCheckoutToCartApplyCouponRatio = "styleV2OfferLegoAddCheckoutToCartApplyCouponRatio"
  val checkoutCartGetAllCouponsRatio = "checkoutCartGetAllCouponsRatio"
  val cartDefaultApplyRatio = "cartCountDefaultAPIRatio"
  val giftCardApplyRatio = "giftCardApplyRatio"
  val checkoutCartNodeAPIRatio = "checkoutCartNodeAPIRatio"

  val myOrdersRatio = "myOrdersRatio"

  val engagementAccountSummaryRatio="engagementAccountSummaryRatio"
  val engagementRewardsRatio="engagementRewardsRatio"
  
  val looksRatio="looksRatio"
  val looksPlaygroundRatio="looksPlaygroundRatio"
  val looksV2LegoRatio="looksV2LegoRatio"
  val GetScratchCardRatio="GetScratchCardRatio"
  val ReferralcodeRatio="ReferralcodeRatio"
  val fetchSlotRatio="fetchSlotRatio"
  
  // Conversion ratio default percentage
  val landingPageValue = "35";
  val landingPageRecommendedValue = "7";
  val landingPageBiroValue = "10";
  val landingPageMenValue = "3";
  val landingPageWomenValue = "3";
  val landingPageSmashboxValue = "9";
  val landingPageMallValue = "4";
  val landingPageGetLoyaltyBalanceValue = "5";

  val searchCallAutoSuggestValue = "27"
  val searchCallPaginationsV2Value = "28"
  val searchpaginationMTV1Value = "1"
  val searchClusterV2Value = "4"
  val searchFiltersV2Value = "43"
  val searchFiltersV1Value = "2"
  
  val pdpSimilarStylesLegoValue = "60"
  val pdpcrossSellLegoValue = "60"
  val pdpSimilarStylesValue="1"
  val pdpsimilarStylesV2Value="4"
  val pdpcrossSellV2Value="4"
  val pdpcrossSellValue="3"
  val pdpStyleOffersValue="1"
  val pdpStyleDataValue="1"
  val pdpStyleV2Value="0"
  val pdpStyleLegoValue="0"
  val stylePdpV2OfferV2SizeRecommendSizeProfileInsiderPointsValue="1"
  val stylePdpLegoOfferLegoShoppableLooksValue="27"                   // Assuming only 1/3 of pdp calls result in shoppable
  val styleLegoOffersLegoShoppableLooksAddCheckoutApplyCouponRemoveCartFillerValue="10"
                                                                      // Assuming only 1/3 of pdp calls result in shoppable
  val checkDeliveryValue="1"
  val checkDeliveryV2Value="1"
  val checkDeliveryLegoValue="1"
  val checkDeliveryV3Value="1"

  val addresscheckoutAddressValue="18"
  val addNewAddressValue="2"
  val addresscheckoutAddressNodeValue="5"
  val addresspinCodeServiceValue="2"
  val checkoutAndselectAddressValue="5"
  
  val UserAPIsgetWishListV2Value="8"
  val wishListSummaryValue="8"

  val addToWishListUsingStylePdpLegoValue = "10"
  //val applyCouponAPIValue = "11"

  val styleLegoOfferLegoAddCheckoutApplyCouponRemoveFromCartCartFillerValue = "4"   // Assuming only 1/3rd of pdp calls result in proper AddToCart calls
  val styleV2OfferLegoAddCheckoutApplyCouponRemoveFromCartCartFillerValue = "2"     // Assuming only 1/3rd of pdp calls result in proper AddToCart calls
  val styleV2OfferLegoAddCheckoutToCartApplyCouponValue = "2"          // Assuming only 1/3rd of pdp calls result in proper AddToCart calls
  val checkoutCartGetAllCouponsValue = "5"
  val cartDefaultApplyValue = "17"
  val giftCardApplyValue = "1"
  val checkoutCartNodeAPIValue = "4"
  val myOrdersValue = "14"

  val engagementAccountSummaryValue="10"
  val engagementRewardsValue="10"
  
  val MyntrawebValue="7"
  val MyntrapwaValue="14"
  
  val looksValue="1"
  val looksPlaygroundValue="1"
  val looksV2LegoValue="53"
  val GetScratchCardValue="1"
  val ReferralcodeValue="1"
  val fetchSlotValue="1"
  
  
}
