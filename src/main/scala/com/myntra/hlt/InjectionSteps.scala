package com.myntra.hlt

import io.gatling.core.Predef._
import io.gatling.core.controller.inject.closed.ClosedInjectionStep
import io.gatling.core.controller.inject.open.OpenInjectionStep

import scala.concurrent.duration._

class InjectionSteps {
  private val loadParams = new LoadParams()

  def getSteps(scenarioParam: String): List[OpenInjectionStep] = {
    val loads = new loadParams.ScenarioParams(scenarioParam)
    List(
      rampUsersPerSec(0) to loads.rampUpMaxLoad during(loadParams.rampUpDuration minutes),
      constantUsersPerSec(loads.constUsersMaxLoad) during(loadParams.maxLoadHoldDuration minutes),
      constantUsersPerSec(loads.constUsersPeakLoad) during(loads.scenarioPeakDuration minutes)
    )
  }
  
  
 def getRatioSteps(scenarioRatio : String, defaultValue : String): List[OpenInjectionStep] = {
   val loads = new loadParams.RatioParams(scenarioRatio, defaultValue)
    println(scenarioRatio + " - " + loads.ratioValue + " ====> " + loads.constUsersMaxLoad + " RPS")
    List(
      rampUsersPerSec(0) to loads.rampUpMaxLoad during(loadParams.rampUpDuration minutes),
      constantUsersPerSec(loads.constUsersMaxLoad) during(loadParams.maxLoadHoldDuration minutes),
     constantUsersPerSec(loads.constUsersPeakLoad) during(loads.scenarioPeakDuration minutes)
    )
  }

 // def getRatioSteps(scenarioRatio : String, defaultValue : String): List[ClosedInjectionStep] = {
   // val loads = new loadParams.RatioParams(scenarioRatio, defaultValue)
    // println(scenarioRatio + " - " + loads.ratioValue + " ====> " + loads.constUsersMaxLoad + " RPS")
    // List(
     // rampConcurrentUsers(0) to loads.rampUpMaxLoad during(loadParams.rampUpDuration minutes),
     // constantConcurrentUsers(loads.constUsersMaxLoad) during(loadParams.maxLoadHoldDuration minutes),
     // constantConcurrentUsers(loads.constUsersPeakLoad) during(loads.scenarioPeakDuration minutes)
    //)
  //}
}
