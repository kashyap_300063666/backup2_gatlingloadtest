package com.myntra.hlt

object OrderEnums extends Enumeration {
  type OrderEnums = Value

  val COD_ORDERS = Value("COD_ORDERS")
  val ONLINE_ORDERS = Value("ONLINE_ORDERS")
  val WALLET_ORDERS = Value("WALLET_ORDERS")

  val COD_ORDERS_COPY = Value("COD_ORDERS_COPY")
  val ONLINE_ORDERS_COPY = Value("ONLINE_ORDERS_COPY")
  val WALLET_ORDERS_COPY = Value("WALLET_ORDERS_COPY")

  val COD_RETRY = Value("COD_RETRY")
  val ONLINE_RETRY = Value("ONLINE_RETRY")
  val WALLET_RETRY = Value("WALLET_RETRY")

  val COD_CANCELLED = Value("COD_CANCELLED")
  val ONLINE_CANCELLED = Value("ONLINE_CANCELLED")
  val WALLET_CANCELLED = Value("WALLET_CANCELLED")
}
