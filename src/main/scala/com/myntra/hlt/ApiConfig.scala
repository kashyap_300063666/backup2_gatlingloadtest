package com.myntra.hlt

object ApiConfig {
  val apiStatusConfig: Iterator[(String, String)] = List(
    "HomePage.home" -> "true",
    "HomePage.topnav" -> "true",
    "HomePage.recommended" -> "true",
    "HomePage.biro" -> "true",
    "HomePage.getLoyaltyBalance" -> "true",
    "HomePage.men" -> "true",
    "HomePage.women" -> "true",
    "HomePage.obs-smashbox" -> "true",
    "HomePage.m-mall" -> "true",
    "HomePage.GetScratchCard" -> "true",
    "HomePage.Referralcode" -> "true",
    "HomePage.fetchSlot" -> "true",

    "ListPage.paginationMTV1" -> "true",
    "ListPage.paginationMTV2" -> "true",
    "ListPage.filtersV1" -> "true",
    "ListPage.filtersV2" -> "true",
    "ListPage.autoSuggest" -> "true",
    "ListPage.clusterV2" -> "true",

    "PdpPage.stylePdp" -> "true",
    "PdpPage.stylePdpV2" -> "true",
    "PdpPage.stylePdpLego" -> "true",
    "PdpPage.clickForOffer" -> "true",
    "PdpPage.clickForOfferV2" -> "true",
    "PdpPage.clickForOfferLego" -> "true",
    "PdpPage.similarStyles" -> "true",
    "PdpPage.similarStylesV2" -> "true",
    "PdpPage.similarStylesLego" -> "true",
    "PdpPage.crossSell" -> "true",
    "PdpPage.crossSellV2" -> "true",
    "PdpPage.crossSellLego" -> "true",
    "PdpPage.styleLooksV2" -> "true",
    "PdpPage.styleShoppableLooks" -> "true",
    "PdpPage.checkDeliveryOptions" -> "true",
    "PdpPage.checkDeliveryOptionsV2" -> "true",
    "PdpPage.checkDeliveryOptionsV3" -> "true",
    "PdpPage.checkDeliveryOptionsLego" -> "true",
    "PdpPage.sizeRecommend" -> "true",
    "PdpPage.sizeProfile" -> "true",
    "PdpPage.looks" -> "true",
    "PdpPage.insiderPoint" -> "true",
    "PdpPage.looksV2" -> "true",
    "PdpPage.looksV2Lego" -> "true",
    "PdpPage.looksPlayground" -> "true",
    "PdpPage.insiderPoints" -> "true",

    "UserAPIs.addToWishList" -> "true", // Will update this once addToWishList is updated
    "UserAPIs.getWishListV2" -> "true",
    "UserAPIs.wishListSummary" -> "true",

    "CartPage.clearCart" -> "true",
    "CartPage.addToCart" -> "true",
    "CartPage.addToCartBrowseFlow" -> "true",
    "CartPage.checkOutCart" -> "true",
    "CartPage.cartCountDefault" -> "false", // No need for HLT runs
    "CartPage.cartDefaultApply" -> "true",
    "CartPage.applyCoupon" -> "true",
    "CartPage.cartFiller" -> "true",
    "CartPage.checkoutCartNode" -> "true",
    "CartPage.getAllCoupons" -> "true",
    "CartPage.giftcardPayment" -> "true",
    "CartPage.removefromcart" -> "true",
    
    "EngagementPage.EngagementAccountSummary" -> "false", //From Myprecious it gives 400 ,so disabling in scaleit
    "EngagementPage.EngagementRewards" -> "false",

    "AddressPage.addNewAddressAddress" ->"true",
    "AddressPage.checkoutAddress" ->"true",
    "AddressPage.checkoutAddressNode" ->"true",
    "AddressPage.pinCodeService" ->"true",
    "AddressPage.selectAddress" ->"true",

    "Payments.cashBackService" ->"true",
    "Payments.checkoutPayment" ->"true",
    "Payments.checkoutPaymentNode" ->"true",
    "Payments.getAllOrders" ->"true",
    "Payments.getCaptcha" ->"true",
    "Payments.plutusEligibility" ->"true",
    "Payments.resolveConflict" ->"true",
    "Payments.verifyCaptcha" ->"true",
    "Payments.paymentInstruments" ->"true",
    "Payments.makeCardPayment" ->"true",
    "Payments.makeWalletPayment" ->"true",
    "Payments.makeCODPayment" ->"true",
    "Payments.postPayment" ->"true",
    
    "WebPage.Myntraweb" -> "true",
    "WebPage.MyntrawebWithQueryParam" -> "true",
    "WebPage.MyntrawebWithStyleID" -> "true",
    "WebPage.Myntrapwa" -> "true",
    "WebPage.MyntrapwaWithQueryParam" -> "true",
    "WebPage.MyntrapwaWithStyleID" -> "true",

    "Orders.myOrders" -> "true"

  ).iterator
}
